package il.ac.technion.cs.ssdl.cs234311.dpSite.server;


import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.googlecode.objectify.Key;

/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date 	3/11/13
 *
 *	http servlet for registering (saving) user to database.
 * 
 * @name RegUser
 * @method http post
 * @params user - json serialized User instance
 */
@SuppressWarnings("serial")
public class UserRegServlet extends HttpServlet {

	private static final String ENTITY_TYPE_USER = "user";
	private static final Logger log = Logger.getLogger(UserRegServlet.class.getName());

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		try{
			log.info("starting reg servlet");
			
			String json_user = req.getParameter(ENTITY_TYPE_USER);
			System.out.println(json_user);
			
			Gson gson = new Gson();
			User user = gson.fromJson(json_user, User.class);
			
			Key<User> k = Key.create(User.class, user.email);
			User us_p = OfyService.ofy().load().key(k).now();
			if(us_p != null){
				log.warning("double registration for: " + user.email);
				throw new Exception("double registration for: " + user.email);
			}
			
			OfyService.ofy().save().entity(user).now();
			resp.getWriter().println("user reg successful");
			resp.setStatus(HttpServletResponse.SC_OK);
			log.info("user registered: " + user.email);
		}
		catch(Exception ex){
			log.severe("user registration fail. error: \n" + ex.toString());
			resp.getWriter().println("user reg fail. error: " + ex.toString());
			resp.setStatus(HttpServletResponse.SC_CONFLICT);
		}
	}
}
