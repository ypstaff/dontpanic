package il.ac.technion.cs.ssdl.cs234311.dpSite.server;


import java.io.IOException;
import java.lang.reflect.Type;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.googlecode.objectify.Key;


/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date 	10/11/13
 *
 *	http servlet for getting codes assigned by user
 * 
 * @name GetCode
 * @method http get
 * @params uid - user id (email)
 */
@SuppressWarnings("serial")
public class GetCodesServlet extends HttpServlet {

	private static final String ENTITY_TYPE_UID = "uid";
	private static final Logger log = Logger.getLogger(UserRegServlet.class.getName());

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		try{
			log.info("starting get codes servlet");
			String user_id = req.getParameter(ENTITY_TYPE_UID);
			
			Key<User> k = Key.create(User.class, user_id);			
			
			User user = OfyService.ofy().load().key(k).now();
			if(user == null){
				log.warning("no user found for: " + user_id);
				throw new Exception("no user found for: " + user_id);
			}
			
			List<Shortcut> code_lst = OfyService.ofy().load().type(Shortcut.class)
					.filter("userEmail ==", user.email).list();
			Gson gson = new Gson();
			Type collectionType = new TypeToken<List<Shortcut>>(){}.getType();
			String codes_json = gson.toJson(code_lst, collectionType);
			log.severe(codes_json);
			
			resp.setContentType("application/json; charset=UTF-8");
			resp.getWriter().println(codes_json);
			resp.setStatus(HttpServletResponse.SC_OK);
			log.info("codes returned for: " + user.username);
		}
		catch(Exception ex){
			log.severe("codes retrieval fail. error: " + ex.toString());
			resp.getWriter().println("codes get fail. error: " + ex.toString());
			resp.setStatus(HttpServletResponse.SC_CONFLICT);
		}
	}
}
