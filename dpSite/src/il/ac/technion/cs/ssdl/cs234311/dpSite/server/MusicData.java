/**
 * 
 */
package il.ac.technion.cs.ssdl.cs234311.dpSite.server;

import java.util.Collection;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import com.googlecode.objectify.annotation.EntitySubclass;

/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date 	14/05/14
 *
 */
@EntitySubclass(index=true)
public class MusicData extends RawData {

    public String[] info;
    
    /**
     * empty c'tor for objectify
     */
    public MusicData() {
        super();
    }

    /**
     * @param _uid
     * @param _time
     * @param _info
     */
    public MusicData(String _uid, Date _time, String[] _info) {
        super(_uid, _time);

        info = _info.clone();
    }
    
    private static class ValueComparator implements Comparator<String> {

        Map<String, Integer> base;
        public ValueComparator(Map<String, Integer> base) {
            this.base = base;
        }

        // Note: this comparator imposes orderings that are inconsistent with equals.    
        public int compare(String a, String b) {
            if (base.get(a) >= base.get(b)) {
                return -1;
            } else {
                return 1;
            } // returning 0 would merge keys
        }
    }
    
    private static String fiveFavoriteElement(Set<MusicData> set, int idx){
        HashMap<String, Integer> map = new HashMap<String, Integer>();
        ValueComparator bvs = new ValueComparator(map);
        TreeMap<String, Integer> sorted_map = new TreeMap<>(bvs);
        
        for(MusicData d: set){
            String key = d.info[idx];
            if(map.containsKey(key))
                map.put(key, map.get(key) + 1);
            else{
                map.put(key, 1);
            }
        }
        
        sorted_map.putAll(map);
        String result = "";
        for(int i = 0; i < 5; ++i){
            Entry<String, Integer> en = sorted_map.pollFirstEntry();
            if(en != null)
            	result += en.getKey() + "," + en.getValue() + ";";
        }
        
        return result;
    }
    
    @Override
    public String[] calculateStatistics(
            Collection<? extends RawData> datasets) {
        HashSet<MusicData> s = new HashSet<MusicData>();
        for(RawData d: datasets){
            if(d instanceof MusicData){
                s.add((MusicData)d);
            }
        }
        
        String[] result = new String[4];
        result[0] = "songs:" + fiveFavoriteElement(s, 0);
        result[1] = "artists:" + fiveFavoriteElement(s, 1);
        result[2] = "albums:" + fiveFavoriteElement(s, 2);
        result[3] = "num_plays:" + s.size();
        
        return result;
        
    }

}
