package il.ac.technion.cs.ssdl.cs234311.dpSite.server;


import il.ac.technion.cs.ssdl.cs234311.dpSite.server.Shortcut.ShortcutLetter;

import java.util.ArrayList;
import java.util.List;

public class ShortcutTranslator {
    
    
    
    /**
     * enum mapping colors to their places in the boolean array. to use this
     * enum, you can use the values in a line looking like this:
     * 
     * array[ColorArrayLocations.<color name>.ordinal()]
     * 
     * this will access the array in the place corresponding to the color.
     * 
     * @author Shachar Nudler
     * 
     */
    public enum ColorArrayLocations {
        YELLOW, BLUE, GREEN, RED
    }
    
    public static class IllegalShortcutLetter extends Exception{

        private static final long serialVersionUID = -563468304240124707L;
        
    }
    
    
    private static ShortcutLetter array2letter(final boolean[] letter) throws IllegalShortcutLetter {
        int code = 0;
        for (int i = 3; i >= 0; --i) {
            code >>= 1;
            code += letter[i] ? 1 : 0;
        }

        switch(code){
        case 1:
            return ShortcutLetter.ONE;
        case 2:
            return ShortcutLetter.TWO;
        case 3:
            return ShortcutLetter.ONE_TWO;
        case 4:
            return ShortcutLetter.THREE;
        case 6:
            return ShortcutLetter.TWO_THREE;
        case 8:
            return ShortcutLetter.FOUR;
        case 9:
            return ShortcutLetter.FOUR_ONE;
        case 12:
            return ShortcutLetter.THREE_FOUR;
        default:
            throw new IllegalShortcutLetter();
                
        }
    }

    /**
     * return the integer encode of a two letter shortcut code.
     * 
     * @param _word
     *            - first letter of the code.
     * @param _word2
     *            - the second letter.
     * @return - the code transform to it's integer encoding
     * @throws Exception 
     */
    public static List<ShortcutLetter> word2code(final List<boolean[]> _word) throws IllegalShortcutLetter {
        List<ShortcutLetter> code = new ArrayList<ShortcutLetter>();
        
        for(int i=0; i< _word.size(); ++i){
            code.add(array2letter(_word.get(i)));
        }
        
        return code;
    }
    
    public static int code2int(final List<ShortcutLetter> code){
      int res = 0;
      int exp = 1;
      for(int i=0; i< code.size(); ++i){
          res += code.get(i).ordinal() * exp;
          exp *= 16;
      }
      return res;
    }
    


    /**
     * return the letter encoding of half of the integer encoding for one letter
     * code. used when creating the two letters codes from the integer encoding.
     * 
     * @param code
     *            - the code for one letter (4 bits integer)
     * @param res
     *            - an array of size 4 to write the answer to.
     */
    public static void wordCode2array(final int code, final boolean[] res) {
        for (int i = 3; i >= 0; --i)
            // cell 3 is lsb so for i = 3 check just first bit
            res[i] = (code >> 3 - i) % 2 == 1;
    }

    /**
     * Get the two letters encoding of a shortcut integer encoding.
     * 
     * @param code
     *            - the shortcut integer encoding
     * @param word1
     *            - a size 4 boolean array to write the first letter to.
     * @param word2
     *            - a size 4 boolean array to write the second letter to.
     */
    public static void code2Arrays(final int code, final boolean[] word1,
            final boolean[] word2) {

        // last 4 bits
        wordCode2array(code >> 4, word1);
        // 2^4 = 16 -> first 4 bits
        wordCode2array(code % 16, word2);

    }


    /**
     * get the integer encoding of a shortcut code if it was only it's first
     * letter. used to check if such code exists in the system and blocks the
     * code's use.
     * 
     * @return the number encoding of the code if it was only one lettered.
     */
    public static int getFirstLetterEncoding(final int code) {
        return code >> 4 << 4;
    }
    
    /**
     * get the encoding of the second letter as though it was the first one.
     * @param code - the given code
     * @return - the value of the encoding of a 
     */
    public static int getSecondLetterEncoding(final int code){
        return (code % 16) << 4;
    }


}
