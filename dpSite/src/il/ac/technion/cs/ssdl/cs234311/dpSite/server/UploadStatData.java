package il.ac.technion.cs.ssdl.cs234311.dpSite.server;


import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.googlecode.objectify.Key;


/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date 	10/05/14
 *
 *	http servlet for adding statistics to database
 * 
 * @name UploadStatData
 * @method http post
 * @params type - string of stat type [click/music/snake]
 *          info - json of stat data class
 */
@SuppressWarnings("serial")
public class UploadStatData extends HttpServlet {

    private static final String ENTITY_TYPE = "type";
    private static final String ENTITY_INFO = "info";
	private static final Logger log = Logger.getLogger(UploadStatData.class.getName());
	
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		try{
			log.info("starting upload stat servlet");
			String type = req.getParameter(ENTITY_TYPE);
			String info = req.getParameter(ENTITY_INFO);
			
			Gson gson = new Gson();
			RawData stat;
			
			switch (type) {
        case "click":
            stat = gson.fromJson(info, ClickData.class);
            break;
        case "music":
            stat = gson.fromJson(info, MusicData.class);
            break;
        case "snake":
            stat = gson.fromJson(info, SnakeData.class);
            break;
        default:
            throw new Exception("unknown statistics type: " + type);
        }
			
		    Key<User> user_key = Key.create(User.class, stat.uid);
        User user = OfyService.ofy().load().key(user_key).now();
        
        if(user == null){
          log.warning("no such user: " + stat.uid);
          throw new Exception("no such user: " + stat.uid);
        }
       
			
			OfyService.ofy().save().entity(stat).now();
			
			resp.getWriter().println(stat.id);
			resp.setStatus(HttpServletResponse.SC_OK);
			log.info("stat saved for user: " + user.email);
		}
		catch(Exception ex){
			log.severe("stat add fail. error: \n" + ex.toString());
			resp.getWriter().println("stat add fail. error: " + ex.toString());
			resp.setStatus(HttpServletResponse.SC_CONFLICT);
		}
	}
}
