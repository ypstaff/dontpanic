package il.ac.technion.cs.ssdl.cs234311.dpSite.server;

import il.ac.technion.cs.ssdl.cs234311.dpSite.client.GetStatisticsServlet;

import java.lang.reflect.Constructor;
import java.util.List;
import java.util.logging.Logger;

import com.google.gwt.user.server.rpc.RemoteServiceServlet;
import com.googlecode.objectify.Key;

/**
 * @author Michael Varvaruk
 * @email mvarvaruk@gmail.com
 * @date 10/05/14
 * 
 *       http servlet for getting statistics
 * 
 * @name GetStatistics
 * @method http get
 * @params uid - user id (email) type - string of stat type [click/music/snake]
 * 
 * @return String array of result
 */
@SuppressWarnings("serial")
public class GetStatisticsServletImpl extends RemoteServiceServlet implements GetStatisticsServlet {

	private static final Logger log = Logger
			.getLogger(GetStatisticsServletImpl.class.getName());

	public String get(String type, String uid) throws Exception {
		log.info("starting get stat servlet");

		Class<? extends RawData> target = null;

		switch (type) {
		case "click":
			target = ClickData.class;
			break;
		case "music":
			target = MusicData.class;
			break;
		case "snake":
			target = SnakeData.class;
			break;
		default:
			throw new Exception("unknown statistics type: " + type);
		}

		Key<User> user_key = Key.create(User.class, uid);
		User user = OfyService.ofy().load().key(user_key).now();

		if (user == null) {
			log.warning("no such user: " + uid);
			throw new Exception("no such user: " + uid);
		}

		List<? extends RawData> lst = OfyService.ofy().load().type(target)
				.filter("uid ==", user.email).list();

		Constructor<? extends RawData> cons = target.getConstructor();
		RawData obj = cons.newInstance();

		String[] result_map = obj.calculateStatistics(lst);
		
		String result = "";
		for(int i = 0; i < result_map.length; ++i){
			result += result_map[i] + "&";
		}
		
		log.info(result);
		return result;
	}
}
