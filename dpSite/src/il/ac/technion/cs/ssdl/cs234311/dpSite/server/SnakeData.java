/**
 * 
 */
package il.ac.technion.cs.ssdl.cs234311.dpSite.server;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.googlecode.objectify.annotation.EntitySubclass;

/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date 	14/05/14
 *
 */
@EntitySubclass(index=true)
public class SnakeData extends GameData {

    /**
     * @param _uid
     * @param _time
     * @param info
     */
    public SnakeData(String _uid, Date _time, String[] info) {
        super(_uid, _time, info);
    }

    /**
     * empty c'tor for objectify
     */
    public SnakeData() {
       super();
    }
    
    private static String highScoreForLvl(Set<SnakeData> set){
        Map<String, Integer> map = new HashMap<>();
        
        for(SnakeData d: set){
            String lvl = d.gameInfo[1];
            int val = Integer.valueOf(d.gameInfo[0]).intValue();
            
            if(map.containsKey(lvl)){
                int map_val = map.get(lvl).intValue();
                val = (map_val < val) ? val : map_val;
            }
            
            map.put(lvl, val);
        }
        
        String result = "";
        for(Entry<String, Integer> e: map.entrySet()){
            result = e.getKey() + "," + e.getValue() + ";";
        }
        
        return result;
    }
    
    public static String favElement(Set<SnakeData> set, int idx){
        HashMap<String, Integer> map = new HashMap<String, Integer>(); 
    	
    	String key = null;
    	int max = 0;
        
        for(SnakeData d: set){
            if(map.containsKey(d.gameInfo[1]))
            	map.put(d.gameInfo[1], map.get(d.gameInfo[1]) + 1);
            else
            	map.put(d.gameInfo[1], 1);
            
            if(map.get(d.gameInfo[1]) > max){
            	max = map.get(d.gameInfo[1]);
            	key = d.gameInfo[1];
            }
                
        }
        
        return key;
    }
    
    public static String maxElement(Set<SnakeData> set, int idx){
        long max = 0;
        
        for(SnakeData d: set){
            long value = Long.valueOf(d.gameInfo[idx]).longValue();
            if(value > max)
                max = value;
        }
        
        return String.valueOf(max);
    }
    
    private static String sumElement(Set<SnakeData> set, int idx){
        long sum = 0;
        
        for(SnakeData d: set){
            long value = Long.valueOf(d.gameInfo[idx]).longValue();
            
            sum += value;
        }
        
        return String.valueOf(sum);
    }
    
    @Override
    public String[] calculateStatistics(
            Collection<? extends RawData> datasets) {
        HashSet<SnakeData> s = new HashSet<SnakeData>();
        for(RawData d: datasets){
            if(d instanceof SnakeData){
                s.add((SnakeData)d);
            }
        }
        
        String[] result = new String[4];
        
        result[0] = "high_score:" + highScoreForLvl(s);
        result[1] = "fav_lvl:" + favElement(s, 1);
        result[2] = "max_time:" + maxElement(s, 2);
        result[3] = "total_time:" + sumElement(s, 2);
        
        return result;
    }

}
