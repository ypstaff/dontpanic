package il.ac.technion.cs.ssdl.cs234311.dpSite.client;

import com.google.gwt.user.client.rpc.AsyncCallback;

/**
 * The async counterpart of <code>GetStatisticsServlet</code>.
 */
public interface GetStatisticsServletAsync {
	void get(String type, String uid, AsyncCallback<String> callback)
			throws IllegalArgumentException;
}
