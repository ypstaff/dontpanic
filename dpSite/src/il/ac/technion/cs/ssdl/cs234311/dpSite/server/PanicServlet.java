package il.ac.technion.cs.ssdl.cs234311.dpSite.server;


import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.Key;


/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date 	25/11/13
 *
 *	http servlet for panic alerts
 * 
 * @name panic
 * @method http get
 * @params uid - user id (email)
 */
@SuppressWarnings("serial")
public class PanicServlet extends HttpServlet {

	private static final String ENTITY_TYPE_UID = "uid";
	private static final Logger log = Logger.getLogger(PanicServlet.class.getName());

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		try{
			String user_id = req.getParameter(ENTITY_TYPE_UID);
		    Key<User> key = Key.create(User.class, user_id);
		    
			User user = OfyService.ofy().load().key(key).now();
			if(user == null){
			    log.warning("user does not exist");
			}
			
			Properties props = new Properties();
			Session session = Session.getDefaultInstance(props, null);
			
	    Message msg = new MimeMessage(session);
	    msg.setFrom(new InternetAddress("noreply@yp7dontpanic.appspotmail.com", "Panic Team"));
	    msg.addRecipient(Message.RecipientType.TO,
            new InternetAddress(user.super_email, user.username + " supervisor"));

	    String msgBody = "hello.\n" + user.username + " (with email " + user.email + ")" + 
            " has sent a panic alert. please contact him or the emergency services.\n\n" + 
                "thank you.\n" + 
                "Don't Panic services";
      
      msg.setSubject(user.username + " Panic alert!");
      msg.setText(msgBody);
      Transport.send(msg);
      
      resp.getWriter().println("panic alert for " + user.email);
			resp.setStatus(HttpServletResponse.SC_OK);
			log.info("panic alert: " + user.email);
		}
		catch(Exception ex){
			log.severe("panic alert fail. error: \n" + ex.toString());
			resp.getWriter().println("panic alert fail. error: " + ex.toString());
			resp.setStatus(HttpServletResponse.SC_CONFLICT);
		}
	}
}
