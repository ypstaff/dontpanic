package il.ac.technion.cs.ssdl.cs234311.dpSite.client;

import java.util.ArrayList;
import java.util.List;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.core.shared.GWT;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.googlecode.gwt.charts.client.ChartLoader;
import com.googlecode.gwt.charts.client.ChartPackage;
import com.googlecode.gwt.charts.client.ColumnType;
import com.googlecode.gwt.charts.client.DataTable;
import com.googlecode.gwt.charts.client.corechart.BarChart;
import com.googlecode.gwt.charts.client.corechart.PieChart;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class DpSite implements EntryPoint {

	private final GetStatisticsServletAsync statisticService = GWT
			.create(GetStatisticsServlet.class);

	class Piedraw {
		private PieChart pieChart;
		private List<Pair> data;

		Piedraw(List<Pair> data) {
			this.data = data;
			ChartLoader chartLoader = new ChartLoader(ChartPackage.CORECHART);
			chartLoader.loadApi(new Runnable() {

				@Override
				public synchronized void run() {

					drawPieChart();
				}
			});
			pieChart = new PieChart();
		}

		public PieChart getChart() {
			return pieChart;
		}

		private void drawPieChart() {
			// Prepare the data
			DataTable dataTable = DataTable.create();
			dataTable.addColumn(ColumnType.STRING, "Name");
			dataTable.addColumn(ColumnType.NUMBER, "Count");
			dataTable.addRows(data.size());
			int i = 0;
			for (Pair p : data) {
				dataTable.setValue(i, 0, p.getName());
				dataTable.setValue(i, 1, p.getValue());
				i++;
			}

			// Draw the chart
			pieChart.draw(dataTable);

		}
	}

	class Bardraw {
		private BarChart barChart;
		private List<Pair> data;

		Bardraw(List<Pair> data) {
			this.data = data;
			ChartLoader chartLoader = new ChartLoader(ChartPackage.CORECHART);
			chartLoader.loadApi(new Runnable() {

				@Override
				public synchronized void run() {
					drawBarChart();
				}
			});

			barChart = new BarChart();
		}

		public BarChart getChart() {
			return barChart;
		}

		private void drawBarChart() {
			// Prepare the data
			DataTable dataTable = DataTable.create();
			dataTable.addColumn(ColumnType.STRING, "Name");
			dataTable.addColumn(ColumnType.NUMBER, "Count");
			dataTable.addRows(data.size());
			int i = 0;
			for (Pair p : data) {
				dataTable.setValue(i, 0, p.getName());
				dataTable.setValue(i, 1, p.getValue());
				i++;
			}

			// Draw the chart
			barChart.draw(dataTable);

		}
	}
/**
 * this class implementing pair , because gwt is not implementing this.
 * @author Ameer Assi
 *
 */
	private static class Pair {
		String name;
		Integer value;
/**
 * contructor 
 * @param name: first in pair
 * @param value: second in pair.
 */
		public Pair(String name, Integer value) {
			this.name = name;
			this.value = value;
		}
/**
 * 
 * @return the first in the pair
 */
		public String getName() {
			return name;
		}
/**
 * 
 * @return the second in the pair
 */
		public Integer getValue() {
			return value;
		}
	}

	private static ArrayList<Pair> splitPairs(String list) {
		ArrayList<Pair> res = new ArrayList<Pair>();

		String[] array = list.split(";");
		for (int j = 0; j < array.length; ++j) {
			String s = array[j];

			if (s.equals(""))
				continue;

			int idx = s.lastIndexOf(',');

			res.add(new Pair(s.substring(0, idx), Integer.valueOf(
					s.substring(idx + 1)).intValue()));
		}

		return res;
	}

	private void AddWarning(String id, List<Pair> data) {
		if (data.size() != 0) {
			return;
		}
		Label l = new Label("This Statistics is never happend until now");
		l.addStyleName("myLabel2");
		RootPanel.get(id).add(l);
	}

	private PieChart update(PieChart chart) {
		chart.setSize("1000px", "220px");
		return chart;
	}

	private BarChart update(BarChart chart) {
		chart.setSize("1000px", "220px");
		return chart;
	}
/**
 * is the first running function in web , determines the web GUI.
 */
	public void onModuleLoad() {

		String userName = getUserName();
		final Label userLabel = new Label(userName);

		RootPanel.get("nameText").add(userLabel);

		statisticService.get("music", getUserName(),
				new AsyncCallback<String>() {

					@Override
					public void onSuccess(String result) {
						// splits the string got from the server to lists of data.
						String[] result_map = result.split("&");

						List<Pair> array1 = splitPairs(result_map[0].split(":")[1]);
						List<Pair> array2 = splitPairs(result_map[1].split(":")[1]);
						List<Pair> array3 = splitPairs(result_map[2].split(":")[1]);
						RootPanel.get("graph1").add(update(new Piedraw(array1).getChart()));
						RootPanel.get("graph2").add(update(
								new Piedraw(array2).getChart()));
						RootPanel.get("graph3").add(update(
								new Piedraw(array3).getChart()));
						AddWarning("graph1", array1);
						AddWarning("graph2", array2);
						AddWarning("graph3", array3);
					}

					@Override
					public void onFailure(Throwable caught) {
						Label lbl = new Label(caught.toString());
						Label lb2 = new Label(caught.toString());
						Label lb3 = new Label(caught.toString());
						lbl.addStyleName("myLabel");
						lb2.addStyleName("myLabel");
						lb3.addStyleName("myLabel");
						RootPanel.get("graph1").add(lbl);
						RootPanel.get("graph2").add(lb2);
						RootPanel.get("graph3").add(lb3);
					}
				});

		statisticService.get("snake", getUserName(),
				new AsyncCallback<String>() {

					@Override
					public void onSuccess(String result) {
						String[] result_map = result.split("&");

						List<Pair> array = splitPairs(result_map[0].split(":")[1]);

						RootPanel.get("graph4").add(update(
								new Bardraw(array).getChart()));
						AddWarning("graph4", array);
						final Label favLabel = new Label("favorite level: "
								+ result_map[1].split(":")[1]);
						RootPanel.get("favouriteLevel").add(favLabel);

						final Label time = new Label("longest game: "
								+ (Integer.valueOf(result_map[2].split(":")[1])
										.doubleValue() / 1000));
						RootPanel.get("longestTime").add(time);

						final Label total = new Label("total game: "
								+ (Integer.valueOf(result_map[3].split(":")[1])
										.doubleValue() / 1000));
						RootPanel.get("totalTime").add(total);

					}

					@Override
					public void onFailure(Throwable caught) {
						Label lbl = new Label(caught.toString());
						lbl.addStyleName("myLabel");
						RootPanel.get("graph4").add(lbl);

					}
				});

		statisticService.get("click", getUserName(),
				new AsyncCallback<String>() {

					@Override
					public void onSuccess(String result) {
						String[] result_map = result.split("&");

						List<Pair> array = new ArrayList<Pair>();
						array.add(new Pair(result_map[0].split(":")[0], Integer
								.valueOf(result_map[0].split(":")[1])
								.intValue()));
						array.add(new Pair(result_map[1].split(":")[0], Integer
								.valueOf(result_map[1].split(":")[1])
								.intValue()));

						RootPanel.get("graph5").add(update(
								new Piedraw(array).getChart()));
						AddWarning("graph5", array);
						List<Pair> array2 = splitPairs(result_map[2].split(":")[1]);

						RootPanel.get("graph6").add(update(
								new Bardraw(array2).getChart()));
						AddWarning("graph6", array2);
					}

					@Override
					public void onFailure(Throwable caught) {
						Label lbl = new Label(caught.toString());
						Label lb2 = new Label(caught.toString());
						lbl.addStyleName("myLabel");
						lb2.addStyleName("myLabel");
						RootPanel.get("graph5").add(lbl);
						RootPanel.get("graph6").add(lb2);

					}
				});

	}

	private String getUserName() {

		String uid = Window.Location.getParameter("param");
		return uid;

	}

}
