package il.ac.technion.cs.ssdl.cs234311.dpSite.client;

import com.google.gwt.user.client.rpc.RemoteService;
import com.google.gwt.user.client.rpc.RemoteServiceRelativePath;

/**
 * The client-side stub for the RPC service.
 */
@RemoteServiceRelativePath("GetStatistics")
public interface GetStatisticsServlet extends RemoteService {
	String get(String type, String uid)  throws Exception;
}
