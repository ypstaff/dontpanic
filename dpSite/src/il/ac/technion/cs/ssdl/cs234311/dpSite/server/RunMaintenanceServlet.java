package il.ac.technion.cs.ssdl.cs234311.dpSite.server;


import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;



/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date 	04/04/13
 * 
 * http servlet for deleting user and shortcuts created by test. must be run mainualy.
 * 
 * @name RunMaintenance
 * @method http post
 */
@SuppressWarnings("serial")
public class RunMaintenanceServlet extends HttpServlet {

	private static final Logger log = Logger.getLogger(RunMaintenanceServlet.class.getName());
	private static final String emailreg = "^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+"
	                                                + "(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$";
	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		try{
		    log.info("RunMaintenance called");
		    
		    List<User> users = OfyService.ofy().load().type(User.class).list();
		    ArrayList<User> bad_users = new ArrayList<>();
		    
		    for(User u: users){
		        if (u.email.contains("test") || u.email.contains("Test") 
		                || !u.email.matches(emailreg)){
		            bad_users.add(u);
		        }
		    }
		    
		    log.info("RunMaintenance: users to delete = " + bad_users.size());
		    
		    for(User u: bad_users){
		        log.info("RunMaintenance: user = " + u.email);
		        
		        List<Shortcut> code_lst = OfyService.ofy().load().type(Shortcut.class)
		                .filter("userEmail ==", u.email).list();
		        
		        OfyService.ofy().delete().entities(code_lst).now();
		        OfyService.ofy().delete().entity(u).now();
		    }
		    
		    
  			resp.getWriter().println("server clean of junk.all users = " + users.size() + ", cleaned = " + bad_users.size());
  			resp.setStatus(HttpServletResponse.SC_OK);
  			log.info("server cleaned");
		}
		catch(Exception ex){
			log.severe("server clean fail. error: \n" + ex.toString());
			ex.printStackTrace(System.out);
			resp.getWriter().println("server clean fail. error: " + ex.toString());
			resp.setStatus(HttpServletResponse.SC_CONFLICT);
		}
	}
}
