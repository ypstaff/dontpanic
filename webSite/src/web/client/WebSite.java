package web.client;


import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.event.dom.client.ClickEvent;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.KeyCodes;
import com.google.gwt.event.dom.client.KeyUpEvent;
import com.google.gwt.event.dom.client.KeyUpHandler;
import com.google.gwt.user.client.Window;
import com.google.gwt.user.client.ui.Button;
import com.google.gwt.user.client.ui.DialogBox;
import com.google.gwt.user.client.ui.HTML;
import com.google.gwt.user.client.ui.Label;
import com.google.gwt.user.client.ui.RootPanel;
import com.google.gwt.user.client.ui.TextBox;
import com.google.gwt.user.client.ui.VerticalPanel;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class WebSite implements EntryPoint {
	private Button sendButton = new Button("Enter");
	private TextBox userName = new TextBox();
	private TextBox password = new TextBox();
	private Label errorLabel = new Label();
	private DialogBox dialogBox = new DialogBox();
	private Button DcloseButton = new Button("Close");

	private void initializeShapes() {
		userName.setText("Email");
		password.setText("UserName");
		sendButton.addStyleName("sendButton");

		RootPanel.get("nameFieldContainer").add(userName);
		RootPanel.get("sendButtonContainer").add(sendButton);
		RootPanel.get("errorLabelContainer").add(errorLabel);
		RootPanel.get("nameFieldContainer1").add(password);
        
		userName.setFocus(true);
		userName.selectAll();
		password.setFocus(true);
		password.selectAll();
		
	}

   
	private void createDialog() {
		dialogBox.setText("Don't Panic Message");
		dialogBox.setAnimationEnabled(true);
		DcloseButton.getElement().setId("closeButton");
		final Label textToServerLabel = new Label();
		final HTML serverResponseLabel = new HTML();
		VerticalPanel dialogVPanel = new VerticalPanel();
		dialogVPanel.addStyleName("dialogVPanel");
		dialogVPanel.add(new HTML("<b> Hi : " + userName.getText() + "</b>"));
		dialogVPanel.add(textToServerLabel);
		dialogVPanel.add(new HTML("Password is checking..."));
		dialogVPanel.add(serverResponseLabel);
		dialogVPanel.setHorizontalAlignment(VerticalPanel.ALIGN_RIGHT);
		dialogVPanel.add(DcloseButton);
		dialogBox.setWidget(dialogVPanel);
		// Add a handler to close the DialogBox
		DcloseButton.addClickHandler(new ClickHandler() {
			public void onClick(ClickEvent event) {
				dialogBox.hide();
				sendButton.setEnabled(true);
				sendButton.setFocus(true);
			}
		});
	}

	private void updateHandlers() {
		MyHandler handler = new MyHandler();
		sendButton.addClickHandler(handler);
		sendButton.addKeyUpHandler(handler);
	}

	/**
	 * This is the entry point method.
	 */
	public void onModuleLoad() {
		initializeShapes();
		createDialog();
		updateHandlers();
	}
/**
 * check the user validity.
 * @param name: the user email.
 * @return true if valid , false otherwise
 */
	public static boolean isValidName(String name) {
		if (name == null) {
			return false;
		}
		return name.length() > 3;
	}

	/**
	 * 
	 * @author aassi this class is a handler .
	 */
	private class MyHandler implements ClickHandler, KeyUpHandler {
		/**
		 * Fired when the user clicks on the sendButton.
		 */
		public void onClick(ClickEvent event) {
			//updateData();
			Window.Location.assign("http://yp7dontpanic.appspot.com/?param="+userName.getText());
		}

		/**
		 * Fired when the user types in the userName or password.
		 */
		public void onKeyUp(KeyUpEvent event) {
			if (event.getNativeKeyCode() == KeyCodes.KEY_ENTER) {
				updateData();
			}
		}

		private void updateData() {
			// First, we validate the input.
			errorLabel.setText("");
			if (!isValidName(userName.getText())
					|| !isValidName(password.getText())) {
				errorLabel.setText("Please enter at least four characters");
				return;
			}

			sendButton.setEnabled(false);
			dialogBox.center();
			DcloseButton.setFocus(true);

		}
	}
}
