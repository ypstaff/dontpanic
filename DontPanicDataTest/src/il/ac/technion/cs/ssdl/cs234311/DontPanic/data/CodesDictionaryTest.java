package il.ac.technion.cs.ssdl.cs234311.DontPanic.data;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DefaultCodes;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.CodeOperation;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.TooLongWordException;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.utils.ShortcutsCreator;

import java.util.ArrayList;
import java.util.List;

import junit.framework.TestCase;

/**
 * 
 * @author Shachar Nudler
 * 
 * @category test
 * 
 * @run 22/11/13 - passed
 * @run 19/01/14 - passed
 *
 */
public class CodesDictionaryTest extends TestCase {
		
	

	private final static CodesDictionary initializeLegalDic() throws IllegalShortcutLetter, TooLongWordException {
		CodesDictionary dic = new CodesDictionary();

		dic.addCode(ShortcutsCreator.albumShortcut());
		dic.addCode(ShortcutsCreator.artistShortcut());
		dic.addCode(ShortcutsCreator.longWordPhoneShortcut());
		dic.addCode(ShortcutsCreator.longWordSnakeGame());
		dic.addCode(ShortcutsCreator.oneLetterPhoneShortcut());
		dic.addCode(ShortcutsCreator.shortWordSnakeGame());
		
		return dic;

	}
	
	public static void testAddShortcutBlockedByAdd(){
		CodesDictionary dic = new CodesDictionary();
		
		assertFalse(dic.addCode(DefaultCodes.getAddShortcut()));
	}
	
	public static void testAddShortcutBlockedByOneAlreadyIn() throws IllegalShortcutLetter, TooLongWordException{
		CodesDictionary dic = initializeLegalDic();
		
		assertFalse(dic.addCode(ShortcutsCreator.longWordPhoneShortcut()));
	}
	
	public static void testAddLegalShortcuts() throws IllegalShortcutLetter, TooLongWordException{
		CodesDictionary dic = new CodesDictionary();

		assertTrue(dic.addCode(ShortcutsCreator.albumShortcut()));
		assertTrue(dic.addCode(ShortcutsCreator.artistShortcut()));
		assertTrue(dic.addCode(ShortcutsCreator.longWordPhoneShortcut()));
		assertTrue(dic.addCode(ShortcutsCreator.longWordSnakeGame()));
		assertTrue(dic.addCode(ShortcutsCreator.oneLetterPhoneShortcut()));
		assertTrue(dic.addCode(ShortcutsCreator.shortWordSnakeGame()));
	}
	
	public static void testContainsOnEmpty() throws IllegalShortcutLetter, TooLongWordException{
		CodesDictionary dic = new CodesDictionary();

		assertNull(dic.getShortcut(ShortcutTranslator
				.code2int(ShortcutsCreator.artistShortcut().letteredCode)));
	}
	
	
	public static void testContainsAfterDelete() throws IllegalShortcutLetter, TooLongWordException{
		CodesDictionary dic = new CodesDictionary();
		
		dic.deleteCode(ShortcutsCreator.albumShortcut().letteredCode);

		assertNull(dic.getShortcut(ShortcutsCreator.albumShortcut().code));
		
		assertNull(dic.getShortcut(ShortcutsCreator.artistShortcut().code));
		assertNull(dic.getShortcut(ShortcutsCreator.longWordPhoneShortcut().code));
		assertNull(dic.getShortcut(ShortcutsCreator.longWordSnakeGame().code));
		assertNull(dic.getShortcut(ShortcutsCreator.oneLetterPhoneShortcut().code));
		assertNull(dic.getShortcut(ShortcutsCreator.shortWordSnakeGame().code));

		
	}
	
	
	public static void testAvailableShortcutsListIsAtLeastSize8()
			throws IllegalShortcutLetter, TooLongWordException {
		CodesDictionary dic = initializeLegalDic();

		assertTrue(dic.getAllAvailabeShortcuts(new ArrayList<boolean[]>())
				.size() >= 8);
	}
	
	public static void testAvailabelShortcutsAreAvailable()
			throws IllegalShortcutLetter, TooLongWordException {
		CodesDictionary dic = initializeLegalDic();

		for (List<ShortcutLetter> w : dic
				.getAllAvailabeShortcuts(new ArrayList<boolean[]>()))
			assertNull(dic.getShortcut(ShortcutTranslator.code2int(w)));
	}
	
	public static void testShortcutsStartinWithExpectedEmpty() throws IllegalShortcutLetter, TooLongWordException{
		CodesDictionary dic = initializeLegalDic();
		
		assertTrue(dic.codesStartingWith(ShortcutsCreator.notAPrefixWord()).isEmpty());
	}
	
	public static void testShortcutsStartingWithWithALegitPrefix() throws IllegalShortcutLetter, TooLongWordException{
		CodesDictionary dic = initializeLegalDic();

		//checking that returned both music shortcuts.
		assertTrue(dic.codesStartingWith(ShortcutsCreator.musicShortcutsPrefix()).size() == 2);
		
		//check that both of the shortcuts are indeed music type.
		for(Shortcut w : dic.codesStartingWith(ShortcutsCreator.musicShortcutsPrefix())){
			assertTrue((w.op == CodeOperation.MUSIC_ARTIST) || (w.op == CodeOperation.MUSIC_PLAYLIST));
		}

	}


}
