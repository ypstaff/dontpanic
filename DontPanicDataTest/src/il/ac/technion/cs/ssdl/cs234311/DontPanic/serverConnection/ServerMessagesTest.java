package il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.TooLongWordException;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.CodesDictionary;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.utils.ShortcutsCreator;

import java.util.ArrayList;
import java.util.Random;

import org.junit.Test;

import android.test.AndroidTestCase;

public class ServerMessagesTest extends AndroidTestCase {

	public static final String randomString() {
		char[] chars = "abcdefghijklmnopqrstuvwxyz0123456789".toCharArray();
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 20; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		return sb.toString();
	}

	public static final User randomUser() {

		final String TEST_MAIL_SERVER = "testMailServer123";

		String userName = randomString();

		return new User("test123" + userName + "@" + TEST_MAIL_SERVER,
				userName, "", "");
	}

	public final static User serverMessagesTestUser = new User(
			"serverMessagesTestUser@test_server.dont.panic.com",
			"server Messages Test User",
			"test_super@test_server.dont.panic.com", "0500000000");

	private ArrayList<Shortcut> addedCodes;

	@Override
	protected final void setUp() throws IllegalShortcutLetter,
			TooLongWordException {

		// save the user on the server
		ServerMessages.regUser(getContext(), serverMessagesTestUser);

		// create basic codes list
		addedCodes = new ArrayList<Shortcut>();

		addedCodes.add(ShortcutsCreator.albumShortcut(serverMessagesTestUser));
		addedCodes.add(ShortcutsCreator.artistShortcut(serverMessagesTestUser));
		addedCodes.add(ShortcutsCreator
				.longWordPhoneShortcut(serverMessagesTestUser));
		addedCodes.add(ShortcutsCreator
				.longWordSnakeGame(serverMessagesTestUser));
		addedCodes.add(ShortcutsCreator
				.oneLetterPhoneShortcut(serverMessagesTestUser));
		addedCodes.add(ShortcutsCreator
				.shortWordSnakeGame(serverMessagesTestUser));

	}
	
	@Override
	protected final void tearDown(){
		//should here call the clean up of the server.
	}

	@Test
	public final void testSendShortcut() {
		// register a user to the server for the test.
		Shortcut sentS = addedCodes.get(0);

		ServerMessages.sendShortcut(sentS, getContext());

		// check that the code was sent and now can get it
		CodesDictionary cd = ServerMessages.getAllShortcuts(getContext(),
				serverMessagesTestUser);

		boolean appears = false;
		for (Shortcut s : cd.getAllShortcuts()) {
			if (s.equals(sentS))
				appears = true;
			else
				fail("there is a shortcut that we dodn't add");
		}
		assertTrue("the sent shortcut doesn't appear", appears);
	}

	@Test
	public final void testDeleteShortcut() {
		Shortcut sentS = addedCodes.get(0);

		ServerMessages.deleteShortcut(sentS, serverMessagesTestUser,
				getContext());

		// check that the code was sent and now can get it
		CodesDictionary cd = ServerMessages.getAllShortcuts(getContext(),
				serverMessagesTestUser);

		boolean appears = false;
		for (Shortcut s : cd.getAllShortcuts()) {
			if (s.equals(sentS))
				appears = true;
			else
				fail("there is a shortcut that we dodn't add");
		}
		assertFalse("the sent shortcut doesn't appear", appears);
	}

	@Test
	public final void testRegUser() {
		User us = randomUser();
		assertNull("a random user was saved on the server",
				ServerMessages.loginUser(getContext(), us.email));

		ServerMessages.regUser(getContext(), us);

		assertNotNull("the user doesn't appeear in server after registration",
				ServerMessages.loginUser(getContext(), us.email));
	}

}
