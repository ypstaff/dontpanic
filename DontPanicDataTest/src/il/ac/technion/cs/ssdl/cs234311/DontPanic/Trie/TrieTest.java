package il.ac.technion.cs.ssdl.cs234311.DontPanic.Trie;

import static org.junit.Assert.*;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.junit.Before;
import org.junit.Test;

public class TrieTest {
	
	private Trie tree;
	private ArrayList<List<ShortcutLetter>> codes;
	
	private static List<ShortcutLetter> randomCode(int length){
		Random rnd = new Random();
		
		ArrayList<ShortcutLetter> code = new ArrayList<ShortcutLetter>();
		for (int j = 0; j < length; ++j){
			ShortcutLetter ltr = ShortcutLetter.values()[rnd.nextInt(8)];
			code.add(ltr);
		}
		
		return code;
	}
	
	public void test_init() {
		codes = new ArrayList<List<ShortcutLetter>>();
		
		tree = new Trie();
		
		assertNotNull(tree);
	}
	
	@Before
	public void insert() {
		
		test_init();
		
		Random rnd = new Random();
		
		for (int i = 0; i < 20 ; ++i){
			int length = rnd.nextInt(16) + 1;
			List<ShortcutLetter> code = randomCode(length);
			
			codes.add(code);
			tree.insert(code);
		}
		
	}

	@Test
	public void test_search(){
		HashMap<List<ShortcutLetter>, Boolean> map = new HashMap<List<ShortcutLetter>, Boolean>();
		
		for (List<ShortcutLetter> l: codes){
			boolean ans = tree.search(l);
			assertTrue(ans);
			map.put(l, Boolean.valueOf(ans));
		}
	
		Random rnd = new Random();
		
		for(int i = 0; i < 200; ++i){
			int length = rnd.nextInt(16) + 1;
			List<ShortcutLetter> code = randomCode(length);
			
			if(!map.containsKey(code))
				assertFalse(tree.search(code));
		}
	}
	
	@Test
	public void test_getAllSons(){
		for(int i = 0; i < 8; ++i){
			ShortcutLetter ltr = ShortcutLetter.values()[i];
			ArrayList<ShortcutLetter> prefix = new ArrayList<ShortcutLetter>();
			prefix.add(ltr);
			
			List<List<ShortcutLetter>> poss_sons = tree.getAllSons(prefix);
						
			for(List<ShortcutLetter> itm: codes){
				if(itm.get(0) == ltr){
					assertTrue(poss_sons.contains(itm));
					poss_sons.remove(itm);
				}
			}
			
			assertTrue(poss_sons.isEmpty());
		}
	}

	@Test
	public void test_remove(){
		Random rnd = new Random();
		
		for(int i = 0; i < 200; ++i){
			int length = rnd.nextInt(16) + 1;
			List<ShortcutLetter> code = randomCode(length);
			
			if(!codes.contains(code))
				tree.remove(code);
		}
		
		for (List<ShortcutLetter> l: codes)
			assertTrue(tree.search(l));
		
		for (List<ShortcutLetter> l: codes){
			tree.remove(l);
			assertFalse(tree.search(l));
		}

		//System.out.println(tree.numberEntries());
		assertTrue(tree.numberEntries() == 0);
	}
	
	@Test
	public void test_numberEntries(){
		assertEquals(tree.numberEntries(), codes.size());
	}
}
