package il.ac.technion.cs.ssdl.cs234311.DontPanic.shortcut;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.DefaultCodes;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.TooLongWordException;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.utils.BasicCodes;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.utils.ShortcutsCreator;

import java.util.ArrayList;
import java.util.List;

import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

public class ShortcutTranslatorTest {
	
	
	

	@Rule
	public ExpectedException exception = ExpectedException.none();

	@Test
	@SuppressWarnings("static-method") // junit test - can't be static
	public final void testIsLegal() {
		for(boolean[] letter : BasicCodes.legalCodes){
			assertTrue(ShortcutTranslator.isLegal(letter));
		}
		for(boolean[] letter : BasicCodes.illegalCodes){
			assertFalse(ShortcutTranslator.isLegal(letter));
		}
	}

	/**
	 * translate all the legal letters and check the translated value
	 * 
	 * @throws IllegalShortcutLetter
	 *             - if one of the legal values is considered as illegal
	 */
	@SuppressWarnings("static-method") // junit test - can't be static
	@Test
	public final void testArray2letterOfLegalCodes()
			throws IllegalShortcutLetter {
		assertEquals(ShortcutLetter.ONE,
				ShortcutTranslator.array2letter(BasicCodes.one_letter));
		assertEquals(ShortcutLetter.TWO,
				ShortcutTranslator.array2letter(BasicCodes.two_letter));
		assertEquals(ShortcutLetter.THREE,
				ShortcutTranslator.array2letter(BasicCodes.three_letter));
		assertEquals(ShortcutLetter.FOUR,
				ShortcutTranslator.array2letter(BasicCodes.four_letter));
		assertEquals(ShortcutLetter.ONE_TWO,
				ShortcutTranslator.array2letter(BasicCodes.one_two_letter));
		assertEquals(ShortcutLetter.FOUR_ONE,
				ShortcutTranslator.array2letter(BasicCodes.one_four_letter));
		assertEquals(ShortcutLetter.TWO_THREE,
				ShortcutTranslator.array2letter(BasicCodes.two_three_letter));
		assertEquals(ShortcutLetter.THREE_FOUR, ShortcutTranslator.array2letter(BasicCodes.three_four_letter));
	}
	
	@Test
	@Ignore
	public final void testArray2letterOfIllegalCodes() throws IllegalShortcutLetter {
		for(boolean[] letter : BasicCodes.illegalCodes){
			exception.expect(IllegalShortcutLetter.class);
			ShortcutTranslator.array2letter(letter);
		}
	}
	

	/**
	 * assume that the array2letter and isLegal works for this test to work
	 * 
	 * @throws IllegalShortcutLetter
	 *             - if thrown the bug is because of a letter that returned tru
	 *             for isLegal isn't legal in the translation
	 */
	@Test
	@SuppressWarnings("static-method") // junit test - can't be static
	public final void testCode2int() throws IllegalShortcutLetter {
		for(int i=0; i<BasicCodes.sortedCodes.length; ++i){
			if(ShortcutTranslator.isLegal(BasicCodes.sortedCodes[i])){
				List<ShortcutLetter> word = new ArrayList<ShortcutLetter>();
				word.add(ShortcutTranslator
						.array2letter(BasicCodes.sortedCodes[i]));
				assertEquals(i, ShortcutTranslator.code2int(word));
			}
		}
	}
	
	private static boolean booleanCodeEquel(boolean[] c1, boolean[] c2){
		if(c1.length != c2.length)
			return false;
		
		for(int i=0; i < c1.length; ++i)
			if(c1[i] != c2[i])
				return false;
		
		return true;
						
	}

	@SuppressWarnings("static-method") // junit test - can't be static
	@Test
	public final void testWordCode2array() {
		for(int i=0; i<16; ++i){
			boolean[] code = new boolean[4];
			
			ShortcutTranslator.wordCode2array(i, code);
			
			assertTrue(booleanCodeEquel(BasicCodes.sortedCodes[i],code));
		}
	}

	@Test
	@SuppressWarnings("static-method") // junit test - can't be static
	public final void testShortcutOperationTypeOrdinal() throws IllegalShortcutLetter, TooLongWordException {
		Shortcut music1 = ShortcutsCreator.albumShortcut();
		Shortcut music2 = ShortcutsCreator.artistShortcut();
		
		Shortcut phone1 = ShortcutsCreator.longWordPhoneShortcut();
		Shortcut phone2 = ShortcutsCreator.oneLetterPhoneShortcut();
		
		Shortcut game1 = ShortcutsCreator.shortWordSnakeGame();
		Shortcut game2 = ShortcutsCreator.longWordSnakeGame();
		
		Shortcut addShortcut = DefaultCodes.getAddShortcut();
		Shortcut delShortcut = DefaultCodes.getDeleteShortcut();
		
		//check that shortcuts of the same type has the same ordinal
		assertEquals(ShortcutTranslator.ShortcutOperationTypeOrdinal(music1),
				ShortcutTranslator.ShortcutOperationTypeOrdinal(music2));
		
		assertEquals(ShortcutTranslator.ShortcutOperationTypeOrdinal(phone1),
				ShortcutTranslator.ShortcutOperationTypeOrdinal(phone2));
		
		assertEquals(ShortcutTranslator.ShortcutOperationTypeOrdinal(game1),
				ShortcutTranslator.ShortcutOperationTypeOrdinal(game2));
		
		assertEquals(ShortcutTranslator.ShortcutOperationTypeOrdinal(addShortcut),
				ShortcutTranslator.ShortcutOperationTypeOrdinal(delShortcut));
		
		//check that all the different ones has different ordinal
		//music is not as all one below
		assertFalse(ShortcutTranslator.ShortcutOperationTypeOrdinal(music1) == ShortcutTranslator
				.ShortcutOperationTypeOrdinal(phone1));
		
		assertFalse(ShortcutTranslator.ShortcutOperationTypeOrdinal(music1) == ShortcutTranslator
				.ShortcutOperationTypeOrdinal(game1));
		
		assertFalse(ShortcutTranslator.ShortcutOperationTypeOrdinal(music1) == ShortcutTranslator
				.ShortcutOperationTypeOrdinal(addShortcut));
		
		//phone is not as all one below		
		assertFalse(ShortcutTranslator.ShortcutOperationTypeOrdinal(phone1) == ShortcutTranslator
				.ShortcutOperationTypeOrdinal(game1));
		
		assertFalse(ShortcutTranslator.ShortcutOperationTypeOrdinal(phone1) == ShortcutTranslator
				.ShortcutOperationTypeOrdinal(addShortcut));
		
		//game is not as all one below
		assertFalse(ShortcutTranslator.ShortcutOperationTypeOrdinal(game1) == ShortcutTranslator
				.ShortcutOperationTypeOrdinal(addShortcut));
		
	}
}
