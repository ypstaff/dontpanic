package phoneOperation;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations.Contact;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import junit.framework.TestCase;

import org.junit.Test;
import org.mockito.Mockito;

import android.database.Cursor;
import android.provider.ContactsContract.CommonDataKinds.Phone;

public class ContactOperationsTest extends TestCase {
	
    private boolean advanceIfHasNext(final Iterator<Contact> i, Contact contact) {
        if (i.hasNext())
            contact = i.next();
        return i.hasNext();
    }

    private int getTypeConstantByString(final String type) {
        if (type.equals("home"))
            return Phone.TYPE_HOME;

        if (type.equals("mobile"))
            return Phone.TYPE_MOBILE;

        return Phone.TYPE_WORK;
    }

    private void contactsListSetUp(final List<Contact> contacts) {
        contacts.add(new Contact("bob", "12345", "home", "123"));
        contacts.add(new Contact("lisa", "12315", "mobile", "321"));
    }

    private void contactsAndMockSetUp(final Cursor phones, final List<Contact> contacts,
            final Contact contact) {
        contactsListSetUp(contacts);
        final Iterator<Contact> i = contacts.iterator();

        Mockito.when(phones.moveToNext()).thenReturn(advanceIfHasNext(i, contact));
        Mockito.when(phones.getInt(0)).thenReturn(
                getTypeConstantByString(contact.type));
        Mockito.when(phones.getColumnIndex("")).thenReturn(0);
    }

    @Test
    public void getAnEmptyListIfNoContacts() {
        final Cursor phones = Mockito.mock(Cursor.class);
//        assertTrue(ContactOperations.getAllContacts(phones).isEmpty());
    }

    @Test
    public void getAllContactsNamesOnSuccess() {
        final Cursor phones = Mockito.mock(Cursor.class);
        final List<Contact> contacts = new ArrayList<Contact>();
        final Contact c = Mockito.mock(Contact.class);

        contactsAndMockSetUp(phones, contacts, c);
        // c - should be initialized at this moment already
        Mockito.when(phones.getString(0)).thenReturn(c.name);

//        final List<Contact> outputContacts = ContactOperations.getAllContacts(phones);

//        assertEquals(outputContacts.size(), contacts.size());
//
//        for (int j = 0; j < contacts.size(); ++j)
//            assertEquals(outputContacts.get(j).name, contacts.get(j).name);
    }

    @Test
    public void getAllContactsPhoneNosOnSuccess() {
        final Cursor phones = Mockito.mock(Cursor.class);
        final List<Contact> contacts = new ArrayList<Contact>();
        final Contact c = Mockito.mock(Contact.class);

        contactsAndMockSetUp(phones, contacts, c);
        // c - should be initialized at this moment already
        Mockito.when(phones.getString(0)).thenReturn(c.phoneNo);

//        final List<Contact> outputContacts = ContactOperations.getAllContacts(phones);

//        assertEquals(outputContacts.size(), contacts.size());
//
//        for (int j = 0; j < contacts.size(); ++j)
//            assertEquals(outputContacts.get(j).phoneNo, contacts.get(j).phoneNo);
    }

    @Test
    public void getAllContactsTypesOnSuccess() {
        final Cursor phones = Mockito.mock(Cursor.class);
        final List<Contact> contacts = new ArrayList<Contact>();
        final Contact contact = Mockito.mock(Contact.class);

        contactsAndMockSetUp(phones, contacts, contact);

//        final List<Contact> outputContacts = ContactOperations.getAllContacts(phones);
//
//        assertEquals(outputContacts.size(), contacts.size());
//
//        for (int j = 0; j < contacts.size(); ++j)
//            assertEquals(outputContacts.get(j).type, contacts.get(j).type);
    }

    @Test
    public void getAllContactsIdsOnSuccess() {
        final Cursor phones = Mockito.mock(Cursor.class);
        final List<Contact> contacts = new ArrayList<Contact>();
        final Contact c = Mockito.mock(Contact.class);

        contactsAndMockSetUp(phones, contacts, c);
        // c - should be initialized at this moment already
        Mockito.when(phones.getString(0)).thenReturn(c.id);

//        final List<Contact> outputContacts = ContactOperations.getAllContacts(phones);
//
//        assertEquals(outputContacts.size(), contacts.size());
//
//        for (int j = 0; j < contacts.size(); ++j)
//            assertEquals(outputContacts.get(j).id, contacts.get(j).id);
    }
    
}
