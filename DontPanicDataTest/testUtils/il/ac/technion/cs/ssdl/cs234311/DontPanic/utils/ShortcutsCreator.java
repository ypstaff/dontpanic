package il.ac.technion.cs.ssdl.cs234311.DontPanic.utils;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.CodeOperation;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.TooLongWordException;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;

import java.util.ArrayList;
import java.util.List;

public final class ShortcutsCreator {

	public final static User mockUser = new User("test_user@test_server.dont.panic.com", "test user", "test_super@test_server.dont.panic.com", "0500000000");

	final static ShortcutLetter[][] cods = {
			{ ShortcutLetter.TWO_THREE },
			{ ShortcutLetter.THREE_FOUR, ShortcutLetter.FOUR_ONE,
					ShortcutLetter.THREE_FOUR, ShortcutLetter.FOUR_ONE },
			{ ShortcutLetter.ONE_TWO, ShortcutLetter.TWO_THREE,
					ShortcutLetter.THREE_FOUR, ShortcutLetter.FOUR_ONE },
			{ ShortcutLetter.FOUR_ONE },
			{ ShortcutLetter.FOUR_ONE, ShortcutLetter.FOUR_ONE,
					ShortcutLetter.ONE_TWO },
			{ ShortcutLetter.FOUR_ONE, ShortcutLetter.FOUR_ONE,
					ShortcutLetter.TWO_THREE } };

	
	public final static List<boolean[]> notAPrefixWord() {
		List<boolean[]> word = new ArrayList<boolean[]>();

		boolean[] twoThreeLetter = new boolean[4];
		twoThreeLetter[1] = twoThreeLetter[2] = true;
		twoThreeLetter[0] = twoThreeLetter[3] = false;

		word.add(twoThreeLetter.clone());
		word.add(twoThreeLetter.clone());

		return word;
	}

	public final static List<boolean[]> musicShortcutsPrefix() {
		List<boolean[]> word = new ArrayList<boolean[]>();

		boolean[] fourOneLetter = new boolean[4];
		fourOneLetter[0] = fourOneLetter[3] = true;
		fourOneLetter[1] = fourOneLetter[2] = false;

		word.add(fourOneLetter.clone());
		word.add(fourOneLetter.clone());

		return word;
	}

	final private static List<ShortcutLetter> getWord(int idx) {
		List<ShortcutLetter> word = new ArrayList<ShortcutLetter>();
		for (ShortcutLetter l : cods[idx]) {
			word.add(l);
		}

		return word;
	}

	public final static Shortcut oneLetterPhoneShortcut()
			throws IllegalShortcutLetter, TooLongWordException {

		return oneLetterPhoneShortcut(mockUser);
	}

	public final static Shortcut longWordPhoneShortcut()
			throws IllegalShortcutLetter, TooLongWordException {
		return longWordPhoneShortcut(mockUser);
	}

	public final static Shortcut longWordSnakeGame()
			throws IllegalShortcutLetter, TooLongWordException {
		return longWordSnakeGame(mockUser);

	}

	public final static Shortcut shortWordSnakeGame()
			throws IllegalShortcutLetter, TooLongWordException {
		return shortWordSnakeGame(mockUser);

	}

	public final static Shortcut albumShortcut() throws IllegalShortcutLetter,
			TooLongWordException {
		return albumShortcut(mockUser);
	}

	public final static Shortcut artistShortcut() throws IllegalShortcutLetter,
			TooLongWordException {
		return artistShortcut(mockUser);
	}
	
	
	private final static Shortcut createShortcut(User user, String disp,
			String json_prop, List<ShortcutLetter> word, CodeOperation type)
			throws IllegalShortcutLetter, TooLongWordException {
		List<String> props = new ArrayList<String>();
		props.add(Shortcut.PROPERTY_DISP_INDEX, disp);
		props.add(Shortcut.PROPERTY_DISP_INDEX, json_prop);

		return new Shortcut(user, word, type, props);
	}

	public final static Shortcut oneLetterPhoneShortcut(User user)
			throws IllegalShortcutLetter, TooLongWordException {

		return createShortcut(user, "Moshe", "Moshe's phone information",
				getWord(0), CodeOperation.PHONE_CALL);
	}

	public final static Shortcut longWordPhoneShortcut(User user)
			throws IllegalShortcutLetter, TooLongWordException {
		return createShortcut(user, "Michael", "Michael's phone informations",
				getWord(1), CodeOperation.PHONE_CALL);
	}

	public final static Shortcut longWordSnakeGame(User user)
			throws IllegalShortcutLetter, TooLongWordException {
		return createShortcut(user, "snake", "snake", getWord(2),
				CodeOperation.GAME_PLAY);

	}

	public final static Shortcut shortWordSnakeGame(User user)
			throws IllegalShortcutLetter, TooLongWordException {
		return createShortcut(user, "snake", "snake", getWord(3),
				CodeOperation.GAME_PLAY);

	}

	public final static Shortcut albumShortcut(User user)
			throws IllegalShortcutLetter, TooLongWordException {
		return createShortcut(user, "the snow goose", "the snow goose",
				getWord(4), CodeOperation.MUSIC_PLAYLIST);
	}

	public final static Shortcut artistShortcut(User user)
			throws IllegalShortcutLetter, TooLongWordException {
		return createShortcut(user, "camel", "camel", getWord(5),
				CodeOperation.MUSIC_ARTIST);
	}

}
