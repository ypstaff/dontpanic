package il.ac.technion.cs.ssdl.cs234311.DontPanic.utils;

public class BasicCodes {

	//legal code
	public static boolean[] one_letter = {true, false, false, false};
	public static boolean[] two_letter = {false, true, false, false};
	public static boolean[] three_letter = {false, false, true, false};
	public static boolean[] four_letter = {false, false, false, true};
	public static boolean[] one_two_letter = {true, true, false, false};
	public static boolean[] two_three_letter = {false, true, true, false};
	public static boolean[] three_four_letter = {false, false, true, true};
	public static boolean[] one_four_letter = {true, false, false, true};
	
	//ilegal codes
	public static boolean[] empty_letter = {false, false, false, false};
	public static boolean[] one_three_letter = {true, false, true, false};
	public static boolean[] two_four_letter = {false, true, false, true};
	public static boolean[] one_two_three_letter = {true, true, true, false};
	public static boolean[] one_two_four_letter = {true, true, false, true};
	public static boolean[] one_three_four_letter = {true, false, true, true};
	public static boolean[] two_three_four_letter = {false, true, true, true};
	public static boolean[] one_two_three_four_letter = {true, true, true, true};
	
	public static boolean[][] legalCodes = { one_letter, two_letter, three_letter,
			four_letter, one_two_letter, two_three_letter, three_four_letter,
			one_four_letter };
	
	public static boolean[][] illegalCodes = { empty_letter, one_three_letter,
			two_four_letter, one_two_three_letter, one_two_four_letter,
			one_three_four_letter, two_three_four_letter,
			one_two_three_four_letter };
	
	public static boolean[][] sortedCodes = { empty_letter, one_letter, two_letter,
			one_two_letter, three_letter, one_three_letter, two_three_letter,
			one_two_three_letter, four_letter, one_four_letter,
			two_four_letter, one_two_four_letter, three_four_letter,
			one_three_four_letter, two_three_four_letter,
			one_two_three_four_letter };

}
