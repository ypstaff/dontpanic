package il.ac.technion.cs.ssdl.cs234311.DontPanic;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.ShortcutTypes;

import java.util.ArrayList;
import java.util.List;

public class ShortcutTranslator {

    /**
     * enum mapping colors to their places in the boolean array. to use this
     * enum, you can use the values in a line looking like this:
     * 
     * array[ColorArrayLocations.<color name>.ordinal()]
     * 
     * this will access the array in the place corresponding to the color.
     * 
     * @author Shachar Nudler
     * 
     */
    public enum ColorArrayLocations {
        RED, YELLOW, BLUE, GREEN
    }

    public static class IllegalShortcutLetter extends Exception {

        private static final long serialVersionUID = -563468304240124707L;

    }

    public enum ShortcutLetter {
        ONE(1), TWO(2), THREE(4), FOUR(8), ONE_TWO(3), TWO_THREE(6), THREE_FOUR(
                12), FOUR_ONE(9);

        final public int val;

        ShortcutLetter(final int _val) {
            val = _val;
        }

        public boolean isDefaultLetter() {
            if (compareTo(ONE) == 0 || compareTo(TWO) == 0
                    || compareTo(THREE) == 0 || compareTo(FOUR) == 0)
                return true;
            return false;
        }

    }

    public static boolean isLegal(final boolean[] l) {
        try {
            array2letter(l);
        } catch (final IllegalShortcutLetter e) {
            return false;
        }
        return true;
    }

    /**
     * translate a boolean array - the input letter into the inner
     * representation of a legal letter - {@link ShortcutLetter}
     * 
     * @param letter
     *            - the input letter got from the user
     * @return - the translated value of letter
     * @throws IllegalShortcutLetter
     *             - if the entered letter isn't legal by the shortcuts letter
     *             rules
     */
    public static ShortcutLetter array2letter(final boolean[] letter)
            throws IllegalShortcutLetter {
        switch (array2int(letter)) {
        case 1:
            return ShortcutLetter.ONE;
        case 2:
            return ShortcutLetter.TWO;
        case 3:
            return ShortcutLetter.ONE_TWO;
        case 4:
            return ShortcutLetter.THREE;
        case 6:
            return ShortcutLetter.TWO_THREE;
        case 8:
            return ShortcutLetter.FOUR;
        case 9:
            return ShortcutLetter.FOUR_ONE;
        case 12:
            return ShortcutLetter.THREE_FOUR;
        default:
            throw new IllegalShortcutLetter();

        }
    }

    private static int array2int(final boolean[] letter) {
        int code = 0;
        for (int i = 3; i >= 0; --i) {
            code <<= 1;
            code += letter[i] ? 1 : 0;
        }
        return code;
    }

    /**
     * translate a list of boolean[] to list of ShortcutLetter. do the same as
     * {@link ShortcutTranslator.array2letter} to each letter separately.
     * 
     * @param _word
     *            - first letter of the code.
     * @return - the code transform to it's integer encoding
     * @throws Exception
     */
    public static List<ShortcutLetter> word2code(final List<boolean[]> _word)
            throws IllegalShortcutLetter {
        final List<ShortcutLetter> code = new ArrayList<ShortcutLetter>();

        for (int i = 0; i < _word.size(); ++i)
            code.add(array2letter(_word.get(i)));

        return code;
    }

    public static int code2int(final List<ShortcutLetter> code) {
        int res = 0;
        int exp = 1;
        for (int i = 0; i < code.size(); ++i) {
            res += code.get(i).val * exp;
            exp *= 16;
        }
        return res;
    }

    /**
     * return the letter encoding of half of the first letter of a given integer
     * code. used in BT connection, to translate the letter code back to boolean
     * arrays.
     * 
     * @param code
     *            - (input) the code for one letter
     * @param res
     *            - (output) an array of size 4 to write the answer to.
     */
    public static void wordCode2array(final int code, final boolean[] res) {
        for (int i = 0; i < 4; ++i)
            // cell 3 is MSB so for i = 3 check just first bit
            res[i] = (code >> i) % 2 == 1;
    }

    /**
     * returns an order between the different shortcut operations based on the
     * type of the activity. the types are of {@link ShortcutTypes}, and thus
     * merge things like different games, different music types and etc. <br>
     * used when ordering the shortcuts by types.
     * 
     * @param r
     *            - the shortcut to get the order of
     * @return the "type order" of the given shortcut r
     */
    public static int ShortcutOperationTypeOrdinal(final Shortcut r) {

        switch (r.op) {
        case ADD_SHORTCUT:
        case DELETE_SHORTCUT:
        case DEFAULT_OPERTAIONS:
        case PANIC_SHORTCUT:
            return ShortcutTypes.DEFAULT.ordinal();
        case PHONE_CALL:
            return ShortcutTypes.PHONE.ordinal();
        case MUSIC_ARTIST:
        case MUSIC_PLAYLIST:
            return ShortcutTypes.MUSIC.ordinal();
        case GAME_2048:
        case GAME_PLAY:
            return ShortcutTypes.GAMES.ordinal();
        case MAGAZINE_BROWSE:
        case MAGAZINE_LIBRARY:
            return ShortcutTypes.MAGAZINES.ordinal();
        case KEYBOARD_SHORTCUT:
        default:
            return ShortcutTypes.DEFAULT.ordinal();
        }

    }

}
