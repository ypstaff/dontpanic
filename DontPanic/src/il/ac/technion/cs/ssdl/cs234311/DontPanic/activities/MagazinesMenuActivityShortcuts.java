package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.MagazineOperations.Magazine;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.CodeOperation;
import android.content.Intent;

import com.google.gson.Gson;

/**
 * Responsible for showing a list of magazines for a certain category in the
 * shortcuts menu.
 * 
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public class MagazinesMenuActivityShortcuts extends MagazinesMenuActivity {

    @Override
    protected void openNextWindowWithSelection() {
        if (getAdapter().getCount() == 0)
            return;

        final Magazine selected = getAdapter().getItem(
                getListView().getCheckedItemPosition());

        final Intent intent = new Intent(MagazinesMenuActivityShortcuts.this,
                CodeAdditionActivity.class);
        intent.putExtra("SHORTCUT_TYPE", CodeOperation.MAGAZINE_BROWSE);
        intent.putExtra("SHORTCUT_OBJ", new Gson().toJson(selected));
        startActivity(intent);
    }
}
