package il.ac.technion.cs.ssdl.cs234311.DontPanic.data;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

/**
 * A class to retrieve all data saved in the app.
 * 
 * @author Shachar Nudler
 * 
 */
public class XmlFileReader {

    /**
     * Get all codes saved in the app.
     * 
     * @param codesFile
     *            - codes xml file as string
     * @return codes dictionary of all codes saved in the app.
     */
    public static CodesDictionary readAllCodes(final String codesFile) {

        final DocumentBuilderFactory factory = DocumentBuilderFactory
                .newInstance();
        final CodesDictionary codes = new CodesDictionary();

        try {
            Log.i("XmlReader", "start");
            Log.i("XmlReader", codesFile);

            final DocumentBuilder builder = factory.newDocumentBuilder();
            // Document dom = builder.parse(codesFile);

            final Document dom = builder.parse(new InputSource(
                    new StringReader(codesFile)));

            Log.i("XmlReader", "got buillder");

            final NodeList items = dom
                    .getElementsByTagName(CodesXmlFormatter.CODE_TAG);

            Log.i("XmlReader", "got root");
            for (int i = 0; i < items.getLength(); i++) {

                final Node child = items.item(i).getFirstChild();

                final Shortcut code = new Gson().fromJson(child.getNodeValue(),
                        Shortcut.class);
                codes.addCode(code);

            }
        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
        return codes;
    }

    /**
     * get the data of the user using the app.
     * 
     * @param userFile
     *            user file as string
     * 
     * @return User class object of the user using the app
     */
    public static User readUserData(final String userFile) {

        final DocumentBuilderFactory factory = DocumentBuilderFactory
                .newInstance();

        try {
            Log.i("XmlReader", "userFile - start");
            Log.i("XmlReader", userFile);

            final DocumentBuilder builder = factory.newDocumentBuilder();
            // Document dom = builder.parse(userFile);
            final Document dom = builder.parse(new InputSource(
                    new StringReader(userFile)));

            Log.i("XmlReader", "got buillder");

            final NodeList items = dom
                    .getElementsByTagName(CodesXmlFormatter.USER_TAG);

            // Log.i("XmlReader", "got root. items:");
            // Log.i("XmlReader", Integer.toString(items.getLength()));
            // Log.i("XmlReader",
            // Boolean.toString(items.item(0).hasChildNodes()));

            final Node child = items.item(0).getFirstChild();
            // Log.i("XmlReader", child.getNodeValue());

            final User user = new Gson().fromJson(child.getNodeValue(),
                    User.class);

            // Log.i("XmlReader", user.username);

            return user;

        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String readSavedData(final Context appContext,
            final String path) {
        String datax = "";
        try {
            final FileInputStream fIn = appContext.openFileInput(path);
            final InputStreamReader isr = new InputStreamReader(fIn);
            final BufferedReader buffreader = new BufferedReader(isr);

            String readString = buffreader.readLine();
            while (readString != null) {
                datax = datax + readString;
                readString = buffreader.readLine();
            }

            isr.close();
        } catch (final IOException ioe) {
            ioe.printStackTrace();
        }
        return datax;
    }
}
