package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.MagazineOperations.Magazine;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.MagazineOperations.MagazineServices;

/**
 * Responsible for showing a list of magazines for a certain category in the
 * magazines mini-app.
 * 
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public class MagazinesMenuActivityMiniApp extends MagazinesMenuActivity {

    @Override
    protected void openNextWindowWithSelection() {
        if (getAdapter().getCount() == 0)
            return;

        final Magazine selected = getAdapter().getItem(
                getListView().getCheckedItemPosition());

        startActivity(MagazineServices.getRssReaderIntentWithMagazine(
                getApplicationContext(), selected));
    }

}
