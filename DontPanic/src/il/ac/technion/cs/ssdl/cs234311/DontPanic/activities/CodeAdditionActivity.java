package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.CodeOperation;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.NoSuchOpException;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.MagazineOperations.Magazine;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.classes.ShortcutsListAdapter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.ShortcutsParser;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations.Contact;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;

/**
 * Responsible for adding new shortcuts.
 * 
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public class CodeAdditionActivity extends CodeOperationsActivity {

    private TextView shortcuDetails;

    private String shortcutName;
    private Object shortcutObj;
    private CodeOperation shortcutType;

    /**
     * Used to retrieve and initialize the new Shortcut details
     */
    private void initShortcutDetails() {
        final Bundle extras = getIntent().getExtras();

        if (extras == null)
            return;

        final String jsonObj = extras.getString("SHORTCUT_OBJ");
        shortcutType = (CodeOperation) extras.getSerializable("SHORTCUT_TYPE");

        if (shortcutType.equals(CodeOperation.PHONE_CALL)) {
            shortcutObj = new Gson().fromJson(jsonObj, Contact.class);
            shortcutName = ((Contact) shortcutObj).name;

        } else if (shortcutType.equals(CodeOperation.MAGAZINE_BROWSE)) {
            shortcutObj = new Gson().fromJson(jsonObj, Magazine.class);
            shortcutName = ((Magazine) shortcutObj).name;

        } else {// for now, all other shortcuts contain only strings
            shortcutObj = jsonObj;
            shortcutName = (String) shortcutObj;
        }

        updateShortcutDetailsDisplayed();
    }

    /**
     * Update the window layout with new Shortcut details
     */
    private void updateShortcutDetailsDisplayed() {
        String shortcutOp = null;
        try {
            shortcutOp = ShortcutsParser.getOperationActionName(shortcutType,
                    getApplicationContext());
        } catch (final NoSuchOpException e) {
            return;
        }

        shortcuDetails.setText(getString(R.string.code_shortcut_new_details)
                + " " + shortcutOp + " " + shortcutName);
    }

    /**
     * Update the list of available codes depending on the entered code (as the
     * prefix)
     */
    private void updateAvailableCodesList() {
        listItems = new ArrayList<Shortcut>();
        stringToCode = new HashMap<String, List<ShortcutLetter>>();

        final List<Shortcut> shortcuts = LoginActivity.dataManager
                .getAvailabeShortcuts(word);

        for (final Shortcut s : shortcuts) {

            final String $ = printWord(s.letteredCode);
            stringToCode.put($, s.letteredCode);
            listItems.add(s);
        }
        adapter = new ShortcutsListAdapter(this,
                R.layout.list_row_small_highlighted, R.id.listItemText,
                R.id.listItemShortcutDescription, listItems, word);

        listView.setAdapter(adapter);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
    }

    /**
     * Adds the new Shortcut if legal
     * 
     * @param code
     *            - the shortcut code represented by a list of ShortcutLetter
     *            Objects
     */
    private void addShortcutIfLegal(final List<ShortcutLetter> code) {
        try {
            if (LoginActivity.dataManager.addShortcut(code, shortcutName,
                    shortcutObj, shortcutType)) {
                showToast(getString(R.string.code_added));
                clearEnteredCode();
                openMainWindow();

            } else {
                ALog("was illegal code");
                showToast(getString(R.string.code_illegal));
                clearEnteredCode();
                updateUI();
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds a new Shortcut using the entered code
     */
    private void addShortcutFromEnteredCode() {
        try {
            addShortcutIfLegal(ShortcutTranslator.word2code(word));
        } catch (final IllegalShortcutLetter e) {
            e.printStackTrace();
        }
    }

    /**
     * Adds a new Shortcut using the code selected from the
     * "available codes list"
     */
    private void addShortcutFromList() {
        addShortcutIfLegal(((Shortcut) listView.getItemAtPosition(listView
                .getCheckedItemPosition())).letteredCode);
    }

    private void ALog(final String msg) {
        Log.i("CodeAddingActivity", msg);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.code_adding);

        initViews();
        initTouchListeners();

        initShortcutDetails();
        setTitle();
        updateUI();
    }

    @Override
    protected void initAdditionalViews() {
        listView = (ListView) findViewById(R.id.shortcuts_not_used_list);
        shortcuDetails = (TextView) findViewById(R.id.shortcut_details);
    }

    @Override
    protected void additionalUpdates() {
        updateAvailableCodesList();
    }

    @Override
    protected void setTitle() {
        title.setText(getString(R.string.title_menu_code_addition));
    }

    @Override
    protected void openMainWindow() {
        final Intent intent = new Intent(CodeAdditionActivity.this,
                MainWindowActivity.class);

        // clear all the previous activities from the stack
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    protected void enter() {
        if (listView.getCheckedItemPosition() != AdapterView.INVALID_POSITION) {
            addShortcutFromList();
            return;
        }

        if (word.isEmpty()) {
            showToast(getString(R.string.code_nothing_entered));
            return;
        }

        addShortcutFromEnteredCode();
    }
}
