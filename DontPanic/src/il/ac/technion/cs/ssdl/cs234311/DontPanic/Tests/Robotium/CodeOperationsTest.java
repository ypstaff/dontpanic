package il.ac.technion.cs.ssdl.cs234311.DontPanic.Tests.Robotium;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DontPanicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.LoginActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.ShortcutsParser;
import android.test.ActivityInstrumentationTestCase2;
import android.widget.EditText;

import com.robotium.solo.Solo;

/**
 * A class that tests general Code Operations and navigation. Please verify that
 * you have an Internet connection.
 * 
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public class CodeOperationsTest extends
        ActivityInstrumentationTestCase2<LoginActivity> {

    private Solo solo;
    private final User user;

    public CodeOperationsTest() {
        super(LoginActivity.class);

        user = ServerMessagesTestUtils.randomUser();

        ServerMessagesTestUtils.runMaintainance();
        ServerMessagesTestUtils.regUserForTest(user);
    }

    @Override
    public void setUp() {
        solo = new Solo(getInstrumentation(), getActivity());

        solo.assertCurrentActivity("Error: Expected Login Activity",
                "LoginActivity");
        try {
            runTestOnUiThread(new Runnable() {
                @Override
                public void run() {
                    final EditText user_name = (EditText) solo
                            .getView(R.id.log_user_name);
                    final EditText email = (EditText) solo
                            .getView(R.id.log_user_email);
                    user_name.setText(user.username);
                    email.setText(user.email);
                }
            });
        } catch (final Throwable e) {
            e.printStackTrace();
        }
        solo.clickOnView(solo.getView(R.id.login_button));
        checkMainWindow();
    }

    @Override
    public void tearDown() throws Exception {
        solo.sleep(5000);
        solo.finishOpenedActivities();
        super.tearDown();
    }

    private void makeCommand(final boolean r, final boolean y, final boolean b,
            final boolean g, final boolean[] command) {
        command[ColorArrayLocations.YELLOW.ordinal()] = y;
        command[ColorArrayLocations.GREEN.ordinal()] = g;
        command[ColorArrayLocations.BLUE.ordinal()] = b;
        command[ColorArrayLocations.RED.ordinal()] = r;
    }

    private void checkMenuNavigationShortcutTypes() {
        solo.assertCurrentActivity("Error: Expected ShortcutTpesActivity",
                "ShortcutTypesActivity");
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity())
                .getString(R.string.title_menu_shortcut_types)));

        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity())
                .getString(R.string.shortcut_phone_call_type)));
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity()).getString(R.string.shortcut_music_type)));
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity()).getString(R.string.shortcut_game_type)));
    }

    private void checkMenuNavigationPhone() {
        solo.assertCurrentActivity("Error: Expected Phone menu activity",
                "PhoneMenuActivity");
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity()).getString(R.string.title_menu_phone)));
    }

    private void checkMenuNavigationMusic() {
        solo.assertCurrentActivity("Error: Expected Music menu activity",
                "MusicMenuActivity");
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity()).getString(R.string.title_menu_music)));

        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity())
                .getString(R.string.music_shortcut_to_an_album)));
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity())
                .getString(R.string.music_shortcut_to_an_artist)));
    }

    private void checkMenuNavigationMusicAlbums() {
        solo.assertCurrentActivity(
                "Error: Expected Music albums menu activity",
                "MusicAlbumsMenuActivity");
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity())
                .getString(R.string.title_menu_music_albums)));

        assertTrue(solo.searchText(ShortcutsParser.ALL_SONGS_ALBUM_NAME));
    }

    private void checkMenuNavigationMusicArtists() {
        solo.assertCurrentActivity(
                "Error: Expected Music artists menu activity",
                "MusicArtistsMenuActivity");
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity())
                .getString(R.string.title_menu_music_artists)));
    }

    private void checkMenuNavigationGames() {
        solo.assertCurrentActivity("Error: Expected Games menu activity",
                "GamesMenuActivity");
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity()).getString(R.string.title_menu_games)));

        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity()).getString(R.string.game_snake)));
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity()).getString(R.string.game_2048)));
    }

    private void checkMenuNavigationMagazinesCategories() {
        solo.assertCurrentActivity(
                "Error: Expected Magazines Categories menu activity",
                "MagazinesCategoriesMenuActivityShortcuts");
        assertTrue(solo.searchText(((DontPanicActivity) solo
                .getCurrentActivity())
                .getString(R.string.title_menu_magazines_categories)));
    }

    private void checkMenuNavigationMagazines() {
        solo.assertCurrentActivity("Error: Expected Magazines menu activity",
                "MagazinesMenuActivityShortcuts");
        assertTrue(solo
                .searchText(((DontPanicActivity) solo.getCurrentActivity())
                        .getString(R.string.title_menu_magazines)));
    }

    private void checkMenuNavigationCodeAddition() {
        solo.assertCurrentActivity("Error: Expected Code addition activity",
                "CodeAdditionActivity");
    }

    private void checkMenuNavigationCodeDeletion() {
        solo.assertCurrentActivity("Error: Expected Code deletion activity",
                "CodeDeletionActivity");
    }

    private void checkMainWindow() {
        solo.assertCurrentActivity("Error: Expected Main activity",
                "MainWindowActivity");
    }

    /**
     * Go through all the menus back and forth, testing that navigation works
     * properly and all the texts needed appear.
     */
    public void testMenuNavigation() throws Throwable {
        checkMainWindow();

        final boolean command[] = new boolean[4];
        makeCommand(true, true, false, false, command); // new shortcut

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationShortcutTypes();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationPhone();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeAddition();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationPhone();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationShortcutTypes();
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationMusic();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationMusicAlbums();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeAddition();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationMusicAlbums();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationMusic();
        solo.clickOnView(solo.getView(R.id.BlueButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationMusicArtists();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeAddition();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationMusicArtists();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationMusic();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationShortcutTypes();
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationGames();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeAddition();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationGames();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationShortcutTypes();
        solo.clickOnView(solo.getView(R.id.YellowButton));
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationMagazinesCategories();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationMagazines();
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeAddition();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationMagazines();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationMagazinesCategories();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMenuNavigationShortcutTypes();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMainWindow();

        makeCommand(false, false, true, true, command); // delete shortcut

        runTestOnUiThread(new Runnable() {
            @Override
            public void run() {
                ((DontPanicActivity) solo.getCurrentActivity())
                        .enterCode(command.clone());
            }
        });
        solo.clickOnView(solo.getView(R.id.GreenButton));
        checkMenuNavigationCodeDeletion();
        solo.clickOnView(solo.getView(R.id.RedButton));
        checkMainWindow();
    }

}
