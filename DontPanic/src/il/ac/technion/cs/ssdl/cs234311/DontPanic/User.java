package il.ac.technion.cs.ssdl.cs234311.DontPanic;

/**
 * @author Michael Varvaruk
 * @Email mvarvaruk@gmail.com
 * @Date 1/11/13
 * 
 *       represent the client using the app. server and client side
 */
public class User {

    public String email;
    public String username;
    public String super_email;
    public String super_phone;

    /**
     * constructor
     * 
     * @param email
     * @param username
     */
    public User(final String email, final String username,
            final String _super_email, final String _super_phone) {
        this.email = email;
        this.username = username;
        this.super_email = _super_email;
        this.super_phone = _super_phone;
    }

    /**
     * empty const for Gson
     */
    public User() { // no-arg cons for Gson
        this.email = null;
        this.username = null;
        this.super_email = null;
        this.super_phone = null;
    }
}
