package il.ac.technion.cs.ssdl.cs234311.DontPanic.Music;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import android.content.ContentResolver;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.MediaStore;
import android.provider.MediaStore.Audio.AlbumColumns;
import android.provider.MediaStore.MediaColumns;
import android.util.Pair;

public class MusicHelper {
    /**
     * 
     * @param context
     *            caller context
     * @param cursor
     *            cursor to fill.
     * @return fills the taken cursor with data of all songs , and returns it.
     */
    public static Cursor getAllCursor(final Context context, Cursor cursor) {
        final String where = null;
        final ContentResolver cr = context.getContentResolver();
        final Uri uri = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
        final String _id = BaseColumns._ID;
        final String album_name = AlbumColumns.ALBUM;
        final String artist = AlbumColumns.ARTIST;
        final String[] columns = { _id, album_name, artist };
        cursor = cr.query(uri, columns, where, null, null);
        return cursor;
    }

    /**
     * 
     * @param cursor
     *            cursor with albums.
     * @return Set of the albums names in the cursor.
     */
    public static Set<String> getAllAlbumsFromCursor(final Cursor cursor) {

        final HashSet<String> albums = new HashSet<String>();
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            final String isMusic = cursor.getString(cursor
                    .getColumnIndex("is_music"));
            final String album = cursor.getString(cursor
                    .getColumnIndex("album"));
            if (isMusic.equals("1"))
                albums.add(album);
        }
        return albums;
    }
/**
 * returns all the music in the cursor.
 * @param cursor : the cursor to get music from.
 * @return set of all the music in the cursor.
 */
    public static Set<String> getAllArtistsFromCursor(final Cursor cursor) {

        final HashSet<String> albums = new HashSet<String>();
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            final String isMusic = cursor.getString(cursor
                    .getColumnIndex("is_music"));
            final String album = cursor.getString(cursor
                    .getColumnIndex("artist"));
            if (isMusic.equals("1"))
                albums.add(album);
        }
        return albums;
    }

    /**
     * @param context
     *            caller context.
     * @param allSongs
     *            List to be updated with all the songs in the phone
     */
    public static void updateAllSongs(final Context context,
            final List<Pair<String, String>> allSongs) {
        final Cursor cursor = context.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null,
                MediaColumns.TITLE);
        getAllCursor(context, cursor);
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            final String songPath = cursor.getString(cursor
                    .getColumnIndex("_data"));
            String songName = cursor.getString(cursor
                    .getColumnIndex(MediaColumns.TITLE));
            final String isMusic = cursor.getString(cursor
                    .getColumnIndex("is_music"));
            final String artist = cursor.getString(cursor
                    .getColumnIndex("artist"));
            final String album = cursor.getString(cursor
                    .getColumnIndex("album"));
            songName = songName + ";" + artist + ";" + album;
            if (isMusic.equals("1"))
                allSongs.add(new Pair<String, String>(songName, songPath));
        }

    }

}
