package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DontPanicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;

import java.util.List;

import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

/**
 * An abstract class that represents a general menu activity.
 * 
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public abstract class MenuActivity extends DontPanicActivity {

    private Button redButton;
    private Button greenButton;
    private Button blueButton;
    private Button yellowButton;

    protected ListView listView;
    protected List<String> listItems;
    protected ArrayAdapter<String> adapter;

    protected TextView title;

    /**
     * @return a list of all items to show in the ListView (String Objects)
     */
    protected abstract List<String> getAllListItems();

    /**
     * @return the listView Object shown on screen.
     */
    protected abstract ListView getListView();

    /**
     * @return an array adapter of String Objects.
     */
    protected abstract ArrayAdapter<String> getAdapter();

    /**
     * @return the layout Id for the current activity
     */
    protected abstract int getLayoutId();

    /**
     * Sets the title of the activity
     */
    protected abstract void setTitle();

    /**
     * Opens next window according to the selected item in the ListView
     */
    protected abstract void openNextWindowWithSelection();

    @Override
    protected void initViews() {
        super.initViews();

        redButton = (Button) findViewById(R.id.RedButton);
        greenButton = (Button) findViewById(R.id.GreenButton);
        blueButton = (Button) findViewById(R.id.BlueButton);
        yellowButton = (Button) findViewById(R.id.YellowButton);

        listView = getListView();

        title = (TextView) findViewById(R.id.title);
    }

    @Override
    protected void initTouchListeners() {
        super.initTouchListeners();

        yellowButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.YELLOW));
        blueButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.BLUE));
        greenButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.GREEN));
        redButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.RED));
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutId());

        initViews();
        initTouchListeners();

        listItems = getAllListItems();
        adapter = getAdapter();
        listView.setAdapter(adapter);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

        listView.setItemChecked(0, true);
        setTitle();

    }

    /**
     * scroll up one item in the list view when the arrowDown is pressed
     */
    private void arrowUp() {
        final int index = listView.getCheckedItemPosition();

        if (index == 0) {
            listView.setItemChecked(listView.getCount() - 1, true);
            listView.smoothScrollToPositionFromTop(listView.getCount() - 1, 0,
                    0);
            return;
        }
        listView.setItemChecked(index - 1, true);
        listView.smoothScrollToPositionFromTop(index - 1,
                listView.getHeight() / 2);
    }

    /**
     * scroll down one item in the list view when the arrowDown is pressed
     */
    private void arrowDown() {
        final int index = listView.getCheckedItemPosition();

        if (index + 1 == listView.getCount()) {
            listView.setItemChecked(0, true);
            listView.smoothScrollToPositionFromTop(0, 0, 0);
            return;
        }
        listView.setItemChecked(index + 1, true);
        listView.smoothScrollToPositionFromTop(index + 1,
                listView.getHeight() / 2);
    }

    /**
     * all the operations supported in this activity
     */
    private enum Operation {
        ENTER, BACK, ARROW_UP, ARROW_DOWN, PANIC, OTHER_CODE
    }

    /**
     * matching input code to OPERATIONS constant
     * 
     * @param code
     *            - boolean array that represents the buttons pressed or the
     *            array given by the enterCode method
     * @return - the Operation constant that is matching the given input
     * @see DontPanicActivity.enterCode
     */
    private Operation getOperationFromCode(final boolean[] code) {
        final boolean green_pressed = code[ColorArrayLocations.GREEN.ordinal()];
        final boolean yellow_pressed = code[ColorArrayLocations.YELLOW
                .ordinal()];
        final boolean red_pressed = code[ColorArrayLocations.RED.ordinal()];
        final boolean blue_pressed = code[ColorArrayLocations.BLUE.ordinal()];

        // enter pressed
        if (green_pressed && !(blue_pressed || red_pressed || yellow_pressed))
            return Operation.ENTER;

        // clear/back pressed
        if (red_pressed && !(blue_pressed || green_pressed || yellow_pressed))
            return Operation.BACK;

        // arrow up pressed
        if (blue_pressed && !(green_pressed || red_pressed || yellow_pressed))
            return Operation.ARROW_UP;

        // arrow down pressed
        if (yellow_pressed && !(blue_pressed || red_pressed || green_pressed))
            return Operation.ARROW_DOWN;

        if (red_pressed && blue_pressed && green_pressed && yellow_pressed)
            return Operation.PANIC;

        return Operation.OTHER_CODE;
    }

    @Override
    public void enterCode(final boolean[] code) {

        switch (getOperationFromCode(code)) {
        case OTHER_CODE:
            return;

        case BACK:
            finish();
            break;

        case ENTER:
            openNextWindowWithSelection();
            break;

        case ARROW_UP:
            arrowUp();
            break;

        case ARROW_DOWN:
            arrowDown();
            break;

        case PANIC:
            enterPanic(LoginActivity.dataManager);
            break;
        }
    }
}
