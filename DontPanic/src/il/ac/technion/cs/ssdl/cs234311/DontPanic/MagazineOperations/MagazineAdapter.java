package il.ac.technion.cs.ssdl.cs234311.DontPanic.MagazineOperations;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;

import java.util.List;

import android.app.Activity;
import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * An ArrayAdapter that contains Magazine objects.
 * 
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public class MagazineAdapter extends ArrayAdapter<Magazine> {

    private final Activity activity;
    private final List<Magazine> items;
    private final int row;
    private Magazine magazine;

    /**
     * A constructor for a ContactAdapter Object.
     * 
     * @param act
     *            - the current running activity
     * @param row
     *            - a row num in the ListView managed by the adapter
     * @param items
     *            - the data to manage
     * 
     */
    public MagazineAdapter(final Activity act, final int row,
            final List<Magazine> items) {
        super(act, row, items);

        this.activity = act;
        this.row = row;
        this.items = items;
    }

    @Override
    public View getView(final int position, final View convertView,
            final ViewGroup parent) {
        View view = convertView;
        ViewHolder holder;

        if (view != null)
            holder = (ViewHolder) view.getTag();
        else {
            final LayoutInflater inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            view = inflater.inflate(row, null);
            holder = new ViewHolder();
            holder.magazineName = (TextView) view
                    .findViewById(R.id.listItemText);
            view.setTag(holder);
        }

        if (items == null || position + 1 > items.size())
            return view;

        magazine = items.get(position);

        if (holder.magazineName != null && magazine.name.trim().length() > 0)
            holder.magazineName.setText(Html.fromHtml(magazine.name));

        return view;
    }

    /**
     * The view holder for a single row item in the ListView managed by the
     * adapter
     */
    public class ViewHolder {
        public TextView magazineName;
    }

}
