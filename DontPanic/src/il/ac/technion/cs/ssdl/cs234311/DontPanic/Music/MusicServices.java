package il.ac.technion.cs.ssdl.cs234311.DontPanic.Music;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.MusicActivity;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;

/**
 * 
 * @author aassi
 * 
 */
public class MusicServices {

    /**
     * returns all the Albums in the Phone.
     * 
     * @param context
     *            is the caller context.
     * @return List of all albums names.
     */
    public static List<String> GetAllAlbums(final Context context) {
        final Cursor cursor = context.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null,
                MediaColumns.TITLE);
        MusicHelper.getAllCursor(context, cursor);
        final ArrayList<String> albums = new ArrayList<String>(
                MusicHelper.getAllAlbumsFromCursor(cursor));

        cursor.close();
        return albums;
    }
    /**
     * returns all the Artists in the Phone.
     * 
     * @param context
     *            is the caller context.
     * @return List of all Artists names.
     */
    public static List<String> GetAllArtists(final Context context) {
        final Cursor cursor = context.getContentResolver().query(
                MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null, null,
                MediaColumns.TITLE);
        MusicHelper.getAllCursor(context, cursor);
        final ArrayList<String> artists = new ArrayList<String>(
                MusicHelper.getAllArtistsFromCursor(cursor));

        cursor.close();
        return artists;
    }

    /**
     * returns an intent to open the music Activity.
     * 
     * @param context
     *            the caller context.
     * @param AlbumName
     *            the song album which want to open the activity with.
     * @return intent to use for opening music activity.
     */
    public static Intent getMusicIntentWithAlbum(final Context context,
            final String AlbumName) {
        final Intent intent = new Intent(context, MusicActivity.class);
        intent.putExtra("Album_Name", AlbumName);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        return intent;
    }
    /**
     * returns an intent to open the music Activity.
     * 
     * @param context
     *            the caller context.
     * @param ArtistName
     *            the song ArtistName which want to open the activity with.
     * @return intent to use for opening music activity.
     */
    public static Intent getMusicIntentWithArtist(final Context context,
            final String ArtistName) {
        final Intent intent = new Intent(context, MusicActivity.class);
        intent.putExtra("Artist_Name", ArtistName);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        return intent;
    }

}
