/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DontPanicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.snake.SnakeView;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.TextView;

/**
 * Snake: a simple game that everyone can enjoy.
 * 
 * This is an implementation of the classic Game "Snake", in which you control a
 * serpent roaming around the garden looking for apples. Be careful, though,
 * because when you catch one, not only will you become longer, but you'll move
 * faster. Running into yourself or the walls will end the game.
 * 
 */

public class Snake extends DontPanicActivity {

    /**
     * Constants for desired direction of moving the snake
     */

    public static int MOVE_LEFT = 0;
    public static int MOVE_UP = 1;
    public static int MOVE_DOWN = 2;
    public static int MOVE_RIGHT = 3;

    private static String ICICLE_KEY = "snake-view";

    private SnakeView mSnakeView;

    public static final int SLOW = 1;
    public static final int NORMAL = 2;
    public static final int FAST = 3;
    public static final int FASTEST = 4;

    public static final int YES = 1;
    public static final int NO = 2;

    public static final int EASY = 1;
    public static final int MEDIUM = 2;
    public static final int HARD = 3;
    public static final int HARDEST = 4;
    public static final int PANIC = 5;

    @Override
    protected void onNewIntent(final Intent intent) {
        // TODO Auto-generated method stub
        super.onNewIntent(intent);
        if (intent.getExtras() == null)
            return;

        mSnakeView.clearTiles();
        final Button greenButton = (Button) findViewById(R.id.GreenButton);

        final String startString = getResources().getString(
                R.string.snake_start);

        greenButton.setText(startString);

        mSnakeView.setMode(SnakeView.READY);

        int gameSpeed = NORMAL, accelerateGame = YES, level = EASY;
        final Bundle extras = intent.getExtras();
        if (extras != null) {
            gameSpeed = extras.getInt("game speed");
            accelerateGame = extras.getInt("accelerate game");
            level = extras.getInt("level");
        }
        mSnakeView.setGameConfiguration(gameSpeed, accelerateGame, level);

        initShowText(gameSpeed, accelerateGame, level);

        mSnakeView.setRightButton(greenButton);

    }

    @Override
    public void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.snake_layout);

        final Button yellowButton = (Button) findViewById(R.id.YellowButton);
        final Button blueButton = (Button) findViewById(R.id.BlueButton);
        final Button greenButton = (Button) findViewById(R.id.GreenButton);
        final Button redButton = (Button) findViewById(R.id.RedButton);

        final String startString = getResources().getString(
                R.string.snake_start);

        yellowButton.setText("");
        blueButton.setText("");
        greenButton.setText(startString);
        redButton.setText("");

        yellowButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.YELLOW));
        blueButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.BLUE));
        greenButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.GREEN));
        redButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.RED));

        mSnakeView = (SnakeView) findViewById(R.id.snake);
        mSnakeView.setDependentViews((TextView) findViewById(R.id.text),
                findViewById(R.id.arrowContainer),
                findViewById(R.id.background));

        if (savedInstanceState == null)
            // We were just launched -- set up a new game
            mSnakeView.setMode(SnakeView.READY);
        else {
            // We are being restored
            final Bundle map = savedInstanceState.getBundle(ICICLE_KEY);
            if (map != null)
                mSnakeView.restoreState(map);
            else
                mSnakeView.setMode(SnakeView.PAUSE);
        }

        mSnakeView.setOnTouchListener(new OnTouchListener() {

            @Override
            public boolean onTouch(final View v, final MotionEvent event) {
                if (mSnakeView.getGameState() == SnakeView.RUNNING) {
                    // Normalize x,y between 0 and 1
                    final float x = event.getX() / v.getWidth();
                    final float y = event.getY() / v.getHeight();

                    // Direction will be [0,1,2,3] depending on quadrant
                    int direction = 0;
                    direction = x > y ? 1 : 0;
                    direction |= x > 1 - y ? 2 : 0;

                    // Direction is same as the quadrant which was clicked
                    mSnakeView.moveSnake(direction);

                } else
                    // If the game is not running then on touching any part of
                    // the screen
                    // we start the game by sending MOVE_RIGHT signal to
                    // SnakeView
                    mSnakeView.moveSnake(MOVE_RIGHT);
                return false;
            }
        });

        int gameSpeed = NORMAL, accelerateGame = YES, level = EASY;
        final Bundle extras = getIntent().getExtras();
        if (extras != null) {
            gameSpeed = extras.getInt("game speed");
            accelerateGame = extras.getInt("accelerate game");
            level = extras.getInt("level");
        }
        mSnakeView.setGameConfiguration(gameSpeed, accelerateGame, level);

        initShowText(gameSpeed, accelerateGame, level);

        mSnakeView.setRightButton(greenButton);

        Log.w("SnakeActivity", "onCreate");
    }

    /**
     * initShowText: update the configuration text at the top of the screen by
     * the parametres passed by configuration activity
     * 
     * @param gameSpeed
     *            : speed of the game - SLOW, NORMAL, FAST, FASTEST.
     * @param accelerateGame
     *            : if we needs acceleration the speed - YES, NO.
     * @param level
     *            : game level - EASY, MEDIUM, HARD, HARDEST, PANIC!.
     */
    private void initShowText(final int gameSpeed, final int accelerateGame,
            final int level) {
        final TextView speedText = (TextView) findViewById(R.id.snakeSpeedShowText);
        final TextView accText = (TextView) findViewById(R.id.snakeAccelerateShowText);
        final TextView scoreText = (TextView) findViewById(R.id.snakeScoreShowText);
        final TextView levelText = (TextView) findViewById(R.id.snakeLevelShowText);

        final String speedString = getResources().getString(
                R.string.snake_speed_show_text);
        final String levelString = getResources().getString(
                R.string.snake_level_show_text);
        final String accString = getResources().getString(
                R.string.snake_accelerate_show_text);
        final String scoreString = getResources().getString(
                R.string.snake_score_show_text);

        switch (gameSpeed) {
        case SLOW:
            final String slowString = getResources().getString(
                    R.string.snake_slow);
            speedText.setText(speedString + " " + slowString);
            break;
        case NORMAL:
            final String normalString = getResources().getString(
                    R.string.snake_normal);
            speedText.setText(speedString + " " + normalString);
            break;
        case FAST:
            final String fastString = getResources().getString(
                    R.string.snake_fast);
            speedText.setText(speedString + " " + fastString);
            break;
        case FASTEST:
            final String fastestString = getResources().getString(
                    R.string.snake_fastest);
            speedText.setText(speedString + " " + fastestString);
            break;
        default:
            break;
        }

        switch (level) {
        case EASY:
            final String easyString = getResources().getString(
                    R.string.snake_easy);
            levelText.setText(levelString + " " + easyString);
            break;
        case MEDIUM:
            final String mediumString = getResources().getString(
                    R.string.snake_medium);
            levelText.setText(levelString + " " + mediumString);
            break;
        case HARD:
            final String hardString = getResources().getString(
                    R.string.snake_hard);
            levelText.setText(levelString + " " + hardString);
            break;
        case HARDEST:
            final String hardestString = getResources().getString(
                    R.string.snake_hardest);
            levelText.setText(levelString + " " + hardestString);
            break;
        case PANIC:
            final String panicString = getResources().getString(
                    R.string.snake_panic);
            levelText.setText(levelString + " " + panicString);
            break;
        default:
            break;
        }

        if (accelerateGame == YES) {
            final String yesString = getResources().getString(
                    R.string.snake_yes);
            accText.setText(accString + " " + yesString);
        } else {// NO
            final String noString = getResources().getString(R.string.snake_no);
            accText.setText(accString + " " + noString);
        }
        scoreText.setText(scoreString + " " + "0");

        mSnakeView.setScoreTextView(scoreText);

    }

    @Override
    protected void onPause() {
        super.onPause();
        // Pause the game along with the activity
        if (mSnakeView.mMode == SnakeView.RUNNING)
            mSnakeView.setMode(SnakeView.PAUSE);
    }

    @Override
    public void onSaveInstanceState(final Bundle outState) {
        // Store the game state
        outState.putBundle(ICICLE_KEY, mSnakeView.saveState());
    }

    /**
     * Handles key events in the game. Update the direction our snake is
     * traveling based on the DPAD.
     * 
     */

    @Override
    public boolean onKeyDown(final int keyCode, final KeyEvent msg) {

        switch (keyCode) {
        case KeyEvent.KEYCODE_DPAD_UP:
            mSnakeView.moveSnake(MOVE_UP);
            break;
        case KeyEvent.KEYCODE_DPAD_RIGHT:
            mSnakeView.moveSnake(MOVE_RIGHT);
            break;
        case KeyEvent.KEYCODE_DPAD_DOWN:
            mSnakeView.moveSnake(MOVE_DOWN);
            break;
        case KeyEvent.KEYCODE_DPAD_LEFT:
            mSnakeView.moveSnake(MOVE_LEFT);
            break;
        }

        return super.onKeyDown(keyCode, msg);
    }

    /**
     * getSnakeIntent: create intent to start this activity with.
     * 
     * @param context
     *            : context to start the activity from.
     * @return intent to start this activity with.
     */
    static public Intent getSnakeIntent(final Context context) {
        final Intent intent = new Intent(context, ConfigurationGame.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        intent.addFlags(Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS);
        return intent;
    }

    @Override
    public void enterCode(final boolean[] code) {
        final boolean yellowPressed = code[ColorArrayLocations.YELLOW.ordinal()];
        final boolean bluePressed = code[ColorArrayLocations.BLUE.ordinal()];
        final boolean greenPressed = code[ColorArrayLocations.GREEN.ordinal()];
        final boolean redPressed = code[ColorArrayLocations.RED.ordinal()];

        if (!yellowPressed && !bluePressed && greenPressed && !redPressed)
            // right
            mSnakeView.moveSnake(MOVE_RIGHT);
        else if (!yellowPressed && !bluePressed && !greenPressed && redPressed)
            // left
            mSnakeView.moveSnake(MOVE_LEFT);
        else if (!yellowPressed && bluePressed && !greenPressed && !redPressed)
            // up
            mSnakeView.moveSnake(MOVE_UP);
        else if (yellowPressed && !bluePressed && !greenPressed && !redPressed)
            // down
            mSnakeView.moveSnake(MOVE_DOWN);
        else if (yellowPressed && bluePressed && !greenPressed && !redPressed) {
            // new game: back to configuration window
            final Intent intent = new Intent(Snake.this,
                    ConfigurationGame.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.putExtra("class_name", "SnakeActivity");
            startActivity(intent);
            finish();
        } else if (!yellowPressed && bluePressed && greenPressed && !redPressed) {
            // pause / continue
            if (mSnakeView.mMode != SnakeView.RUNNING
                    && mSnakeView.mMode != SnakeView.PAUSE)
                return; // nothing to do if we choose to pause and the game
                        // didn't start yet

            if (mSnakeView.mMode == SnakeView.RUNNING)
                mSnakeView.setMode(SnakeView.PAUSE);
            else { // PAUSE
                   // to let the user another "move delay" time when choose
                   // "continue"
                mSnakeView.updateTime();
                mSnakeView.setMode(SnakeView.RUNNING);
                mSnakeView.update();
            }

        } else if (yellowPressed && !bluePressed && !greenPressed && redPressed) {
            final Intent intent = new Intent(Snake.this,
                    MainWindowActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.putExtra("class_name", "SnakeActivity");
            startActivity(intent);
            return;

        } else if (yellowPressed && bluePressed && greenPressed && redPressed)
            // panic
            enterPanic(LoginActivity.dataManager);
    }

}
