package il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.LoginActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.MusicData;

import java.util.Date;

import android.content.Context;

import com.google.gson.Gson;

public class MusicServerMessages {

    static final String SERVER_URL = "http://yp7dontpanic.appspot.com/";
/**
 * sends the music information when music run to the server.
 * @param songName: the name of the running song
 * @param artist: the artist of the running song.
 * @param album: the album of the running song.
 * @param context: current run context.
 */
    public static void sendScore(final String songName, final String artist,
            final String album, final Context context) {

        final String[] packetFields = new String[5];
        final String[] musicInfo = new String[3];

        musicInfo[0] = songName;
        musicInfo[1] = artist;
        musicInfo[2] = album;
        final Date date = new Date();

        final MusicData musicData = new MusicData(
                LoginActivity.dataManager.getUser().email, date, musicInfo);

        packetFields[0] = "UploadStatData";
        packetFields[1] = "type";
        packetFields[2] = "music";
        packetFields[3] = "info";
        packetFields[4] = new Gson().toJson(musicData);

        new HttpPostPackets(context).execute(packetFields);

    }

}
