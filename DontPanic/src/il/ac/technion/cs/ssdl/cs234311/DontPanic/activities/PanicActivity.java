package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

public class PanicActivity extends Activity {
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Log.i("panic", "panic pressed");
        LoginActivity.dataManager.panicAlert();
        Toast.makeText(getApplicationContext(),
                getString(R.string.panic_sent_announcement), Toast.LENGTH_SHORT)
                .show();

        finish();
    }
}
