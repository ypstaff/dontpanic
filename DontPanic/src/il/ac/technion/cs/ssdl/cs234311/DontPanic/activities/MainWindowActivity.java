package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DefaultCodes;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.DontPanicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.CodeOperation;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.NoSuchOpException;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.classes.ActiveWindowShortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.classes.MyExpandableListAdapter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.classes.ShortcutDescription;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.ShortcutsParser;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.ShortcutsParser.WrongShortCutType;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection.HttpPostPackets;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.ClickData;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.gson.Gson;

@SuppressLint("NewApi")
public class MainWindowActivity extends DontPanicActivity {
    // the four buttons in the four_buttons.xml layout
    Button yellowButton;
    Button blueButton;
    Button greenButton;
    Button redButton;

    //
    ImageButton newButton;
    ImageButton deleteButton;

    // the word representing the shortcut code entered so far
    List<boolean[]> word = new ArrayList<boolean[]>();

    // main expandable list
    MyExpandableListAdapter expListAdapter;
    ExpandableListView expListView;

    List<String> listDataHeader;
    HashMap<String, List<Shortcut>> listDataChild;
    List<Shortcut> activeWindows = new ArrayList<Shortcut>();

    // other
    int listOffsetFromTop = 50;

    // the counter counts the number of back presses after the word get cleared
    private int exitCounter = 0;
    private boolean statisticsBackTag = true;

    @Override
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);

        final String class_name = intent.getStringExtra("class_name");

        if (class_name == null)
            LOG("class_name = null");
        else if (class_name.matches("MusicActivity")) {
            final String displayName = intent.getStringExtra("playing_music");
            final ActiveWindowShortcut s = new ActiveWindowShortcut(
                    CodeOperation.MUSIC_PLAYLIST, displayName);
            if (!activeWindows.contains(s))
                activeWindows.add(s);
            else
                activeWindows.set(activeWindows.indexOf(s), s);
        } else if (class_name != null && class_name.matches("SnakeActivity")) {
            final ActiveWindowShortcut s = new ActiveWindowShortcut(
                    CodeOperation.GAME_PLAY,
                    this.getString(R.string.game_snake));
            if (!activeWindows.contains(s))
                activeWindows.add(s);
            else {
                // TODO
            }
        } else if (class_name != null && class_name.matches("Game2048Activity")) {
            final ActiveWindowShortcut s = new ActiveWindowShortcut(
                    CodeOperation.GAME_2048, "2048");
            if (!activeWindows.contains(s))
                activeWindows.add(s);
            else {
                // TODO
            }
        } else {
            // TODO
        }
    }

    @Override
    protected synchronized void onResume() {
        super.onResume();
        updateUI();
        LOG("onResume()");
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        initTouchListeners();

        prepareListData();
        expListAdapter = new MyExpandableListAdapter(this, listDataHeader,
                listDataChild);
        expListView.setAdapter(expListAdapter);

        expListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        expListView.setItemChecked(1, true);

        // ListView Group click listener
        expListView.setOnGroupClickListener(new OnGroupClickListener() {
            @Override
            public boolean onGroupClick(final ExpandableListView parent,
                    final View v, final int groupPosition, final long id) {
                // do nothing
                return false;
            }
        });

        // ListView on child click listener
        expListView.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(final ExpandableListView parent,
                    final View v, final int groupPosition,
                    final int childPosition, final long id) {
                // do nothing
                return false;
            }
        });

        // updateUI();
    }

    @Override
    protected void initTouchListeners() {
        super.initTouchListeners();

        yellowButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.YELLOW));
        blueButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.BLUE));
        greenButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.GREEN));
        redButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.RED));

        newButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(final View v, final MotionEvent event) {
                final int action = event.getActionMasked();
                if (action == MotionEvent.ACTION_DOWN) {
                    // open the NewShortcutActivity when the new code pressed
                    final Intent intent = new Intent(MainWindowActivity.this,
                            ShortcutTypesActivity.class);
                    startActivity(intent);
                    word = new ArrayList<boolean[]>();
                }
                /*
                if (action == MotionEvent.ACTION_DOWN) {
                    Intent intent = new Intent(MainWindowActivity.this,
                            RssReaderActivity.class);
                    intent.putExtra("XML_LINK",
                            "http://rss.nytimes.com/services/xml/rss/nyt/HomePage.xml");
                    startActivity(intent);
                    // startNewShortcutActivity();
                }
                */
                return true;
            }
        });

        deleteButton.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(final View v, final MotionEvent event) {
                final int action = event.getActionMasked();
                if (action == MotionEvent.ACTION_DOWN) {
                    // open the Game2048Activity when delete code pressed
                    final Intent intent = new Intent(MainWindowActivity.this,
                            Game2048Activity.class);
                    startActivity(intent);
                }
                /*
                if (action == MotionEvent.ACTION_DOWN) {
                    // open the deleteShortcutActivity when delete code pressed
                    final Intent intent = new Intent(MainWindowActivity.this,
                            CodeDeletionActivity.class);
                    startActivity(intent);
                    word = new ArrayList<boolean[]>();
                }
                */
                return true;
            }
        });
    }

    @Override
    protected void initViews() {
        super.initViews();

        redButton = (Button) findViewById(R.id.RedButton);
        greenButton = (Button) findViewById(R.id.GreenButton);
        blueButton = (Button) findViewById(R.id.BlueButton);
        yellowButton = (Button) findViewById(R.id.YellowButton);

        newButton = (ImageButton) findViewById(R.id.newButton);
        deleteButton = (ImageButton) findViewById(R.id.deleteButton);

        expListView = (ExpandableListView) findViewById(R.id.mainListView);
    }

    /**
     * update the gui
     */
    private void updateUI() {
        updateRedButton();
        updateGreenButton();
        updateYellowButton();
        updateBlueButton();

        updateExpList();

        // update the graphical shorcut's description
        ShortcutDescription.updateDescriptions(this, word,
                R.id.listItemShortcutDescription);

        final int count = expListAdapter.getGroupCount();
        for (int position = 0; position < count; position++)
            expListView.expandGroup(position);

        expListView.setItemChecked(0, true);
        expListView.smoothScrollToPositionFromTop(0, 0);
    }

    /*
     * update the main expandable list data
     */
    private void updateExpList() {
        prepareListData();

        expListAdapter._listDataHeader = listDataHeader;
        expListAdapter._listDataChild = listDataChild;
        expListAdapter.currentWord = word;
        expListAdapter.notifyDataSetChanged();
    }

    /*
     * Preparing the main list data
     */
    private void prepareListData() {
        // TODO finish implementation
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<Shortcut>>();

        if (!word.isEmpty()) {
            // Adding child data
            listDataHeader.add("Suggested Shortcuts");

            // Adding child data
            final List<Shortcut> suggestedShortcuts = LoginActivity.dataManager
                    .getCodesStartingWith(word);

            listDataChild.put(listDataHeader.get(0), suggestedShortcuts);

        } else {
            // Adding child data
            listDataHeader.add(getString(R.string.active_windows));

            listDataHeader.add(getString(R.string.mini_apps));

            listDataHeader.add(getString(R.string.saved_shortcuts));

            final List<Shortcut> allShortcuts = LoginActivity.dataManager
                    .getCodesStartingWith(new ArrayList<boolean[]>());

            listDataChild.put(listDataHeader.get(0), activeWindows); // Header,
                                                                     // Child
                                                                     // data

            listDataChild.put(listDataHeader.get(1), DefaultCodes
                    .getAllMiniAppsShortcuts(getApplicationContext()));

            listDataChild.put(listDataHeader.get(2), allShortcuts);
        }
    }

    private void updateBlueButton() {
        /*
        blueButton.setText(getString(R.string.up));
        blueButton.setCompoundDrawablesWithIntrinsicBounds(0,
                R.drawable.ic_action_collapse, 0, 0);*/
    }

    private void updateYellowButton() {
        /*
        yellowButton.setText(getString(R.string.down));
        yellowButton.setCompoundDrawablesWithIntrinsicBounds(0, 
                R.drawable.ic_action_expand, 0, 0);*/
    }

    private void updateGreenButton() {

    }

    private void updateRedButton() {
        if (!word.isEmpty()) {
            redButton.setText(getString(R.string.clear));
            redButton.setCompoundDrawablesWithIntrinsicBounds(0,
                    R.drawable.ic_action_backspace, 0, 0);
        } else {
            redButton.setText(getString(R.string.exit_app));
            redButton.setCompoundDrawablesWithIntrinsicBounds(0,
                    R.drawable.ic_action_back, 0, 0);
        }
    }

    /**
     * all the operations supported in this activity
     */
    private enum OPERATIONS {
        ARROW_UP, ARROW_DOWN, ENTER, ClEAR_BACK, OTHER_CODE
        // NEW_CODE, DELETE_CODE, PANIC
    }

    /**
     * matching input code to OPERATIONS constant
     * 
     * @param code
     *            - boolean array that represent the buttons pressed or the
     *            array given by the enterCode method
     * @return - the OPERATIONS constant that is matching the given input
     * @see DontPanicActivity.enterCode
     */
    private static OPERATIONS getOperationFromCode(final boolean[] code) {
        final boolean green_pressed = code[ColorArrayLocations.GREEN.ordinal()];
        final boolean yellow_pressed = code[ColorArrayLocations.YELLOW
                .ordinal()];
        final boolean red_pressed = code[ColorArrayLocations.RED.ordinal()];
        final boolean blue_pressed = code[ColorArrayLocations.BLUE.ordinal()];

        // enter pressed
        if (green_pressed && !(blue_pressed || red_pressed || yellow_pressed))
            return OPERATIONS.ENTER;

        // clear/back pressed
        if (red_pressed && !(blue_pressed || green_pressed || yellow_pressed))
            return OPERATIONS.ClEAR_BACK;

        // arrow up pressed
        if (blue_pressed && !(green_pressed || red_pressed || yellow_pressed))
            return OPERATIONS.ARROW_UP;

        // arrow down pressed
        if (yellow_pressed && !(blue_pressed || red_pressed || green_pressed))
            return OPERATIONS.ARROW_DOWN;

        // else
        return OPERATIONS.OTHER_CODE;
    }

    @Override
    public void enterCode(final boolean[] code) {
        switch (getOperationFromCode(code)) {
        case ARROW_DOWN:
            exitCounter = 0;
            arrowDown();
            break;
        case ARROW_UP:
            exitCounter = 0;
            arrowUp();
            break;
        case ClEAR_BACK:
            clearBack();
            break;
        case ENTER:
            exitCounter = 0;
            enter();
            break;
        case OTHER_CODE:
            exitCounter = 0;
            otherWord(code);
            break;
        default:
            break;

        /*
        case DELETE_CODE:
            startDeleteShortcutActivity();
            break;
        case NEW_CODE:
            startNewShortcutActivity();
            break;
        case PANIC:
            enterPanic(LoginActivity.dataManager);
            break;
         */
        }
    }

    //

    /**
     * scroll up one item in the list view when the arrowDown is pressed
     */
    private void arrowUp() {
        int index = expListView.getCheckedItemPosition();
        if (index == 0)
            return;

        --index;
        expListView.setItemChecked(index, true);
        expListView.smoothScrollToPositionFromTop(index,
                expListView.getHeight() / 2);

        speakContentInIndex(index);
    }

    /**
     * scroll down one item in the list view when the arrowDown is pressed
     */
    private void arrowDown() {
        int index = expListView.getCheckedItemPosition();
        if (index + 1 == expListView.getCount()) {
            expListView.setItemChecked(0, true);
            expListView.smoothScrollToPositionFromTop(0, 0, 0);
            return;
        }

        ++index;
        expListView.setItemChecked(index, true);
        expListView.smoothScrollToPositionFromTop(index,
                expListView.getHeight() / 2);

        speakContentInIndex(index);
    }

    /**
     * execute the shortcut entered so far, when the enter code pressed
     */
    private void enter() {
        int index = expListView.getCheckedItemPosition();
        final Object item = expListView.getItemAtPosition(index);

        if (item instanceof Shortcut) {

            Intent intent;
            if (item instanceof ActiveWindowShortcut) {// active windows
                if (((ActiveWindowShortcut) item).op == CodeOperation.MUSIC_PLAYLIST)
                    intent = new Intent(MainWindowActivity.this,
                            MusicActivity.class);
                else if (((ActiveWindowShortcut) item).op == CodeOperation.GAME_PLAY)
                    intent = new Intent(MainWindowActivity.this, Snake.class);
                else if (((ActiveWindowShortcut) item).op == CodeOperation.GAME_2048)
                    intent = new Intent(MainWindowActivity.this,
                            Game2048Activity.class);
                else
                    return;

                intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
                startActivity(intent);
            } else
                // (item instanceof Shortcut) - saved shortcuts
                executeShortcut((Shortcut) item);

            return;
        }

        if (word.isEmpty())
            if (item instanceof String) {// group headers
                // TODO should be changed later
                if (((String) item).matches(getString(R.string.active_windows)))
                    index = 0;
                if (((String) item).matches(getString(R.string.mini_apps)))
                    index = 1;
                if (((String) item)
                        .matches(getString(R.string.saved_shortcuts)))
                    index = 2;

                if (expListView.isGroupExpanded(index))
                    expListView.collapseGroup(index);
                else
                    expListView.expandGroup(index);

                return;
            }

        Shortcut shortcut;
        try {
            shortcut = LoginActivity.dataManager.getShortcut(word);
            sendStatistics(ShortcutTranslator.word2code(word),
                    ShortcutLetter.FOUR);
        } catch (final IllegalShortcutLetter e) {
            informUserWithToast(getString(R.string.shortcut_not_found));
            e.printStackTrace();
            return;
        }
        executeShortcut(shortcut);
    }

    private void sendStatistics(final List<ShortcutLetter> letterCode,
            final ShortcutLetter letter) {
        letterCode.add(letter);
        final ClickData data = new ClickData(
                LoginActivity.dataManager.getUser().email, new Date(),
                letterCode);

        final String[] packetFields = new String[5];
        packetFields[0] = "UploadStatData";
        packetFields[1] = "type";
        packetFields[2] = "click";
        packetFields[3] = "info";
        packetFields[4] = new Gson().toJson(data);

        new HttpPostPackets(this).execute(packetFields);
    }

    /**
     * clear the the last word entered when the clear code pressed. pressing
     * back three times will result closing the application.
     */
    private void clearBack() {
        if (!word.isEmpty()) {
            if (statisticsBackTag)
                try {
                    sendStatistics(ShortcutTranslator.word2code(word),
                            ShortcutLetter.ONE);
                } catch (final IllegalShortcutLetter e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
            statisticsBackTag = false;
            word.remove(word.size() - 1);
            updateUI();
        } else {
            ++exitCounter;
            if (exitCounter == 3) // exit the application
                this.sendBroadcast(new Intent("CLOSE_ALL"));
            else if (exitCounter == 2)
                // inform user before exiting
                informUserWithToast(getString(R.string.are_you_sure_you_want_to_exit_if_yes_press_back_again_to_exit));
            else
                informUserWithToast(getString(R.string.press_back_again_to_exit));
        }
    }

    /**
     * adding the letter (code) to the word
     * 
     * @param code
     */
    private void otherWord(final boolean[] code) {
        statisticsBackTag = true;
        try {
            ShortcutTranslator.array2letter(code);
        } catch (final IllegalShortcutLetter e) {
            informUserWithToast(getString(R.string.code_illegal));
            return;
        }

        word.add(code);
        updateUI();
    }

    /**
     * this method implement the shortcut execution according to its operation.
     * for now it support only phone call operations. when executed the user
     * will be informed via toast with message "operation done". if the shortcut
     * is null, the user will be informed via toast, that the combination he
     * entered is incorrect.
     * 
     * @param shortcut
     *            - the Shortcut to be executed
     */
    private void executeShortcut(final Shortcut shortcut) {
        if (shortcut != null) {
            Intent intent;
            try {
                intent = ShortcutsParser.getShortcutIntent(shortcut,
                        getApplicationContext());
                startActivity(intent);
                word = new ArrayList<boolean[]>();
            } catch (final WrongShortCutType e) {
                informUserWithToast(getString(R.string.shortcut_not_found));
                e.printStackTrace();
            }
        } else {
            informUserWithToast(getString(R.string.shortcut_not_found));
            updateUI();
        }
    }

    /**
     * speak the shortcut's display name that is placed in at the index position
     * in the main expandable list using the google tts engine. if no such
     * shortcut found, nothing will be spoke
     * 
     * @param index
     *            - the index of the item to be spoken in the expandable list
     */
    private void speakContentInIndex(final int index) {
        final Object item = expListView.getItemAtPosition(index);
        if (item instanceof Shortcut)
            try {
                speakText(((Shortcut) item).getDisplayName(this));
            } catch (final NoSuchOpException e) {
                e.printStackTrace();
            }
    }

    /**
     * display long duration toast with the given message
     * 
     * @param msg
     *            - the message that will be displayed by the toast
     */
    private void informUserWithToast(final String msg) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show();
    }

    /**
     * Logging data for debug.
     * 
     * @param msg
     *            - message to log.
     */
    private final static void LOG(final String msg) {
        Log.w("MainWindowActivity", msg);
    }
}
