package il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations;

import java.util.Locale;

/**
 * Enum for phone types.
 * 
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public enum PhoneType {
    HOME, WORK, MOBILE;

    @Override
    public String toString() {
        return name().toString().toLowerCase(Locale.US);
    }
}
