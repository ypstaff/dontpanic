/**
 * 
 */
package il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics;

import java.util.Collection;
import java.util.Date;

import com.googlecode.objectify.annotation.Entity;
import com.googlecode.objectify.annotation.Id;
import com.googlecode.objectify.annotation.Index;

/**
 * @author Michael Varvaruk
 * @email mvarvaruk@gmail.com
 * @date 11/5/14
 * 
 */
@Entity
public class RawData {

    @Id
    public Long id;
    @Index
    public String uid;
    public Date timestamp;

    /**
     * empty ctor for objectify
     */
    public RawData() {
        id = null;
        uid = null;
        timestamp = null;
    }

    /**
     * ctor
     */
    public RawData(final String _uid, final Date _time) {
        id = null;
        uid = _uid;
        timestamp = _time;
    }

    public String[] calculateStatistics(
            final Collection<? extends RawData> datasets) {
        throw new UnsupportedOperationException();
    }
}
