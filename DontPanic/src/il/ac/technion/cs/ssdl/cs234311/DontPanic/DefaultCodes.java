package il.ac.technion.cs.ssdl.cs234311.DontPanic;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.CodeOperation;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.TooLongWordException;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.ShortcutsParser;
import java.util.ArrayList;
import java.util.List;
import android.content.Context;

public class DefaultCodes {
    private final static int PANIC_CLICKS = 3;

    final static boolean[] createBoolLetter(final boolean red,
            final boolean yellow, final boolean blue, final boolean green) {
        final boolean[] code = new boolean[4];
        code[ShortcutTranslator.ColorArrayLocations.YELLOW.ordinal()] = yellow;
        code[ShortcutTranslator.ColorArrayLocations.BLUE.ordinal()] = blue;
        code[ShortcutTranslator.ColorArrayLocations.GREEN.ordinal()] = green;
        code[ShortcutTranslator.ColorArrayLocations.RED.ordinal()] = red;

        return code;
    }

    private final static List<ShortcutLetter> arrayToList(final boolean[] letter) {
        final List<boolean[]> bWord = new ArrayList<boolean[]>();
        bWord.add(letter);

        try {
            return ShortcutTranslator.word2code(bWord);
        } catch (final IllegalShortcutLetter e) {
            // should never happen since used only locally and with legal codes
            return null;
        }
    }

    private final static List<ShortcutLetter> createPanicCode() {
        final List<boolean[]> panic = new ArrayList<boolean[]>();

        for (int i = 0; i < PANIC_CLICKS; ++i)
            panic.add(createBoolLetter(true, false, false, true));

        try {
            return ShortcutTranslator.word2code(panic);
        } catch (final IllegalShortcutLetter e) {
            e.printStackTrace();
            return new ArrayList<ShortcutLetter>();
        }
    }

    final static List<ShortcutLetter> UP_BUTTON_CODE = arrayToList(createBoolLetter(
            false, false, true, false));

    final static List<ShortcutLetter> DOWN_BUTTON_CODE = arrayToList(createBoolLetter(
            false, true, false, false));

    final static List<ShortcutLetter> ADD_CODE_BUTTON_CODE = arrayToList(createBoolLetter(
            true, true, false, false));

    final static List<ShortcutLetter> DELETE_CODE_BUTTON_CODE = arrayToList(createBoolLetter(
            false, false, true, true));

    final static List<ShortcutLetter> OK_BUTTON_CODE = arrayToList(createBoolLetter(
            false, false, false, true));

    final static List<ShortcutLetter> CLEAR_BUTTON_CODE = arrayToList(createBoolLetter(
            true, false, false, false));

    final static List<ShortcutLetter> PANIC_BUTTON_CODE = createPanicCode();

    /**
     * default operations Codes.
     */
    public enum DeafaultOperations {
        UP_BUTTON(UP_BUTTON_CODE), DOWN_BUTTON(DOWN_BUTTON_CODE), NEW_CODE(
                ADD_CODE_BUTTON_CODE), DELETE_CODE(DELETE_CODE_BUTTON_CODE), OK_BUTTON(
                OK_BUTTON_CODE), CLEAR_BUTTON(CLEAR_BUTTON_CODE), PANIC_BUTTON(
                PANIC_BUTTON_CODE);

        Shortcut s;

        private DeafaultOperations(final List<ShortcutLetter> word) {
            try {
                s = Shortcut.newDefaultShortcut(word);
            } catch (final TooLongWordException e) {
                // shouldn't happen since all default codes are one lettered
                s = new Shortcut();
            }

        }

        public Shortcut getShortcut() {
            return s;
        }

    }

    private static Shortcut getMiniAppShortcut(final CodeOperation type,
            final String displayName, final String miniAppParam) {
        try {
            final List<String> props = new ArrayList<String>();
            props.add(Shortcut.PROPERTY_DISP_INDEX, displayName);
            props.add(Shortcut.PROPERTY_JSON_INDEX, miniAppParam);
            return new Shortcut(new User(), OK_BUTTON_CODE, type, props);
        } catch (final IllegalShortcutLetter e) {
            return new Shortcut();
        } catch (final TooLongWordException e) {
            return new Shortcut();
        }
    }

    public static Shortcut getMusicMiniAppShortcut(final Context appCon) {
        return getMiniAppShortcut(CodeOperation.MUSIC_PLAYLIST,
                appCon.getString(R.string.all_songs_dispaly_name),
                ShortcutsParser.ALL_SONGS_ALBUM_NAME);
    }

    public static Shortcut getSnakeMiniAppShortcut(final Context appCon) {
        return getMiniAppShortcut(CodeOperation.GAME_PLAY,
                appCon.getString(R.string.game_snake), "");
    }

    public static Shortcut getKeyboardMiniAppShortcut(final Context appCon) {
        return getMiniAppShortcut(CodeOperation.KEYBOARD_SHORTCUT,
                appCon.getString(R.string.text_messages_description), "");
    }

    public static Shortcut getMagazinesMiniAppShortcut(final Context appCon) {
        return getMiniAppShortcut(CodeOperation.MAGAZINE_LIBRARY,
                appCon.getString(R.string.magazines_from_a_library), "");
    }

    public static Shortcut getGame2048MiniAppShortcut(final Context appCon) {
        return getMiniAppShortcut(CodeOperation.GAME_2048,
                appCon.getString(R.string.game_2048), "");
    }

    public static List<Shortcut> getAllMiniAppsShortcuts(final Context appCon) {
        final List<Shortcut> res = new ArrayList<Shortcut>();

        res.add(getMusicMiniAppShortcut(appCon));
        res.add(getSnakeMiniAppShortcut(appCon));
        res.add(getGame2048MiniAppShortcut(appCon));
        res.add(getKeyboardMiniAppShortcut(appCon));
        res.add(getMagazinesMiniAppShortcut(appCon));
        return res;
    }

    private static Shortcut getDefaultShortcut(final CodeOperation type,
            final List<ShortcutLetter> code) {
        try {
            final List<String> props = new ArrayList<String>();
            props.add(Shortcut.PROPERTY_DISP_INDEX, "");
            props.add(Shortcut.PROPERTY_JSON_INDEX, "");
            return new Shortcut(new User(), code, type, props);
        } catch (final IllegalShortcutLetter e) {
            return new Shortcut();
        } catch (final TooLongWordException e) {
            return new Shortcut();
        }
    }

    public static Shortcut getDeleteShortcut() {
        return getDefaultShortcut(CodeOperation.DELETE_SHORTCUT,
                DELETE_CODE_BUTTON_CODE);
    }

    public static boolean isDeleteLetter(final List<boolean[]> word) {
        if (word.size() != 1)
            return false;

        try {
            return ShortcutTranslator.array2letter(word.get(0)).equals(
                    DELETE_CODE_BUTTON_CODE.get(0));
        } catch (final IllegalShortcutLetter e) {
            return false;
        }
    }

    public static Shortcut getAddShortcut() {
        return getDefaultShortcut(CodeOperation.ADD_SHORTCUT,
                ADD_CODE_BUTTON_CODE);
    }

    public static Shortcut getPanicShortcut() {
        return getDefaultShortcut(CodeOperation.PANIC_SHORTCUT,
                PANIC_BUTTON_CODE);
    }

    public static boolean isAddLetter(final List<boolean[]> word) {
        if (word.size() != 1)
            return false;

        try {
            return ShortcutTranslator.array2letter(word.get(0)).equals(
                    ADD_CODE_BUTTON_CODE.get(0));
        } catch (final IllegalShortcutLetter e) {
            return false;
        }
    }

    public static boolean isPanicPrefix(final List<boolean[]> word) {
        if (word.size() > PANIC_CLICKS)
            return false;

        List<ShortcutLetter> code;
        try {
            code = ShortcutTranslator.word2code(word);
        } catch (final IllegalShortcutLetter e) {
            return false;
        }
        for (final ShortcutLetter l : code)
            if (l != ShortcutLetter.FOUR_ONE)
                return false;

        return true;
    }
}
