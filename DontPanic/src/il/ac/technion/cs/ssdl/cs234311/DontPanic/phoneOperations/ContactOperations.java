package il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.media.AudioManager;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.telephony.TelephonyManager;
import android.view.KeyEvent;

import com.android.internal.telephony.ITelephony;

/**
 * A service class that provides phone-related static methods.
 * 
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 * @date 17.11.13
 */
public class ContactOperations {

    /**
     * @param phones
     *            - a cursor that provides interface to go over all contacts
     * @param context
     *            - the current app context
     * 
     * @return An intent that will start a phone call.
     */
    public static Intent getCallIntentWithContact(final Contact contact) {
        final String phoneNumber = "tel:" + contact.phoneNo;
        final Intent intent = new Intent(Intent.ACTION_CALL,
                Uri.parse(phoneNumber));

        return intent;
    }

    /**
     * Switches the mic On/Off on an active call.
     * 
     * @param context
     *            - the current app context
     */
    public static void toggleMuteMic(final Context context) {
        final AudioManager audioManager = (AudioManager) context
                .getSystemService(Context.AUDIO_SERVICE);

        audioManager.setMode(AudioManager.MODE_IN_CALL);
        audioManager.setMicrophoneMute(!audioManager.isMicrophoneMute());
    }

    /**
     * switches the speaker On/Off on an active call.
     * 
     * @param context
     *            - the current app context
     */
    public static void toggleSpeakerPhone(final Context context) {
        final AudioManager audioManager = (AudioManager) context
                .getSystemService(Context.AUDIO_SERVICE);

        audioManager.setMode(AudioManager.MODE_IN_CALL);
        audioManager.setSpeakerphoneOn(!audioManager.isSpeakerphoneOn());
    }

    /**
     * Answers a pending incoming call, if there is no pending incoming call
     * does nothing.
     * 
     * @param context
     *            - the current app context
     */
    public static void answerCall(final Context context) {
        final Intent answer = new Intent(Intent.ACTION_MEDIA_BUTTON);
        answer.putExtra(Intent.EXTRA_KEY_EVENT, new KeyEvent(
                KeyEvent.ACTION_UP, KeyEvent.KEYCODE_HEADSETHOOK));

        context.sendOrderedBroadcast(answer, null);
    }

    /**
     * Ends an active ongoing call, if there is no active call does nothing.
     * 
     * @param context
     *            - the current app context
     */
    public static void endCall(final Context context) {
        final TelephonyManager telephonyManager = (TelephonyManager) context
                .getSystemService(Context.TELEPHONY_SERVICE);

        try {
            final Class<?> c = Class.forName(telephonyManager.getClass()
                    .getName());
            final Method m = c.getDeclaredMethod("getITelephony");

            m.setAccessible(true);
            final ITelephony telephonyService = (ITelephony) m
                    .invoke(telephonyManager);

            telephonyService.endCall();
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * @param phones
     *            - a cursor that provides interface to go over all contacts
     * @param context
     *            - the current app context
     * 
     * @return A list consisting of all contacts retrieved from device
     */
    public static List<Contact> getAllContacts(final Cursor phones,
            final Context context) {
        // create new list
        final List<Contact> list = new ArrayList<Contact>();

        // iterate over contacts and retrieve name/phoneNo/etc..
        while (phones.moveToNext()) {

            final String name = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME));

            final String phoneNumber = phones
                    .getString(phones
                            .getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));

            final int PHONE_TYPE = phones.getInt(phones
                    .getColumnIndex(Phone.TYPE));
            String type = "";

            switch (PHONE_TYPE) {
            case Phone.TYPE_HOME:
                // home number
                type = context.getString(R.string.phone_type_home);
                break;
            case Phone.TYPE_MOBILE:
                // mobile number
                type = context.getString(R.string.phone_type_mobile);
                break;
            case Phone.TYPE_WORK:
                // work(office) number
                type = context.getString(R.string.phone_type_work);
                break;
            }

            final String id = phones.getString(phones
                    .getColumnIndex(BaseColumns._ID));

            // create a new contact using the extracted data
            final Contact contact = new Contact(name, phoneNumber, type, id);

            // insert new contact into the list
            list.add(contact);
        }
        phones.close();

        return list;
    }
}
