package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import android.content.Intent;

/**
 * Responsible for showing a list of magazine categories in the magazines
 * mini-app.
 * 
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public class MagazinesCategoriesMenuActivityMiniApp extends
        MagazinesCategoriesMenuActivity {

    @Override
    protected void openNextWindowWithSelection() {
        if (getAdapter().getCount() == 0)
            return;

        final String selected = getAdapter().getItem(
                getListView().getCheckedItemPosition());

        final Intent intent = new Intent(
                MagazinesCategoriesMenuActivityMiniApp.this,
                MagazinesMenuActivityMiniApp.class);
        intent.putExtra("MAGAZINES_CATEGORY", selected);
        startActivity(intent);
    }

}
