package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.CodeOperation;

import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;

import android.content.Intent;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Responsible for showing a list of games to set a shortcut to.
 * 
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public class GamesMenuActivity extends MenuActivity {

    @Override
    protected List<String> getAllListItems() {
        final List<String> list = new ArrayList<String>();

        list.add(getString(R.string.game_snake));
        list.add(getString(R.string.game_2048));

        return list;
    }

    @Override
    protected ListView getListView() {
        return (ListView) findViewById(R.id.games_list);
    }

    @Override
    protected ArrayAdapter<String> getAdapter() {
        return new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_activated_1, listItems);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.games_menu;
    }

    @Override
    protected void openNextWindowWithSelection() {
        if (adapter.getCount() == 0)
            return;

        final String selected = adapter.getItem(0);
        final Intent intent = new Intent(GamesMenuActivity.this,
                CodeAdditionActivity.class);
        intent.putExtra("SHORTCUT_OBJ", new Gson().toJson(selected));
        if (selected.equals(getString(R.string.game_snake)))
            intent.putExtra("SHORTCUT_TYPE", CodeOperation.GAME_PLAY);
        else
            intent.putExtra("SHORTCUT_TYPE", CodeOperation.GAME_2048);

        startActivity(intent);
    }

    @Override
    protected void setTitle() {
        title.setText(getString(R.string.title_menu_games));
    }
}
