package il.ac.technion.cs.ssdl.cs234311.DontPanic.Music;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.MusicActivity;

import java.util.ArrayList;
import java.util.Random;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.Pair;

enum PlayListOptions {
    PLAY_STATUS, PAUSE_STATUS, STOP_STATUS
};

/**
 * 
 * @author aassi
 * @class this class is used to play a List of songs, and it is singleton.
 */
public class MediaPlayerClass implements OnCompletionListener {

    final int NEXT_VALUE = 1;
    final int PREV_VALUE = -1;
    private ArrayList<Pair<String, String>> songsList = new ArrayList<Pair<String, String>>();
    private int song_index = 0;
    private PlayListOptions song_status = PlayListOptions.STOP_STATUS;
    private MediaPlayer mediaPlayer;
    private MusicActivity owner = null;
    private static MediaPlayerClass media = null;
    boolean shuffle = false;

    /**
     * created new instance of the this singleton class.
     * 
     * @param list
     */
    public static void MediaPlayerCreate(
            final ArrayList<Pair<String, String>> list) {
        if (media == null)
            media = new MediaPlayerClass(list);
        else if (!list.equals(media.songsList)) {
            media.doStop();
            media = new MediaPlayerClass(list);
        }
    }

    /**
     * @return returns the instance of the singlton.
     */
    public static MediaPlayerClass MediaPlayerGetInstance() {
        return media;

    }

    public static void MediaPlayerDistroyInstance() {
        if (media == null)
            return;
        media.doStop();
        media = null;
    }

    /**
     * private constructor for the singleton;
     * 
     * @param list
     *            is a list of music to play.
     */
    private MediaPlayerClass(final ArrayList<Pair<String, String>> list) {
        mediaPlayer = new MediaPlayer();
        songsList = list;
    }

    /**
     * handles the inner index of the current playing song.
     * 
     * @param value
     *            if NEXT_VALUE +1 for the inner index if PREV_VALUE -1 for the
     *            inner index and it makes the list circuit.
     */
    private void setSongIndex(final int value) {
        // if +1 then next (need to return to first if got to the end).
        // if -1 then previous (need to go to last if got to the first).
        if (NEXT_VALUE == value) {
            if (1 + song_index == songsList.size())
                song_index = 0;
            else
                song_index++;
        } else if (PREV_VALUE == value) {
            if (song_index + PREV_VALUE == -1)
                song_index = songsList.size() - 1;
            else
                song_index--;
        } else
            return;
    }

    /**
     * sets the owner of the mediaClass to be the one who used it.
     * 
     * @param activiy
     *            is the music activity who opened it.
     */
    public void SetOwner(final MusicActivity activiy) {
        owner = activiy;
    }

    // =====================================================================

    /**
     * 
     * @return the current playing song , else it returns empty string.
     */
    public String getCurrentPlayingSong() {
        if (songsList.size() == 0)
            return "";
        return songsList.get(song_index).first;
    }

    /**
     * 
     * @return the index of the current playing song.
     */
    public int getCurrentSongIndex() {
        return song_index;
    }

    /**
     * plays the song in the path.
     * 
     * @param path
     *            is path of the song to play.
     */
    private void audioPlayer(final String path) {

        try {
            mediaPlayer.setDataSource(path);
            mediaPlayer.prepare();
            mediaPlayer.start();
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * plays the song which is in the place of the inner index.
     */
    public void doPlay() {
        if (songsList.size() == 0)
            return;
        if (owner != null) {
            owner.changeSongName();
            owner.changeListViewIndex(song_index);
            owner.updateShuffleView(shuffle);
            owner.updatePlayButton(true);
        }
        if (song_status == PlayListOptions.STOP_STATUS) {
            final Pair<String, String> current_song = songsList.get(song_index);
            final String song_path = current_song.second;
            audioPlayer(song_path);
            mediaPlayer.setOnCompletionListener(this);
        } else if (song_status == PlayListOptions.PAUSE_STATUS)
            mediaPlayer.start();
        else if (song_status == PlayListOptions.PLAY_STATUS)
            return;

        song_status = PlayListOptions.PLAY_STATUS;
    }

    /**
     * stops the playing song.
     */
    public void doStop() {
        if (songsList.size() == 0 || song_status == PlayListOptions.STOP_STATUS)
            return;
        try {
            mediaPlayer.stop();
            mediaPlayer = new MediaPlayer();
            song_status = PlayListOptions.STOP_STATUS;
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * pauses the current playing song.
     */
    public void doPause() {
        if (songsList.size() == 0)
            return;
        try {
            mediaPlayer.pause();
            song_status = PlayListOptions.PAUSE_STATUS;
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * plays the next song in the list .
     */
    public void doNext() {
        exec(NEXT_VALUE);
    }

    /**
     * plays the previous song in the list.
     */
    public void doPrev() {
        exec(PREV_VALUE);
    }

    private void exec(final int to) {
        if (songsList.size() == 0)
            return;
        if (shuffle)
            shuffle();
        else {
            setSongIndex(to);
            mediaPlayer.reset();
            song_status = PlayListOptions.STOP_STATUS;
            doPlay();
        }
    }
    /**
     * change the shuffle flag state when shuffle is clicked in GUI
     */
    public void setToShuffleRun() {
        shuffle = !shuffle;
    }

    /**
     * shuffles a new index and plays it.
     */
    public void shuffle() {
        if (songsList.size() == 0)
            return;
        final Random generator = new Random();
        song_index = generator.nextInt(songsList.size());
        mediaPlayer.reset();
        song_status = PlayListOptions.STOP_STATUS;
        doPlay();

    }

    /**
     * Handler which calls when the current song is ended.
     */
    @Override
    public void onCompletion(final MediaPlayer media) {
        doNext();
    }

}
