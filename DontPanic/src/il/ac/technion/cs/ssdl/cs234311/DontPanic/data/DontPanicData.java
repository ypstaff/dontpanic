package il.ac.technion.cs.ssdl.cs234311.DontPanic.data;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DefaultCodes;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.CodeOperation;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.TooLongWordException;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection.ServerMessages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ExecutionException;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

/**
 * A class to manage the Don't Panic application data. stores data on the fly in
 * a local repository and on the application server. In addition giving parsing
 * services for shortcuts.
 * 
 * @author Shachar Nudler
 * 
 */
public class DontPanicData {

    /**
     * The name of the private file containing all saved shortcuts by the user.
     */
    final static String CODES_FILE_NAME = "codes.xml";

    /**
     * The name of the private file containing data on the platform user.
     */
    final static String USER_FILE_NAME = "user.xml";

    Context appContext;

    XmlFileWriter xmlsWriter;

    CodesDictionary shortcuts;

    User user;

    /**
     * Logging data for debug.
     * 
     * @param msg
     *            - message to log.
     */
    private final static void LOG(final String msg) {
        Log.w("DontPaincData", msg);
    }

    public final class UserAlreadyRegisteredExeption extends Exception {
        private static final long serialVersionUID = 8848786437101038615L;
    }

    public final class UserNotRegisteredExeption extends Exception {
        private static final long serialVersionUID = -1313988104028704332L;
    }

    /**
     * check if the app is new on the platform or already used.
     * 
     * @param appContext
     *            - the application context. Used to read the application
     *            private files, if there are any.
     * @return - true if the application hasn't got it's basic data files (is
     *         new), false if the files exists (the application already ran on
     *         the platform)
     */
    public static boolean isNew(final Context appContext) {
        LOG("is new - start");
        final String[] fileList = appContext.fileList();
        for (final String f : fileList)
            LOG(f);
        return !Arrays.asList(fileList).contains(USER_FILE_NAME);
    }

    /**
     * check is a user with a given email address is already registered. do work
     * in front of the server. night take a few seconds.
     * 
     * @param email
     *            - the given email
     * @return
     */
    public static boolean isUserRegistered(final Context appContext,
            final String email) {
        return ServerMessages.loginUser(appContext, email) != null;
    }

    /**
     * thrown when application data is starting and the app state dosn't fit to
     * the called constructor
     * 
     * @author Shachar Nudler
     * 
     */
    public static class IsNewException extends Exception {
        private static final long serialVersionUID = -7004553647764572556L;
    }

    /**
     * don't use this c'tor any more. we now use only on c'tor that gets all
     * user data. <br>
     * C'tor used to create the data management on any operation but the first
     * application run. will check the app files for any saved codes, and user
     * data.
     * 
     * @param _appContext
     *            - the context of the app. used for private files use.
     * @param userName
     *            - the user name.
     * @param email
     *            - the user email, will search the user by this email on the
     *            server database.
     * @throws IsNewException
     *             - throws this exception if it IS the first application run
     *             and there is no saved data.
     */
    @Deprecated
    public DontPanicData(final Context _appContext) throws IsNewException {
        if (isNew(_appContext))
            throw new IsNewException();

        appContext = _appContext;

        user = XmlFileReader.readUserData(XmlFileReader.readSavedData(
                _appContext, USER_FILE_NAME));

        shortcuts = XmlFileReader.readAllCodes(XmlFileReader.readSavedData(
                _appContext, CODES_FILE_NAME));

        xmlsWriter = new XmlFileWriter(CODES_FILE_NAME, USER_FILE_NAME,
                _appContext);

    }

    /**
     * check if a user name of a user that tries to login is the same as the
     * user appears in the local application files.
     * 
     * @param _appContext
     *            - the context of the application.
     * @param userName
     *            - the user-name of the user trying to login
     * @return - true if matches. false otherwise.
     */
    public static boolean isSameUser(final Context _appContext,
            final String userName) {
        try {
            return userName.equals(getUserDataFromFiles(_appContext).username);
        } catch (final IsNewException e) {
            return false;
        }
    }

    /**
     * method to get the details of a user saved in the files of the application
     * 
     * @param _appContext
     *            - the application context
     * @return - the saved user.
     * @throws IsNewException
     *             - thrown if there is no user saved in the application data
     */
    public static User getUserDataFromFiles(final Context _appContext)
            throws IsNewException {
        if (isNew(_appContext))
            throw new IsNewException();
        return XmlFileReader.readUserData(XmlFileReader.readSavedData(
                _appContext, USER_FILE_NAME));
    }

    /**
     * read the user data from the files of the application. writes to the
     * fields user, shortcuts.
     * 
     * @param _appContext
     *            - the context of the application
     * @throws IsNewException
     *             - if there are no files of the application on the phone
     */
    private void getValuesFromFiles(final Context _appContext)
            throws IsNewException {
        if (isNew(_appContext))
            throw new IsNewException();

        user = getUserDataFromFiles(_appContext);

        shortcuts = XmlFileReader.readAllCodes(XmlFileReader.readSavedData(
                _appContext, CODES_FILE_NAME));
    }

    /**
     * initializing the field of the xml file writer.
     * 
     * @param _appContext
     *            - the context of the application
     * @throws IsNewException
     *             - if there are no files of the application on the phone
     */
    private void initializeFileWriter(final Context _appContext) {
        xmlsWriter = new XmlFileWriter(CODES_FILE_NAME, USER_FILE_NAME,
                _appContext);
    }

    private void saveUserDataToFiles() throws IOException {
        // starting an empty codes file
        xmlsWriter.startCodesFile(CodesXmlFormatter.startCodesFile());

        // starting user file
        xmlsWriter.regUser(user);

        // and now save all given data on the phone local repository.
        xmlsWriter.replaceFile(shortcuts);
    }

    /**
     * Login constructor. c'tor used to create the data management on the first
     * application run. Will check on the server to see if the user is
     * activating the app on a new platform. If so will download all user's
     * saved data and sync to it. Otherwise will create a new and empty database
     * on the platform. <br>
     * 
     * 
     * @param _appContext
     *            - the context of the app. used for private files use.
     * @param userName
     *            - the user name.
     * @param email
     *            - the user email, will search the user by this email on the
     *            server database.
     * @throws IOException
     *             - thrown if can't access to private file from the app
     *             context.
     * @throws UserNotRegisteredExeption
     * @throws IsNewException
     *             - thrown if the app thought that there is a user file but
     *             there wasn't
     * @throws ExecutionException
     *             - connectivity problem with the server
     * @throws InterruptedException
     *             - connectivity problem with the server
     */
    public DontPanicData(final Context _appContext, final String userName,
            final String email) throws IOException, UserNotRegisteredExeption,
            IsNewException {
        appContext = _appContext;
        initializeFileWriter(_appContext);

        if (!isNew(_appContext) && isSameUser(_appContext, userName))
            // user is saved in the local files so he can enter.
            getValuesFromFiles(_appContext);

        else {
            // sending user data to the server
            user = ServerMessages.loginUser(appContext, email);

            if (user == null)
                // server hasn't got user. throw error.
                throw new UserNotRegisteredExeption();

            // server has user. download all user's data from the server.
            shortcuts = ServerMessages.getAllShortcuts(appContext, user);

            saveUserDataToFiles();
        }
    }

    /**
     * we don't use this c'tor any more. we changed to the c'tor getting all the
     * needed user data <br>
     * Login constructor. c'tor used to create the data management on the first
     * application run. Will check on the server to see if the user is
     * activating the app on a new platform. <br>
     * this constructor will not download user data from the server. It was
     * created to add a spinner while doing this heavy operations. <br>
     * for initializing the dictionary see {@link BackgroundServerShortcuts} <br>
     * 
     * @param _appContext
     *            - the context of the app. used for private files use.
     * @param userName
     *            - the user name.
     * @param email
     *            - the user email, will search the user by this email on the
     *            server database.
     * @param n
     *            - a mock parameter do defer between this constructor to the
     *            login constructor that do the operations with the server.
     * @throws IOException
     *             - thrown if can't access to private file from the app
     *             context.
     * @throws UserNotRegisteredExeption
     * @throws ExecutionException
     *             - connectivity problem with the server
     * @throws InterruptedException
     *             - connectivity problem with the server
     */
    @Deprecated
    public DontPanicData(final Context _appContext, final String userName,
            final String email, final int n) throws IOException,
            UserNotRegisteredExeption {
        appContext = _appContext;

        if (!isNew(_appContext)) {
            user = XmlFileReader.readUserData(XmlFileReader.readSavedData(
                    _appContext, USER_FILE_NAME));
            if (user.email.equals(email)) {

                xmlsWriter = new XmlFileWriter(CODES_FILE_NAME, USER_FILE_NAME,
                        _appContext);

                return;
            }
        }

        xmlsWriter = new XmlFileWriter(CODES_FILE_NAME, USER_FILE_NAME,
                _appContext);

        // starting an empty codes file
        xmlsWriter.startCodesFile(CodesXmlFormatter.startCodesFile());

        user = ServerMessages.loginUser(appContext, email);

        if (user == null)
            // server hasn't got user. throw error.
            throw new UserNotRegisteredExeption();
    }

    /**
     * @author Shachar Nudler not using this class until the feature will be
     *         announced <br>
     *         a class to do the heavy network download of all the shortcuts as
     *         and the operation of saving the downloaded data as an background
     *         task. this is done so the GUI thread will be able to add a
     *         spinner over the operation. <br>
     *         to do so you should create the class with a given handler that
     *         will close the spinner when called
     *         {@link GetShortcutsPostExecuteHandler} <br>
     * 
     *         code example: <br>
     *         new {@link BackgroundServerShortcuts}(new myHandler extends
     *         {@link GetShortcutsPostExecuteHandler}{ <br>
     *         ____override <br>
     *         ____void postExecute(){ <br>
     *         ________my code to close the spinner <br>
     *         ____} <br>
     *         }.execute();
     * 
     * 
     */
    @Deprecated
    public class BackgroundServerShortcuts extends
            AsyncTask<Void, Void, Boolean> {

        GetShortcutsPostExecuteHandler h;

        /**
         * @author Shachar Nudler
         * 
         *         a handler class for executing spinner while doing heavy
         *         server operations.
         * 
         * @see BackgroundServerShortcuts
         * 
         */
        public abstract class GetShortcutsPostExecuteHandler {
            abstract void postExecute();
        }

        /**
         * the constructor of the backgroundGetShortcuts class.
         * 
         * @param _h
         *            - handler of type {@link GetShortcutsPostExecuteHandler}
         *            to close the spinner.
         */
        public BackgroundServerShortcuts(final GetShortcutsPostExecuteHandler _h) {
            h = _h;
        }

        @Override
        protected void onPostExecute(final Boolean result) {

            h.postExecute();

            super.onPostExecute(result);
        }

        @Override
        protected Boolean doInBackground(final Void... arg0) {

            try {
                // starting user file
                xmlsWriter.regUser(user);

                // server has user. download all user's data from the server.
                shortcuts = ServerMessages.getAllShortcuts(appContext, user);

                // and now save all given data on the phone local repository.
                xmlsWriter.replaceFile(shortcuts);
            } catch (final IOException e) {
                return Boolean.FALSE;
            }
            return Boolean.valueOf(shortcuts != null);
        }

    }

    /**
     * @author Shachar Nudler unused class. the class will be used when the
     *         feature will be announced <br>
     *         a class to do the heavy local read of all the saved shortcuts as
     *         a background task. this is done so the GUI thread will be able to
     *         add a spinner over the operation. <br>
     *         to do so you should create the class with a given handler that
     *         will close the spinner when called
     *         {@link GetShortcutsPostExecuteHandler} <br>
     * 
     *         code example: <br>
     *         new {@link BackgroundLocalShortcuts}(new myHandler extends
     *         {@link GetShortcutsPostExecuteHandler}{ <br>
     *         ____override <br>
     *         ____void postExecute(){ <br>
     *         ________my code to close the spinner <br>
     *         ____} <br>
     *         }.execute();
     * 
     * 
     */
    @Deprecated
    public class BackgroundLocalShortcuts extends BackgroundServerShortcuts {

        public BackgroundLocalShortcuts(final GetShortcutsPostExecuteHandler _h) {
            super(_h);
        }

        @Override
        protected Boolean doInBackground(final Void... arg0) {
            shortcuts = XmlFileReader.readAllCodes(XmlFileReader.readSavedData(
                    appContext, CODES_FILE_NAME));
            return Boolean.TRUE;
        }
    }

    /**
     * Registration constructor. c'tor used to create the data management on the
     * first application run. Will check on the server to see if the user is
     * activating the app on a new platform. If so will download all user's
     * saved data and sync to it. Otherwise will create a new and empty database
     * on the platform.
     * 
     * @param _appContext
     *            - the context of the app. used for private files use.
     * @param userName
     *            - the user name.
     * @param email
     *            - the user email, will search the user by this email on the
     *            server database.
     * @throws IOException
     *             - thrown if can't access to private file from the app
     *             context.
     * @throws UserAlreadyRegisteredExeption
     * @throws ExecutionException
     *             - connectivity problem with the server
     * @throws InterruptedException
     *             - connectivity problem with the server
     */
    public DontPanicData(final Context _appContext, final String userName,
            final String email, final String supervisor_email,
            final String supervisor_phone) throws IOException,
            UserAlreadyRegisteredExeption {

        appContext = _appContext;

        // sending user data to the server
        if (isUserRegistered(appContext, email))
            // server has user. throw error.
            throw new UserAlreadyRegisteredExeption();

        initializeFileWriter(_appContext);

        // initialize the user based on the given information
        user = new User(email, userName, supervisor_email, supervisor_phone);

        // Initialize empty shortcuts database
        shortcuts = new CodesDictionary();

        // a new user, so register it on the server.
        ServerMessages.regUser(appContext, user);

        // and now save all given data on the phone local repository.
        saveUserDataToFiles();

    }

    /**
     * now use only {@link DontPanicData.getCodesStartingWith} with an empty
     * list. <br>
     * use this to Iterate over all shortcuts. on each shortcut you can get its
     * data by {@link Shortcut} methods: <br>
     * 
     * @see #com - this get the name to diaply of the shortcut.
     * @see #com - this will return the arrays of the assigned shortcut.
     * @see #getContactFromShortcut(Shortcut) - this will return the contact
     *      assigned to the shortcut.
     * @return - All shortcuts to iterate over.
     */
    @Deprecated
    public List<Shortcut> getAllShortcuts() {
        return shortcuts.getAllShortcuts();
    }

    /**
     * Adds a short cut to the system. The shortcut code to be saved is given in
     * a two letter code.
     * 
     * @param word1
     *            - code's first letter.
     * @param word2
     *            - code's second letter.
     * @param propertyDisplayName
     *            - name to display in the shortcut name. The name will be the
     *            name corresponding to the shortcut op and then the property
     *            name.
     * @param theShortcutPropertyObject
     *            - the Object saved for the operation shortcut.
     * @param operationType
     *            - type of the operation
     * @return - true if the given shortcut was legal. false otherwise.
     * @throws IOException
     *             - thrown if the shortcut couldn't be written to files.
     * @throws TooLongWordException
     * @throws IllegalShortcutLetter
     */
    public boolean addShortcut(final List<ShortcutLetter> code2,
            final String propertyDisplayName,
            final Object theShortcutPropertyObject,
            final CodeOperation operationType) throws IOException {
        final ArrayList<String> properties = new ArrayList<String>();
        properties.add(propertyDisplayName);
        properties.add(new Gson().toJson(theShortcutPropertyObject));

        Shortcut code;
        try {
            code = new Shortcut(user, code2, operationType, properties);
        } catch (final IllegalShortcutLetter e) {
            // illegal letter, so can't add the code
            return false;
        } catch (final TooLongWordException e) {
            // illegal letter, so can't add the code
            return false;
        }

        final boolean addSuccess = shortcuts.addCode(code);

        if (addSuccess) {
            // if the adding succeeded add the shortcut to the file and server.
            xmlsWriter.replaceFile(shortcuts);
            ServerMessages.sendShortcut(code, appContext);
        }

        return addSuccess;

    }

    /**
     * check if a given shortcut can be added to the data. Use this before
     * adding a shortcut, to announce to user if the shortcut he asked for is
     * legal in the system.
     * 
     * @param word1
     *            - the first letter in the rquested code. must be a valid
     *            boolean array of size 4.
     * @param word2
     *            - the second letter on the requested code. must be a valid
     *            boolean array of size 4, even if the letter is not used!! if
     *            so, just give 4 booleans all false.
     * @return - true if the code can be added to the data, false if it blocks a
     *         shortcut already in the system (is one letter and there is a code
     *         starting with that letter) or the code is blocked by a one letter
     *         assigned shortcut (there is one lettered shortcut starting with
     *         the same letter as {@link #word1}, or the shortcut is already
     *         assigned to a different operation.
     */
    public boolean canAddShortcut(final List<boolean[]> word) {
        return shortcuts.isLleagal(word);
    }

    /**
     * deletes a shortcut from the system, corresponding to a given two letters
     * code.
     * 
     * @param word1
     *            - letter one.
     * @param word2
     *            - letter two.
     * @return - true if the code was deleted. false otherwise.
     * @throws IOException
     *             - throws exception if couldn't change saved data in file.
     * @throws IllegalShortcutLetter
     */
    public boolean deleteShortcut(final List<ShortcutLetter> code)
            throws IOException {

        int shortcutEncoding;
        shortcutEncoding = ShortcutTranslator.code2int(code);
        final boolean res = shortcuts.isremovable(shortcutEncoding);

        if (res) {
            // can delete the shortcut
            final Shortcut s = shortcuts.getShortcut(shortcutEncoding);

            // delete from the server
            ServerMessages.deleteShortcut(s, user, appContext);

            // delete from repository
            shortcuts.deleteCode(code);

            // update the saved file
            xmlsWriter.replaceFile(shortcuts);
        }
        return res;

    }

    /**
     * return a shortcut from the data base according to a given two letters
     * code.
     * 
     * @param word1
     *            - letter one
     * @param word2
     *            - letter two
     * @return - the shortcut with code fits to the two letters code. null if
     *         there is no such shortcut
     * @throws IllegalShortcutLetter
     */
    public Shortcut getShortcut(final List<boolean[]> word)
            throws IllegalShortcutLetter {
        return shortcuts.getShortcut(ShortcutTranslator
                .code2int(ShortcutTranslator.word2code(word)));
    }

    /**
     * sending a panic request to the server. the user will send an
     * automatically made email announcing he is in need, and will send it to
     * his email address. more features will be added later.
     */
    public void panicAlert() {
        ServerMessages.Panic(appContext, user);
    }

    /**
     * return a new user object
     * 
     * @author Or Maayan
     */
    public User getUser() {
        return new User(user.email, user.username, user.super_email,
                user.super_phone);
    }

    /**
     * method for live search. getting all the shortcuts starting with a given
     * prefix.
     * 
     * @param word
     *            - a list of the letters that are the prefix of the searched
     *            code.
     * @return - a list of all the shortcuts code that can be added to the
     *         dictionary. if the given letters aren't null, the list will
     *         contain any part of the available shortcuts starting with the
     *         given partial code
     * @throws IllegalShortcutLetter
     *             - thrown if one of the letters is illegal letter
     */
    public List<Shortcut> getCodesStartingWith(final List<boolean[]> word) {
        try {
            final List<Shortcut> l = shortcuts.codesStartingWith(word);

            Collections.sort(l, new Comparator<Shortcut>() {

                @Override
                public int compare(final Shortcut lhs, final Shortcut rhs) {
                    return ShortcutTranslator.ShortcutOperationTypeOrdinal(lhs)
                            - ShortcutTranslator
                                    .ShortcutOperationTypeOrdinal(rhs);
                }
            });

            if (DefaultCodes.isAddLetter(word))
                l.add(0, DefaultCodes.getAddShortcut());
            else if (DefaultCodes.isDeleteLetter(word))
                l.add(0, DefaultCodes.getDeleteShortcut());

            if (DefaultCodes.isPanicPrefix(word))
                l.add(0, DefaultCodes.getPanicShortcut());

            return l;
        } catch (final IllegalShortcutLetter e) {
            return new ArrayList<Shortcut>();
        }
    }

    /**
     * method for live search. getting all the shortcuts that can be deleted,
     * starting with a given prefix.
     * 
     * @param word
     *            - a list of the letters that are the prefix of the searched
     *            code.
     * @return - a list of all the shortcuts code that can be added to the
     *         dictionary. if the given letters aren't null, the list will
     *         contain any part of the available shortcuts starting with the
     *         given partial code
     * @throws IllegalShortcutLetter
     *             - thrown if one of the letters is illegal letter
     */
    public List<Shortcut> getCodesStartingWithForDeletion(
            final List<boolean[]> word) {
        try {
            return shortcuts.codesStartingWith(word);
        } catch (final IllegalShortcutLetter e) {
            return new ArrayList<Shortcut>();
        }
    }

    /**
     * return all the available shortcuts starting with a given prefix.
     * 
     * @param word
     * @return
     * @throws IllegalShortcutLetter
     */
    public List<Shortcut> getAvailabeShortcuts(final List<boolean[]> word) {
        try {
            final List<Shortcut> l = new ArrayList<Shortcut>();

            for (final List<ShortcutLetter> w : shortcuts
                    .getAllAvailabeShortcuts(word))
                l.add(new Shortcut(w));
            return l;
        } catch (final IllegalShortcutLetter e) {
            return new ArrayList<Shortcut>();
        }
    }

}
