package il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.googlecode.objectify.annotation.EntitySubclass;

@EntitySubclass(index = true)
public class ClickData extends RawData {

    public ArrayList<ShortcutLetter> wordSeq;

    /**
     * empty ctor for objectify
     */
    public ClickData() {
        super();

        wordSeq = null;
    }

    /**
     * ctor
     */
    public ClickData(final String _uid, final Date _time,
            final List<ShortcutLetter> _seq) {
        super(_uid, _time);

        wordSeq = new ArrayList<ShortcutLetter>(_seq);
    }

    public boolean isHit() {
        return wordSeq.get(wordSeq.size() - 1) == ShortcutLetter.FOUR;
    }

    private static String colorHistogram(final Set<ClickData> set) {
        final Map<ShortcutLetter, Integer> map = new HashMap<ShortcutLetter, Integer>();

        for (final ClickData d : set)
            for (final ShortcutLetter l : d.wordSeq)
                if (map.containsKey(l))
                    map.put(l, map.get(l) + 1);
                else
                    map.put(l, 1);

        String result = "";
        for (final Entry<ShortcutLetter, Integer> e : map.entrySet())
            result += e.getKey().name() + "," + e.getValue();

        return result;
    }

    @Override
    public String[] calculateStatistics(
            final Collection<? extends RawData> datasets) {

        System.out.println("size:" + datasets.size());

        final HashSet<ClickData> s = new HashSet<ClickData>();
        for (final RawData d : datasets)
            if (d instanceof ClickData)
                s.add((ClickData) d);

        int hit = 0;

        for (final ClickData d : s)
            if (d.isHit())
                hit++;

        final String[] result = new String[3];

        final double hit_rate = s.size() == 0 ? (double) hit * 100 / s.size()
                : 0;
        final double miss_rate = s.size() == 0 ? 100 - hit_rate : 0;

        result[0] = "hit:" + hit_rate;
        result[1] = "miss:" + miss_rate;
        result[2] = "histo:" + colorHistogram(s);

        return result;
    }
}
