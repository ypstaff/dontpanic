/*
 * Copyright (C) 2007 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package il.ac.technion.cs.ssdl.cs234311.DontPanic.snake;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.View;

/**
 * TileView: a View-variant designed for handling arrays of "icons" or other
 * drawables.
 * 
 */
public class TileView extends View {

    /**
     * Parameters controlling the size of the tiles and their range within view.
     * Width/Height are in pixels, and Drawables will be scaled to fit to these
     * dimensions. X/Y Tile Counts are the number of tiles that will be drawn.
     */

    protected static int mTileSize;

    protected static int mXTileCount;
    protected static int mYTileCount;

    private static int mXOffset;
    private static int mYOffset;

    public static final int EASY = 1;
    public static final int MEDIUM = 2;
    public static final int HARD = 3;
    public static final int HARDEST = 4;
    public static final int PANIC = 5;

    protected int level = EASY;

    private final Paint mPaint = new Paint();

    /**
     * A hash that maps integer handles specified by the subclasser to the
     * drawable that will be used for that reference
     */
    private Bitmap[] mTileArray;

    /**
     * A two-dimensional array of integers in which the number represents the
     * index of the tile that should be drawn at that locations
     */
    private int[][] mTileGrid;

    public TileView(final Context context, final AttributeSet attrs) {
        super(context, attrs);

        final TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.TileView);
        mTileSize = a.getDimensionPixelSize(R.styleable.TileView_tileSize, 14);

        a.recycle();
    }

    public TileView(final Context context, final AttributeSet attrs,
            final int defStyle) {
        super(context, attrs, defStyle);

        final TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.TileView);
        mTileSize = a.getDimensionPixelSize(R.styleable.TileView_tileSize, 14);

        a.recycle();

    }

    /**
     * Resets all tiles to 0 (empty)
     * 
     */
    public void clearTiles() {
        for (int x = 0; x < mXTileCount; x++)
            for (int y = 0; y < mYTileCount; y++)
                setTile(0, x, y);
    }

    /**
     * checks if te coordintae is on wall of the grid.
     * 
     * @param x
     * @param y
     * @return return true if yes, false otherwise.
     */
    protected boolean isWallCordinate(final int x, final int y) {
        boolean res = false;
        switch (level) {
        case EASY:
            res = false;
            break;
        case MEDIUM:
            res = isPlusGridCoordinate(x, y);
            break;
        case HARD:
            res = isHardGridCoordinate(x, y);
            break;
        case HARDEST:
            res = isHardestGridCoordinate(x, y);
            break;

        case PANIC:
            res = isPanicGridCoordinate(x, y);
            break;

        default:
            assert false; // grid not supported
        }

        return res;
    }

    /**
     * Medium Level, draws plus as medium level grid
     * 
     * @param x
     *            coordinate x pixel.
     * @param y
     *            coordinate y pixel.
     * @return true if is inside the shape, false otherwise.
     */
    private boolean isPlusGridCoordinate(final int x, final int y) {
        if (x == mXTileCount / 2 && y < 3 * mYTileCount / 4
                && y > mYTileCount / 4 || y == mYTileCount / 2
                && x < 3 * mXTileCount / 4 && x > mXTileCount / 4)
            return true;
        return false;
    }

    /**
     * Hard Level, draws hard level grid
     * 
     * @param x
     *            coordinate x pixel.
     * @param y
     *            coordinate y pixel.
     * @return true if is inside the shape, false otherwise.
     */
    private boolean isHardGridCoordinate(final int x, final int y) {
        if (x == mXTileCount / 2
                && (y < 8 * mYTileCount / 10 && y > 2 * mYTileCount / 10
                        || y < mYTileCount / 10 || y > 9 * mYTileCount / 10)
                || y == mYTileCount / 2
                && (x < 8 * mXTileCount / 10 && x > 2 * mXTileCount / 10
                        || x < mXTileCount / 10 || x > 9 * mXTileCount / 10))
            return true;
        return false;
    }

    /**
     * Hard Level, draws hard level grid
     * 
     * @param x
     *            coordinate x pixel.
     * @param y
     *            coordinate y pixel.
     * @return true if is inside the shape, false otherwise.
     */
    private boolean isHardestGridCoordinate(final int x, final int y) {
        if (x == y + (mXTileCount - mYTileCount) / 2
                && x < 8 * mXTileCount / 10 && x > 2 * mXTileCount / 10
                || x == 7 * mXTileCount / 10 && y < 4 * mYTileCount / 10
                || x == 3 * mXTileCount / 10 && y > 6 * mYTileCount / 10)
            return true;
        return false;
    }

    /**
     * Panic Level, draws Panic letters as Panic level grid
     * 
     * @param x
     *            coordinate x pixel.
     * @param y
     *            coordinate y pixel.
     * @return true if is inside the shape, false otherwise.
     */
    private boolean isPanicGridCoordinate(final int x, final int y) {
        // P A N I C
        return isPCoordinate(x, y) || isACoordinate(x, y)
                || isNCoordinate(x, y) || isICoordinate(x, y)
                || isCCoordinate(x, y);

    }

    /**
     * checks if pixel inside "P" letter.
     * 
     * @param x
     *            coordinate x pixel.
     * @param y
     *            coordinate y pixel.
     * @return true if is inside the shape, false otherwise.
     */
    private boolean isPCoordinate(final int x, final int y) {
        final int PLocX = mXTileCount / 6;
        final int PLocY = mYTileCount / 6;
        // drawing P
        if (x == PLocX
                && y == PLocY - 2
                || x == PLocX - 1
                && y == PLocY - 1 // || x == PLocX && y == PLocY - 1
                || x == PLocX + 1
                && y == PLocY - 1
                || x == PLocX - 1
                && y == PLocY // || x == PLocX && y == PLocY
                || x == PLocX + 1 && y == PLocY || x == PLocX - 1
                && y == PLocY + 1 || x == PLocX && y == PLocY + 1
                || x == PLocX - 1 && y == PLocY + 2 || x == PLocX - 1
                && y == PLocY + 3)
            return true;
        return false;
    }

    /**
     * checks if pixel inside "A" letter.
     * 
     * @param x
     *            coordinate x pixel.
     * @param y
     *            coordinate y pixel.
     * @return true if is inside the shape, false otherwise.
     */

    private boolean isACoordinate(final int x, final int y) {
        final int ALocX = mXTileCount / 3;
        final int ALocY = mYTileCount / 3;
        // drawing A
        if (x == ALocX
                && y == ALocY - 2
                || x == ALocX - 1
                && y == ALocY - 1 // || x == ALocX && y == ALocY - 1
                || x == ALocX + 1 && y == ALocY - 1 || x == ALocX - 2
                && y == ALocY || x == ALocX - 1 && y == ALocY || x == ALocX
                && y == ALocY || x == ALocX + 1 && y == ALocY || x == ALocX + 2
                && y == ALocY || x == ALocX - 2 && y == ALocY + 1
                || x == ALocX + 2 && y == ALocY + 1)
            return true;
        return false;
    }

    /**
     * checks if pixel inside "N" letter.
     * 
     * @param x
     *            coordinate x pixel.
     * @param y
     *            coordinate y pixel.
     * @return true if is inside the shape, false otherwise.
     */

    private boolean isNCoordinate(final int x, final int y) {
        final int NLocX = mXTileCount / 2;
        final int NLocY = mYTileCount / 2;
        // drawing N
        if (x == NLocX - 1 && y == NLocY || x == NLocX && y == NLocY + 1
                || x == NLocX - 2 && y == NLocY || x == NLocX - 2
                && y == NLocY - 1 || x == NLocX - 2 && y == NLocY + 1
                || x == NLocX - 2 && y == NLocY + 2 || x == NLocX + 1
                && y == NLocY || x == NLocX + 1 && y == NLocY - 1
                // || x==NLocX && y==NLocY || x==NLocX-1 && y== NLocY+1
                || x == NLocX + 1 && y == NLocY + 1 || x == NLocX + 1
                && y == NLocY + 2)
            return true;

        return false;
    }

    /**
     * checks if pixel inside "I" letter.
     * 
     * @param x
     *            coordinate x pixel.
     * @param y
     *            coordinate y pixel.
     * @return true if is inside the shape, false otherwise.
     */

    private boolean isICoordinate(final int x, final int y) {
        final int ILocX = 2 * mXTileCount / 3;
        final int ILocY = 2 * mYTileCount / 3;
        // drawing I
        if (x == ILocX - 1 && y == ILocY - 2 || x == ILocX && y == ILocY - 2
                || x == ILocX + 1 && y == ILocY - 2 || x == ILocX
                && y == ILocY - 1 || x == ILocX && y == ILocY || x == ILocX - 1
                && y == ILocY + 1 || x == ILocX && y == ILocY + 1
                || x == ILocX + 1 && y == ILocY + 1)
            return true;

        return false;
    }

    /**
     * checks if pixel inside "C" letter.
     * 
     * @param x
     *            coordinate x pixel.
     * @param y
     *            coordinate y pixel.
     * @return true if is inside the shape, false otherwise.
     */

    private boolean isCCoordinate(final int x, final int y) {
        final int CLocX = 5 * mXTileCount / 6;
        final int CLocY = 5 * mYTileCount / 6;
        // drawing C
        if (x == CLocX - 1 && y == CLocY - 1 || x == CLocX && y == CLocY - 1
                || x == CLocX + 1 && y == CLocY - 1 || x == CLocX - 2
                && y == CLocY || x == CLocX - 2 && y == CLocY + 1
                || x == CLocX - 1 && y == CLocY + 2 || x == CLocX
                && y == CLocY + 2 || x == CLocX + 1 && y == CLocY + 2)
            return true;

        return false;
    }

    /**
     * Function to set the specified Drawable as the tile for a particular
     * integer key.
     * 
     * @param key
     * @param tile
     */
    public void loadTile(final int key, final Drawable tile) {
        final Bitmap bitmap = Bitmap.createBitmap(mTileSize, mTileSize,
                Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bitmap);
        tile.setBounds(0, 0, mTileSize, mTileSize);
        tile.draw(canvas);

        mTileArray[key] = bitmap;
    }

    @Override
    public void onDraw(final Canvas canvas) {
        super.onDraw(canvas);
        for (int x = 0; x < mXTileCount; x += 1)
            for (int y = 0; y < mYTileCount; y += 1)
                if (mTileGrid[x][y] > 0)
                    canvas.drawBitmap(mTileArray[mTileGrid[x][y]], mXOffset + x
                            * mTileSize, mYOffset + y * mTileSize, mPaint);

    }

    /**
     * Rests the internal array of Bitmaps used for drawing tiles, and sets the
     * maximum index of tiles to be inserted
     * 
     * @param tilecount
     */

    public void resetTiles(final int tilecount) {
        mTileArray = new Bitmap[tilecount];
    }

    /**
     * Used to indicate that a particular tile (set with loadTile and referenced
     * by an integer) should be drawn at the given x/y coordinates during the
     * next invalidate/draw cycle.
     * 
     * @param tileindex
     * @param x
     * @param y
     */
    public void setTile(final int tileindex, final int x, final int y) {
        mTileGrid[x][y] = tileindex;
    }

    @Override
    protected void onSizeChanged(final int w, final int h, final int oldw,
            final int oldh) {
        mXTileCount = (int) Math.floor(w / mTileSize);
        mYTileCount = (int) Math.floor(h / mTileSize);

        mXOffset = (w - mTileSize * mXTileCount) / 2;
        mYOffset = (h - mTileSize * mYTileCount) / 2;

        mTileGrid = new int[mXTileCount][mYTileCount];
        clearTiles();
    }

}
