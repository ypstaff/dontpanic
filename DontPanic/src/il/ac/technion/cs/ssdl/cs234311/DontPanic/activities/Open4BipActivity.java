package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DontPanicActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.bluetooth.BluetoothBroadcastReceiver;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.widget.Toast;

/**
 * @author Shachar
 * 
 *         A class to open the 4 buttons keyboard application. This intent
 *         should signal the Blue-Tooth receiver that the control is transfered
 *         to the keyboard application and then starts it. this activity should
 *         close itself, so it won't be returned when the keyboard app will be
 *         closed, but the main window will.
 * 
 */

public class Open4BipActivity extends Activity {

    private boolean firstRun = true;

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        DontPanicActivity.sendBtLocal = false;
    }

    @Override
    protected void onResume() {
        super.onResume();

        if (firstRun) {
            final PackageManager m = getPackageManager();
            final Intent i = m
                    .getLaunchIntentForPackage("il.ac.technion.cs.ssdl.cs234311.yp09");
            if (i != null)
                startActivity(i);
            else {
                Toast.makeText(getApplicationContext(),
                        "please download 4Bip to activate this miniApp",
                        Toast.LENGTH_SHORT).show();
                finish();
            }
        } else {

            DontPanicActivity.sendBtLocal = true;

            finish();
        }

        firstRun = false;

    }

    @Override
    protected void onPause() {
        // TODO Auto-generated method stub
        super.onPause();
    }

    public static Byte convertCodeToByte(final boolean[] code) {
        byte b = 0;
        if (code[0])
            b = (byte) (b | BluetoothBroadcastReceiver.redButtonPushed);
        if (code[1])
            b = (byte) (b | BluetoothBroadcastReceiver.yellowButtonPushed);
        if (code[2])
            b = (byte) (b | BluetoothBroadcastReceiver.blueButtonPushed);
        if (code[3])
            b = (byte) (b | BluetoothBroadcastReceiver.greenButtonPushed);
        if (code[4])
            b = (byte) (b | BluetoothBroadcastReceiver.longPush);
        return b;
    }
}