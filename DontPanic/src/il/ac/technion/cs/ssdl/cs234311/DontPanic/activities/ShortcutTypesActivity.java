package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.NoSuchOpException;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.ShortcutTypes;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.ShortcutsParser;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.widget.ArrayAdapter;
import android.widget.ListView;

/**
 * Responsible for showing a list of shortcut types.
 * 
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public class ShortcutTypesActivity extends MenuActivity {

    @Override
    protected List<String> getAllListItems() {
        final List<String> list = new ArrayList<String>();

        for (final ShortcutTypes t : ShortcutTypes.values())
            try {
                list.add(ShortcutsParser.getOpTypeName(t,
                        getApplicationContext()));
            } catch (final NoSuchOpException e) {
                continue;
            }

        return list;
    }

    @Override
    protected ListView getListView() {
        return (ListView) findViewById(R.id.shortcut_types_list);
    }

    @Override
    protected void openNextWindowWithSelection() {
        if (adapter.getCount() == 0)
            return;
        final String selected = adapter.getItem(listView
                .getCheckedItemPosition());

        final Intent intent = new Intent();

        if (selected.equals(getString(R.string.shortcut_phone_call_type)))
            intent.setClass(ShortcutTypesActivity.this, PhoneMenuActivity.class);

        else if (selected.equals(getString(R.string.shortcut_music_type)))
            intent.setClass(ShortcutTypesActivity.this, MusicMenuActivity.class);

        else if (selected.equals(getString(R.string.shortcut_game_type)))
            intent.setClass(ShortcutTypesActivity.this, GamesMenuActivity.class);
        else if (selected.equals(getString(R.string.shortcut_magazine_type)))
            intent.setClass(ShortcutTypesActivity.this,
                    MagazinesCategoriesMenuActivityShortcuts.class);
        else
            return;

        startActivity(intent);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.shortcut_types;
    }

    @Override
    protected ArrayAdapter<String> getAdapter() {
        return new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_activated_1, listItems);
    }

    @Override
    protected void setTitle() {
        title.setText(getString(R.string.title_menu_shortcut_types));
    }

}
