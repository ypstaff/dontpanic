package il.ac.technion.cs.ssdl.cs234311.DontPanic.data;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;

import java.io.StringWriter;

import org.xmlpull.v1.XmlSerializer;

import android.util.Xml;

import com.google.gson.Gson;

/**
 * A class to create the needed XML format for saving the app data
 * 
 * @author Shachar Nudler
 * 
 */
public class CodesXmlFormatter {

    /**
     * XML tag for codes
     */
    public final static String CODE_TAG = "CODE_TAG";
    /**
     * XML root tag for codes file
     */
    public final static String CODES_ROOT_TAG = "CODES_ROOT_TAG";
    /**
     * XML tag for user
     */
    public final static String USER_TAG = "USER_TAG";

    private final static String XML_ENCODING = "UTF-8";

    public static String startCodesFile() {
        final XmlSerializer serializer = Xml.newSerializer();
        final StringWriter writer = new StringWriter();
        try {

            serializer.setOutput(writer);
            serializer.startDocument(XML_ENCODING, Boolean.valueOf(true));

            serializer.startTag("", CODES_ROOT_TAG);
            serializer.endTag("", CODES_ROOT_TAG);

            serializer.endDocument();
            return writer.toString();

        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * format all the user's codes. used after deleting a code, to save a new
     * updated file
     * 
     * @param dic
     *            - the user codes dictionary
     * @return - string to be saved in the file, matching the app XML format.
     */
    public static String FormatDictionary(final CodesDictionary dic) {
        final XmlSerializer serializer = Xml.newSerializer();
        final StringWriter writer = new StringWriter();
        try {

            serializer.setOutput(writer);
            serializer.startDocument(XML_ENCODING, Boolean.valueOf(true));

            serializer.startTag("", CODES_ROOT_TAG);

            for (final Shortcut code : dic.getAllShortcuts()) {
                serializer.startTag("", CODE_TAG);
                serializer.text(new Gson().toJson(code));
                serializer.endTag("", CODE_TAG);
            }

            serializer.endTag("", CODES_ROOT_TAG);

            serializer.endDocument();
            return writer.toString();

        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Formats a single code. Used to get a code for appending in the app XML
     * codes file.
     * 
     * @param code
     *            - the code to be formatted
     * @return string of the formatted code, matching the app XML format.
     */
    /*
    public static String FormatCode(Shortcut code) {
    XmlSerializer serializer = Xml.newSerializer();
    StringWriter writer = new StringWriter();
    try {

    	serializer.setOutput(writer);

    	serializer.startTag("", CODE_TAG);
    	serializer.text(new Gson().toJson(code));
    	serializer.endTag("", CODE_TAG);

    	serializer.endDocument();
    	return writer.toString();

    } catch (Exception e) {
    	throw new RuntimeException(e);
    }
    }*/

    /**
     * Formats the data on the user. Used to save data on a new user in the app.
     * 
     * @param user
     *            - the data on the user to be listed.
     * @return string of the formatted user, matching the app XML format.
     */
    public static String FormatUser(final User user) {
        final XmlSerializer serializer = Xml.newSerializer();
        final StringWriter writer = new StringWriter();
        try {

            serializer.setOutput(writer);
            serializer.startDocument(XML_ENCODING, Boolean.valueOf(true));

            serializer.startTag("", USER_TAG);
            serializer.text(new Gson().toJson(user));
            serializer.endTag("", USER_TAG);

            serializer.endDocument();
            return writer.toString();

        } catch (final Exception e) {
            throw new RuntimeException(e);
        }
    }

}
