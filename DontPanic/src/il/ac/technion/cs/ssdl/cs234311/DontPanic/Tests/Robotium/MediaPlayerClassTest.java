package il.ac.technion.cs.ssdl.cs234311.DontPanic.Tests.Robotium;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.Music.MediaPlayerClass;

import java.util.ArrayList;

import android.test.AndroidTestCase;
import android.util.Pair;

/**
 * 
 * @author Ameer Assi
 * @date 18.12.2013
 * @passed 05.04.2014
 */
public class MediaPlayerClassTest extends AndroidTestCase {
    ArrayList<Pair<String, String>> playList = new ArrayList<Pair<String, String>>();

    private final void initialize() {
        for (int i = 0; i < 10; i++)
            playList.add(new Pair<String, String>(String.valueOf(i), String
                    .valueOf(i)));
    }

    public final void testPlayEmptyList() {
        final ArrayList<Pair<String, String>> playList = new ArrayList<Pair<String, String>>();
        MediaPlayerClass.MediaPlayerCreate(playList);
        final MediaPlayerClass media = MediaPlayerClass
                .MediaPlayerGetInstance();
        media.doPlay();
        media.doNext();
        media.doPrev();
        media.doPause();
        media.doStop();
    }

    public final void testPlaydummyPlayLists() {
        initialize();
        MediaPlayerClass.MediaPlayerCreate(playList);
        final MediaPlayerClass media = MediaPlayerClass
                .MediaPlayerGetInstance();
        try {
            media.doPlay();
        } catch (final Exception e) {
        }
        String str = "";

        str = media.getCurrentPlayingSong();

        assertEquals(str, String.valueOf(0));
        try {
            media.doNext();
        } catch (final Exception e) {
        }
        str = media.getCurrentPlayingSong();
        assertEquals(str, String.valueOf(1));
        try {
            media.doPrev();
        } catch (final Exception e) {
        }
        str = media.getCurrentPlayingSong();
        assertEquals(str, String.valueOf(0));

    }

    public final void testPlayCircularPlayLists() {
        initialize();
        MediaPlayerClass.MediaPlayerCreate(playList);
        final MediaPlayerClass media = MediaPlayerClass
                .MediaPlayerGetInstance();
        try {
            media.doPlay();
        } catch (final Exception e) {
        }
        String str = "";

        str = media.getCurrentPlayingSong();

        assertEquals(str, String.valueOf(0));
        try {
            media.doPrev();
        } catch (final Exception e) {
        }
        str = media.getCurrentPlayingSong();
        assertEquals(str, String.valueOf(9));
        try {
            media.doPrev();
        } catch (final Exception e) {
        }
        str = media.getCurrentPlayingSong();
        assertEquals(str, String.valueOf(8));
        try {
            media.doNext();
        } catch (final Exception e) {
        }
        str = media.getCurrentPlayingSong();
        assertEquals(str, String.valueOf(9));
        try {
            media.doNext();
        } catch (final Exception e) {
        }
        str = media.getCurrentPlayingSong();
        assertEquals(str, String.valueOf(0));

    }

    public final void testDoubleCall() {
        initialize();
        MediaPlayerClass.MediaPlayerCreate(playList);
        final MediaPlayerClass media = MediaPlayerClass
                .MediaPlayerGetInstance();
        try {
            media.doPlay();
        } catch (final Exception e) {
        }
        try {
            media.doPlay();
        } catch (final Exception e) {
        }
        String str = "";

        str = media.getCurrentPlayingSong();

        assertEquals(str, String.valueOf(0));
        try {
            media.doStop();
        } catch (final Exception e) {
        }
        try {
            media.doStop();
        } catch (final Exception e) {
        }
        str = media.getCurrentPlayingSong();
        assertEquals(str, String.valueOf(0));
        try {
            media.doPause();
        } catch (final Exception e) {
        }
        try {
            media.doPause();
        } catch (final Exception e) {
        }
        str = media.getCurrentPlayingSong();
        assertEquals(str, String.valueOf(0));
    }

    public final void testIterateEmptyList() {
        playList = new ArrayList<Pair<String, String>>();
        MediaPlayerClass.MediaPlayerCreate(playList);
        final MediaPlayerClass media = MediaPlayerClass
                .MediaPlayerGetInstance();
        try {
            media.doPlay();
        } catch (final Exception e) {
        }
        String str = media.getCurrentPlayingSong();
        assertTrue(str == "");
        try {
            media.doNext();
        } catch (final Exception e) {
        }
        str = media.getCurrentPlayingSong();
        assertTrue(str == "");
        try {
            media.doPrev();
        } catch (final Exception e) {
        }
        str = media.getCurrentPlayingSong();
        assertTrue(str == "");
        try {
            media.doStop();
        } catch (final Exception e) {
        }
        str = media.getCurrentPlayingSong();
        assertTrue(str == "");
        try {
            media.doPause();
        } catch (final Exception e) {
        }
        str = media.getCurrentPlayingSong();
        assertTrue(str == "");

    }
}
