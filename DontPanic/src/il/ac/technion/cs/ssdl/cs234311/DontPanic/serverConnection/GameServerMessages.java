package il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.LoginActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.snake.SnakeView;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.SnakeData;

import java.util.Date;
import java.util.concurrent.ExecutionException;

import android.content.Context;

import com.google.gson.Gson;

public class GameServerMessages {

    static final String SERVER_URL = "http://yp7dontpanic.appspot.com/";
/**
 * this function is for sending score at the end of the game to the server.
 * @param mScore current score
 * @param level current level.
 * @param gameTime  game time for this game.
 * @param context game context.
 */
    public static void sendScore(final long mScore, final int level,
            final long gameTime, final Context context) {
        String levelName = null;
        switch (level) {

        case SnakeView.EASY:
            levelName = "Easy";
            break;
        case SnakeView.MEDIUM:
            levelName = "Medium";
            break;
        case SnakeView.HARD:
            levelName = "Hard";
            break;
        case SnakeView.HARDEST:
            levelName = "Hardest";
            break;
        case SnakeView.PANIC:
            levelName = "Panic";
            break;

        }

        final String[] packetFields = new String[5];
        final String[] snakeInfo = new String[3];

        final Long l_mScore = Long.valueOf(mScore);
        final Long l_gameTime = Long.valueOf(gameTime);

        snakeInfo[0] = l_mScore.toString();
        snakeInfo[1] = levelName;
        snakeInfo[2] = l_gameTime.toString();
        final Date date = new Date();

        final SnakeData snakeData = new SnakeData(
                LoginActivity.dataManager.getUser().email, date, snakeInfo);

        packetFields[0] = "UploadStatData";
        packetFields[1] = "type";
        packetFields[2] = "snake";
        packetFields[3] = "info";
        packetFields[4] = new Gson().toJson(snakeData);

        new HttpPostPackets(context).execute(packetFields);

    }
/**
 * this function is used to get the current high score for specific user.
 * @param context game context
 * @return : high score
 */
    public static long getHighScore(final Context context) {
        final String MESSAGE_TYPE = "Snake";
        final String GET_HIGH_SCORE = "High Score";
        final String highScore = null;

        final String[] packetFields = new String[3];

        packetFields[0] = MESSAGE_TYPE;
        packetFields[1] = GET_HIGH_SCORE;
        packetFields[2] = highScore;

        try {
            new HttpGetPackets(context).execute(packetFields).get();
        } catch (final InterruptedException e) {
            e.printStackTrace();
        } catch (final ExecutionException e) {
            e.printStackTrace();
        }

        return Long.parseLong(highScore);
    }

}
