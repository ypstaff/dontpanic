package il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics;

import java.util.Date;

import com.googlecode.objectify.annotation.EntitySubclass;

@EntitySubclass(index = true)
public class GameData extends RawData {

    public String[] gameInfo;

    public GameData(final String _uid, final Date _time, final String[] info) {
        super(_uid, _time);

        gameInfo = info.clone();
    }

    public GameData() {
        super();

        gameInfo = null;
    }
}
