package il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations;


/**
 * A class that represents a contact object.
 * 
 * @author michael leybovich
 * @email mishana4life@gmail.com
 * @date 7.11.13
 * 
 */
public class Contact {
    public final String name;
    public final String phoneNo;
    public final String type;
    public final String id;

    /**
     * An empty constructor for a Contact Object
     */
    public Contact() {
        this.name = "";
        this.phoneNo = "";
        this.type = "";
        this.id = "";
    }

    /**
     * A constructor for a Contact Object.
     * 
     * @param name
     *            - the name of the contact
     * @param phoneNo
     *            - the phone number of the contact
     * @param type
     *            - Home, Mobile etc.
     * @param id
     *            - a unique id of the contact
     * 
     */
    public Contact(final String name, final String phoneNo, final String type,
            final String id) {
        this.name = name;
        this.phoneNo = phoneNo;
        this.type = type;
        this.id = id;
    }
}
