package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.NoSuchOpException;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.classes.ShortcutsListAdapter;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;

/**
 * Responsible for deleting an existing shortcut.
 * 
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public class CodeDeletionActivity extends CodeOperationsActivity {

    /**
     * Update the list of matching codes depending on the entered code (as the
     * prefix)
     */
    private void updateMatchingShortcutsList() {
        listItems = new ArrayList<Shortcut>();
        stringToCode = new HashMap<String, List<ShortcutLetter>>();

        final List<Shortcut> shortcuts = LoginActivity.dataManager
                .getCodesStartingWithForDeletion(word);

        for (final Shortcut s : shortcuts)
            try {
                final String $ = s.getDisplayName(getApplicationContext())
                        + "   " + printWord(s.letteredCode);

                stringToCode.put($, s.letteredCode);
                listItems.add(s);
            } catch (final NoSuchOpException e) {
                continue;
            }
        
        adapter = new ShortcutsListAdapter(this,
                R.layout.list_row_small_highlighted, R.id.listItemText,
                R.id.listItemShortcutDescription, listItems, word);

        listView.setAdapter(adapter);
        listView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
    }

    /**
     * Deletes the passed shortcut
     * 
     * @param code
     *            - the shortcut code represented by a list of ShortcutLetter
     *            Objects
     */
    private void deleteShortcut(final List<ShortcutLetter> code) {
        try {
            if (LoginActivity.dataManager.deleteShortcut(code)) {
                showToast(getString(R.string.code_deleted));
                clearEnteredCode();
                openMainWindow();

            } else {
                showToast(getString(R.string.code_not_deleted));
                clearEnteredCode();
                updateUI();
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Deletes the shortcut from entered code
     */
    private void deleteShortcutFromEnteredCode() {
        try {
            deleteShortcut(ShortcutTranslator.word2code(word));
        } catch (final IllegalShortcutLetter e) {
            e.printStackTrace();
        }
    }

    /**
     * Deletes the Shortcut selected from the
     * "matching shortcuts list"
     */
    private void deleteShortcutFromList() {
        deleteShortcut(((Shortcut) listView.getItemAtPosition(listView
                .getCheckedItemPosition())).letteredCode);
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.code_deleting);

        initViews();
        initTouchListeners();

        setTitle();
        updateUI();
    }

    @Override
    protected void additionalUpdates() {
        updateMatchingShortcutsList();
    }

    @Override
    protected void setTitle() {
        title.setText(getString(R.string.title_menu_code_deletion));
    }

    @Override
    protected void openMainWindow() {
        final Intent intent = new Intent(CodeDeletionActivity.this,
                MainWindowActivity.class);

        // clear all the previous activities from the stack
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    protected void enter() {
        if (listView.getCheckedItemPosition() != AdapterView.INVALID_POSITION) {
            deleteShortcutFromList();
            return;
        }

        if (word.isEmpty()) {
            showToast(getString(R.string.code_nothing_entered));
            return;
        }

        deleteShortcutFromEnteredCode();
    }

    @Override
    protected void initAdditionalViews() {
        listView = (ListView) findViewById(R.id.shortcuts_matching_list);
    }
}
