package il.ac.technion.cs.ssdl.cs234311.DontPanic.data;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.CodeOperation;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.NoSuchOpException;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.ShortcutTypes;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.TooLongWordException;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.MagazineOperations.Magazine;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.MagazineOperations.MagazineServices;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Music.MusicServices;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.CodeDeletionActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.Game2048Activity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.PanicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.ShortcutTypesActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.Snake;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.Open4BipActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations.Contact;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations.ContactOperations;

import java.io.IOException;
import java.util.List;

import android.content.Context;
import android.content.Intent;

import com.google.gson.Gson;

public final class ShortcutsParser {

    final public static String ALL_SONGS_ALBUM_NAME = "__ALL_SONGS__";

    /**
     * thrown when there is a try to parse a shortcut from a different type of
     * the parsing try.
     * 
     * @author Shachar Nudler
     * 
     */
    public static class WrongShortCutType extends Exception {
        private static final long serialVersionUID = 6186474259446604358L;
    }

    /**
     * This exception is thrown when an album shortcut is parsed but the
     * shortcut is for playing all songs and not the album.
     * 
     * @author Shachar Nudler
     * 
     */
    public static class AllSongsShortcut extends Exception {
        private static final long serialVersionUID = -9057656375235033041L;
    }

    /**
     * return the intent related to a given shortcut.
     * 
     * @param s
     *            - the shortcut
     * @return the intent represented by the shortcut
     * @throws WrongShortCutType
     *             - never.
     */
    public static Intent getShortcutIntent(final Shortcut s,
            final Context _appCon) throws WrongShortCutType {
        switch (s.op) {
        case MUSIC_ARTIST:
            return MusicServices.getMusicIntentWithArtist(_appCon,
                    getArtistFromShortcut(s));
        case MUSIC_PLAYLIST:
            return MusicServices.getMusicIntentWithAlbum(_appCon,
                    getAlbumFromShortcut(s));
        case GAME_PLAY:
            return Snake.getSnakeIntent(_appCon);
        case GAME_2048:
            return new Intent(_appCon, Game2048Activity.class);
        case PHONE_CALL:
            return ContactOperations
                    .getCallIntentWithContact(getContactFromShortcut(s));
        case MAGAZINE_BROWSE:
            return MagazineServices.getRssReaderIntentWithMagazine(_appCon,
                    getMagazineFromShortcut(s));
        case MAGAZINE_LIBRARY:
            return MagazineServices.getMagazinesLibraryIntent(_appCon);
        case ADD_SHORTCUT:
            return new Intent(_appCon, ShortcutTypesActivity.class);
        case DELETE_SHORTCUT:
            return new Intent(_appCon, CodeDeletionActivity.class);
        case PANIC_SHORTCUT:
            return new Intent(_appCon, PanicActivity.class);
        case KEYBOARD_SHORTCUT:
            return new Intent(_appCon, Open4BipActivity.class);
            // return
            // _appCon.getPackageManager().getLaunchIntentForPackage("il.ac.technion.cs.ssdl.cs234311.yp09");
        default:
            return null;
        }
    }

    /**
     * Adds a short cut to play all the songs in the device to the system. The
     * shortcut code to be saved is given in a two letter code.
     * 
     * @param word1
     *            - code's first letter.
     * @param word2
     *            - code's second letter.
     * 
     * @return - true if the given shortcut was legal. false otherwise.
     * @throws IOException
     *             - thrown if the shortcut couldn't be written to files.
     * @throws TooLongWordException
     * @throws IllegalShortcutLetter
     */
    public static boolean saveAllSongsShortcut(final DontPanicData data,
            final List<boolean[]> word) throws IOException,
            IllegalShortcutLetter, TooLongWordException {
        return data.addShortcut(ShortcutTranslator.word2code(word),
                ALL_SONGS_ALBUM_NAME, ALL_SONGS_ALBUM_NAME,
                Shortcut.CodeOperation.MUSIC_PLAYLIST);
    }

    /**
     * parse a Music shortcut, and returns the Artist name. use this method
     * after getting a requested shortcut from the user, to parse the Shortcut
     * itself. prior to this method call check that the shortcut is indeed of
     * type {@link Shortcut.CodeOperation}.MUSIC_ARTIST.
     * 
     * @param shortcut
     *            - the given shortcut to parse
     * @return - the artist name saved in the shortcut.
     * @throws WrongShortCutType
     *             - Thrown if the shortcut is not of MUSIC_ARTIST type
     */
    public static String getArtistFromShortcut(final Shortcut shortcut)
            throws WrongShortCutType {

        if (shortcut.op != CodeOperation.MUSIC_ARTIST)
            throw new WrongShortCutType();

        final String artistName = new Gson().fromJson(
                shortcut.properties.get(Shortcut.PROPERTY_JSON_INDEX),
                String.class);

        return artistName;
    }

    /**
     * parse a Music shortcut, and returns the Album name. use this method after
     * getting a requested shortcut from the user, to parse the Shortcut itself.
     * prior to this method call check that the shortcut is indeed of type
     * {@link Shortcut.CodeOperation}.MUSIC_PLAYLIST.
     * 
     * @param shortcut
     *            - the given shortcut to parse
     * @return - the album name saved in the shortcut.
     * @throws WrongShortCutType
     *             - Thrown if the shortcut is not of MUSIC_PLAYLIST type
     */
    public static String getAlbumFromShortcut(final Shortcut shortcut)
            throws WrongShortCutType {

        if (shortcut.op != CodeOperation.MUSIC_PLAYLIST)
            throw new WrongShortCutType();

        final String albumName = new Gson().fromJson(
                shortcut.properties.get(Shortcut.PROPERTY_JSON_INDEX),
                String.class);

        return albumName;
    }

    /**
     * parse a contact shortcut, and returns the contact of the shortcut. use
     * this method after getting a requested shortcut from the user, to parse
     * the Contact itself. prior to this method call check that the shortcut is
     * indeed of type {@link Shortcut.CodeOperation}.PHONE_CALL.
     * 
     * @param shortcut
     *            - the given shortcut to parse
     * @return - the contact saved in the shortcut.
     * @throws WrongShortCutType
     *             - Thrown if the shortcut is not of PHONE_CALL type
     */
    public static Contact getContactFromShortcut(final Shortcut shortcut)
            throws WrongShortCutType {

        if (shortcut.op != CodeOperation.PHONE_CALL)
            throw new WrongShortCutType();

        return new Gson().fromJson(
                shortcut.properties.get(Shortcut.PROPERTY_JSON_INDEX),
                Contact.class);
    }

    public static Magazine getMagazineFromShortcut(final Shortcut shortcut)
            throws WrongShortCutType {

        if (shortcut.op != CodeOperation.MAGAZINE_BROWSE)
            throw new WrongShortCutType();

        return new Gson().fromJson(
                shortcut.properties.get(Shortcut.PROPERTY_JSON_INDEX),
                Magazine.class);
    }

    /**
     * Getting the name to be displayed for a specific shortcut type, in the
     * categories options.
     * 
     * @param op2
     *            - The shortcut type to get the display name for.
     * @return - The display name for the shortcut type.
     * @throws NoSuchOpException
     *             - thrown if the operation is of an illegal type or a default
     *             operation type.
     */
    public static String getOperationActionName(final CodeOperation op,
            final Context appContext) throws NoSuchOpException {
        switch (op) {
        case PHONE_CALL:
            return appContext.getString(R.string.shortcut_phone_call_operation);
        case MUSIC_PLAYLIST:
        case MUSIC_ARTIST:
            return appContext.getString(R.string.shortcut_music_operation);
        case GAME_PLAY:
            return appContext.getString(R.string.shortcut_game_operation);
        case GAME_2048:
            return appContext.getString(R.string.shortcut_game_2048_operation);
        case MAGAZINE_BROWSE:
            return appContext.getString(R.string.shortcut_magazine_operation);
        case ADD_SHORTCUT:
            return appContext.getString(R.string.shortcut_add_operation);
        case DELETE_SHORTCUT:
            return appContext.getString(R.string.shortcut_delete_operation);
        case PANIC_SHORTCUT:
            return appContext.getString(R.string.shortcut_panic_operation);
        case DEFAULT_OPERTAIONS:
            return "";
        case KEYBOARD_SHORTCUT:
            return appContext.getString(R.string.keyboard_operation);
        case MAGAZINE_LIBRARY:
            return appContext.getString(R.string.magazine_operation);
        default:
            throw new NoSuchOpException();
        }
    }

    /**
     * Getting the name to be displayed for a specific shortcut type, in the
     * shortcuts list - the operation name.
     * 
     * @param t
     *            - The shortcut type to get the display name for.
     * @return - The display name for the shortcut type.
     * @throws NoSuchOpException
     *             - thrown if the operation is of an illegal type or a default
     *             operation type.
     */
    public static String getOpTypeName(final ShortcutTypes t,
            final Context appContext) throws NoSuchOpException {
        switch (t) {
        case PHONE:
            return appContext.getString(R.string.shortcut_phone_call_type);
        case MUSIC:
            return appContext.getString(R.string.shortcut_music_type);
        case GAMES:
            return appContext.getString(R.string.shortcut_game_type);
        case MAGAZINES:
            return appContext.getString(R.string.shortcut_magazine_type);

        default:
            throw new NoSuchOpException();
        }
    }

}
