package il.ac.technion.cs.ssdl.cs234311.DontPanic.phoneOperations;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.Music.MediaPlayerClass;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.activities.CallerActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.WindowManager;

/**
 * A broadcast receiver that opens Caller Activity on outgoing call.
 * 
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 * @date 3.1.14
 */
public class OutgoingCallReceiver extends BroadcastReceiver {
    private static final String TAG = "Phone call";
    private String number;
    private Context context;

    /**
     * A java runnable that initiates and starts a caller activity on outgoing
     * call.
     */
    private class CallerActivation implements Runnable {

        @Override
        public void run() {
            try {
                synchronized (this) {
                    Log.d(TAG, "Waiting for 1 sec ");
                    this.wait(1000);
                }
            } catch (final Exception e) {
                Log.d(TAG, "Exception while waiting !!");
                e.printStackTrace();
            } finally {

                final MediaPlayerClass player = MediaPlayerClass
                        .MediaPlayerGetInstance();
                if (player != null)
                    player.doPause();

                final Intent caller = new Intent(context, CallerActivity.class);
                caller.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                caller.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                caller.addFlags(WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED);
                caller.addFlags(WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

                if (number != null)
                    caller.putExtra("PHONE_NUMBER", number);

                caller.putExtra("IS_INCOMING_CALL", false);
                context.startActivity(caller);
            }
        }
    }

    @Override
    public void onReceive(final Context context, final Intent intent) {

        if (!intent.getAction().equals(
                "android.intent.action.NEW_OUTGOING_CALL"))
            return;// shouldn't happen because of the intent-filter in the
                   // manifest file

        this.context = context;
        number = intent.getStringExtra(Intent.EXTRA_PHONE_NUMBER);

        new Thread(new CallerActivation()).start();

        return;
    }
}