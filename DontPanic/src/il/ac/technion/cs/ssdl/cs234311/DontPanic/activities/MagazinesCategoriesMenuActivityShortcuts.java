package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import android.content.Intent;

/**
 * Responsible for showing a list of magazine articles in the Shortcuts menu.
 * 
 * @author michael leybovich
 * @mail mishana4life@gmail.com
 */
public class MagazinesCategoriesMenuActivityShortcuts extends
        MagazinesCategoriesMenuActivity {

    @Override
    protected void openNextWindowWithSelection() {
        if (getAdapter().getCount() == 0)
            return;

        final String selected = getAdapter().getItem(
                getListView().getCheckedItemPosition());

        final Intent intent = new Intent(
                MagazinesCategoriesMenuActivityShortcuts.this,
                MagazinesMenuActivityShortcuts.class);
        intent.putExtra("MAGAZINES_CATEGORY", selected);
        startActivity(intent);
    }

}
