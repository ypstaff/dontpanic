package il.ac.technion.cs.ssdl.cs234311.DontPanic.activities;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.DontPanicActivity;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ColorArrayLocations;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Music.MediaPlayerClass;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Music.MusicHelper;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.data.ShortcutsParser;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection.MusicServerMessages;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.MediaStore.MediaColumns;
import android.util.Log;
import android.util.Pair;
import android.widget.AbsListView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

@SuppressLint("NewApi")
/**
 * 
 * @author Ameer Assi
 * @gmail ameer.assi1991@gmail.com
 * @date 03.1.2014
 *
 */
public class MusicActivity extends DontPanicActivity {
    private boolean SongIsPlaying = true;
    private ArrayAdapter<String> adapter;
    private MediaPlayerClass media;
    private final ArrayList<Pair<String, String>> allSongs = new ArrayList<Pair<String, String>>();
    private ArrayList<Pair<String, String>> listToPlay = new ArrayList<Pair<String, String>>();
    private Button yellowButton;
    private Button blueButton;
    private Button greenButton;
    private Button redButton;
    private TextView AlbumText;
    private ListView list;
    private ImageView shuffleView;

    /**
     * This method is called when the activity is opened , it initialize all the
     * music layout, and starts to play songs.
     */
    @Override
    protected void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.music_window);
        getShapes();
        updateFourButtonsHandlers();
        MusicHelper.updateAllSongs(getApplicationContext(), allSongs);
        updateListToPlay(getIntent());
        updateFourButtonsShape();
        MediaPlayerClass.MediaPlayerCreate(listToPlay);
        media = MediaPlayerClass.MediaPlayerGetInstance();
        updateUI();
        media.doPlay();
        Log.w("MusicActivity", "onCreate");
    }

    @Override
    protected void onNewIntent(final Intent intent) {
        super.onNewIntent(intent);
        if (intent.getExtras() == null)
            return;
        listToPlay = new ArrayList<Pair<String, String>>();
        updateListToPlay(intent);
        updateFourButtonsShape();
        MediaPlayerClass.MediaPlayerCreate(listToPlay);
        media = MediaPlayerClass.MediaPlayerGetInstance();
        updateUI();
        media.doPlay();
    }

    /**
     * updates the GUI of the List view.
     */
    private void updateUI() {
        final ArrayList<String> listSongsToPlay = (ArrayList<String>) fillAllSongsNames(listToPlay);
        adapter = new ArrayAdapter<String>(this,
                R.layout.list_row_small_highlighted, R.id.listItemText,
                listSongsToPlay);
        list.setAdapter(adapter);
        list.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
        changeListViewIndex(media.getCurrentSongIndex());
        changeSongName();
        media.SetOwner(this);
    }

    /**
     * updates list content with the new sonogs to play.
     * 
     * @param intent
     *            caller intent
     */
    private void updateListToPlay(final Intent intent) {
        final TextView AlbumWord = (TextView) findViewById(R.id.Album);
        final Bundle extras = intent.getExtras();
        String AlbumName = "";
        String ArtistName = "";
        if (extras != null) {
            if (extras.getString("Album_Name") != null) {
                AlbumName = extras.getString("Album_Name");
                AlbumText.setText(AlbumName);
                PlayAlbumSongs(AlbumName);
            }
            if (extras.getString("Artist_Name") != null) {
                ArtistName = extras.getString("Artist_Name");
                AlbumText.setText(ArtistName);
                AlbumWord.setText(R.string.artist);
                PlayArtistSongs(ArtistName);
            }
        }
        if (AlbumName.equals(ShortcutsParser.ALL_SONGS_ALBUM_NAME)
                || ArtistName.equals(ShortcutsParser.ALL_SONGS_ALBUM_NAME))
            PlayAllSongs();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        MediaPlayerClass.MediaPlayerDistroyInstance();
        Log.w("MusicActivity", "onDestroy");
    }

    /**
     * This method updates buttons handlers for the superclass to get multi
     * touch option.
     */
    private void updateFourButtonsHandlers() {
        yellowButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.YELLOW));
        blueButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.BLUE));
        greenButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.GREEN));
        redButton.setOnTouchListener(new DontPanicTouchListener(
                ColorArrayLocations.RED));
    }

    /**
     * This method gets the four buttons from the layout instance.
     */
    private void getShapes() {
        yellowButton = (Button) findViewById(R.id.YellowButton);
        blueButton = (Button) findViewById(R.id.BlueButton);
        greenButton = (Button) findViewById(R.id.GreenButton);
        redButton = (Button) findViewById(R.id.RedButton);
        AlbumText = (TextView) findViewById(R.id.AlbumText);
        list = (ListView) findViewById(R.id.songsListView);
        shuffleView = (ImageView) findViewById(R.id.imageView2);
    }

    /**
     * This method update the Four buttons shape to be used by Music Activity.
     */
    private void updateFourButtonsShape() {
        redButton.setText(getResources().getString(R.string.previous));
        greenButton.setText(getResources().getString(R.string.next));
        blueButton.setText(getResources().getString(R.string.stop));
        yellowButton.setText(getResources().getString(R.string.pause));
        redButton.setCompoundDrawablesWithIntrinsicBounds(null, getResources()
                .getDrawable(android.R.drawable.ic_media_previous), null, null);
        greenButton.setCompoundDrawablesWithIntrinsicBounds(null,
                getResources().getDrawable(android.R.drawable.ic_media_next),
                null, null);
        blueButton.setCompoundDrawablesWithIntrinsicBounds(null, getResources()
                .getDrawable(R.drawable.ic_media_stop), null, null);
        yellowButton.setCompoundDrawablesWithIntrinsicBounds(null,
                getResources().getDrawable(android.R.drawable.ic_media_pause),
                null, null);
    }

    /**
     * this method gets code and updates what should the Music view to do.
     * 
     * @param code
     *            is the given code for the method.
     */
    @Override
    public void enterCode(final boolean[] code) {
        final boolean yellowPressed = code[ColorArrayLocations.YELLOW.ordinal()];
        final boolean bluePressed = code[ColorArrayLocations.BLUE.ordinal()];
        final boolean greenPressed = code[ColorArrayLocations.GREEN.ordinal()];
        final boolean redPressed = code[ColorArrayLocations.RED.ordinal()];
        // back button.
        if (!greenPressed && redPressed && yellowPressed && !bluePressed) {
            // finish();
            final Intent intent = new Intent(MusicActivity.this,
                    MainWindowActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
            intent.putExtra("class_name", "MusicActivity");
            intent.putExtra("playing_music", AlbumText.getText());
            startActivity(intent);
            return;
        } else if (!greenPressed && !redPressed && yellowPressed
                && !bluePressed) {
            if (SongIsPlaying) {
                pause();
                updatePlayButton(false);
            } else {
                play();
                updatePlayButton(true);
            }
        } else if (!greenPressed && !redPressed && yellowPressed && bluePressed)
            shuffle();
        else if (!greenPressed && !redPressed && !yellowPressed && bluePressed) {
            stop();
            updatePlayButton(false);
        } else if (greenPressed && !redPressed && !yellowPressed
                && !bluePressed)
            next();
        else if (!greenPressed && redPressed && !yellowPressed && !bluePressed)
            previous();
        else if (greenPressed && redPressed && yellowPressed && bluePressed)
            enterPanic(LoginActivity.dataManager);
    }

    /**
     * This method updates the view in the layout instance when song changes.
     */
    public void changeSongName() {
        try {
            final TextView SongText = (TextView) findViewById(R.id.SongText);
            final String[] names = media.getCurrentPlayingSong().split(";");
            if (names.length < 2)
                return;
            if (!SongText.getText().equals(names[0])) {
                SongText.setText(names[0]);
                MusicServerMessages.sendScore(names[0], names[1], names[2],
                        getApplicationContext());
            }
        } catch (final Exception e) {
            Log.e("musicActivity",
                    "failed to send statistics " + e.getMessage());
        }
    }

    /**
     * This method updates the listView index
     * 
     * @param index
     *            is the index which to update the listView shape
     */
    public void changeListViewIndex(final int index) {
        final ListView list = (ListView) findViewById(R.id.songsListView);
        list.setItemChecked(index, true);
        list.smoothScrollToPositionFromTop(index, 0);
    }

    /**
     * This method fill the list which played with the activity with songs of
     * the given album.
     * 
     * @param AlbumName
     *            is the album name which to play its songs
     */
    private void PlayAlbumSongs(final String AlbumName) {
        heplGettingPlay(AlbumName, true);

    }

    private void PlayArtistSongs(final String ArtistName) {
        heplGettingPlay(ArtistName, false);

    }

    private void heplGettingPlay(final String Name, final boolean isAlbum) {
        final Cursor cursor = getApplicationContext().getContentResolver()
                .query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, null, null,
                        null, MediaColumns.TITLE);
        MusicHelper.getAllCursor(getApplicationContext(), cursor);
        for (cursor.moveToFirst(); !cursor.isAfterLast(); cursor.moveToNext()) {
            final String songPath = cursor.getString(cursor
                    .getColumnIndex("_data"));
            String songName = cursor.getString(cursor
                    .getColumnIndex(MediaColumns.TITLE));
            final String isMusic = cursor.getString(cursor
                    .getColumnIndex("is_music"));
            final String artist = cursor.getString(cursor
                    .getColumnIndex("artist"));
            final String album = cursor.getString(cursor
                    .getColumnIndex("album"));
            songName = songName + ";" + artist + ";" + album;
            if (isAlbum) {
                if (isMusic.equals("1") && album.equals(Name))
                    listToPlay
                            .add(new Pair<String, String>(songName, songPath));
            } else if (isMusic.equals("1") && artist.equals(Name))
                listToPlay.add(new Pair<String, String>(songName, songPath));
        }
        cursor.close();
    }

    /**
     * This method returns all song names of the gevin list to Play.
     * 
     * @param pairSongs
     *            is list of songs ( pair song Name and song Path).
     * @return List of songs Names
     */
    private List<String> fillAllSongsNames(
            final ArrayList<Pair<String, String>> pairSongs) {
        final ArrayList<String> songs = new ArrayList<String>();
        for (final Pair<String, String> pair : pairSongs)
            songs.add(pair.first.split(";")[0]);
        return songs;

    }

    /**
     * updates the list to play be all Songs.
     */
    private void PlayAllSongs() {

        listToPlay = allSongs;
    }

    /**
     * pauses the song.
     */
    public void pause() {
        media.doPause();
    }

    /**
     * continue playing the paused/stoped song.
     */
    public void play() {
        media.doPlay();
    }

    /**
     * plays the previous song.
     */
    public void previous() {
        media.doPrev();
        updatePlayButton(true);
    }

    /**
     * plays the next song.
     */
    public void next() {
        media.doNext();
        updatePlayButton(true);
    }

    /**
     * plays a random song.
     */
    public void shuffle() {
        media.setToShuffleRun();
        media.shuffle();
        updatePlayButton(true);

    }

    /**
     * stops the playing song.
     */
    public void stop() {
        media.doStop();

    }

    /**
     * updates the word and the shape which displayed on the play button.
     * 
     * @param value
     *            true if to make pause else false.
     */
    public void updatePlayButton(final boolean value) {
        SongIsPlaying = value;
        if (value == true) {
            yellowButton.setCompoundDrawablesWithIntrinsicBounds(
                    null,
                    getResources().getDrawable(
                            android.R.drawable.ic_media_pause), null, null);
            yellowButton.setText(getResources().getString(R.string.pause));
        } else {
            yellowButton.setCompoundDrawablesWithIntrinsicBounds(null,
                    getResources()
                            .getDrawable(android.R.drawable.ic_media_play),
                    null, null);
            yellowButton.setText(getResources().getString(R.string.play));
        }
    }

    /**
     * update shuffle click 
     * @param shuffle
     *                the shuffle state
     */
    public void updateShuffleView(final boolean shuffle) {
        if (shuffle)
            shuffleView.setImageResource(R.drawable.shuffle);
        else
            shuffleView.setImageResource(R.drawable.not_shuffle);
    }

}
