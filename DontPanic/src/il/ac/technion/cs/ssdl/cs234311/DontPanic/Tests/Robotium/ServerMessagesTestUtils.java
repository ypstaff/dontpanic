package il.ac.technion.cs.ssdl.cs234311.DontPanic.Tests.Robotium;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection.HttpPostPackets;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.serverConnection.ServerMessages;

import java.util.Random;
import java.util.concurrent.ExecutionException;

import com.google.gson.Gson;

public class ServerMessagesTestUtils extends ServerMessages {
    /**
     * run maintainance servlet on server
     * 
     * @param shortcut
     */
    public static void runMaintainance() {
        final String ADD_SHORTCUT_SERVLET_NAME = "RunMaintainance";

        final String[] packetFields = new String[1];
        packetFields[0] = ADD_SHORTCUT_SERVLET_NAME;

        try {
            new HttpPostPackets(null, "", "").execute(packetFields).get();
        } catch (final NullPointerException e) {

        } catch (final InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (final ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static void regUserForTest(final User user) {
        final String REG_USER_SERVLET_NAME = "RegUser";
        final String USER_PARAM_NAME = "user";

        final String[] packetFields = new String[3];

        packetFields[0] = REG_USER_SERVLET_NAME;
        packetFields[1] = USER_PARAM_NAME;
        packetFields[2] = new Gson().toJson(user);

        try {
            new HttpPostPackets(null, "", "").execute(packetFields).get();
        } catch (final NullPointerException e) {

        } catch (final InterruptedException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        } catch (final ExecutionException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public static final String randomString() {
        final char[] chars = "abcdefghijklmnopqrstuvwxyz0123456789"
                .toCharArray();
        final StringBuilder sb = new StringBuilder();
        final Random random = new Random();
        for (int i = 0; i < 20; i++) {
            final char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        return sb.toString();
    }

    public static final User randomUser() {

        final String TEST_MAIL_SERVER = "testMailServer123";

        final String userName = randomString();

        return new User("test123" + userName + "@" + TEST_MAIL_SERVER,
                userName, "", "");
    }
}
