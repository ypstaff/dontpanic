package il.ac.technion.cs.ssdl.cs234311.DontPanic.classes;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.R;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.NoSuchOpException;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.ShortcutLetter;

import java.util.Iterator;
import java.util.List;

import android.content.Context;
import android.graphics.drawable.GradientDrawable;
import android.graphics.drawable.LayerDrawable;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ShortcutsListAdapter extends ArrayAdapter<Shortcut> {

    private final Context _context;
    private final List<Shortcut> shortcuts;
    private final List<boolean[]> currentWord;

    public ShortcutsListAdapter(final Context context, final int resource,
            final int textViewResourceId, final int imageViewResourceId,
            final List<Shortcut> objects, final List<boolean[]> word) {
        super(context, resource, textViewResourceId, objects);

        _context = context;
        shortcuts = objects;
        currentWord = word;
    }

    @Override
    public View getView(final int position, View convertView,
            final ViewGroup parent) {
        if (convertView == null) {
            final LayoutInflater infalInflater = (LayoutInflater) this._context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = infalInflater.inflate(
                    R.layout.list_row_small_highlighted, null);
        }

        final Shortcut s = shortcuts.get(position);

        final TextView textView = (TextView) convertView
                .findViewById(R.id.listItemText);

        try {
            textView.setText(s.getDisplayName(_context));
        } catch (final NoSuchOpException e1) {
            e1.printStackTrace();
        }

        final LinearLayout visualDescription = (LinearLayout) convertView
                .findViewById(R.id.listItemShortcutDescription);
        visualDescription.removeAllViews();

        final List<ShortcutLetter> list = s.letteredCode;

        int i = 0, j = 0, k = 0;
        for (final Iterator<ShortcutLetter> iterator = list.iterator(); iterator
                .hasNext() && i < 4; j++) {// 4
            final ShortcutLetter shortcutLetter = iterator.next();

            if (j < currentWord.size()) {
                if (k < 1) { // 1
                    k++;// the number of letters in the suggestion to show
                    final LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                            LayoutParams.WRAP_CONTENT,
                            LayoutParams.MATCH_PARENT);
                    lParams.gravity = Gravity.CENTER;

                    final ImageView imgNew = new ImageView(this._context);
                    imgNew.setPadding(0, 4, 4, 0);
                    imgNew.setImageResource(R.drawable.ic_action_forward);
                    visualDescription.addView(imgNew, lParams);
                }
                ShortcutLetter currentShortcutLetter;
                try {
                    currentShortcutLetter = ShortcutTranslator
                            .array2letter(currentWord.get(j));
                } catch (final IllegalShortcutLetter e) {
                    // TODO Auto-generated catch block
                    // Shouldn't get here
                    e.printStackTrace();
                    return convertView;
                }
                if (shortcutLetter.equals(currentShortcutLetter))
                    continue;
            }

            int leftColor, rightColor;
            switch (shortcutLetter) {// red -> yellow -> blue -> green
            case ONE_TWO:
                leftColor = _context.getResources().getColor(
                        R.color.holo_red_light);
                rightColor = _context.getResources().getColor(
                        R.color.holo_yellow_light);
                break;
            case TWO_THREE:
                leftColor = _context.getResources().getColor(
                        R.color.holo_yellow_light);
                rightColor = _context.getResources().getColor(
                        R.color.holo_blue_light);
                break;
            case THREE_FOUR:
                leftColor = _context.getResources().getColor(
                        R.color.holo_blue_light);
                rightColor = _context.getResources().getColor(
                        R.color.holo_green_light);
                break;
            case FOUR_ONE:
                leftColor = _context.getResources().getColor(
                        R.color.holo_red_light);
                rightColor = _context.getResources().getColor(
                        R.color.holo_green_light);
                break;
            default:
                continue;
            }

            final LinearLayout.LayoutParams lParams = new LinearLayout.LayoutParams(
                    LayoutParams.WRAP_CONTENT, LayoutParams.MATCH_PARENT);
            lParams.gravity = Gravity.CENTER;

            final LayerDrawable background = (LayerDrawable) this._context
                    .getResources().getDrawable(
                            R.drawable.two_colors_round_background);
            final GradientDrawable leftDrawable = (GradientDrawable) background
                    .findDrawableByLayerId(R.id.leftColor);
            final GradientDrawable rightDrawable = (GradientDrawable) background
                    .findDrawableByLayerId(R.id.rightColor);
            leftDrawable.setColor(leftColor);
            rightDrawable.setColor(rightColor);

            final ImageView imgNew = new ImageView(this._context);
            imgNew.setPadding(0, 4, 4, 0);
            imgNew.setImageDrawable(background);
            visualDescription.addView(imgNew, lParams);

            i++; // the number of letters in the suggestion to show
        }

        return convertView;
    }

}
