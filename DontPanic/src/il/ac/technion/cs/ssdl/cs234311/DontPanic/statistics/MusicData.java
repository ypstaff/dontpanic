/**
 * 
 */
package il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import com.googlecode.objectify.annotation.EntitySubclass;

/**
 * @author Michael Varvaruk
 * @email mvarvaruk@gmail.com
 * @date 14/05/14
 * 
 */
@EntitySubclass(index = true)
public class MusicData extends RawData {

    public String[] info;

    /**
     * empty c'tor for objectify
     */
    public MusicData() {
        super();
    }

    /**
     * @param _uid
     * @param _time
     * @param _info
     */
    public MusicData(final String _uid, final Date _time, final String[] _info) {
        super(_uid, _time);

        info = _info.clone();
    }

    private static String favoriteElement(final Set<MusicData> set,
            final int idx) {
        final HashMap<String, Integer> map = new HashMap<String, Integer>();

        String maxKey = null;
        int maxValue = 0;

        for (final MusicData d : set) {
            final String key = d.info[idx];
            if (map.containsKey(key))
                map.put(key, map.get(key) + 1);
            else
                map.put(key, 1);

            if (map.get(key) > maxValue) {
                maxKey = key;
                maxValue = map.get(key);
            }
        }

        return maxKey;
    }

    @Override
    public String[] calculateStatistics(
            final Collection<? extends RawData> datasets) {
        final HashSet<MusicData> s = new HashSet<MusicData>();
        for (final RawData d : datasets)
            if (d instanceof MusicData)
                s.add((MusicData) d);

        final String[] result = new String[4];
        result[0] = "fav_song:" + favoriteElement(s, 0);
        result[1] = "fav_artist:" + favoriteElement(s, 1);
        result[2] = "fav_album:" + favoriteElement(s, 2);
        result[3] = "num_plays:" + s.size();

        return result;

    }

}
