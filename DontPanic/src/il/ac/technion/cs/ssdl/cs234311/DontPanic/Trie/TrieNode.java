package il.ac.technion.cs.ssdl.cs234311.DontPanic.Trie;

import java.util.ArrayList;

public class TrieNode<T> {

    private final T value;

    private boolean endMarker;

    public ArrayList<TrieNode<T>> children;

    public TrieNode(final T value) {
        this.value = value;
        this.endMarker = false;
        this.children = new ArrayList<TrieNode<T>>();
    }

    public TrieNode<T> findChild(final T _value) {
        if (children != null)
            for (final TrieNode<T> n : children)
                if (n.getValue().equals(_value))
                    return n;
        return null;
    }

    public T getValue() {
        return value;
    }

    public void setEndMarker(final boolean endMarker) {
        this.endMarker = endMarker;
    }

    public boolean isEndMarker() {
        return endMarker;
    }

    public TrieNode<T> addChild(final T _value) {
        final TrieNode<T> n = new TrieNode<T>(_value);
        children.add(n);
        return n;
    }

}