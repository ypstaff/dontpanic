
package com.android.internal.telephony;

/**
 * Interface used to interact with the phone.  Mostly this is used by the
 * TelephonyManager class.  A few places are still using this directly.
 * Please clean them up if possible and use TelephonyManager instead.
 *
 */
interface ITelephony {      

boolean endCall();     

void answerRingingCall();      

void silenceRinger(); 

}