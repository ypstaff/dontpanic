package il.ac.technion.cs.ssdl.cs234311.yp09.contacts;

/**
 * @author Daniel Eidel
 * 
 */
public class SMSItem {
  /**
   * Contact name as saved in the Contacts list
   */
  public String contactName;
  /**
   * The current phone number
   */
  public String contactPhone;
  /**
   * The phone number's type
   */
  public int contactType;
  /**
   * Flag whether the contact has been selected on screen
   */
  public boolean checked;

  /**
   * @param nContactName
   *          - Contact name as saved in the Contacts list
   * @param nContactPhone
   *          - The current phone number
   * @param nContactType
   *          - The phone number's type
   */
  public SMSItem(String nContactName, String nContactPhone, int nContactType) {
    this.contactName = nContactName;
    this.contactPhone = nContactPhone;
    this.contactType = nContactType;
    this.checked = false;
  }

}
