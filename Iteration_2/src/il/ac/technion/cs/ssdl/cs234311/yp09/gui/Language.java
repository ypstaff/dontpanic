package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 16/1/2014
 * 
 */
public class Language {
  private int iconId;
  private final String language;

  /**
   * @param iconId
   *          icon identifier
   * @param language
   *          the language
   */
  public Language(final int iconId, final String language) {
    this.iconId = iconId;
    this.language = language;
  }

  /**
   * @return icon identifier
   */
  public int getIconId() {
    return iconId;
  }

  /**
   * @param iconId
   *          the icon id to be set
   */
  public void setIconId(final int iconId) {
    this.iconId = iconId;
  }

  /**
   * @return the language field
   */
  public String getLanguage() {
    return language;
  }

  @Override
  public String toString() {
    return iconId + "\n" + language;
  }
}