package il.ac.technion.cs.ssdl.cs234311.yp09.utilities;

import il.ac.technion.cs.ssdl.cs234311.yp09.gui.OpCodeInterpreter.Op;
import android.text.format.Time;

public class SaveStatistics {
  private static Time startAppTime;
  private static Statistics statistics;
  private static Time timeForLastChar;
  private static int timeForLastSentence, timeForLastWord;

  /**
   * the types of the optional clicks, including long and combination of clicks.
   * 
   */
  public enum ClickType {
    /**
     * blue button click
     */
    BLUE,
    /**
     * yellow button click
     */
    YELLOW,
    /**
     * green button click
     */
    GREEN,
    /**
     * red button click
     */
    RED,
    /**
     * 
     */
    BLUE_YELLOW,

    /**
     * 
     */
    YELLOW_GREEN,
    /**
     * 
     */
    GREEN_RED,
    /**
     * 
     */
    RED_BLUE
  }

  public ClickType convertOpToClickType(Op op) {
    switch (op) {
    case BLUE:
    case LONG_BLUE:
      return ClickType.BLUE;
    case ORANGE:
    case LONG_ORANGE:
      return ClickType.YELLOW;
    case GREEN:
    case LONG_GREEN:
      return ClickType.GREEN;
    case RED:
    case LONG_RED:
      return ClickType.RED;
    case BLUE_ORANGE:
      return ClickType.BLUE_YELLOW;
    case ORANGE_GREEN:
      return ClickType.YELLOW_GREEN;
    case GREEN_RED:
      return ClickType.GREEN_RED;
    default:
      break;
    }
    return null;
  }

  public enum MessageType {
    FACEBOOK_MESSAGE, SMS_MESSAGE, GOOGLE_MESSAGE;
  }

  public static void startApp() {
    statistics = new Statistics();
    startAppTime = new Time();
    startAppTime.setToNow();
  }

  // Watad - Done
  // ATTENTION !!!!!!
  // HUSAM, YOU ARE NOT CONSIDERING LONG PRESSES !
  // DON'T FORGET TO ADD A SUPPORT FOR LONG PRESSES, TOO.
  public static void clickOn(Op type) {
    switch (type) {
    case RED:
      statistics.numberOfRedClicks++;
      break;
    case BLUE:
      statistics.numberOfBlueClicks++;
      break;
    case GREEN:
      statistics.numberOfGreenClicks++;
      break;
    case ORANGE:
      statistics.numberOfOrangeClicks++;
      break;
    case BLUE_ORANGE:
      statistics.numberOfBlueOrangeClicks++;
      break;
    case ORANGE_GREEN:
      statistics.numberOfOrangeGreenClicks++;
      break;
    case GREEN_RED:
      statistics.numberOfGreenRedClicks++;
      break;
    case BLUE_RED:
      statistics.numberOfBlueRedClicks++;
      break;
    default:
      break;
    }
  }

  // Owais
  public static void startTyping() {
    timeForLastChar = new Time();
    timeForLastChar.setToNow();
    timeForLastSentence = timeForLastWord = 0;
  }

  private static Double calcNewAvg(Double oldAverage,
      Double oldNumberOfElements, int newElem) {
    return (oldAverage * oldNumberOfElements + newElem)
        / (oldNumberOfElements + 1);
  }

  // Owais
  public static void addSentence() {
    statistics.timePerSentenceAvg = calcNewAvg(statistics.timePerSentenceAvg,
        statistics.numberOfSentence, timeForLastSentence);
    timeForLastSentence = 0;
    statistics.numberOfSentence++;
  }

  // Owais
  public static void addChar() {
    if (timeForLastChar != null) {
      Time currTime = new Time();
      currTime.setToNow();
      int diffFromLastTime = (int) (currTime.toMillis(true) - timeForLastChar
          .toMillis(true));
      if (diffFromLastTime <= 3000) {
        statistics.timePerCharAvg = calcNewAvg(statistics.timePerCharAvg,
            statistics.numberOfChars, diffFromLastTime);
        statistics.numberOfChars++;
        timeForLastSentence += diffFromLastTime;
        timeForLastWord += diffFromLastTime;
      } else {
        timeForLastSentence += 1500;
        timeForLastWord += 1500;
      }
    }
    timeForLastChar = new Time();
    timeForLastChar.setToNow();
  }

  // Owais
  public static void addWord() {
    statistics.timePerWordAvg = calcNewAvg(statistics.timePerWordAvg,
        statistics.numberOfWords, timeForLastWord);
    timeForLastWord = 0;
    statistics.numberOfWords++;
  }

  // Owais
  public static void deleteChar() {
    statistics.numberOfDeletedChars++;
  }

  // Owais
  public static void addAutoComplete() {
    statistics.numberOfAutoComplete++;
  }

  // Owias & Itamar
  public static void addAutoCorrect() {
    statistics.numberOfAutoCorrect++;
  }

  // Daniel
  public static void sendMessage(MessageType type) {
    switch (type) {
    case FACEBOOK_MESSAGE:
      statistics.numberOfFaceBookMessages++;
      break;
    case SMS_MESSAGE:
      statistics.numberOfSMS++;
      break;
    case GOOGLE_MESSAGE:
      statistics.numberOfGoogleMessages++;
      break;
    default:
      break;
    }
  }

  // Watad - Done
  // Changing activity
  public static void stopTyping() {
    timeForLastChar = null;
    timeForLastSentence = 0;
    timeForLastWord = 0;
    startAppTime = null;
    statistics = null;
  }

  // Watad - Done
  public static Statistics exitTheApp() {
    timeForLastChar = null;
    Time now = new Time();
    now.setToNow();
    statistics.timeUsingTheApp = (now.toMillis(true) - startAppTime
        .toMillis(true)) / 1000.0;
    return statistics;
  }
}
