package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import android.app.Fragment;
import android.os.Bundle;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 4/1/2014
 * 
 */
public class AutoCompleteFragment extends Fragment {

  // private FrameLayout autoCompleteView;
  // private BorderedTextView borderedView;
  private FontFitTextView borderedView;

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    final View $ = inflater.inflate(R.layout.auto_complete, container, false);

    borderedView = (FontFitTextView) $.findViewById(R.id.auto_complete_view);

    // borderedView = new AutoScaleTextView(getActivity(),
    // R.attr.autoScaleTextViewStyle, R.style.autoScaleTextView);

    // borderedView = new BorderedTextView(getActivity(), Color.BLACK);
    //
    // final Border[] borders = new Border[4];
    // borders[0] = new Border(0, 4, Color.BLACK, Style.BORDER_TOP);
    // borders[1] = new Border(0, 4, Color.BLACK, Style.BORDER_RIGHT);
    // borders[2] = new Border(0, 4, Color.BLACK, Style.BORDER_BOTTOM);
    // borders[3] = new Border(0, 4, Color.BLACK, Style.BORDER_LEFT);
    // borderedView.setBorders(borders);
    borderedView
        .setGravity(Gravity.CENTER_VERTICAL | Gravity.CENTER_HORIZONTAL);
    // borderedView.setText("option #1 | option #2 | option #3");
    //
    // autoCompleteView.addView(borderedView);
    // autoCompleteView.addView(borderedView);

    final FrameLayout box1 = (FrameLayout) $
        .findViewById(R.id.auto_complete_box1_frame);
    final DrawView rec1 = new DrawView(getActivity(), DrawView.Color.Blue,
        DrawView.Position.Right);
    box1.addView(rec1);

    final FrameLayout box2 = (FrameLayout) $
        .findViewById(R.id.auto_complete_box2_frame);
    final DrawView rec2 = new DrawView(getActivity(), DrawView.Color.Red,
        DrawView.Position.Left);
    box2.addView(rec2);

    final FrameLayout icon = (FrameLayout) $
        .findViewById(R.id.switch_icon_frame);
    final ImageView img = new ImageView(getActivity());
    img.setImageResource(R.drawable.up_down);
    icon.addView(img);

    return $;
  }

  /**
   * @param options
   *          A String composed by concatenating the 3 auto-completed options
   */
  public void setOptionsText(String options) {
    borderedView.setText(options);
    borderedView.resizeText(borderedView.getWidth(), borderedView.getHeight());
  }
}
