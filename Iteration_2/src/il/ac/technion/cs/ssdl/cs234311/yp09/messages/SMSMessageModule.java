package il.ac.technion.cs.ssdl.cs234311.yp09.messages;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.TimeZone;

import android.app.Activity;
import android.database.Cursor;
import android.net.Uri;

/**
 * @author Daniel
 * @desc This class is used to provide the SMS conversation in a comprehensive
 *       format
 */
public class SMSMessageModule {

  /**
   * @param currAct
   *          - Type Activity. The main activity, in which the SMS storage is
   *          parsed.
   * @param phoneNum
   *          - Type String. The phone number of the person whose conversation
   *          we wish to present
   * @return - Type ArrayList<SMSMessageItem>. An ArrayList of SMSMessageItem
   *         objects, in which holds the conversation's messages.
   */
  static public ArrayList<SMSMessageItem> SMSParser(Activity currAct,
      String phoneNum) {
    ArrayList<SMSMessageItem> messagesList = new ArrayList<SMSMessageItem>();
    Uri SMS_INBOX = Uri.parse("content://sms");
    Cursor c = currAct.getContentResolver().query(SMS_INBOX, null, null, null,
        "date desc");

    String threadid = null;
    String phoneNumNoDash = phoneNum.replace("-", "");
    String interPhoneNum = "+972 " + phoneNum.substring(1);
    String interPhoneNumInc = "+972" + phoneNum.replace("-", "").substring(1);
    String interPhoneNumSpace = "+972 "
        + phoneNum.replace("-", "").substring(1);
    // System.out.println("ATTEMPTINGTOPULL");
    c.moveToFirst();
    for (int i = 0; i < c.getCount(); i++) {
      String p = c.getString(c.getColumnIndexOrThrow("address"));
      if (p.equalsIgnoreCase(phoneNum) || p.equalsIgnoreCase(interPhoneNum)
          || p.equalsIgnoreCase(phoneNumNoDash)
          || p.equalsIgnoreCase(interPhoneNumInc)
          || p.equalsIgnoreCase(interPhoneNumSpace)) {
        threadid = c.getString(c.getColumnIndexOrThrow("thread_id"));
        break;
      }

      c.moveToNext();
    }
    c.close();

    String where = "thread_id=" + threadid;
    Cursor mycursor = currAct.getContentResolver().query(SMS_INBOX, null,
        where, null, null);
    if (mycursor.getCount() > 0)
      while (mycursor.moveToNext()) {
        int type = Integer.parseInt(mycursor.getString(mycursor
            .getColumnIndexOrThrow("type")));
        String body = mycursor
            .getString(mycursor.getColumnIndexOrThrow("body"));
        long time = Long.parseLong(mycursor.getString(mycursor
            .getColumnIndexOrThrow("date")));

        Calendar calendar = Calendar.getInstance(TimeZone.getDefault());
        calendar.setTimeInMillis(time);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy MMM dd h:mma");
        String date = sdf.format(calendar.getTime());
        SMSMessageItem newMessage = new SMSMessageItem(type, body, date);
        messagesList.add(newMessage);
      }
    mycursor.close();
    return messagesList;
  }

}
