package il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeed;

import java.util.ArrayList;
import java.util.List;

import android.os.AsyncTask;
import android.util.Log;

import com.facebook.Session;
import com.restfb.Connection;
import com.restfb.DefaultFacebookClient;
import com.restfb.Facebook;
import com.restfb.FacebookClient;
import com.restfb.Parameter;
import com.restfb.json.JsonObject;
import com.restfb.types.Post;

/**
 * The main class for Facebook component
 * 
 * @author Owais Musa
 * 
 */
public class FacebookManager {

  private static final int INIT_POSTS_NUM = 10;

  /**
   * @return facebook posts to be shown
   */
  // Suppress warning to use Mokito!
  @SuppressWarnings({ "synthetic-access", "static-method" })
  public Iterable<FacebookPost> getNewsFeed() {
    List<FacebookPost> $ = new ArrayList<FacebookPost>();

    List<FacebookPost> posts = null;
    try {
      posts = new NewsFeedGetterAsyncTask().execute().get();
    } catch (Exception e) {
      e.printStackTrace();
    }

    if (null == posts)
      return $;
    /*
     * $.add(new FacebookPost( "1234", "error", "owais",
     * "asd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asdasd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd asd "
     * , 4, 5, 6, new Date()));
     * 
     * $.add(new FacebookPost("1234", "error", "owais", "owais maher ", 4, 5, 6,
     * new Date()));
     * 
     * $.add(new FacebookPost("1234", "error", "owais", "owais maher mawasy", 4,
     * 5, 6, new Date()));
     * 
     * $.add(new FacebookPost("1234", "error", "owais", "owais maher mawasy2",
     * 4, 5, 6, new Date())); $.add(new FacebookPost("1234", "error", "owais",
     * "owais maher mawasy3", 4, 5, 6, new Date())); $.add(new
     * FacebookPost("1234", "error", "owais", "owais maher mawasy4", 4, 5, 6,
     * new Date())); $.add(new FacebookPost("1234", "error", "owais",
     * "owais maher mawasy5", 4, 5, 6, new Date())); $.add(new
     * FacebookPost("1234", "error", "owais", "owais maher mawasy6", 4, 5, 6,
     * new Date())); $.add(new FacebookPost("1234", "error", "owais",
     * "owais maher mawasy7", 4, 5, 6, new Date())); $.add(new
     * FacebookPost("1234", "error", "owais", "owais maher mawasy8", 4, 5, 6,
     * new Date())); $.add(new FacebookPost("1234", "error", "owais",
     * "owais maher mawasy9", 4, 5, 6, new Date()));
     */
    for (FacebookPost p : posts)
      $.add(p);

    Log.d("facebook - news feed", "FINISH FETCHING");

    return $;
  }

  /**
   * @param postId
   *          post id
   */
  @SuppressWarnings("synthetic-access")
  public static void addLike(String postId) {
    Log.d("owais like", "start");
    new DoLikeAsyncTask().execute(postId);
    Log.d("owais like", "finish");
  }

  /**
   * @param postId
   *          post id
   */
  @SuppressWarnings("synthetic-access")
  public static void removeLike(String postId) {
    Log.d("facebook - unlike", "start");
    new DoLikeAsyncTask().execute(postId);
    Log.d("facebook - unlike", "finish");
  }

  /**
   * @param postId
   *          post id
   */
  @SuppressWarnings("synthetic-access")
  public static void addComment(String postId) {
    Log.d("facebook - comment", "start");
    new DoLikeAsyncTask().execute(postId);
    Log.d("facebook - comment", "finish");
  }

  private static class NewsFeedGetterAsyncTask extends
      AsyncTask<Void, Void, List<FacebookPost>> {

    private FacebookClient mFacebookClient;

    private String getUserProfilePictureUrl(String userId) {
      String query = "SELECT pic_big_with_logo FROM user WHERE uid=" + userId;

      List<UserPicture> userList = mFacebookClient.executeFqlQuery(query,
          UserPicture.class);

      if (!(null == userList || userList.size() == 0 || null == userList.get(0)))
        return userList.get(0).pic_big_with_logo;

      query = "SELECT pic_big FROM page WHERE page_id=" + userId;
      List<PagePicture> pageList = mFacebookClient.executeFqlQuery(query,
          PagePicture.class);

      if (!(null == pageList || pageList.size() == 0 || null == pageList.get(0)))
        return pageList.get(0).pic_big;

      return "";
    }

    public static class PagePicture {
      @SuppressWarnings("unused")
      public PagePicture() {
      }

      @Facebook
      String pic_big;
    }

    public static class UserPicture {
      @SuppressWarnings("unused")
      public UserPicture() {
      }

      @Facebook
      String pic_big_with_logo;
    }

    private long getLikes(Post post) {
      long $;

      try {
        JsonObject jsonObject = mFacebookClient.fetchObject(post.getId()
            + "/likes", JsonObject.class,
            Parameter.with("summary", Boolean.valueOf(true)),
            Parameter.with("limit", Integer.valueOf(1)));
        $ = jsonObject.getJsonObject("summary").getLong("total_count");
      } catch (Exception e) {
        $ = 0;
      }

      return $;
    }

    private long getComments(Post post) {
      long $;

      try {
        JsonObject jsonObject = mFacebookClient.fetchObject(post.getId()
            + "/comments", JsonObject.class,
            Parameter.with("summary", Boolean.valueOf(true)),
            Parameter.with("limit", Integer.valueOf(1)));
        $ = jsonObject.getJsonObject("summary").getLong("total_count");
      } catch (Exception e) {
        $ = 0;
      }

      return $;
    }

    private static long getShares(Post post) {
      long $;

      try {
        $ = post.getSharesCount().longValue();
      } catch (Exception e) {
        $ = 0;
      }

      return $;
    }

    private boolean likedByMe(Post post, String id) {
      // post.getLikes().getData();
      // post.
      // // retrieve a specific like, then
      // client.delete(like);
      return false;
    }

    @Override
    protected List<FacebookPost> doInBackground(Void... params) {
      try {
        Session session = Session.getActiveSession();
        mFacebookClient = new DefaultFacebookClient(session.getAccessToken());

        Log.d("facebook - news feed", "before query");

        List<FacebookPost> $ = new ArrayList<FacebookPost>();
        Connection<Post> myFeed = mFacebookClient.fetchConnection("me/home",
            Post.class,
            Parameter.with("limit", Integer.valueOf(INIT_POSTS_NUM)));

        Log.d("facebook - news feed", "after query");

        int i = 0;
        for (List<Post> myFeedConnectionPage : myFeed) {
          for (Post post : myFeedConnectionPage) {
            i++;
            Log.d("facebook - news feed", "post: " + i);

            if (post.getMessage() == null)
              continue;

            String imgUrl = getUserProfilePictureUrl(post.getFrom().getId());
            $.add(new FacebookPost(post.getId(), imgUrl, post.getFrom()
                .getName(), post.getMessage(), getLikes(post),
                getComments(post), getShares(post), post.getCreatedTime()));

            if (i >= INIT_POSTS_NUM)
              break;
          }

          if (i >= INIT_POSTS_NUM)
            break;
        }

        return $;

      } catch (Exception e) {
        e.printStackTrace();
        return null;
      }
    }
  }

  private static class DoLikeAsyncTask extends AsyncTask<String, Void, Void> {

    private FacebookClient mFacebookClient;

    @Override
    protected Void doInBackground(String... params) {
      try {
        Session session = Session.getActiveSession();
        mFacebookClient = new DefaultFacebookClient(session.getAccessToken());

        mFacebookClient.publish(params[0] + "/likes", Boolean.class);
      } catch (Exception e) {
        e.printStackTrace();
        return null;
      }

      return null;
    }
  }

  private static class DoUnlikeAsyncTask extends AsyncTask<String, Void, Void> {

    private FacebookClient mFacebookClient;

    @Override
    protected Void doInBackground(String... params) {
      try {
        Session session = Session.getActiveSession();
        mFacebookClient = new DefaultFacebookClient(session.getAccessToken());

        // DefaultFacebookClient client = new
        // DefaultFacebookClient(access_token);
        // post.getLikes();
        // // retrieve a specific like, then
        // client.delete(like);
      } catch (Exception e) {
        e.printStackTrace();
        return null;
      }

      return null;
    }
  }

  private static class DoAddCommentAsyncTask extends
      AsyncTask<String, Void, Void> {

    private FacebookClient mFacebookClient;

    @Override
    protected Void doInBackground(String... params) {
      try {
        Session session = Session.getActiveSession();
        mFacebookClient = new DefaultFacebookClient(session.getAccessToken());

        // DefaultFacebookClient client = new
        // DefaultFacebookClient(access_token);
        // client.publish(post.getId()+"/comments", String.class,
        // Parameter.with("message", "Your comment here"));
      } catch (Exception e) {
        e.printStackTrace();
        return null;
      }

      return null;
    }
  }

}
