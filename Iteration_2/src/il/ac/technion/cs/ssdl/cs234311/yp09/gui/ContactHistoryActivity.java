package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.commsModule.CommsModule;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.ContactActivity.FragmentType;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.BaseKeyboardFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.IActivityWithKeyboard;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.IKeyboardListener;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.ISearchInContactsActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.InputColor;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment.KeyboardStyle;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment2;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardListener;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardType;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListView;


/**
 * @author Muhammad
 * 
 */
public class ContactHistoryActivity extends GeneralActivity implements
    IActivityWithKeyboard, ISearchInContactsActivity {

  private FragmentType current;
  private LongFragment longFragment;
  private ContactFragment contactFragment;
  private BaseKeyboardFragment keyboardFragment;

  private ListView contactsListView;
  private String value;

  static public String contactHistoryKey = "il.ac.technion.cs.ssdl.cs234311.yp09.gui.contactHistoryKey";

  /**
   * The record being marked now
   */
  public int currentRecord = 0;

  private ContactListViewAdapter sContactAdapter;
  private FacebookContactListViewAdapter fContactAdapter;
  private GoogleContactListViewAdapter gContactAdapter;

  // private GoogleContactListViewAdapter gContactAdapter;

  private void setKeyboardType() {
    try {
      keyboardFragment.setKeyboardType(KeyboardType.SEARCH_IN_CONTACTS);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_contact_history);

    final Intent intent = getIntent();
    value = intent.getStringExtra(contactHistoryKey);
    refreshActivity();

    current = FragmentType.Keyboard;

    longFragment = LongFragment.newInstance(LongFragment.Screen.ContactHistory);
    getFragmentManager().beginTransaction()
        .add(R.id.long_press_info_frame, longFragment).commit();

    contactFragment = new ContactFragment();
    getFragmentManager().beginTransaction()
        .add(R.id.contact_name_frame, contactFragment).commit();

    final ProgressBarFragment progressBarFragment = ProgressBarFragment
        .newInstance(ProgressBarFragment.Screen.ContactHistory);
    getFragmentManager().beginTransaction()
        .add(R.id.progress_bar_frame, progressBarFragment).commit();

    contactsListView = (ListView) findViewById(R.id.contacts_list_frame);
    contactsListView.setOnTouchListener(new NoTouchListener());
    contactsListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
    if (value.equals("sms")) {
      sContactAdapter = new ContactListViewAdapter(this, R.layout.sms_listview,
          Controller.smsList);
      contactsListView.setAdapter(sContactAdapter);
    } else if (value.equals("facebook")) {
      fContactAdapter = new FacebookContactListViewAdapter(this,
          R.layout.sms_listview, CommsModule.contactsFacebookParsed);
      contactsListView.setAdapter(fContactAdapter);
    } else {
      gContactAdapter = new GoogleContactListViewAdapter(this,
          R.layout.sms_listview, CommsModule.contactsGTalkParsed);
      contactsListView.setAdapter(gContactAdapter);
    }
    contactsListView.setItemChecked(0, true);

    setTitle(R.string.select_contact);

    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame, mFBFragment).commit();

    switch (TypingActivity.getKeyboardFragmentType()) {
    case DEFAULT:
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.DEFAULT);
      keyboardFragment = new KeyboardFragment();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .add(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    case ALPHABETICAL:
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.ALPHABETICAL);
      keyboardFragment = new KeyboardFragment();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .add(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    case KEYBOARD2:
      keyboardFragment = new KeyboardFragment2();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .add(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    default:
      // TODO
      break;
    }

  }

  @Override
  protected void onResume() {
    super.onResume();
    refreshActivity();

    if (!TypingActivity.getChangeKeyboard())
      return;

    TypingActivity.setChangeKeyboard(false);

    switch (TypingActivity.getKeyboardFragmentType()) {
    case DEFAULT:
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.DEFAULT);
      keyboardFragment = new KeyboardFragment();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .replace(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    case ALPHABETICAL:
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.ALPHABETICAL);
      keyboardFragment = new KeyboardFragment();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .replace(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    case KEYBOARD2:
      keyboardFragment = new KeyboardFragment2();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .replace(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    default:
      // TODO
      break;
    }

  }

  @Override
  protected void onStart() {
    super.onStart();
    ((EditText) findViewById(R.id.contact_name_view))
        .setOnTouchListener(new NoTouchListener());
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.smscontact, menu);
    return true;
  }

  @Override
  public IKeyboardListener getKeyboardListener() {
    final EditText messageEditText = (EditText) findViewById(R.id.contact_name_view);
    if (null == messageEditText)
      return null;

    attached = true;
    messageEditText.setOnTouchListener(new NoTouchListener());
    return new KeyboardListener(messageEditText, false);
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return false;
  }

  /**
   * @return current list in this activity
   */
  public ListView getCurrentList() {
    return contactsListView;
  }

  @Override
  public void onOperation(final int c) {
    super.onOperation(c);
    Intent intent;
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      keyboardFragment.click(new InputColor[] { InputColor.blue });
      break;
    case BLUE_ORANGE:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.blue,
          InputColor.orange });
      break;
    case BLUE_RED:
      makeSound(0);
      keyboardFragment
          .click(new InputColor[] { InputColor.red, InputColor.blue });
      break;
    case GREEN:
      if (Controller.smsList.size() == 0)
        return;
      makeSound(0);
      if (current == FragmentType.Keyboard)
        keyboardFragment.click(new InputColor[] { InputColor.green });
      else {
        ListView list = getCurrentList();
        currentRecord = (currentRecord + 1) % list.getCount();
        list.smoothScrollToPosition(currentRecord);
        list.setItemChecked(currentRecord, true);
        list.setSelection(currentRecord);
      }
      break;
    case GREEN_RED:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.green,
          InputColor.red });
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      makeSound(1);
      intent = new Intent(this, SettingsActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      intent.putExtra(LanguageActivity.languageKey, Controller.language);
      startActivity(intent);
      break;
    case LONG_GREEN:
      makeSound(1);
      // Delete all
      keyboardFragment.longClick(new InputColor[] { InputColor.green });
      break;
    case LONG_ORANGE:
      break;
    case LONG_RED:
      makeSound(1);
      finish();
      overridePendingTransition(0, 0);
      break;
    case ORANGE:
      if (Controller.smsList.size() == 0)
        return;
      makeSound(0);
      if (current == FragmentType.Keyboard)
        keyboardFragment.click(new InputColor[] { InputColor.orange });
      else {
        ListView list = getCurrentList();
        currentRecord = (currentRecord - 1 + list.getCount()) % list.getCount();
        list.smoothScrollToPosition(currentRecord);
        list.setItemChecked(currentRecord, true);
        list.setSelection(currentRecord);
      }

      break;
    case ORANGE_GREEN:
      makeSound(0);
      if (current == FragmentType.Keyboard)
        keyboardFragment.click(new InputColor[] { InputColor.orange,
            InputColor.green });
      else {
        current = FragmentType.Keyboard;

        FragmentTransaction transaction = getFragmentManager()
            .beginTransaction();
        transaction.replace(R.id.keyboard_frame, keyboardFragment);
        transaction.addToBackStack(null);
        transaction.commit();
      }
      break;
    case RED:
      makeSound(0);
      if (current == FragmentType.Keyboard)
        keyboardFragment.click(new InputColor[] { InputColor.red });
      else
        selectEntry();
      break;
    default:
      break;
    }
  }

  private void selectEntry() {

    if (Controller.smsList.size() == 0)
      return;

    Intent intent = new Intent(this, ConversationActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);

    if (value.equals("sms")) {
      String phone = Controller.smsList.get(currentRecord).contactPhone;
      String name = Controller.smsList.get(currentRecord).contactName;
      intent.putExtra(ConversationActivity.conversationKey, phone);
      intent.putExtra(ConversationActivity.typeKey, "sms");
      intent.putExtra(ConversationActivity.nameKey, name);
    } else if (value.equals("facebook")) {
      String facebookId = CommsModule.contactsFacebookParsed.get(currentRecord).contactID;
      String name = CommsModule.contactsFacebookParsed.get(currentRecord).contactName;
      intent.putExtra(ConversationActivity.conversationKey, facebookId);
      intent.putExtra(ConversationActivity.typeKey, "facebook");
      intent.putExtra(ConversationActivity.nameKey, name);
    } else {
      String googleId = CommsModule.contactsGTalkParsed.get(currentRecord).contactID;
      String name = CommsModule.contactsGTalkParsed.get(currentRecord).contactName;
      if (googleId != null && googleId.endsWith("gmail.com"))
        name = googleId;
      intent.putExtra(ConversationActivity.conversationKey, googleId);
      intent.putExtra(ConversationActivity.typeKey, "google");
      intent.putExtra(ConversationActivity.nameKey, name);
    }
    startActivity(intent);

  }

  @Override
  public void onScrollingClick() {
    current = FragmentType.Scrolling;
    ScrollingFragment scrolling = ScrollingFragment
        .newInstance(ScrollingFragment.Screen.ContactHistory);

    FragmentTransaction transaction = getFragmentManager().beginTransaction();
    transaction.replace(R.id.keyboard_frame, scrolling);
    transaction.addToBackStack(null);
    transaction.commit();

  }

  // Maybe should be removed. Talk with Owais.
  @Override
  public void onDoneClick() {
    // sendMessage();
  }

  @Override
  public void onSelectClick() {
    selectEntry();
  }

  private boolean attached = false;

  private void refreshActivity() {
    setTitle(R.string.select_contact);
    if (keyboardFragment != null && attached)
      keyboardFragment.refreshFragment();
  }

}
