package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.commsModule.CommsModule;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.BaseKeyboardFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.IActivityWithKeyboard;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.IKeyboardListener;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.ISearchInContactsActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.InputColor;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment.KeyboardStyle;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment2;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardListener;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardType;
import il.ac.technion.cs.ssdl.cs234311.yp09.user.UserInfoFileIO;
import android.annotation.SuppressLint;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.util.SparseArray;
import android.view.Menu;
import android.widget.AbsListView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;


/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 10/1/2014
 * 
 */
@SuppressLint("CutPasteId")
public class ContactActivity extends GeneralActivity implements
    IActivityWithKeyboard, ISearchInContactsActivity {

  /**
   * set of possible fragments
   * 
   */
  public enum FragmentType {
    /**
     * Keyboard fragment
     */
    Keyboard, /**
     * Scrolling fragment
     */
    Scrolling
  }

  /**
   * set of possible service types
   * 
   */
  public enum ServiceType {
    /**
     * SMS service
     */
    SMS, /**
     * Facebook service
     */
    Facebook, /**
     * Google - Talk service
     */
    GTalk
  }

  /**
   * a key for passing an argument to the next activity
   */
  public static final String Service = "service";

  private boolean messageSent = false;

  private BaseKeyboardFragment keyboardFragment;

  private ListView smsListView;
  private ListView googleListView;
  private ListView facebookListView;

  private SMSListViewAdapter smsAdapter;
  private GoogleListViewAdapter googleAdapter;
  private FacebookListViewAdapter facebookAdapter;

  private FragmentType current;

  /**
   * the service that is being used.
   */
  public ServiceType currentService;

  private SparseArray<String> smsReceiver;
  private SparseArray<String> gTalkReceiver;
  private SparseArray<String> facebookReceiver;
  private LongFragment longFragment;
  private ContactFragment contactFragment;

  /**
   * The index of the record in the list that is marked.
   */
  public int currentRecord = 0;

  private void setKeyboardType() {
    try {
      keyboardFragment.setKeyboardType(KeyboardType.SEARCH_IN_CONTACTS);
    } catch (Exception e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

  @SuppressLint("CutPasteId")
  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_contact);

    current = FragmentType.Keyboard;

    smsReceiver = new SparseArray<String>();
    gTalkReceiver = new SparseArray<String>();
    facebookReceiver = new SparseArray<String>();

    longFragment = LongFragment.newInstance(LongFragment.Screen.Contact);
    getFragmentManager().beginTransaction()
        .add(R.id.long_press_info_frame, longFragment).commit();

    contactFragment = new ContactFragment();
    getFragmentManager().beginTransaction()
        .add(R.id.contact_name_frame, contactFragment).commit();

    final ProgressBarFragment progressBarFragment = ProgressBarFragment
        .newInstance(ProgressBarFragment.Screen.Contact);
    getFragmentManager().beginTransaction()
        .add(R.id.progress_bar_frame, progressBarFragment).commit();

    final Intent intent = getIntent();
    final String value = intent.getStringExtra(Service);

    if (value.equals("google")) {
      currentService = ServiceType.GTalk;

      Controller.googleList = CommsModule.contactsGTalkParsed;

      googleListView = (ListView) findViewById(R.id.contacts_list_frame);
      googleListView.setOnTouchListener(new NoTouchListener());
      googleListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
      googleAdapter = new GoogleListViewAdapter(this, R.layout.google_listview,
          Controller.googleList);
      googleListView.setAdapter(googleAdapter);
      googleListView.setItemChecked(0, true);

      setTitle(R.string.google_contacts);

    }

    if (value.equals("sms")) {

      currentService = ServiceType.SMS;
      smsListView = (ListView) findViewById(R.id.contacts_list_frame);
      smsListView.setOnTouchListener(new NoTouchListener());
      smsListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
      smsAdapter = new SMSListViewAdapter(this, R.layout.sms_listview,
          Controller.smsList);
      smsListView.setAdapter(smsAdapter);
      smsListView.setItemChecked(0, true);

      setTitle(R.string.sms);
    }

    if (value.equals("facebook")) {
      currentService = ServiceType.Facebook;

      Controller.facebookList = CommsModule.contactsFacebookParsed;

      facebookListView = (ListView) findViewById(R.id.contacts_list_frame);
      facebookListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
      facebookAdapter = new FacebookListViewAdapter(this,
          R.layout.facebook_listview, Controller.facebookList);
      facebookListView.setAdapter(facebookAdapter);
      facebookListView.setItemChecked(0, true);

      setTitle(R.string.facebook_contacts);
    }

    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame, mFBFragment).commit();

    switch (TypingActivity.getKeyboardFragmentType()) {
    case DEFAULT:
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.DEFAULT);
      keyboardFragment = new KeyboardFragment();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .add(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    case ALPHABETICAL:
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.ALPHABETICAL);
      keyboardFragment = new KeyboardFragment();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .add(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    case KEYBOARD2:
      keyboardFragment = new KeyboardFragment2();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .add(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    default:
      // TODO
      break;
    }
  }

  @Override
  protected void onResume() {
    super.onResume();
    refreshActivity();

    if (!TypingActivity.getChangeKeyboard())
      return;

    TypingActivity.setChangeKeyboard(false);

    switch (TypingActivity.getKeyboardFragmentType()) {
    case DEFAULT:
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.DEFAULT);
      keyboardFragment = new KeyboardFragment();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .replace(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    case ALPHABETICAL:
      KeyboardFragment.setKeyboardStyle(KeyboardStyle.ALPHABETICAL);
      keyboardFragment = new KeyboardFragment();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .replace(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    case KEYBOARD2:
      keyboardFragment = new KeyboardFragment2();
      setKeyboardType();
      getFragmentManager().beginTransaction()
          .replace(R.id.keyboard_frame, keyboardFragment).commit();
      break;
    default:
      // TODO
      break;
    }

  }

  @Override
  protected void onStart() {
    super.onStart();
    ((EditText) findViewById(R.id.contact_name_view))
        .setOnTouchListener(new NoTouchListener());
  }

  @Override
  public boolean onCreateOptionsMenu(final Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.sms_contact, menu);
    return true;
  }

  @Override
  public IKeyboardListener getKeyboardListener() {
    final EditText messageEditText = (EditText) findViewById(R.id.contact_name_view);
    if (null == messageEditText)
      return null;

    attached = true;
    messageEditText.setOnTouchListener(new NoTouchListener());
    return new KeyboardListener(messageEditText, false);
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return false;
  }

  /**
   * @return current list in this activity
   */
  public ListView getCurrentList() {
    switch (currentService) {
    case Facebook:
      return facebookListView;
    case GTalk:
      return googleListView;
    case SMS:
      return smsListView;
    default:
      break;
    }

    return null;
  }

  @Override
  public void onOperation(final int c) {
    super.onOperation(c);
    Intent intent;
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      makeSound(0);
      if (current == FragmentType.Keyboard)
        keyboardFragment.click(new InputColor[] { InputColor.blue });
      else
        sendMessage();
      break;
    case BLUE_ORANGE:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.blue,
          InputColor.orange });
      break;
    case BLUE_RED:
      makeSound(0);
      keyboardFragment
          .click(new InputColor[] { InputColor.red, InputColor.blue });
      break;
    case GREEN:
      makeSound(0);
      if (current == FragmentType.Keyboard)
        keyboardFragment.click(new InputColor[] { InputColor.green });
      else {
        ListView list = getCurrentList();
        if (list.getCount() == 0)
          return;
        currentRecord = (currentRecord + 1) % list.getCount();
        list.smoothScrollToPosition(currentRecord);
        list.setItemChecked(currentRecord, true);
        list.setSelection(currentRecord);
      }
      break;
    case GREEN_RED:
      makeSound(0);
      keyboardFragment.click(new InputColor[] { InputColor.green,
          InputColor.red });
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      makeSound(1);
      intent = new Intent(this, SettingsActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      intent.putExtra(LanguageActivity.languageKey, Controller.language);
      startActivity(intent);
      break;
    case LONG_GREEN:
      makeSound(1);
      // Delete all
      keyboardFragment.longClick(new InputColor[] { InputColor.green });
      break;
    case LONG_ORANGE:
      break;
    case LONG_RED:
      makeSound(1);
      finish();
      overridePendingTransition(0, 0);
      break;
    case ORANGE:
      makeSound(0);
      if (current == FragmentType.Keyboard)
        keyboardFragment.click(new InputColor[] { InputColor.orange });
      else {
        ListView list = getCurrentList();
        if (list.getCount() == 0)
          return;
        currentRecord = (currentRecord - 1 + list.getCount()) % list.getCount();
        list.smoothScrollToPosition(currentRecord);
        list.setItemChecked(currentRecord, true);
        list.setSelection(currentRecord);
      }

      break;
    case ORANGE_GREEN:
      makeSound(0);
      if (current == FragmentType.Keyboard)
        keyboardFragment.click(new InputColor[] { InputColor.orange,
            InputColor.green });
      else {
        current = FragmentType.Keyboard;

        FragmentTransaction transaction = getFragmentManager()
            .beginTransaction();
        transaction.replace(R.id.keyboard_frame, keyboardFragment);
        transaction.addToBackStack(null);
        transaction.commit();
      }
      break;
    case RED:
      makeSound(0);
      if (current == FragmentType.Keyboard)
        keyboardFragment.click(new InputColor[] { InputColor.red });
      else
        selectEntry();
      break;
    default:
      break;
    }
  }

  private void selectEntry() {
    switch (currentService) {
    case SMS:
      if (Controller.smsList.size() == 0)
        return;
      if (Controller.smsList.get(currentRecord).checked == true) {
        smsReceiver.delete(currentRecord);
        Controller.smsList.get(currentRecord).checked = false;
      } else {
        smsReceiver.append(currentRecord,
            Controller.smsList.get(currentRecord).contactPhone);
        Controller.smsList.get(currentRecord).checked = true;
      }
      smsAdapter.notifyDataSetChanged();
      break;
    case Facebook:
      if (Controller.facebookList.size() == 0)
        return;
      if (Controller.facebookList.get(currentRecord).checked == true) {
        facebookReceiver.delete(currentRecord);
        Controller.facebookList.get(currentRecord).checked = false;
      } else {
        facebookReceiver.append(currentRecord,
            Controller.facebookList.get(currentRecord).contactID);
        Controller.facebookList.get(currentRecord).checked = true;
      }
      facebookAdapter.notifyDataSetChanged();
      break;
    case GTalk:
      if (Controller.googleList.size() == 0)
        return;
      if (Controller.googleList.get(currentRecord).checked == true) {
        gTalkReceiver.delete(currentRecord);
        Controller.googleList.get(currentRecord).checked = false;
      } else {
        gTalkReceiver.append(currentRecord,
            Controller.googleList.get(currentRecord).contactID);
        Controller.googleList.get(currentRecord).checked = true;
      }
      googleAdapter.notifyDataSetChanged();
      break;
    default:
      break;
    }
  }

  @Override
  public void onScrollingClick() {
    current = FragmentType.Scrolling;
    ScrollingFragment scrolling = ScrollingFragment
        .newInstance(ScrollingFragment.Screen.Contact);

    FragmentTransaction transaction = getFragmentManager().beginTransaction();
    transaction.replace(R.id.keyboard_frame, scrolling);
    transaction.addToBackStack(null);
    transaction.commit();

  }

  private void sendSms() {
    if (Controller.message == null || Controller.message.equals(""))
      return;

    for (int i = 0; i < smsReceiver.size(); i++) {
      int key = smsReceiver.keyAt(i);

      // get the object by the key.
      String receiver = "+972" + smsReceiver.get(key).substring(1);
      CommsModule.sendSMS(Controller.message, receiver);

      messageSent = true;
    }

    if (messageSent)
      return;

    // Maybe the user have inserted a phone number
    EditText contactEditText = (EditText) findViewById(R.id.contact_name_view);
    if (null == contactEditText)
      return;

    String contactNumber = contactEditText.getText().toString();
    if (validNumber(contactNumber)) {
      String receiver = "+972" + contactNumber.substring(1);
      CommsModule.sendSMS(Controller.message, receiver);

      messageSent = true;
    }
  }

  private static boolean validNumber(String number) {
    if (null == number || number.length() != 10)
      return false;

    for (char ch : number.toCharArray())
      if (!(ch >= '0' && ch <= '9'))
        return false;

    return true;
  }

  private void sendGoogleTalk() {
    if (Controller.message == null || Controller.message.equals(""))
      return;

    for (int i = 0; i < gTalkReceiver.size(); i++) {
      int key = gTalkReceiver.keyAt(i);

      // get the object by the key.
      String receiver = gTalkReceiver.get(key);
      GeneralActivity.commsModule
          .sendGTalkMessage(Controller.message, receiver);

      messageSent = true;
    }
  }

  private void sendFacebook() {
    if (Controller.message == null || Controller.message.equals(""))
      return;

    for (int i = 0; i < facebookReceiver.size(); i++) {
      int key = facebookReceiver.keyAt(i);

      // get the object by the key.
      String receiver = facebookReceiver.get(key);
      GeneralActivity.commsModule.sendFacebookMessage(Controller.message,
          receiver);

      messageSent = true;
    }
  }

  /**
   * a method for sending a message
   */
  public void sendMessage() {
    switch (currentService) {
    case SMS:
      sendSms();
      break;
    case Facebook:
      sendFacebook();
      break;
    case GTalk:
      sendGoogleTalk();
      break;
    default:
      break;
    }

    if (!messageSent) {
      Toast.makeText(this, R.string.no_destination, Toast.LENGTH_LONG).show();
      return;
    }
    messageSent = false;

    // Toast.makeText(this, ":)", Toast.LENGTH_LONG).show();

    uncheckAllRecords();

    Intent intent = new Intent(this, TypingActivity.class);
    startActivity(intent);
  }

  @Override
  public void onDoneClick() {
    sendMessage();
  }

  private void uncheckAllRecords() {
    if (Controller.smsList != null)
      for (int i = 0; i < Controller.smsList.size(); i++)
        Controller.smsList.get(i).checked = false;
    if (Controller.facebookList != null)
      for (int i = 0; i < Controller.facebookList.size(); i++)
        Controller.facebookList.get(i).checked = false;
    if (Controller.googleList != null)
      for (int i = 0; i < Controller.googleList.size(); i++)
        Controller.googleList.get(i).checked = false;

  }

  @Override
  public void onSelectClick() {
    selectEntry();
  }

  private boolean attached = false;

  private void refreshActivity() {
    if (keyboardFragment != null && attached && attached)
      keyboardFragment.refreshFragment();

    switch (currentService) {
    case SMS:
      setTitle(R.string.sms);
      break;
    case Facebook:
      setTitle(R.string.facebook_contacts);
      UserInfoFileIO.writeInfoToFile(this, LoginActivity.facebookUser,
          LoginActivity.facebookPass, "facebook");
      break;
    case GTalk:
      setTitle(R.string.google_contacts);
      UserInfoFileIO.writeInfoToFile(this, LoginActivity.googleUser,
          LoginActivity.googlePass, "google");
      break;
    default:
      break;
    }

    // Owais:
    // owaisFragment.refreshFragment();

  }
}
