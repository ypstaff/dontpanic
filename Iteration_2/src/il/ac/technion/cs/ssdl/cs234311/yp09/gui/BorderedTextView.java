package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.widget.TextView;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 3/1/2014
 * 
 */
public class BorderedTextView extends TextView {
  private final Paint paint = new Paint();

  /**
   * All possible styles of a border
   * 
   */
  public enum Style {
    /**
     * Top side border
     */
    BORDER_TOP,
    /**
     * right side border
     */
    BORDER_RIGHT,
    /**
     * bottom side border
     */
    BORDER_BOTTOM,
    /**
     * left side border
     */
    BORDER_LEFT;
  }

  private Border[] borders;

  /**
   * @param context
   *          current context
   * @param color
   *          color of the border
   */
  public BorderedTextView(final Context context, final int color) {
    super(context);
    paint.setStyle(Paint.Style.STROKE);
    paint.setColor(Color.BLACK);
    paint.setStrokeWidth(100);
  }

  /**
   * @param context
   *          current context
   */
  public BorderedTextView(final Context context) {
    super(context);
  }

  @Override
  protected void onDraw(final Canvas canvas) {
    super.onDraw(canvas);
    if (borders == null)
      return;

    for (final Border border : borders) {
      paint.setColor(border.getColor());
      paint.setStrokeWidth(border.getWidth());

      if (border.getStyle() == Style.BORDER_TOP)
        canvas.drawLine(0, 0, getWidth(), 0, paint);
      else if (border.getStyle() == Style.BORDER_RIGHT)
        canvas.drawLine(getWidth(), 0, getWidth(), getHeight(), paint);
      else if (border.getStyle() == Style.BORDER_BOTTOM)
        canvas.drawLine(0, getHeight(), getWidth(), getHeight(), paint);
      else if (border.getStyle() == Style.BORDER_LEFT)
        canvas.drawLine(0, 0, 0, getHeight(), paint);
    }
  }

  /**
   * @return the TextView's 4 borders
   */
  public Border[] getBorders() {
    return borders;
  }

  /**
   * @param borders
   *          borders initialized by the user
   */
  public void setBorders(final Border[] borders) {
    this.borders = borders;
  }
}
