package il.ac.technion.cs.ssdl.cs234311.yp09.keyboards;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.ContactActivity;

import java.util.Locale;
import java.util.Timer;
import java.util.TimerTask;

import android.annotation.SuppressLint;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TableRow;
import android.widget.TextView;


/**
 * A fragment for typing text, will be used in several activities
 * 
 * @date 4/1/2013
 * @email owais.musa@gmail.com
 * @author Owais Musa
 * 
 */
public class KeyboardFragment extends BaseKeyboardFragment {

  /**
   * Set this variable to true if you are debugging!
   */
  public static boolean debugMode = false;

  /**
   * @author Owais Musa
   * 
   */
  public enum KeyboardStyle {
    /**
     * 
     */
    DEFAULT,
    /**
     * 
     */
    ALPHABETICAL
  }

  /**
   * @param keyboardStyle
   *          keyboard style
   */
  public static void setKeyboardStyle(KeyboardStyle keyboardStyle) {
    mKeyboardStyle = keyboardStyle;
  }

  private static KeyboardStyle mKeyboardStyle = KeyboardStyle.DEFAULT;

  private DefaultKeyboardLayoutFragment mDefaultKeyboardLayoutFragment;
  private AlphabeticcalKeyboardFragment mAlphabeticcalKeyboardFragment;

  // For tagging
  protected static final String KEYBOARD_FRAGMENT = "T09_KEYBOARD_FRAGMENT";

  private static final int TIME_THRESHOLD = 1200;

  int lastClickedColumnIndex = -1;
  long mPressTime = 0;
  private Timer mTimer;

  private char[][] mCharactersArrays = null;
  private int[] mMarkedCharacter;
  private boolean mUpperCase = false;

  private String[] mSmiles = null;

  private TableRow mMainTableRow;
  private TableRow mSecondaryTableRow;
  private TableRow mColorsTableRow;
  private TableRow mEditingTableRow;

  private TextView[] mainTextViews;
  private TextView[] secondaryTextViews;

  private boolean mSwitched = false;

  private boolean meditWithShift = false;

  /**
   * @return mSmiles
   */
  public String[] getSmiles() {
    return mSmiles;
  }

  /**
   * @author Owais
   * 
   */
  public enum KeyboardContent {
    /**
     * 
     */
    LETTERS,
    /**
     * 
     */
    NUMBERS_AND_SYMBOLS,
    /**
     * 
     */
    SMILIES
  }

  private KeyboardContent mKeyboardContent = KeyboardContent.LETTERS;

  private enum EditingContent {
    DEFAULT, EDIT_TEXT;
  }

  private EditingContent MEditingContent = EditingContent.DEFAULT;

  /**
   * @return mKeyboardContent
   */
  public KeyboardContent getKeyboardContent() {
    return mKeyboardContent;
  }

  /**
   * @return mSwitched
   */
  public boolean getSwitched() {
    return mSwitched;
  }

  /**
   * @return mCharactersArrays
   */
  public char[][] getCharactersArrays() {
    return mCharactersArrays;
  }

  /**
   * @return mMarkedCharacter
   */
  public int[] getMarkedCharacter() {
    return mMarkedCharacter;
  }

  private boolean markNextCharacter(final int index) {
    if (mMarkedCharacter[index] == mCharactersArrays[index].length - 1) {
      mMarkedCharacter[index] = -1;
      return false;
    }

    mMarkedCharacter[index] = mMarkedCharacter[index] + 1;
    return true;
  }

  // It is not private to avoid warnings in chooseColumn
  void resetForChooseColumn() {
    mPressTime = 0;
    lastClickedColumnIndex = -1;
    resetContent();
    updateTableRows();
  }

  /**
   * Call this function to tell the fragment that a specific "column" was
   * "clicked". Note: this method should NOT be called from the activities!
   * instead, call click
   * 
   * @param index
   *          column index to be "clicked"
   */
  private void chooseColumn(final int index) {
    if (mKeyboardListener == null)
      throw new ClassCastException(getActivity().toString()
          + " must set KeyboardListener!");

    assert index >= 0 && index < mCharactersArrays.length;

    if (mKeyboardContent == KeyboardContent.SMILIES) {
      mKeyboardListener.insertChars(mSmiles[index]);
      mKeyboardContent = KeyboardContent.LETTERS;
      resetForChooseColumn();
      return;
    }

    boolean insertNewCharWithoutSave = false;

    if (lastClickedColumnIndex != index)
      resetContent();
    else if (mPressTime > 0) {
      final String text = mKeyboardListener.getText();

      if (mKeyboardListener.getCursorPosition() > 0)
        assert text.charAt(mKeyboardListener.getCursorPosition() - 1) == mCharactersArrays[index][mMarkedCharacter[index]];

      // mKeyboardListener.deleteChar();
      // TODO fix undo bug!
      mKeyboardListener.deleteWithoutSaving();
      insertNewCharWithoutSave = true;

      if (mMarkedCharacter[index] == mCharactersArrays[index].length - 1) {
        resetForChooseColumn();
        return;
      }
    }
    lastClickedColumnIndex = index;
    final boolean res = markNextCharacter(index);
    assert res;
    updateTableRows();

    if (insertNewCharWithoutSave)
      mKeyboardListener
          .insertCharWithoutSaving(mCharactersArrays[index][mMarkedCharacter[index]]);
    else
      mKeyboardListener
          .insertChar(mCharactersArrays[index][mMarkedCharacter[index]]);

    mPressTime = System.currentTimeMillis();
    mTimer = new Timer();

    final long currentTime = mPressTime;
    mTimer.schedule(new TimerTask() {
      @Override
      public void run() {
        getActivity().runOnUiThread(new Runnable() {
          @Override
          public void run() {
            if (mPressTime == currentTime)
              resetForChooseColumn();
          }
        });
      }
    }, debugMode ? TIME_THRESHOLD * 2 : TIME_THRESHOLD);
  }

  private void defaultEditingClick(final int index) {
    switch (index) {
    case 0:
      // Space
      mKeyboardListener.insertChar(' ');
      break;
    case 1:
      // Other keyboards
      if (mKeyboardContent == KeyboardContent.NUMBERS_AND_SYMBOLS)
        mKeyboardContent = KeyboardContent.LETTERS;
      else
        mKeyboardContent = KeyboardContent.NUMBERS_AND_SYMBOLS;

      // Switch to the new keyboard
      mSwitched = false;

      resetContent();
      updateTableRows();
      break;
    case 2:
      switch (mKeyboardType) {
      case TYPING_DEFAULT:
        // Smiles
        if (mKeyboardType != KeyboardType.TYPING_DEFAULT)
          break;

        if (mKeyboardContent == KeyboardContent.SMILIES)
          mKeyboardContent = KeyboardContent.LETTERS;
        else
          mKeyboardContent = KeyboardContent.SMILIES;

        // Switch to the new keyboard
        mSwitched = false;

        resetContent();
        updateTableRows();
        break;
      case SEARCH_IN_CONTACTS:
        // Scrolling
        mSearchInContactsActivity.onScrollingClick();
        break;
      case USERNAME_PASSWORD:
        // User name or password
        if (mMovedToPasswordTextView)
          mLoginActivity.onSetFocusOnUsername();
        else
          mLoginActivity.onSetFocusOnPassword();

        mMovedToPasswordTextView = mMovedToPasswordTextView ? false : true;

        // Switch to the new keyboard
        mSwitched = false;

        resetContent();
        updateTableRows();
        break;
      default:
        break;
      }

      break;
    case 3:
      switch (mKeyboardType) {
      case TYPING_DEFAULT:
        // Auto complete
        if (mKeyboardType != KeyboardType.TYPING_DEFAULT)
          break;

        mKeyboardListener.nextAutoComplete();
        break;
      case SEARCH_IN_CONTACTS:
        // Done
        mSearchInContactsActivity.onDoneClick();
        break;
      case USERNAME_PASSWORD:
        // Login
        mLoginActivity.onLoginClick();
        break;
      default:
        break;
      }
      break;
    case 4:
      // Delete
      mKeyboardListener.deleteChar();
      break;
    case 5:
      switch (mKeyboardType) {
      case TYPING_DEFAULT:
        // Upper case
        mKeyboardContent = KeyboardContent.LETTERS;
        mUpperCase = mUpperCase ? false : true;

        // Switch to the new keyboard
        mSwitched = false;

        resetContent();
        updateTableRows();
        break;
      case SEARCH_IN_CONTACTS:
        mSearchInContactsActivity.onSelectClick();
        break;
      case USERNAME_PASSWORD:
        // Upper case
        mKeyboardContent = KeyboardContent.LETTERS;
        mUpperCase = mUpperCase ? false : true;

        // Switch to the new keyboard
        mSwitched = false;

        resetContent();
        updateTableRows();
        break;
      default:
        break;

      }
      break;
    case 6:
      // Editing
      MEditingContent = EditingContent.EDIT_TEXT;
      resetContent();
      updateTableRows();
      break;
    default:
      break;
    }
  }

  private void editTextEditingClick(final int index) {
    switch (index) {
    case 0:
      // Move cursor to the left
      if (meditWithShift == true)
        mKeyboardListener.shiftMoveCursorToTheLeft();
      else
        mKeyboardListener.moveCurserToTheLeft();
      break;
    case 1:
      // Shift
      meditWithShift = meditWithShift ? false : true;
      break;
    case 2:
      // Move cursor to the right
      if (meditWithShift == true)
        mKeyboardListener.shiftMoveCursorToTheRight();
      else
        mKeyboardListener.moveCurserToTheRight();
      break;
    case 3:
      // Delete
      if (meditWithShift == true)
        mKeyboardListener.shiftDelete();
      else
        mKeyboardListener.deleteChar();
      break;
    case 4:
      // Enter
      mKeyboardListener.insertChar('\n');
      break;
    case 5:
      // Undo
      mKeyboardListener.undo();
      break;
    case 6:
      // Keyboard
      MEditingContent = EditingContent.DEFAULT;
      resetContent();
      updateTableRows();
      break;
    default:
      break;
    }
  }

  /**
   * Call this function to tell the fragment that a specific "column" was
   * "clicked" when already switched to editing mode. Note: this method should
   * NOT be called from the activities! instead, call click
   * 
   * @param index
   *          column index to be "clicked"
   */
  private void editingClick(final int index) {
    if (MEditingContent == EditingContent.DEFAULT)
      defaultEditingClick(index);
    else
      editTextEditingClick(index);
  }

  private void updateSmilesTableRow() {
    assert mSmiles != null;

    for (int i = 0; i < mSmiles.length; i++) {
      final TextView smileTextView = new TextView(getActivity());

      smileTextView.setText(mSmiles[i]);

      smileTextView.setGravity(Gravity.CENTER);

      smileTextView.setTextColor(!mSwitched ? getResources().getColor(
          R.color.clickable_color) : getResources().getColor(
          R.color.unclickable_color));
      smileTextView.setTextColor(!mSwitched ? getResources().getColor(
          R.color.clickable_color) : getResources().getColor(
          R.color.unclickable_color));

      mSecondaryTableRow.addView(getAppropriateFrameLayout(smileTextView));
    }
  }

  private void updateDefaultKeyboard() {
    mDefaultKeyboardLayoutFragment.updateFragment();
  }

  @Override
  public void refreshFragment() {
    updateTableRows();
  }

  // This method is not private to avoid some warnings in chooseColumn method!
  void updateTableRows() {
    importEditingRow();

    if (mKeyboardStyle == KeyboardStyle.DEFAULT) {
      updateDefaultKeyboard();
      return;
    }

    mMainTableRow.removeAllViews();
    mSecondaryTableRow.removeAllViews();

    if (mKeyboardContent == KeyboardContent.SMILIES) {
      updateSmilesTableRow();
      return;
    }

    mainTextViews = new TextView[mCharactersArrays.length];
    secondaryTextViews = new TextView[mCharactersArrays.length];

    for (int i = 0; i < mCharactersArrays.length; i++) {
      final TextView mainTextView = new TextView(getActivity());
      mainTextViews[i] = mainTextView;

      final TextView secondaryTextView = new TextView(getActivity());
      secondaryTextViews[i] = secondaryTextView;

      String markedColor = getActivity().getResources().getString(
          R.string.marked_color_as_string);

      String mainHtmlChar;
      if (mMarkedCharacter[i] == 0)
        mainHtmlChar = "<font color=\"" + markedColor + "\"><b>"
            + mCharactersArrays[i][0] + "</b></font>";
      else
        mainHtmlChar = mCharactersArrays[i][0] + "";

      mainTextView.setText(Html.fromHtml(mainHtmlChar));

      String htmlLetters = "";
      for (int j = 1; j < mCharactersArrays[i].length; j++)
        if (j == mMarkedCharacter[i])
          htmlLetters += "<font color=\"" + markedColor + "\"><b>"
              + mCharactersArrays[i][j] + "</b></font>";
        else
          htmlLetters += mCharactersArrays[i][j];

      secondaryTextView.setText(Html.fromHtml(htmlLetters));

      mainTextView.setGravity(Gravity.CENTER);
      secondaryTextView.setGravity(Gravity.CENTER);

      mainTextView.setTextColor(!mSwitched ? getResources().getColor(
          R.color.clickable_color) : getResources().getColor(
          R.color.unclickable_color));
      secondaryTextView.setTextColor(!mSwitched ? getResources().getColor(
          R.color.clickable_color) : getResources().getColor(
          R.color.unclickable_color));

      mMainTableRow.addView(getAppropriateFrameLayout(mainTextView));
      mSecondaryTableRow.addView(getAppropriateFrameLayout(secondaryTextView));
    }
  }

  private String[] getCharactersFromResources() {
    String[] $ = null;

    if (mKeyboardContent == KeyboardContent.LETTERS)
      $ = getResources().getStringArray(R.array.lettersArrays);
    else if (mKeyboardContent == KeyboardContent.NUMBERS_AND_SYMBOLS)
      $ = getResources().getStringArray(R.array.numbersAndSymbolsArray);

    return $;
  }

  // This method is not private to avoid some warnings in chooseColumn method!
  void resetContent() {
    if (mKeyboardContent == KeyboardContent.SMILIES) {
      if (mSmiles == null)
        mSmiles = getResources().getStringArray(R.array.keyboardSmiles);
      return;
    }

    String[] characters = { "abcd", "efg", "hijk", "lmno", "pqrs", "tuv",
        "wxyz" };

    if (mKeyboardContent != KeyboardContent.LETTERS
        || mKeyboardStyle == KeyboardStyle.DEFAULT)
      characters = getCharactersFromResources();

    mCharactersArrays = new char[characters.length][];
    mMarkedCharacter = new int[characters.length];

    for (int i = 0; i < characters.length; i++) {
      mMarkedCharacter[i] = -1; // No marked character yet

      if (mKeyboardContent == KeyboardContent.LETTERS) {
        if (mUpperCase)
          mCharactersArrays[i] = characters[i].toUpperCase(Locale.US)
              .toCharArray();
        else
          mCharactersArrays[i] = characters[i].toLowerCase(Locale.US)
              .toCharArray();
      } else
        mCharactersArrays[i] = characters[i].toCharArray();
    }
  }

  private void drawRectangles() {
    final ColorsRowGen colorsRowGen = new ColorsRowGen(mColorsTableRow,
        getActivity());

    final String[] combinationColors = getResources().getStringArray(
        R.array.keyboardInputColorsArrays);

    final InputColor inputColorsArr[][] = new InputColor[combinationColors.length][];

    for (int i = 0; i < combinationColors.length; i++) {
      final String res[] = combinationColors[i].split("-");

      inputColorsArr[i] = new InputColor[res.length];

      for (int j = 0; j < res.length; j++)
        inputColorsArr[i][j] = convertStringToColor(res[j]);
    }

    colorsRowGen.draw(inputColorsArr);
  }

  /**
   * It is public because Default keyboard fragment use it!
   * 
   * @param view
   *          view
   * @return frame lauout with the smile
   */
  public FrameLayout getAppropriateFrameLayout(View view) {
    final TableRow.LayoutParams params = new TableRow.LayoutParams(0,
        LayoutParams.MATCH_PARENT, 1);
    params.gravity = Gravity.CENTER;

    FrameLayout $ = new FrameLayout(getActivity());
    $.setLayoutParams(params);
    $.addView(view);

    return $;
  }

  private void addTextToRow(TableRow row, final int textId) {
    Resources resources = getActivity().getResources();
    TextView textView = new TextView(getActivity());
    textView.setText(resources.getString(textId));
    textView.setTextColor(!mSwitched ? resources
        .getColor(R.color.unclickable_color) : resources
        .getColor(R.color.clickable_color));
    textView.setGravity(Gravity.CENTER);
    row.addView(getAppropriateFrameLayout(textView));
  }

  private void importDefaultEditingRow() {
    addTextToRow(mEditingTableRow, R.string.editing_keyboard_space);

    if (mKeyboardContent == KeyboardContent.NUMBERS_AND_SYMBOLS)
      addTextToRow(mEditingTableRow, R.string.keyboard_letters_icon);
    else
      addImageToRow(mEditingTableRow, !mSwitched ? R.drawable.other_keyboards
          : R.drawable.other_keyboards_black);

    switch (mKeyboardType) {
    case TYPING_DEFAULT:
      if (mKeyboardContent == KeyboardContent.SMILIES)
        addTextToRow(mEditingTableRow, R.string.keyboard_letters_icon);
      else
        addImageToRow(
            mEditingTableRow,
            !mSwitched || mKeyboardType != KeyboardType.TYPING_DEFAULT ? R.drawable.smilies
                : R.drawable.smilies_black);
      addImageToRow(
          mEditingTableRow,
          !mSwitched || mKeyboardType != KeyboardType.TYPING_DEFAULT ? R.drawable.auto_complete
              : R.drawable.auto_complete_black);
      break;
    case USERNAME_PASSWORD:
      if (mMovedToPasswordTextView)
        addTextToRow(mEditingTableRow, R.string.editing_keyboard_username);
      else
        addTextToRow(mEditingTableRow, R.string.editing_keyboard_password);
      addTextToRow(mEditingTableRow, R.string.editing_keyboard_login);
      break;
    case SEARCH_IN_CONTACTS:
      addTextToRow(mEditingTableRow, R.string.editing_keyboard_scrolling);
      if (getActivity() instanceof ContactActivity)
        addTextToRow(mEditingTableRow, R.string.editing_keyboard_done);
      else {
        // Contact history activity!
        Resources resources = getActivity().getResources();
        TextView textView = new TextView(getActivity());
        textView.setText("---");
        textView.setTextColor(!mSwitched ? resources
            .getColor(R.color.unclickable_color) : resources
            .getColor(R.color.clickable_color));
        textView.setGravity(Gravity.CENTER);
        mEditingTableRow.addView(getAppropriateFrameLayout(textView));
      }
      break;
    default:
      break;
    }

    addImageToRow(mEditingTableRow, !mSwitched ? R.drawable.delete
        : R.drawable.delete_black);

    switch (mKeyboardType) {
    case TYPING_DEFAULT:
      addImageToRow(mEditingTableRow, !mSwitched ? R.drawable.upper_case
          : R.drawable.upper_case_black);
      break;
    case USERNAME_PASSWORD:
      addImageToRow(mEditingTableRow, !mSwitched ? R.drawable.upper_case
          : R.drawable.upper_case_black);
      break;
    case SEARCH_IN_CONTACTS:
      addTextToRow(mEditingTableRow, R.string.select);
      break;
    default:
      break;
    }

    addImageToRow(mEditingTableRow, !mSwitched ? R.drawable.editing
        : R.drawable.editing_black);
  }

  private void addImageToRow(final TableRow row, final int imageId) {
    ImageView img;
    img = new ImageView(getActivity());
    img.setImageResource(imageId);
    row.addView(getAppropriateFrameLayout(img));
  }

  /*
   * In the editing row, there is a possibility to choose (editing)
   */
  private void importEditTextEditingRow() {
    addImageToRow(mEditingTableRow, !mSwitched ? R.drawable.move_cursor_left
        : R.drawable.move_cursor_left_black);
    addImageToRow(mEditingTableRow, !mSwitched ? R.drawable.shift
        : R.drawable.shift_black);
    addImageToRow(mEditingTableRow, !mSwitched ? R.drawable.move_cursor_right
        : R.drawable.move_cursor_right_black);
    addImageToRow(mEditingTableRow, !mSwitched ? R.drawable.delete
        : R.drawable.delete_black);
    addImageToRow(mEditingTableRow, !mSwitched ? R.drawable.enter
        : R.drawable.enter_black);
    addImageToRow(mEditingTableRow, !mSwitched ? R.drawable.undo
        : R.drawable.undo_black);
    addImageToRow(mEditingTableRow, !mSwitched ? R.drawable.keyboard
        : R.drawable.keyboard_black);
  }

  private void importEditingRow() {
    mEditingTableRow.removeAllViews();

    if (MEditingContent == EditingContent.DEFAULT)
      importDefaultEditingRow();
    else
      importEditTextEditingRow();
  }

  @SuppressLint("NewApi")
  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {
    final View $ = inflater.inflate(R.layout.keyboard_fragment, container,
        false);

    mColorsTableRow = (TableRow) $.findViewById(R.id.colorsTableRow);
    mEditingTableRow = (TableRow) $.findViewById(R.id.editingTableRow);

    drawRectangles();
    importEditingRow();
    resetContent();

    if (mKeyboardStyle == KeyboardStyle.ALPHABETICAL) {
      mAlphabeticcalKeyboardFragment = new AlphabeticcalKeyboardFragment();
      getChildFragmentManager().beginTransaction()
          .add(R.id.internal_keyboard_frame, mAlphabeticcalKeyboardFragment)
          .commit();
    } else {
      mDefaultKeyboardLayoutFragment = new DefaultKeyboardLayoutFragment();
      getChildFragmentManager().beginTransaction()
          .add(R.id.internal_keyboard_frame, mDefaultKeyboardLayoutFragment)
          .commit();
    }

    return $;
  }

  /**
   * @param mainTableRow
   *          main table row
   * @param secondaryTableRow
   *          secondary table row
   */
  public void attachAlphaBeticalKeyboardFragment(TableRow mainTableRow,
      TableRow secondaryTableRow) {
    mMainTableRow = mainTableRow;
    mSecondaryTableRow = secondaryTableRow;

    resetContent();
    updateTableRows();
  }

  /**
   * Call this function to tell the fragment that a specific colors combination
   * was "clicked".
   * 
   * @param colors
   *          colors combination
   */
  @Override
  public void click(final InputColor colors[]) {
    assert colors != null;

    final String[] combinationColors = getResources().getStringArray(
        R.array.keyboardInputColorsArrays);

    // Maybe this is switch option (blue + red)
    if (colors.length == 2
        && (colors[0] == InputColor.blue && colors[1] == InputColor.red || colors[1] == InputColor.blue
            && colors[0] == InputColor.red)) {
      mSwitched = mSwitched ? false : true;
      updateTableRows();
      return;
    }

    for (int i = 0; i < combinationColors.length; i++) {
      final String res[] = combinationColors[i].split("-");
      if (equalCombination(res, colors)) {
        if (!mSwitched)
          chooseColumn(i);
        else
          editingClick(i);
        return;
      }
    }
  }

  /**
   * @param keyboardType
   *          keyboard type
   * @throws Exception
   *           too late exception :)
   */
  @Override
  public void setKeyboardType(KeyboardType keyboardType) throws Exception {
    // Should not call this method after attaching!
    if (mAttached)
      throw new Exception("too late :(");

    mKeyboardType = keyboardType;
  }

  /**
   * Use this method just when you want to change the listener to point to
   * another text edit in the same activity!
   * 
   * @param keyboardListener
   *          keyboard listener
   */
  @Override
  public void setKeyboardListener(KeyboardListener keyboardListener) {
    assert mKeyboardListener != null;
    mKeyboardListener = keyboardListener;
  }
}
