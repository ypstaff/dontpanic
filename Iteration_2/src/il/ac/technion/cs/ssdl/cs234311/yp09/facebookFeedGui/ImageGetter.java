package il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeedGui;

import java.util.concurrent.ExecutionException;

import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.text.Html;

/**
 * @author Owais Musa
 * 
 */
public class ImageGetter implements Html.ImageGetter {

  @Override
  public Drawable getDrawable(String source) {

    Drawable d = null;

    try {
      d = new LoadImgAsyncTask().execute(source).get();
    } catch (InterruptedException e) {
      e.printStackTrace();
    } catch (ExecutionException e) {
      e.printStackTrace();
    }

    if (null == d)
      return null;

    d.setBounds(0, 0, d.getIntrinsicWidth(), d.getIntrinsicHeight());
    return d;
  }

  private class LoadImgAsyncTask extends AsyncTask<String, Void, Drawable> {
    public LoadImgAsyncTask() {
    }

    @Override
    protected Drawable doInBackground(String... urls) {
      try {
        return Drawable.createFromStream(
            (java.io.InputStream) new java.net.URL(urls[0]).getContent(),
            "src name");
      } catch (Exception e) {
        return null;
      }
    }

    @Override
    protected void onPostExecute(Drawable drawable) {
      // TODO: check this.exception
    }
  }
}
