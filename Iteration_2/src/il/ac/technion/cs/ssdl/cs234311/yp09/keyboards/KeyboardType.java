package il.ac.technion.cs.ssdl.cs234311.yp09.keyboards;

/**
 * These values will be used by keyboard fragment to determine what options to
 * display
 * 
 * @author Owais Musa
 * 
 */
public enum KeyboardType {
  /**
   * Default one
   */
  TYPING_DEFAULT,

  /**
   * Display all options but smiles
   */
  USERNAME_PASSWORD,

  /**
   * Display all options but smiles
   */
  SEARCH_IN_CONTACTS
}
