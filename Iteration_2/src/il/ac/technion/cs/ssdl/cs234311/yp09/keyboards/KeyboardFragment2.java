package il.ac.technion.cs.ssdl.cs234311.yp09.keyboards;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.ContactActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.ContactHistoryActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.LoginActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * @author Itamar
 * 
 */
public class KeyboardFragment2 extends BaseKeyboardFragment {

  private boolean shifted = false;
  private boolean loginPassField = false;

  private enum ValueType {
    LETTER, OTHER, SMILEY
  }

  private ValueType type = ValueType.LETTER;

  /** DEBUG PURPOSES ONLY **/
  public static boolean testMode = false;

  /** DEBUG PURPOSES ONLY **/
  public void setPos(int x, int y) {
    if (testMode) {
      pos[1] = x;
      pos[0] = y;
    }
  }

  /** DEBUG PURPOSES ONLY **/
  public int getX() {
    return pos[1];
  }

  /** DEBUG PURPOSES ONLY **/
  public int getY() {
    return pos[0];
  }

  int[][] grid_id = {
      { R.id.kb2_00, R.id.kb2_01, R.id.kb_02, 0, 0, 0, 0, 0, 0 },
      { R.id.kb2_10, R.id.kb2_11, R.id.kb2_12, R.id.kb2_13, R.id.kb2_14,
          R.id.kb2_15, R.id.kb2_16, R.id.kb2_17, 0 },
      { R.id.kb2_20, R.id.kb2_21, R.id.kb2_22, R.id.kb2_23, R.id.kb2_24,
          R.id.kb2_25, R.id.kb2_26, R.id.kb2_27, R.id.kb2_28 },
      { 0, R.id.kb2_31, R.id.kb2_32, R.id.kb2_33, R.id.kb2_34, R.id.kb2_35,
          R.id.kb2_36, R.id.kb2_37, R.id.kb2_38 },
      { 0, R.id.kb2_41, R.id.kb2_42, R.id.kb2_43, R.id.kb2_44, R.id.kb2_45,
          R.id.kb2_46, R.id.kb2_47, R.id.kb2_48 } };
  int[][] grid_selected = {
      { R.drawable.kb2_option_selected, R.drawable.kb2_extra_selected,
          R.drawable.kb2_extra_selected, 0, 0, 0, 0, 0, 0 },
      { R.drawable.kb2_extra_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected, 0 },
      { R.drawable.kb2_extra_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_blue_orange_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_extra_selected },
      { 0, R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_green_red_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_extra_selected },
      { 0, R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_extra_selected } };
  int[][] grid_shifted = {
      { R.drawable.kb2_option, R.drawable.kb2_extra_shifted,
          R.drawable.kb2_extra_shifted, 0, 0, 0, 0, 0, 0 },
      { R.drawable.kb2_extra, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter, R.drawable.kb2_letter, R.drawable.kb2_letter,
          R.drawable.kb2_letter, R.drawable.kb2_letter, R.drawable.kb2_letter,
          0 },
      { R.drawable.kb2_extra_shifted, R.drawable.kb2_letter,
          R.drawable.kb2_blue_orange, R.drawable.kb2_letter,
          R.drawable.kb2_letter, R.drawable.kb2_letter, R.drawable.kb2_letter,
          R.drawable.kb2_letter, R.drawable.kb2_extra },
      { 0, R.drawable.kb2_letter, R.drawable.kb2_letter, R.drawable.kb2_letter,
          R.drawable.kb2_letter, R.drawable.kb2_letter,
          R.drawable.kb2_green_red, R.drawable.kb2_letter, R.drawable.kb2_extra },
      { 0, R.drawable.kb2_letter, R.drawable.kb2_letter, R.drawable.kb2_letter,
          R.drawable.kb2_letter, R.drawable.kb2_letter, R.drawable.kb2_letter,
          R.drawable.kb2_letter, R.drawable.kb2_extra } };
  int[][] grid_shifted_selected = {
      { R.drawable.kb2_option_selected, R.drawable.kb2_extra_shifted_selected,
          R.drawable.kb2_extra_shifted_selected, 0, 0, 0, 0, 0, 0 },
      { R.drawable.kb2_extra_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected, 0 },
      { R.drawable.kb2_extra_shifted_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_blue_orange_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_extra_selected },
      { 0, R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_green_red_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_extra_selected },
      { 0, R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_letter_selected,
          R.drawable.kb2_letter_selected, R.drawable.kb2_extra_selected } };
  int[][] grid_letters = {
      { 0, R.drawable.cursor_left, R.drawable.cursor_right, 0, 0, 0, 0, 0, 0 },
      { R.drawable.undo, 'a', 'b', 'c', 'd', 'e', 'f', 'g', 0 },
      { R.drawable.shift, 'h', R.drawable.delete_black, 'i', 'j', 'k', 'l',
          'm', R.drawable.other_keyboards },
      { 0, 'n', 'o', 'p', 'q', 'r', R.drawable.space_black, 's',
          R.drawable.enter },
      { 0, 't', 'u', 'v', 'w', 'x', 'y', 'z', R.drawable.smilies } };
  int[][] grid_letters_shifted = {
      { 0, R.drawable.ic_av_rewind, R.drawable.ic_av_fast_forward, 0, 0, 0, 0,
          0, 0 },
      { R.drawable.undo, 'A', 'B', 'C', 'D', 'E', 'F', 'G', 0 },
      { R.drawable.shift, 'H', R.drawable.ic_delete_word_black, 'I', 'J', 'K',
          'L', 'M', R.drawable.other_keyboards },
      { 0, 'N', 'O', 'P', 'Q', 'R', R.drawable.space_black, 'S',
          R.drawable.enter },
      { 0, 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', R.drawable.smilies } };
  int[][] grid_others = {
      { 0, R.drawable.cursor_left, R.drawable.cursor_right, 0, 0, 0, 0, 0, 0 },
      { R.drawable.undo, '0', '1', '2', '3', '4', '5', '6', 0 },
      { R.drawable.shift, '7', R.drawable.delete_black, '8', '9', '@', '#',
          '$', R.drawable.other_keyboards },
      { 0, '%', '&', '-', '+', '.', R.drawable.space_black, ',',
          R.drawable.enter },
      { 0, '*', '\"', '\'', ':', ';', '!', '?', R.drawable.smilies } };
  int[][] grid_others_shifted = {
      { 0, R.drawable.ic_av_rewind, R.drawable.ic_av_fast_forward, 0, 0, 0, 0,
          0, 0 },
      { R.drawable.undo, '~', '`', '|', '\u00b7', '\u221a', '\u03c0', '\u00f7',
          0 },
      { R.drawable.shift, '\u00d7', R.drawable.ic_delete_word_black, '\u00a3',
          '\u00a2', '\u20ac', '\u00a5', '^', R.drawable.other_keyboards },
      { 0, '\u00b0', '=', '{', '}', '(', R.drawable.space_black, ')',
          R.drawable.enter },
      { 0, '\\', '/', '_', '<', '>', '[', ']', R.drawable.smilies } };
  int[][][][] shifts = {
      // regular keyboard
      {
          // blue shifts
          { { 0, -1, 1, 0, 0, 0, 0, 0 }, { -7, 1, 1, 1, 1, 1, 1, 1, 0 },
              { -8, 1, 1, 1, 1, 1, 1, 1, 1 }, { 0, -7, 1, 1, 1, 1, 1, 1, 1 },
              { 0, -7, 1, 1, 1, 1, 1, 1, 1 } },
          // orange shifts
          { { 0, -4, -4, 0, 0, 0, 0, 0 }, { -1, 1, 1, -3, -3, -3, -3, -3, 0 },
              { 1, 1, 1, 1, 1, 1, 1, 1, -2 }, { 0, 1, 1, 1, 1, 1, 1, 1, 1 },
              { 0, 1, 1, 1, 1, 1, 1, 1, 1 } },
          // green shifts
          { { 0, 1, 1, 0, 0, 0, 0, 0 }, { 1, 1, 1, 1, 1, 1, 1, 1, 0 },
              { -1, 1, 1, 1, 1, 1, 1, 1, 1 }, { 0, 1, 1, 1, 1, 1, 1, 1, 1 },
              { 0, -4, -4, -3, -3, -3, -3, -3, -2 } },
          // red shifts
          { { 0, 1, -1, 0, 0, 0, 0, 0 }, { 1, 1, 1, 1, 1, 1, 1, -7, 0 },
              { 1, 1, 1, 1, 1, 1, 1, 1, -8 }, { 0, 1, 1, 1, 1, 1, 1, 1, -7 },
              { 0, 1, 1, 1, 1, 1, 1, 1, -7 } } },
      // login keyboard
      {
          // blue shifts
          { { -2, 1, 1, 0, 0, 0, 0, 0 }, { -7, 1, 1, 1, 1, 1, 1, 1, 0 },
              { -8, 1, 1, 1, 1, 1, 1, 1, 1 }, { 0, -7, 1, 1, 1, 1, 1, 1, 1 },
              { 0, -7, 1, 1, 1, 1, 1, 1, 1 } },
          // orange shifts
          { { -2, -4, -4, 0, 0, 0, 0, 0 }, { 1, 1, 1, -3, -3, -3, -3, -3, 0 },
              { 1, 1, 1, 1, 1, 1, 1, 1, -2 }, { 0, 1, 1, 1, 1, 1, 1, 1, 1 },
              { 0, 1, 1, 1, 1, 1, 1, 1, 1 } },
          // green shifts
          { { 1, 1, 1, 0, 0, 0, 0, 0 }, { 1, 1, 1, 1, 1, 1, 1, 1, 0 },
              { -2, 1, 1, 1, 1, 1, 1, 1, 1 }, { 0, 1, 1, 1, 1, 1, 1, 1, 1 },
              { 0, -4, -4, -3, -3, -3, -3, -3, -2 } },
          // red shifts
          { { 1, 1, -2, 0, 0, 0, 0, 0 }, { 1, 1, 1, 1, 1, 1, 1, -7, 0 },
              { 1, 1, 1, 1, 1, 1, 1, 1, -8 }, { 0, 1, 1, 1, 1, 1, 1, 1, -7 },
              { 0, 1, 1, 1, 1, 1, 1, 1, -7 } } } };
  int[] pos = { 2, 2 };

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {
    Log.d("KB2_2", "onCreate");
    final View $ = inflater.inflate(R.layout.keyboard2, container, false);

    // set text
    for (int i = 0; i < 5; i++)
      for (int j = 0; j < 9; j++)
        // set shifted background
        if (grid_selected[i][j] == R.drawable.kb2_letter_selected) {
          TextView t = (TextView) $.findViewById(grid_id[i][j]);
          switch (type) {
          case LETTER:
            if (shifted)
              t.setText(String.valueOf((char) grid_letters_shifted[i][j]));
            else
              t.setText(String.valueOf((char) grid_letters[i][j]));
            break;
          case OTHER:
            if (shifted)
              t.setText(String.valueOf((char) grid_others_shifted[i][j]));
            else
              t.setText(String.valueOf((char) grid_others[i][j]));
            break;
          /*
           * case SMILEY: if (shifted) t.setText(String.valueOf((char)
           * grid_smileys_shifted[i][j])); else t.setText(String.valueOf((char)
           * grid_smileys[i][j])); break;
           */
          default:
            break;
          }
        } else if (grid_selected[i][j] != 0
            && grid_selected[i][j] != R.drawable.kb2_option_selected) {
          ImageView img = (ImageView) $.findViewById(grid_id[i][j]);
          if (shifted) {
            img.setBackgroundResource(grid_shifted[i][j]);
            img.setImageDrawable(getResources().getDrawable(
                grid_letters_shifted[i][j]));
          } else
            img.setImageDrawable(getResources().getDrawable(grid_letters[i][j]));
        }
    View v;
    // hide login if applicable
    if (!(getActivity() instanceof LoginActivity)) {
      v = $.findViewById(grid_id[0][0]);
      v.setVisibility(View.INVISIBLE);
    }
    // highlight selection
    v = $.findViewById(grid_id[pos[0]][pos[1]]);
    if (shifted)
      v.setBackground(getResources().getDrawable(
          grid_shifted_selected[pos[0]][pos[1]]));
    else
      v.setBackground(getResources().getDrawable(grid_selected[pos[0]][pos[1]]));

    // highlight keyboard type
    switch (type) {
    case OTHER:
      v = $.findViewById(grid_id[2][8]);
      // if others is currently selected
      if (pos[0] == 2 && pos[1] == 8)
        v.setBackgroundResource(R.drawable.kb2_extra_shifted_selected);
      else
        v.setBackgroundResource(R.drawable.kb2_extra_shifted);
      break;
    case SMILEY:
      v = $.findViewById(grid_id[4][8]);
      // if others is currently selected
      if (pos[0] == 4 && pos[1] == 8)
        v.setBackgroundResource(R.drawable.kb2_extra_shifted_selected);
      else
        v.setBackgroundResource(R.drawable.kb2_extra_shifted);
      break;
    default:
      break;
    }
    return $;
  }

  @Override
  public void click(InputColor[] colors) {
    // move position
    if (colors.length == 1) {
      if (colors[0] == convertStringToColor("blue"))
        if (getActivity() instanceof LoginActivity)
          pos[1] -= shifts[1][0][pos[0]][pos[1]];
        else
          pos[1] -= shifts[0][0][pos[0]][pos[1]];
      else if (colors[0] == convertStringToColor("orange"))
        if (getActivity() instanceof LoginActivity)
          pos[0] -= shifts[1][1][pos[0]][pos[1]];
        else
          pos[0] -= shifts[0][1][pos[0]][pos[1]];
      else if (colors[0] == convertStringToColor("green"))
        if (getActivity() instanceof LoginActivity)
          pos[0] += shifts[1][2][pos[0]][pos[1]];
        else
          pos[0] += shifts[0][2][pos[0]][pos[1]];
      else if (colors[0] == convertStringToColor("red"))
        if (getActivity() instanceof LoginActivity)
          pos[1] += shifts[1][3][pos[0]][pos[1]];
        else
          pos[1] += shifts[0][3][pos[0]][pos[1]];
    } else if (colors.length == 2)
      // select
      if (colors[0] == convertStringToColor("red")
          && colors[1] == convertStringToColor("blue")) {
        // login
        if (pos[0] == 0 && pos[1] == 0)
          ((LoginActivity) getActivity()).onLoginClick();
        // left cursor
        else if (pos[0] == 0 && pos[1] == 1)
          if (shifted)
            mKeyboardListener.shiftMoveCursorToTheLeft();
          else
            mKeyboardListener.moveCurserToTheLeft();
        // right cursor
        else if (pos[0] == 0 && pos[1] == 2)
          if (shifted)
            mKeyboardListener.shiftMoveCursorToTheRight();
          else
            mKeyboardListener.moveCurserToTheRight();
        // undo
        else if (pos[0] == 1 && pos[1] == 0)
          mKeyboardListener.undo();
        // shift
        else if (pos[0] == 2 && pos[1] == 0)
          shifted = !shifted;
        // delete
        else if (pos[0] == 2 && pos[1] == 2)
          if (shifted)
            mKeyboardListener.shiftDelete();
          else
            mKeyboardListener.deleteChar();
        // others
        else if (pos[0] == 2 && pos[1] == 8)
          type = type == ValueType.OTHER ? ValueType.LETTER : ValueType.OTHER;
        // space
        else if (pos[0] == 3 && pos[1] == 6)
          mKeyboardListener.insertChar(' ');
        // enter
        else if (pos[0] == 3 && pos[1] == 8)
          mKeyboardListener.insertChar('\n');
        // smileys
        else if (pos[0] == 4 && pos[1] == 8)
          type = type == ValueType.SMILEY ? ValueType.LETTER : ValueType.SMILEY;
        // insert character
        else if (shifted)
          switch (type) {
          case LETTER:
            mKeyboardListener
                .insertChar((char) grid_letters_shifted[pos[0]][pos[1]]);
            break;
          case OTHER:
            mKeyboardListener
                .insertChar((char) grid_others_shifted[pos[0]][pos[1]]);
            break;
          // case SMILEY:
          // mKeyboardListener
          // .insertChar((char) grid_smileys_shifted[pos[0]][pos[1]]);
          // break;
          default:
            break;
          }
        // insert shifted character
        else
          switch (type) {
          case LETTER:
            mKeyboardListener.insertChar((char) grid_letters[pos[0]][pos[1]]);
            break;
          case OTHER:
            mKeyboardListener.insertChar((char) grid_others[pos[0]][pos[1]]);
            break;
          // case SMILEY:
          // mKeyboardListener.insertChar((char) grid_smileys[pos[0]][pos[1]]);
          // break;
          default:
            break;
          }
      }
      // move to delete
      else if (colors[0] == convertStringToColor("blue")
          && colors[1] == convertStringToColor("orange"))
        pos = new int[] { 2, 2 };
      // move to space
      else if (colors[0] == convertStringToColor("green")
          && colors[1] == convertStringToColor("red"))
        pos = new int[] { 3, 6 };
      // autocomplete or switch
      else if (colors[0] == convertStringToColor("orange")
          && colors[1] == convertStringToColor("green"))
        if (getActivity() instanceof LoginActivity) {
          if (!loginPassField)
            ((LoginActivity) getActivity()).onSetFocusOnPassword();
          else
            ((LoginActivity) getActivity()).onSetFocusOnUsername();
          loginPassField = !loginPassField;
        } else if (getActivity() instanceof ContactActivity) {
          ((ContactActivity) getActivity()).onScrollingClick();
          return;
        } else if (getActivity() instanceof ContactHistoryActivity) {
          ((ContactHistoryActivity) getActivity()).onScrollingClick();
          return;
        } else
          mKeyboardListener.nextAutoComplete();
    refreshFragment();
  }

  @Override
  public void refreshFragment() {
    onDestroy();
    getActivity()
        .getFragmentManager()
        .beginTransaction()
        .remove(this)
        .replace(
            getActivity() instanceof LoginActivity ? R.id.typing_frame
                : R.id.keyboard_frame, this).addToBackStack(null).commit();
  }
}
