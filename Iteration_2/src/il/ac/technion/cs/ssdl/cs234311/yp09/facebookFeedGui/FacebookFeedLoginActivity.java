package il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeedGui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.facebook.LoggingBehavior;
import com.facebook.Session;
import com.facebook.Session.OpenRequest;
import com.facebook.SessionLoginBehavior;
import com.facebook.SessionState;
import com.facebook.Settings;

/**
 * @author Owais
 * 
 */
public class FacebookFeedLoginActivity extends Activity {
  private static final String URL_PREFIX_FRIENDS = "https://graph.facebook.com/me/friends?access_token=";

  private TextView textInstructionsOrLink;
  private Button buttonLoginLogout;
  private Button showPostsButton;
  private Button publishLoginButton;

  @SuppressWarnings("synthetic-access")
  private Session.StatusCallback statusCallback = new SessionStatusCallback();

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_facebook_feed_login);
    buttonLoginLogout = (Button) findViewById(R.id.buttonLoginLogout);
    showPostsButton = (Button) findViewById(R.id.showPostsButton);
    publishLoginButton = (Button) findViewById(R.id.publishLoginButton);
    textInstructionsOrLink = (TextView) findViewById(R.id.instructionsOrLink);

    Settings.addLoggingBehavior(LoggingBehavior.INCLUDE_ACCESS_TOKENS);

    Session session = Session.getActiveSession();
    if (session == null) {
      if (savedInstanceState != null)
        session = Session.restoreSession(this, null, statusCallback,
            savedInstanceState);

      if (session == null)
        session = new Session(this);
      Session.setActiveSession(session);
      if (session.getState().equals(SessionState.CREATED_TOKEN_LOADED))
        session.openForRead(new Session.OpenRequest(this)
            .setCallback(statusCallback));
    }

    updateView();
  }

  @Override
  public void onStart() {
    super.onStart();
    Session.getActiveSession().addCallback(statusCallback);
  }

  @Override
  public void onStop() {
    super.onStop();
    Session.getActiveSession().removeCallback(statusCallback);
  }

  @Override
  public void onActivityResult(int requestCode, int resultCode, Intent data) {
    super.onActivityResult(requestCode, resultCode, data);
    Session.getActiveSession().onActivityResult(this, requestCode, resultCode,
        data);
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    Session session = Session.getActiveSession();
    Session.saveSession(session, outState);
  }

  private void updateView() {
    Session session = Session.getActiveSession();
    showPostsButton.setOnClickListener(new OnClickListener() {
      @SuppressWarnings("synthetic-access")
      @Override
      public void onClick(View view) {
        onClickShowPosts();
      }
    });
    publishLoginButton.setOnClickListener(new OnClickListener() {
      @SuppressWarnings("synthetic-access")
      @Override
      public void onClick(View view) {
        onClickLoginWithPublish();
      }
    });
    if (session.isOpened()) {
      textInstructionsOrLink.setText(URL_PREFIX_FRIENDS
          + session.getAccessToken());
      buttonLoginLogout.setText("temp logout");
      buttonLoginLogout.setOnClickListener(new OnClickListener() {
        @SuppressWarnings("synthetic-access")
        @Override
        public void onClick(View view) {
          onClickLogout();
        }
      });
    } else {
      textInstructionsOrLink.setText("temp instructions");
      buttonLoginLogout.setText(R.string.login);
      buttonLoginLogout.setOnClickListener(new OnClickListener() {
        @SuppressWarnings("synthetic-access")
        @Override
        public void onClick(View view) {
          onClickLogin();
        }
      });
    }
  }

  private void onClickLoginWithPublish() {
    Session session = Session.getActiveSession();
    if (!session.isOpened() && !session.isClosed()) {
      // Ask for username and password
      OpenRequest op = new Session.OpenRequest(this);

      op.setLoginBehavior(SessionLoginBehavior.SUPPRESS_SSO);
      op.setCallback(null);

      List<String> permissions = new ArrayList<String>();
      permissions.add("user_status");
      permissions.add("read_stream");
      permissions.add("friends_status");
      permissions.add("publish_stream");
      permissions.add("user_likes");
      permissions.add("friends_likes");
      permissions.add("email");
      permissions.add("user_birthday");
      op.setPermissions(permissions);

      Session newSession = new Session.Builder(FacebookFeedLoginActivity.this)
          .build();
      Session.setActiveSession(newSession);
      newSession.openForPublish(op);
    } else
      Session.openActiveSession(this, true, statusCallback);
  }

  private void onClickLogin() {
    Session session = Session.getActiveSession();
    if (!session.isOpened() && !session.isClosed())
      session.openForRead(new Session.OpenRequest(this)
          .setCallback(statusCallback));
    else
      Session.openActiveSession(this, true, statusCallback);
  }

  private void onClickShowPosts() {
    Intent intent = new Intent(this, PostsActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
    startActivity(intent);
  }

  private static void onClickLogout() {
    Session session = Session.getActiveSession();
    if (!session.isClosed())
      session.closeAndClearTokenInformation();
  }

  private class SessionStatusCallback implements Session.StatusCallback {
    @SuppressWarnings("synthetic-access")
    @Override
    public void call(Session session, SessionState state, Exception exception) {
      updateView();
    }
  }
}
