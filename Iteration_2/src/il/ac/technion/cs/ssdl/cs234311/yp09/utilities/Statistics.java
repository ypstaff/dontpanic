package il.ac.technion.cs.ssdl.cs234311.yp09.utilities;

import java.io.Serializable;

/**
 * @author husam
 * 
 */
public class Statistics implements Serializable {
  private static final long serialVersionUID = 7766L;
  public Double timeUsingTheApp;
  public Double timePerSentenceAvg;
  public Double numberOfSentence;
  public Double timePerWordAvg;
  public Double numberOfWords;
  public Double timePerCharAvg;
  public Double numberOfChars;
  public Double numberOfDeletedChars;
  public Double numberOfAutoComplete;
  public Double numberOfAutoCorrect;
  public Double numberOfFaceBookMessages;
  public Double numberOfSMS;
  public Double numberOfGoogleMessages;
  public Double numberOfGreenClicks;
  public Double numberOfOrangeClicks;
  public Double numberOfRedClicks;
  public Double numberOfBlueClicks;
  public Double numberOfBlueOrangeClicks;
  public Double numberOfOrangeGreenClicks;
  public Double numberOfGreenRedClicks;
  public Double numberOfBlueRedClicks;

  public Statistics() {
    timeUsingTheApp = new Double(0);
    timePerSentenceAvg = new Double(0.0);
    numberOfSentence = new Double(0);
    timePerWordAvg = new Double(0.0);
    numberOfWords = new Double(0);
    timePerCharAvg = new Double(0.0);
    numberOfChars = new Double(0);
    numberOfDeletedChars = new Double(0);
    numberOfAutoComplete = new Double(0);
    numberOfAutoCorrect = new Double(0);
    numberOfFaceBookMessages = new Double(0);
    numberOfSMS = new Double(0);
    numberOfGoogleMessages = new Double(0);
    numberOfGreenClicks = new Double(0);
    numberOfOrangeClicks = new Double(0);
    numberOfRedClicks = new Double(0);
    numberOfBlueClicks = new Double(0);
    numberOfBlueOrangeClicks = new Double(0);
    numberOfOrangeGreenClicks = new Double(0);
    numberOfGreenRedClicks = new Double(0);
    numberOfBlueRedClicks = new Double(0);
  }

  @Override
  public int hashCode() {
    return (int) (timeUsingTheApp + timePerSentenceAvg + numberOfSentence
        + timePerWordAvg + numberOfWords + timePerCharAvg + numberOfChars
        + numberOfDeletedChars + numberOfAutoComplete + numberOfAutoCorrect
        + numberOfFaceBookMessages + numberOfSMS + numberOfGoogleMessages
        + numberOfGreenClicks + numberOfOrangeClicks + numberOfRedClicks
        + numberOfBlueClicks + numberOfBlueOrangeClicks
        + numberOfOrangeGreenClicks + numberOfGreenRedClicks + numberOfBlueRedClicks);
  }

  @Override
  public boolean equals(Object o) {
    if (!(o instanceof Statistics))
      return false;
    Statistics other = (Statistics) o;
    return timeUsingTheApp == other.timeUsingTheApp
        && timePerSentenceAvg == other.timePerSentenceAvg
        && numberOfSentence == other.numberOfSentence
        && timePerWordAvg == other.timePerWordAvg
        && numberOfWords == other.numberOfWords
        && timePerCharAvg == other.timePerCharAvg
        && numberOfChars == other.numberOfChars
        && numberOfDeletedChars == other.numberOfDeletedChars
        && numberOfAutoComplete == other.numberOfAutoComplete
        && numberOfAutoCorrect == other.numberOfAutoCorrect
        && numberOfFaceBookMessages == other.numberOfFaceBookMessages
        && numberOfSMS == other.numberOfSMS
        && numberOfGoogleMessages == other.numberOfGoogleMessages
        && numberOfGreenClicks == other.numberOfGreenClicks
        && numberOfOrangeClicks == other.numberOfOrangeClicks
        && numberOfRedClicks == other.numberOfRedClicks
        && numberOfBlueClicks == other.numberOfBlueClicks
        && numberOfBlueOrangeClicks == other.numberOfBlueOrangeClicks
        && numberOfOrangeGreenClicks == other.numberOfOrangeGreenClicks
        && numberOfGreenRedClicks == other.numberOfGreenRedClicks
        && numberOfBlueRedClicks == other.numberOfBlueRedClicks;
  }
}
