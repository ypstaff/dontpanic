package il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeedGui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeed.FacebookManager;
import il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeed.FacebookPost;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.Controller;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.GeneralActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.LanguageActivity;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.LongFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.OpCodeInterpreter;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.PostFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.ProgressBarFragment;
import il.ac.technion.cs.ssdl.cs234311.yp09.gui.SettingsActivity;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ScrollView;


/**
 * This activity is responsible on showing user's feed
 * 
 * @author Owais Musa
 * 
 */
public class PostsActivity extends GeneralActivity {

  static FacebookManager mFacebookManager = new FacebookManager();

  static List<FacebookPostFragment> mPostsFragments = new ArrayList<FacebookPostFragment>();
  private ScrollView mScrollView;

  private int mActivePostIndex = 0;
  private int mCurrentY = 0;

  private boolean mSpecialScrollMode = false;
  private int mIndexInSpecialMode = 0;
  private int mMaxIndexInSpecialMode = 0;

  private boolean mSwitched = false;

  private void switchColors() {
    mSwitched = !mSwitched;
    mPostsFragments.get(mActivePostIndex).updateButtonsColor(mSwitched);

    if (mSwitched) {
      findViewById(R.id.postUpImg).setVisibility(View.INVISIBLE);
      findViewById(R.id.postDownImg).setVisibility(View.INVISIBLE);
      findViewById(R.id.postnextImg).setVisibility(View.INVISIBLE);
      findViewById(R.id.postPrevImg).setVisibility(View.INVISIBLE);
    } else {
      findViewById(R.id.postUpImg).setVisibility(View.VISIBLE);
      findViewById(R.id.postDownImg).setVisibility(View.VISIBLE);
      findViewById(R.id.postnextImg).setVisibility(View.VISIBLE);
      findViewById(R.id.postPrevImg).setVisibility(View.VISIBLE);
    }
  }

  private void updateCurrentActivePost() {
    int h = 0;
    int i = 0;
    for (FacebookPostFragment f : mPostsFragments) {
      if (mCurrentY >= h && mCurrentY < h + f.calcHeight()) {
        mPostsFragments.get(mActivePostIndex).turnOff();
        mActivePostIndex = i;
        f.turnOn();
        return;
      }

      h += f.calcHeight();
      i++;
    }
  }

  private void handleSpecialScrollMode(int y) {
    if (y < 0 && mIndexInSpecialMode == 1) {
      mIndexInSpecialMode = 0;
      mMaxIndexInSpecialMode = 0;
      mSpecialScrollMode = false;
      mPostsFragments.get(mActivePostIndex).turnOff();
      mActivePostIndex--;
      mPostsFragments.get(mActivePostIndex).turnOn();
      return;
    }
    mSpecialScrollMode = true;

    if (mMaxIndexInSpecialMode == 0)
      mMaxIndexInSpecialMode = mPostsFragments.size() - mActivePostIndex - 1;

    if (y > 0 && mIndexInSpecialMode >= mMaxIndexInSpecialMode)
      return; // temp .. handle this, maybe get more posts

    mPostsFragments.get(mActivePostIndex).turnOff();
    mActivePostIndex = y > 0 ? mActivePostIndex + 1 : mActivePostIndex - 1;
    mIndexInSpecialMode = y > 0 ? mIndexInSpecialMode + 1
        : mIndexInSpecialMode - 1;
    mPostsFragments.get(mActivePostIndex).turnOn();
  }

  private boolean reachedBottom() {
    View view = mScrollView.getChildAt(mScrollView.getChildCount() - 1);
    int diff = view.getBottom()
        - (mScrollView.getHeight() + mScrollView.getScrollY());

    return diff <= 0;
  }

  private void scrollInPosts(int y) {
    if (mPostsFragments.size() <= 0)
      return;

    if (mSpecialScrollMode || reachedBottom() && y > 0
        && mActivePostIndex < mPostsFragments.size() - 1) {
      handleSpecialScrollMode(y);
      return;
    }

    mScrollView.scrollBy(0, y);
    mCurrentY = mScrollView.getScrollY();
    updateCurrentActivePost();
  }

  private void scrollToNextPost() {
    if (mPostsFragments.size() <= 0)
      return;

    if (!mSpecialScrollMode && !reachedBottom())
      alignPosition();

    scrollInPosts(mPostsFragments.get(mActivePostIndex).calcHeight());
  }

  private void scrollToPrevtPost() {
    if (mPostsFragments.size() <= 0)
      return;

    if (!mSpecialScrollMode)
      alignPosition();

    if (mActivePostIndex == 0)
      return;

    scrollInPosts(-mPostsFragments.get(mActivePostIndex - 1).calcHeight());
  }

  private void alignPosition() {
    int h = 0;
    for (FacebookPostFragment f : mPostsFragments) {
      if (mCurrentY >= h && mCurrentY < h + f.calcHeight()) {
        mCurrentY = h;
        mScrollView.scrollTo(0, mCurrentY);
        return;
      }

      h += f.calcHeight();
    }
  }

  /**
   * Change facebook manager, mainly for setting mock manager
   * 
   * @param facebookManager
   *          facebook manager
   */
  public static void setFacebookManager(FacebookManager facebookManager) {
    mFacebookManager = facebookManager;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_posts);

    mPostsFragments = new ArrayList<FacebookPostFragment>();
    mScrollView = (ScrollView) findViewById(R.id.scrollViewContainer);

    mScrollView.setOnTouchListener(new NoTouchListener());

    if (savedInstanceState == null)
      getFragmentManager().beginTransaction()
          .add(R.id.container, new PlaceholderFragment()).commit();

    final LongFragment longFragment = LongFragment
        .newInstance(LongFragment.Screen.Posts);
    getFragmentManager().beginTransaction()
        .add(R.id.long_press_info_frame, longFragment).commit();

    final ProgressBarFragment progressBarFragment = ProgressBarFragment
        .newInstance(ProgressBarFragment.Screen.Posts);
    getFragmentManager().beginTransaction()
        .add(R.id.progress_bar_frame, progressBarFragment).commit();

    final PostFragment postFragment = new PostFragment();
    getFragmentManager().beginTransaction()
        .add(R.id.post_fragment, postFragment).commit();

    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame, mFBFragment).commit();

    refreshActivity();
  }

  private void refreshActivity() {
    setTitle(R.string.posts);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    getMenuInflater().inflate(R.menu.main, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    int id = item.getItemId();
    if (id == R.id.action_settings)
      return true;
    return super.onOptionsItemSelected(item);
  }

  @Override
  public void onResume() {
    super.onResume(); // Always call the superclass method first

    refreshActivity();
    if (mPostsFragments.size() >= 1)
      mPostsFragments.get(0).turnOn();
  }

  /**
   * A placeholder fragment.
   */
  public static class PlaceholderFragment extends Fragment {

    @Override
    public void onAttach(final Activity activity) {
      super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
        Bundle savedInstanceState) {
      View rootView = inflater.inflate(R.layout.fragment_posts, container,
          false);

      FragmentManager fm = getChildFragmentManager();
      final FragmentTransaction ft = fm.beginTransaction();

      getActivity().runOnUiThread(new Runnable() {
        @Override
        public void run() {
          for (FacebookPost post : mFacebookManager.getNewsFeed()) {
            FacebookPostFragment f = FacebookPostFragment.getInstance(post);
            ft.add(R.id.FacebookPostsLinearLayout, f);
            mPostsFragments.add(f);
          }
          ft.commit();
        }
      });

      return rootView;
    }
  }

  @Override
  public void onOperation(final int c) {
    super.onOperation(c);
    final Intent intent;
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      if (mSwitched)
        mPostsFragments.get(mActivePostIndex).likeCommand();
      else
        scrollInPosts(40);
      break;
    case BLUE_ORANGE:
      break;
    case BLUE_RED:
      switchColors();
      break;
    case GREEN:
      if (!mSwitched)
        scrollToNextPost();
      break;
    case GREEN_RED:
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      makeSound(1);
      intent = new Intent(this, SettingsActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      intent.putExtra(LanguageActivity.languageKey, Controller.language);
      startActivity(intent);
      break;
    case LONG_GREEN:
      break;
    case LONG_ORANGE:
      break;
    case LONG_RED:
      makeSound(1);
      finish();
      overridePendingTransition(0, 0);
      break;
    case ORANGE:
      if (mSwitched)
        mPostsFragments.get(mActivePostIndex).commentCommand();
      else
        scrollInPosts(-40);
      break;
    case ORANGE_GREEN:
      break;
    case RED:
      if (mSwitched)
        mPostsFragments.get(mActivePostIndex).shareCommand();
      else
        scrollToPrevtPost();
      break;
    default:
      break;
    }
  }
}
