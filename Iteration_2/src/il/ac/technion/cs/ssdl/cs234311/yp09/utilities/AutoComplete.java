package il.ac.technion.cs.ssdl.cs234311.yp09.utilities;

import il.ac.technion.cs.ssdl.cs234311.yp09.gui.FourButtonsFragment;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.app.Activity;
import android.content.res.AssetManager;
import android.util.Xml;

/**
 * @author Husam
 * 
 *         this class gives the service of autoComplete words, and it get the
 *         optional words to complete the given word from a database of most
 *         common words in English saved in xml file of words that saved in xml
 *         file
 */
public class AutoComplete {
  private static String fileName = "common_words.xml";
  private static final String ns = null;
  private Activity parserActivity;
  private HashMap<String, List<String>> wordsCache;
  private String lastFirstChar;

  /**
   * @param activity
   *          the current urning activity, it needed to open a file. this is the
   *          constructor of the class, and it will create an empty cache to
   *          save the last used words in it.
   * 
   * 
   */
  public AutoComplete(Activity activity) {
    parserActivity = activity;
    wordsCache = new HashMap<String, List<String>>();
  }

  /**
   * @param word
   *          the given word to complete
   * @return list of most common words that start with the received word, the
   *         words will return sorted according to there Frequency, i.e. the
   *         most frequent word first, the maximal length of the list is three
   */
  public List<String> getWordCompletion(String word) {
    if (FourButtonsFragment.debugMode == true) {
      // the debug mode is on.
      List<String> returndList = new LinkedList<String>();
      returndList.add(word.concat("aaa"));
      returndList.add(word.concat("bbb"));
      returndList.add(word.concat("ccc"));
      return returndList;
    }
    List<String> lowerCaseWrodsList = null;
    try {
      lowerCaseWrodsList = getWordCompletionAux(word
          .toLowerCase(Locale.ENGLISH));
    } catch (IOException e) {
      e.printStackTrace();
    } catch (XmlPullParserException e) {
      e.printStackTrace();
    }
    List<String> returnList = new LinkedList<String>();
    if (lowerCaseWrodsList == null)
      return null;
    for (String i : lowerCaseWrodsList) {
      String returndWord = word.concat(i.substring(word.length()));
      returnList.add(returndWord);
    }
    return returnList;
  }

  private List<String> getWordCompletionAux(String word) throws IOException,
      XmlPullParserException {
    if (word.length() == 1) {
      lastFirstChar = word;
      return handelWordWithOneChar(word);
    }
    if (lastFirstChar == null || !word.subSequence(0, 1).equals(lastFirstChar))
      prebareDataFor(word.substring(0, 1));
    return getCompFromCash(word);
  }

  private List<String> getCompFromCash(String word) {
    synchronized (wordsCache) {
      List<String> wordsList = wordsCache.get(word.subSequence(1, 2));
      List<String> returnedList = new LinkedList<String>();
      int counter = 0;
      for (String i : wordsList) {
        if (i.startsWith(word) && !i.equals(word)) {

          returnedList.add(i);
          counter++;
        }
        if (counter == 3)
          return returnedList;
      }
      return returnedList;
    }
  }

  private List<String> handelWordWithOneChar(final String c)
      throws IOException, XmlPullParserException {

    new Thread(new Runnable() {
      @Override
      public void run() {
        try {
          prebareDataFor(c);
        } catch (IOException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        } catch (XmlPullParserException e) {
          // TODO Auto-generated catch block
          e.printStackTrace();
        }
      }
    }).start();
    return getFirstThree(c);
  }

  void prebareDataFor(String firstLetter) throws IOException,
      XmlPullParserException {
    synchronized (wordsCache) {
      getWordsSupGroup(firstLetter);
    }
  }

  private List<String> getFirstThree(final String firstLetter)
      throws IOException, XmlPullParserException {
    AssetManager assetManager = parserActivity.getAssets();
    InputStream inputeFile = null;
    inputeFile = assetManager.open(fileName);
    XmlPullParser parser = createParser(inputeFile);
    List<String> returnedList = getCommonWordsByFirst(parser, firstLetter);
    inputeFile.close();
    return returnedList;
  }

  private static XmlPullParser createParser(InputStream inputeFile)
      throws XmlPullParserException, IOException {

    XmlPullParser parser = Xml.newPullParser();
    parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
    parser.setInput(inputeFile, null);
    parser.nextTag();

    return parser;
  }

  private void getWordsSupGroup(String firstChar) throws IOException,
      XmlPullParserException {
    InputStream inputeFile = null;
    try {
      AssetManager assetManager = parserActivity.getAssets();
      inputeFile = assetManager.open(fileName);
      XmlPullParser parser = createParser(inputeFile);
      getWordsByPrefex(parser, firstChar);
    } finally {
      if (inputeFile != null)
        inputeFile.close();
    }
  }

  private void getWordsByPrefex(XmlPullParser parser, String firstChar)
      throws XmlPullParserException, IOException {
    parser.require(XmlPullParser.START_TAG, ns, "common_words");
    while (parser.next() != XmlPullParser.END_TAG) {
      if (parser.getEventType() != XmlPullParser.START_TAG)
        continue;
      String currFirstLetter = parser.getAttributeValue(null, "firstLetter");
      if (!currFirstLetter.equals(firstChar))
        skip(parser);
      else
        while (true) {
          parser.next();
          if (parser.getEventType() == XmlPullParser.START_TAG
              && parser.getName().equals("wordsSupGroup")) {
            String newKey = parser.getAttributeValue(ns, "secondLetter");
            List<String> newValue = getAllWordsInSupGroup(parser);

            wordsCache.put(newKey, newValue);
          }
          if (parser.getEventType() == XmlPullParser.END_TAG
              && parser.getName().equals("wordsGroup"))
            return;
        }
    }
    assert false;

  }

  private static List<String> getAllWordsInSupGroup(XmlPullParser parser)
      throws XmlPullParserException, IOException {
    List<String> wordsList = new LinkedList<String>();
    while (true) {
      if (parser.getEventType() == XmlPullParser.START_TAG
          && parser.getName().equals("word"))
        wordsList.add(readText(parser));
      if (parser.getEventType() == XmlPullParser.END_TAG
          && parser.getName().equals("wordsSupGroup"))
        return wordsList;

      parser.next();
    }
  }

  private static List<String> getCommonWordsByFirst(final XmlPullParser parser,
      final String firstLetter) throws XmlPullParserException, IOException {
    parser.require(XmlPullParser.START_TAG, ns, "common_words");
    while (parser.next() != XmlPullParser.END_TAG) {
      if (parser.getEventType() != XmlPullParser.START_TAG)
        continue;
      String currFirstLetter = parser.getAttributeValue(null, "firstLetter");
      if (!currFirstLetter.equals(firstLetter))
        skip(parser);
      else
        return getWordsFromElement(parser);
    }
    return null;
  }

  private static List<String> getWordsFromElement(XmlPullParser parser)
      throws XmlPullParserException, IOException {
    List<String> wordsList = new LinkedList<String>();
    int counter = 0;
    while (true) {
      if (parser.getEventType() == XmlPullParser.START_TAG
          && parser.getName().equals("word")) {

        wordsList.add(readText(parser));
        counter++;

      }
      if (counter == 3 || parser.getEventType() == XmlPullParser.END_TAG
          && parser.getName().equals("wordsGroup"))
        return wordsList;

      parser.next();
    }
  }

  private static String readText(XmlPullParser parser) throws IOException,
      XmlPullParserException {
    String result = "";
    if (parser.next() == XmlPullParser.TEXT) {
      result = parser.getText();
      parser.nextTag();
    }
    return result;
  }

  private static void skip(XmlPullParser parser) throws XmlPullParserException,
      IOException {
    if (parser.getEventType() != XmlPullParser.START_TAG)
      throw new IllegalStateException();
    int depth = 1;
    while (depth != 0)
      switch (parser.next()) {
      case XmlPullParser.END_TAG:
        depth--;
        break;
      case XmlPullParser.START_TAG:
        depth++;
        break;
      default:
        break;
      }
  }

}
