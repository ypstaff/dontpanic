package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

/**
 * The class that interprets a given code from a four button device to an
 * operation code.
 * 
 * @author Itamar
 * 
 */
public class OpCodeInterpreter {
  /*
   * Important: when change these values, you MUST change debug section in
   * FourButtonsFragment accordingly!
   */
  private static final short LONG_MASK = 1, BLUE_MASK = 2, ORANGE_MASK = 4,
      GREEN_MASK = 8, RED_MASK = 16;

  /**
   * Operation codes.
   * 
   * @author Itamar
   * 
   */
  public static enum Op {
    /** invalid code */
    INVALID,
    /** blue press */
    BLUE,
    /** orange press */
    ORANGE,
    /** green press */
    GREEN,
    /** red press */
    RED,
    /** long blue press */
    LONG_BLUE,
    /** long orange press */
    LONG_ORANGE,
    /** long green press */
    LONG_GREEN,
    /** long red press */
    LONG_RED,
    /** blue and orange press */
    BLUE_ORANGE,
    /** orange and green press */
    ORANGE_GREEN,
    /** green and red press */
    GREEN_RED,
    /** blue and red press */
    BLUE_RED
  }

  /**
   * Interprets the given device code into an operation code.
   * 
   * @param c
   *          The given code.
   * @return The equivalent operation code.
   */
  public static Op getOp(final int c) {
    switch (c) {
    case BLUE_MASK:
      return Op.BLUE;
    case ORANGE_MASK:
      return Op.ORANGE;
    case GREEN_MASK:
      return Op.GREEN;
    case RED_MASK:
      return Op.RED;
    case LONG_MASK | BLUE_MASK:
      return Op.LONG_BLUE;
    case LONG_MASK | ORANGE_MASK:
      return Op.LONG_ORANGE;
    case LONG_MASK | GREEN_MASK:
      return Op.LONG_GREEN;
    case LONG_MASK | RED_MASK:
      return Op.LONG_RED;
    case BLUE_MASK | ORANGE_MASK:
      return Op.BLUE_ORANGE;
    case BLUE_MASK | RED_MASK:
      return Op.BLUE_RED;
    case ORANGE_MASK | GREEN_MASK:
      return Op.ORANGE_GREEN;
    case GREEN_MASK | RED_MASK:
      return Op.GREEN_RED;
    default:
      return Op.INVALID;
    }
  }
}
