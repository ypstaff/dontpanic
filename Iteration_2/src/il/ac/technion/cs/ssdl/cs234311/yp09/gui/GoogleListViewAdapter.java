package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.commsModule.GoogleItem;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 17/1/2014
 * 
 */
public class GoogleListViewAdapter extends ArrayAdapter<GoogleItem> {

  Context context;

  /**
   * @param context
   *          current context
   * @param resourceId
   *          resource identifier
   * @param items
   *          google contacts list
   */
  public GoogleListViewAdapter(final Context context, final int resourceId,
      final List<GoogleItem> items) {
    super(context, resourceId, items);
    this.context = context;
  }

  /* private view holder class */
  private class ViewHolder {
    ImageView contactAvatar;
    TextView contactName;
    TextView contactID;
    CheckBox checkbox;
  }

  @SuppressWarnings("synthetic-access")
  @Override
  public View getView(final int position, View convertView,
      final ViewGroup parent) {

    ViewHolder holder = null;
    View $ = convertView;
    final GoogleItem googleRow = getItem(position);

    final LayoutInflater mInflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    if ($ == null) {
      $ = mInflater.inflate(R.layout.google_listview, null);
      holder = new ViewHolder();
      holder.contactName = (TextView) $.findViewById(R.id.username);
      holder.contactID = (TextView) $.findViewById(R.id.mail);
      holder.contactAvatar = (ImageView) $.findViewById(R.id.avatar);
      holder.checkbox = (CheckBox) $.findViewById(R.id.check_box);
      $.setTag(holder);
    } else
      holder = (ViewHolder) $.getTag();

    holder.contactName.setText(googleRow.contactName);
    // holder.contactID.setText(R.string.google_account);

    if (googleRow.contactPresence.equals("available")) {
      holder.contactAvatar.setImageDrawable(context.getResources().getDrawable(
          R.drawable.available));
      holder.contactID.setText("available");
    } else if (googleRow.contactPresence.equals("unavailable")) {
      holder.contactAvatar.setImageDrawable(context.getResources().getDrawable(
          R.drawable.signout));
      holder.contactID.setText("unavailable");
    } else if (googleRow.contactPresence.equals("dnd")) {
      holder.contactAvatar.setImageDrawable(context.getResources().getDrawable(
          R.drawable.busy));
      holder.contactID.setText("busy");
    } else if (googleRow.contactPresence.equals("chat")) {
      holder.contactAvatar.setImageDrawable(context.getResources().getDrawable(
          R.drawable.available));
      holder.contactID.setText("available");
    } else if (googleRow.contactPresence.equals("xa")) {
      holder.contactAvatar.setImageDrawable(context.getResources().getDrawable(
          R.drawable.away));
      holder.contactID.setText("away");
    } else if (googleRow.contactPresence.equals("away")) {
      holder.contactAvatar.setImageDrawable(context.getResources().getDrawable(
          R.drawable.away));
      holder.contactID.setText("away");
    } else {
      holder.contactAvatar.setImageDrawable(context.getResources().getDrawable(
          R.drawable.signout));
      holder.contactID.setText("unavailable");
    }
    if (googleRow.checked)
      holder.checkbox.setChecked(true);
    else
      holder.checkbox.setChecked(false);

    if (googleRow.contactID != null
        && googleRow.contactID.endsWith("gmail.com"))
      holder.contactName.setText(googleRow.contactID);

    return $;

  }
}
