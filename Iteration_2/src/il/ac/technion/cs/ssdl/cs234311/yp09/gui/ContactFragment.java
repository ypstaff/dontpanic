package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.EntryNavigator;
import android.app.Fragment;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;


/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 10/1/2014
 * 
 */
public class ContactFragment extends Fragment {

  private EditText contactEditText;

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    final View $ = inflater.inflate(R.layout.contact_name, container, false);

    final FrameLayout box1 = (FrameLayout) $
        .findViewById(R.id.contact_box1_frame);
    final DrawView rec1 = new DrawView(getActivity(), DrawView.Color.Blue,
        DrawView.Position.Right);
    box1.addView(rec1);

    final FrameLayout box2 = (FrameLayout) $
        .findViewById(R.id.contact_box2_frame);
    final DrawView rec2 = new DrawView(getActivity(), DrawView.Color.Red,
        DrawView.Position.Left);
    box2.addView(rec2);

    final FrameLayout icon = (FrameLayout) $
        .findViewById(R.id.switch_icon_frame);
    final ImageView img = new ImageView(getActivity());
    img.setImageResource(R.drawable.up_down);
    icon.addView(img);

    contactEditText = (EditText) $.findViewById(R.id.contact_name_view);

    contactEditText.addTextChangedListener(new TextWatcher() {

      @Override
      public void afterTextChanged(Editable arg0) {
        // TODO Auto-generated method stub
      }

      @Override
      public void beforeTextChanged(CharSequence arg0, int arg1, int arg2,
          int arg3) {
        // TODO Auto-generated method stub

      }

      @Override
      public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
        if (arg0 == null || arg0.toString().equals(""))
          return;

        if (getActivity() instanceof ContactActivity) {

          ContactActivity activity = (ContactActivity) getActivity();
          ListView list = activity.getCurrentList();

          int size = 0;

          switch (activity.currentService) {
          case Facebook:
            size = Controller.facebookList.size();
            break;
          case GTalk:
            size = Controller.googleList.size();
            break;
          case SMS:
            size = Controller.smsList.size();
            break;
          default:
            break;

          }

          String[] contacts = new String[size];

          for (int i = 0; i < size; i++)
            switch (activity.currentService) {
            case Facebook:
              if (Controller.facebookList.get(i).contactName != null)
                contacts[i] = Controller.facebookList.get(i).contactName;
              else
                contacts[i] = Controller.facebookList.get(i).contactID;
              break;
            case GTalk:
              if (Controller.googleList.get(i).contactName != null)
                contacts[i] = Controller.googleList.get(i).contactName;
              else
                contacts[i] = Controller.googleList.get(i).contactID;
              break;
            case SMS:
              if (Controller.smsList.get(i).contactName != null)
                contacts[i] = Controller.smsList.get(i).contactName;
              else
                contacts[i] = Controller.smsList.get(i).contactPhone;
              break;
            default:
              break;
            }
          int index = EntryNavigator.getEntryIndex(contacts, arg0.toString());

          activity.currentRecord = index;
          list.smoothScrollToPosition(activity.currentRecord);
          list.setItemChecked(activity.currentRecord, true);
          list.setSelection(activity.currentRecord);
        } else if (getActivity() instanceof ContactHistoryActivity) {
          ContactHistoryActivity activity = (ContactHistoryActivity) getActivity();
          ListView list = activity.getCurrentList();

          int size = 0;

          size = Controller.smsList.size();

          String[] contacts = new String[size];

          for (int i = 0; i < size; i++)
            if (Controller.smsList.get(i).contactName != null)
              contacts[i] = Controller.smsList.get(i).contactName;
            else
              contacts[i] = Controller.smsList.get(i).contactPhone;

          int index = EntryNavigator.getEntryIndex(contacts, arg0.toString());

          activity.currentRecord = index;
          list.smoothScrollToPosition(activity.currentRecord);
          list.setItemChecked(activity.currentRecord, true);
          list.setSelection(activity.currentRecord);
        }
      }

    });

    return $;
  }

  @Override
  public void onResume() {
    super.onResume();
    refreshFragment();
  }

  private void refreshFragment() {
    contactEditText.setHint(R.string.contact_name);
  }
}
