package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.messages.ConversationItem;
import il.ac.technion.cs.ssdl.cs234311.yp09.messages.SMSMessageItem;
import il.ac.technion.cs.ssdl.cs234311.yp09.messages.SMSMessageModule;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.AbsListView;
import android.widget.ListView;


/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 16/1/2014
 * 
 */
public class ConversationActivity extends GeneralActivity {

  /**
   * a key to send a parameter with an intent
   */
  static public String conversationKey = "il.ac.technion.cs.ssdl.cs234311.yp09.gui.conversationKey";
  static public String typeKey = "il.ac.technion.cs.ssdl.cs234311.yp09.gui.typeKey";
  static public String nameKey = "il.ac.technion.cs.ssdl.cs234311.yp09.gui.nameKey";

  // ????????????????????????????????????????????????????
  private int currentRecord = 0;
  private ConversationFragment conversationFragment;
  private String value;
  private String type;

  ListView conversationListView;
  List<SMSMessageItem> conversationList;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_conversation);

    String name = getIntent().getStringExtra(nameKey);

    conversationFragment = ConversationFragment.newInstance(name);
    getFragmentManager().beginTransaction()
        .add(R.id.long_press_info_frame, conversationFragment).commit();

    final ProgressBarFragment progressBarFragment = ProgressBarFragment
        .newInstance(ProgressBarFragment.Screen.Conversation);
    getFragmentManager().beginTransaction()
        .add(R.id.progress_bar_frame, progressBarFragment).commit();

    final ScrollingFragment scrollingFragment = ScrollingFragment
        .newInstance(ScrollingFragment.Screen.Conversation);
    getFragmentManager().beginTransaction()
        .add(R.id.scrolling_fragment, scrollingFragment).commit();

    final Intent intent = getIntent();
    value = intent.getStringExtra(conversationKey); // conversationKey = id
    type = intent.getStringExtra(typeKey); // typeKey = name

    if (type.equals("google")) {
      if (value != null)
        conversationList = commsModule.messagesGTalk.getMessageList(value);
      else
        conversationList = commsModule.messagesGTalk.getMessageList(type);
    } else if (type.equals("facebook"))
      conversationList = commsModule.messagesFacebook.getMessageList(value);
    else
      conversationList = SMSMessageModule.SMSParser(this, value);

    conversationListView = (ListView) findViewById(R.id.conversation_fragment);
    conversationListView.setOnTouchListener(new NoTouchListener());
    conversationListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);

    final ConversationListViewAdapter adapter = new ConversationListViewAdapter(
        this, R.layout.message_listview, conversationList);
    conversationListView.setAdapter(adapter);
    conversationListView.setItemChecked(0, true);

    refreshActivity();

    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame_main, mFBFragment).commit();
  }

  @Override
  protected void onResume() {
    super.onResume();
    refreshActivity();
  }

  @Override
  public boolean onCreateOptionsMenu(final Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.language, menu);
    return true;
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return false;
  }

  @Override
  public void onOperation(final int c) {
    super.onOperation(c);
    final Intent intent;
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      break;
    case BLUE_ORANGE:
      break;
    case BLUE_RED:
      break;
    case GREEN:
      if (conversationListView.getCount() == 0)
        return;
      makeSound(0);
      currentRecord = (currentRecord + 1) % conversationListView.getCount();
      conversationListView.setItemChecked(currentRecord, true);
      conversationListView.smoothScrollToPosition(currentRecord);
      break;
    case GREEN_RED:
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      makeSound(1);
      intent = new Intent(this, SettingsActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      intent.putExtra(LanguageActivity.languageKey, Controller.language);
      startActivity(intent);
      break;
    case LONG_GREEN:
      break;
    case LONG_ORANGE:
      break;
    case LONG_RED:
      makeSound(1);
      finish();
      overridePendingTransition(0, 0);
      break;
    case ORANGE:
      if (conversationListView.getCount() == 0)
        return;
      makeSound(0);
      currentRecord = (currentRecord - 1 + conversationListView.getCount())
          % conversationListView.getCount();
      conversationListView.setItemChecked(currentRecord, true);
      conversationListView.smoothScrollToPosition(currentRecord);
      break;
    case ORANGE_GREEN:
      break;
    case RED:
      if (conversationListView.getCount() == 0)
        return;
      makeSound(0);
      intent = new Intent(this, TextActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      intent.putExtra(TextActivity.prevKey, "ignored_parameter");
      String messageBody = conversationList.get(currentRecord).messageBody;
      intent.putExtra(TextActivity.messageKey, messageBody);

      startActivity(intent);
      break;
    default:
      break;
    }
  }

  private void refreshActivity() {
    if (type.equals("google"))
      setTitle(R.string.google_history);
    else if (type.equals("facebook"))
      setTitle(R.string.facebook_history);
    else
      setTitle(R.string.sms_history);

  }

  static private List<ConversationItem> getGoogleMessagesStub() {

    ArrayList<ConversationItem> messagesList = new ArrayList<ConversationItem>();
    ConversationItem msg1 = new ConversationItem(1, "I'm Daniel !", "123",
        "Daniel");
    ConversationItem msg2 = new ConversationItem(2, "Hey Husam !", "123",
        "Husam");
    messagesList.add(msg1);
    messagesList.add(msg2);

    return messagesList;

  }

  static private List<ConversationItem> getFacebookMessagesStub() {

    ArrayList<ConversationItem> messagesList = new ArrayList<ConversationItem>();
    ConversationItem msg1 = new ConversationItem(1, "I'm Daniel !", "123",
        "Daniel");
    ConversationItem msg2 = new ConversationItem(2, "Hey Husam !", "123",
        "Husam");
    messagesList.add(msg1);
    messagesList.add(msg2);

    return messagesList;

  }

}
