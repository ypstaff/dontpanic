package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import android.os.Bundle;
import android.view.Menu;
import android.widget.ImageView;


public class VolumeActivity extends GeneralActivity {

  private ImageView image;

  static float volume = 1;
  static int strength = 8;
  static boolean mute = false;

  static int[] pictures = { R.drawable.mute, R.drawable.vol_1,
      R.drawable.vol_2, R.drawable.vol_3, R.drawable.vol_4, R.drawable.vol_5,
      R.drawable.vol_6, R.drawable.vol_7, R.drawable.vol_8 };

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_volume);

    final LongFragment longFragment = LongFragment
        .newInstance(LongFragment.Screen.Volume);
    getFragmentManager().beginTransaction()
        .add(R.id.long_press_info_frame, longFragment).commit();

    final ProgressBarFragment progressBarFragment = ProgressBarFragment
        .newInstance(ProgressBarFragment.Screen.Volume);
    getFragmentManager().beginTransaction()
        .add(R.id.progress_bar_frame, progressBarFragment).commit();

    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame, mFBFragment).commit();

    final VolumeFragment volumeFragment;
    volumeFragment = VolumeFragment.newInstance(!mute);

    getFragmentManager().beginTransaction()
        .add(R.id.keyboard_frame, volumeFragment).commit();

    image = (ImageView) findViewById(R.id.vol_bar);
    if (mute)
      image.setImageResource(pictures[0]);
    else
      image.setImageResource(pictures[strength]);

    refreshActivity();

  }

  private void refreshActivity() {
    setTitle(R.string.volume);
  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    // getMenuInflater().inflate(R.menu.volume, menu);
    return true;
  }

  @Override
  public void onOperation(final int c) {
    super.onOperation(c);
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      decreaseVol();
      makeSound(0);
      break;
    case BLUE_ORANGE:
      break;
    case BLUE_RED:
      break;
    case GREEN:
      break;
    case GREEN_RED:
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      break;
    case LONG_GREEN:
      break;
    case LONG_ORANGE:
      break;
    case LONG_RED:
      makeSound(1);
      finish();
      overridePendingTransition(0, 0);
      break;
    case ORANGE:
      break;
    case ORANGE_GREEN:
      if (mute && strength != 0) {
        mute = false;
        volume = (float) strength / 8;
        image.setImageResource(pictures[strength]);
        final VolumeFragment volumeFragment;
        volumeFragment = VolumeFragment.newInstance(true);
        getFragmentManager().beginTransaction()
            .replace(R.id.keyboard_frame, volumeFragment).commit();
      } else if (mute && strength == 0) {
        mute = false;
        volume = (float) 1 / 8;
        strength++;
        image.setImageResource(pictures[1]);
        final VolumeFragment volumeFragment;
        volumeFragment = VolumeFragment.newInstance(true);
        getFragmentManager().beginTransaction()
            .replace(R.id.keyboard_frame, volumeFragment).commit();
      } else {
        mute = true;
        volume = 0;
        image.setImageResource(R.drawable.mute);
        final VolumeFragment volumeFragment;
        volumeFragment = VolumeFragment.newInstance(false);
        getFragmentManager().beginTransaction()
            .replace(R.id.keyboard_frame, volumeFragment).commit();
      }
      makeSound(0);
      break;
    case RED:
      increaseVol();
      makeSound(0);
      break;
    default:
      break;
    }
  }

  private void decreaseVol() {
    if (mute)
      return;
    else if (strength == 1) {
      mute = true;
      strength = 0;
      volume = 0;
      image.setImageResource(pictures[0]);
      final VolumeFragment volumeFragment;
      volumeFragment = VolumeFragment.newInstance(false);
      getFragmentManager().beginTransaction()
          .replace(R.id.keyboard_frame, volumeFragment).commit();
    } else {
      strength--;
      volume = (float) strength / 8;
      image.setImageResource(pictures[strength]);
      final VolumeFragment volumeFragment;
      volumeFragment = VolumeFragment.newInstance(true);
      getFragmentManager().beginTransaction()
          .replace(R.id.keyboard_frame, volumeFragment).commit();
    }

  }

  private void increaseVol() {
    mute = false;
    strength++;
    if (strength > 8)
      strength = 8;
    volume = (float) strength / 8;
    image.setImageResource(pictures[strength]);
    final VolumeFragment volumeFragment;
    volumeFragment = VolumeFragment.newInstance(true);
    getFragmentManager().beginTransaction()
        .replace(R.id.keyboard_frame, volumeFragment).commit();
  }

}
