package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;


/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 18/1/2014
 * 
 */
public class SwitchFragment extends Fragment {

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    final View $ = inflater.inflate(R.layout.switch_fragment, container, false);

    final FrameLayout box1 = (FrameLayout) $
        .findViewById(R.id.switch_box1_frame);
    final DrawView rec1 = new DrawView(getActivity(), DrawView.Color.Blue,
        DrawView.Position.Right);
    box1.addView(rec1);

    final FrameLayout box2 = (FrameLayout) $
        .findViewById(R.id.switch_box2_frame);
    final DrawView rec2 = new DrawView(getActivity(), DrawView.Color.Red,
        DrawView.Position.Left);
    box2.addView(rec2);

    final FrameLayout icon = (FrameLayout) $
        .findViewById(R.id.switch_icon_frame);
    final ImageView img = new ImageView(getActivity());
    img.setImageResource(R.drawable.up_down);
    icon.addView(img);

    return $;
  }
}
