package il.ac.technion.cs.ssdl.cs234311.yp09.keyboards;

/**
 * An interface that will be given to KeyboardFragment in order to update the
 * appropriate edit texts
 * 
 * @date 4/1/2013
 * @email owais.musa@gmail.com
 * @author Owais Musa
 * 
 */
public interface IKeyboardListener {

  /**
   * Inserts list of chars to the text starting from the current cursor position
   * 
   * @param str
   *          The string to be added
   */
  public void insertChars(final String str);

  /**
   * Inserts a new character to the text at the current cursor position
   * 
   * @param ch
   *          The new character to be added to the text
   */
  public void insertCharWithoutSaving(final char ch);

  /**
   * Inserts a new character to the text at the current cursor position
   * 
   * @param ch
   *          The new character to be added to the text
   */
  public void insertChar(final char ch);

  /**
   * Deletes the character behind the cursor
   */
  public void deleteChar();

  /**
   * Moves the cursor one step to the right
   */
  public void moveCurserToTheRight();

  /**
   * Moves the cursor one step to the left
   */
  public void moveCurserToTheLeft();

  /**
   * Deletes all the text, the text after this operation will be empty
   */
  public void deleteAll();

  /**
   * Undo the last operation was done on the text
   */
  public void undo();

  /**
   * 
   * @return the current text
   */
  public String getText();

  /**
   * 
   * @return Get cursor position
   */
  public int getCursorPosition();

  /**
   * Delete last word
   */
  public void shiftDelete();

  /**
   * Move the cursor to one place before the last word's first character
   */
  public void shiftMoveCursorToTheRight();

  /**
   * Move the cursor to one place after the next word's last character
   */
  public void shiftMoveCursorToTheLeft();

  /**
   * Delete without saving (for undo operation)
   */
  public void deleteWithoutSaving();

  /**
   * Move to the next auto complete option
   */
  public void nextAutoComplete();

}
