package il.ac.technion.cs.ssdl.cs234311.yp09.keyboards;

import il.ac.technion.cs.ssdl.cs234311.yp09.gui.TypingActivity;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import android.widget.EditText;

/**
 * An implementation of IKeyboardListener
 * 
 * @date 4/1/2013
 * @email owais.musa@gmail.com
 * @author Owais Musa
 * 
 */
public class KeyboardListener implements IKeyboardListener {

  private final EditText mEditText;

  // It is not private to enhance performance in nextAutoComplete, try to set
  // it to private and you will see the warning :)
  final ITypingDefaultActivity mTypingDefaultActivityListener;

  // For the Undo operation, save data before each edit
  private final List<String> mPrevTextList;
  private final List<Integer> mPrevCursorPositionList;

  private boolean mCatipalizeChars;

  private List<String> mAutoCompleteOptions;

  private int currentAutoCompleteOption = 0;

  private Timer mTimer = new Timer();
  private static final int TIME_THRESHOLD = 1600;

  private void saveData() {
    assert mPrevTextList.size() == mPrevCursorPositionList.size();

    if (mEditText == null)
      return;

    if (mPrevTextList.size() > 0
        && mPrevTextList.get(mPrevTextList.size() - 1).equals(
            mEditText.getText().toString()))
      return;

    mPrevTextList.add(mEditText.getText().toString());
    mPrevCursorPositionList.add(Integer.valueOf(getCursorPosition()));
  }

  private void resetAutoCompleteState() {
    mAutoCompleteOptions = null;
    currentAutoCompleteOption = 0;
    if (null != mTypingDefaultActivityListener)
      mTypingDefaultActivityListener.autoComplete(null);
  }

  /**
   * Constructor
   * 
   * @param editText
   *          the edit text to be edited
   * @param catipalizeChars
   *          set true if you want to use upper case feature
   */
  public KeyboardListener(final EditText editText, final boolean catipalizeChars) {
    mEditText = editText;
    mPrevTextList = new ArrayList<String>();
    mPrevCursorPositionList = new ArrayList<Integer>();
    mCatipalizeChars = catipalizeChars;
    mTypingDefaultActivityListener = null;
  }

  /**
   * Constructor
   * 
   * @param editText
   *          the edit text to be edited
   * @param catipalizeChars
   *          set true if you want to use upper case feature
   * @param typingDefaultActivityListener
   *          listener
   */
  public KeyboardListener(final EditText editText,
      final boolean catipalizeChars,
      final ITypingDefaultActivity typingDefaultActivityListener) {
    mEditText = editText;
    mPrevTextList = new ArrayList<String>();
    mPrevCursorPositionList = new ArrayList<Integer>();

    assert null != typingDefaultActivityListener;
    mTypingDefaultActivityListener = typingDefaultActivityListener;

    mCatipalizeChars = catipalizeChars;
    resetAutoCompleteState();
  }

  // Returns true if next letter should be capitalized!
  private boolean needToBeCapitalized() {
    if (!mCatipalizeChars)
      return false;

    final String currentText = mEditText.getText().toString();

    if (getCursorPosition() == 0)
      return true;

    for (int i = getCursorPosition() - 1; i >= 0; i--) {
      if (currentText.charAt(i) == '.')
        return true;
      if (currentText.charAt(i) == ' ' || currentText.charAt(i) == '\n')
        continue;
      return false;
    }

    return true;
  }

  @Override
  public void insertChars(final String str) {
    saveData();
    resetAutoCompleteState();
    deleteMarkedChars();

    final int cursorPosition = getCursorPosition();

    String newStr;

    if (str.length() > 0 && needToBeCapitalized())
      newStr = Character.toUpperCase(str.charAt(0)) + str.substring(1);
    else
      newStr = str;

    final String currentText = mEditText.getText().toString();

    final String newText = currentText.substring(0, cursorPosition) + newStr
        + currentText.substring(cursorPosition, currentText.length());

    mEditText.setText(newText);
    mEditText.setSelection(cursorPosition + newStr.length());
  }

  private void requireAutoComplete() {
    currentAutoCompleteOption = 0;

    String lastWord = getLastWord();

    mAutoCompleteOptions = mTypingDefaultActivityListener
        .autoComplete(lastWord);
  }

  private String getLastWord() {
    final String currentText = mEditText.getText().toString();

    assert getCursorPosition() != 0;

    if (!Character.isLetter(currentText.charAt(getCursorPosition() - 1)))
      return null;

    int i = getCursorPosition() - 2;
    for (; i >= 0; i--)
      if (!Character.isLetter(currentText.charAt(i)))
        break;

    return currentText.substring(i + 1, getCursorPosition());
  }

  @Override
  public void insertCharWithoutSaving(final char ch) {
    resetAutoCompleteState();
    deleteMarkedChars();

    final int cursorPosition = getCursorPosition();

    char newCh = ch;

    if (needToBeCapitalized())
      newCh = Character.toUpperCase(newCh);

    final String currentText = mEditText.getText().toString();
    final String newText = currentText.substring(0, cursorPosition) + newCh
        + currentText.substring(cursorPosition, currentText.length());
    mEditText.setText(newText);

    mEditText.setSelection(cursorPosition + 1);

    if (null != mTypingDefaultActivityListener)
      requireAutoComplete();
  }

  @Override
  public void insertChar(final char ch) {
    saveData();
    insertCharWithoutSaving(ch);
  }

  @Override
  public void deleteWithoutSaving() {
    resetAutoCompleteState();

    if (containsMarkedChars()) {
      deleteMarkedChars();
      return;
    }

    final int cursorPosition = getCursorPosition();

    if (cursorPosition == 0)
      return;

    final String currentText = mEditText.getText().toString();

    final String newText = currentText.substring(0, cursorPosition - 1)
        + currentText.substring(cursorPosition, currentText.length());
    mEditText.setText(newText);

    mEditText.setSelection(cursorPosition - 1);
  }

  private boolean containsMarkedChars() {
    return mEditText.getSelectionStart() != mEditText.getSelectionEnd();
  }

  @Override
  public void deleteChar() {
    if (getCursorPosition() > 0)
      saveData();

    deleteWithoutSaving();
  }

  @Override
  public void moveCurserToTheRight() {
    resetAutoCompleteState();

    if (containsMarkedChars()) {
      saveDataForAutoCompleteFinish();
      mEditText.setSelection(mEditText.getSelectionEnd());
      return;
    }

    final int cursorPosition = getCursorPosition();

    if (cursorPosition == mEditText.getText().length())
      return;

    mEditText.setSelection(cursorPosition + 1);
  }

  @Override
  public void moveCurserToTheLeft() {
    resetAutoCompleteState();

    if (containsMarkedChars()) {
      saveDataForAutoCompleteFinish();
      mEditText.setSelection(mEditText.getSelectionStart());
      return;
    }

    final int cursorPosition = getCursorPosition();

    if (cursorPosition == 0)
      return;

    mEditText.setSelection(cursorPosition - 1);
  }

  @Override
  public void deleteAll() {
    saveData();
    resetAutoCompleteState();
    mEditText.setText("");
  }

  @Override
  public void undo() {
    resetAutoCompleteState();
    final int size = mPrevTextList.size();

    if (size == 0)
      return;

    mEditText.setText(mPrevTextList.get(size - 1));
    mEditText.setSelection(mPrevCursorPositionList.get(size - 1).intValue());

    mPrevTextList.remove(size - 1);
    mPrevCursorPositionList.remove(size - 1);
  }

  @Override
  public int getCursorPosition() {
    return mEditText.getSelectionStart();
  }

  @Override
  public String getText() {
    return mEditText.getText().toString();
  }

  @Override
  public void shiftDelete() {
    saveData();
    resetAutoCompleteState();

    if (containsMarkedChars()) {
      deleteMarkedChars();
      return;
    }

    int cursorPosition = getCursorPosition();

    if (cursorPosition == 0)
      return;

    String currentText = mEditText.getText().toString();

    if (currentText.charAt(cursorPosition - 1) == '\n')
      deleteChar();
    else if (currentText.charAt(cursorPosition - 1) == ' ')
      do {
        deleteChar();

        currentText = mEditText.getText().toString();
        cursorPosition = getCursorPosition();
      } while (cursorPosition > 0
          && currentText.charAt(cursorPosition - 1) == ' ');
    else
      do {
        deleteChar();

        currentText = mEditText.getText().toString();
        cursorPosition = getCursorPosition();
      } while (cursorPosition != 0
          && currentText.charAt(cursorPosition - 1) != ' '
          && currentText.charAt(cursorPosition - 1) != '\n');
  }

  @Override
  public void shiftMoveCursorToTheRight() {
    resetAutoCompleteState();

    if (containsMarkedChars()) {
      saveDataForAutoCompleteFinish();
      mEditText.setSelection(mEditText.getSelectionEnd());
      return;
    }

    int cursorPosition = getCursorPosition();

    if (cursorPosition == mEditText.getText().length())
      return;

    final String currentText = mEditText.getText().toString();

    if (currentText.charAt(cursorPosition) == '\n')
      moveCurserToTheRight();
    else if (currentText.charAt(cursorPosition) == ' ')
      do {
        moveCurserToTheRight();
        cursorPosition = getCursorPosition();
      } while (cursorPosition < mEditText.getText().length()
          && currentText.charAt(cursorPosition) == ' ');
    else
      do {
        moveCurserToTheRight();
        cursorPosition = getCursorPosition();
      } while (cursorPosition < mEditText.getText().length()
          && currentText.charAt(cursorPosition) != ' '
          && currentText.charAt(cursorPosition) != '\n');
  }

  private void saveDataForAutoCompleteFinish() {
    assert mPrevTextList.size() == mPrevCursorPositionList.size();

    if (mEditText == null)
      return;

    int start = mEditText.getSelectionStart();
    int end = mEditText.getSelectionEnd();

    String currentText = mEditText.getText().toString();

    String textBeforeCompletion = currentText.substring(0, start)
        + currentText.substring(end, currentText.length());

    mPrevTextList.add(textBeforeCompletion);
    mPrevCursorPositionList.add(Integer.valueOf(start));
  }

  @Override
  public void shiftMoveCursorToTheLeft() {
    resetAutoCompleteState();

    if (containsMarkedChars()) {
      saveDataForAutoCompleteFinish();
      mEditText.setSelection(mEditText.getSelectionStart());
      return;
    }

    int cursorPosition = getCursorPosition();

    if (cursorPosition == 0)
      return;

    final String currentText = mEditText.getText().toString();

    if (currentText.charAt(cursorPosition - 1) == '\n')
      moveCurserToTheLeft();
    else if (currentText.charAt(cursorPosition - 1) == ' ')
      do {
        moveCurserToTheLeft();
        cursorPosition = getCursorPosition();
      } while (cursorPosition > 0
          && currentText.charAt(cursorPosition - 1) == ' ');
    else
      do {
        moveCurserToTheLeft();
        cursorPosition = getCursorPosition();
      } while (cursorPosition != 0
          && currentText.charAt(cursorPosition - 1) != ' '
          && currentText.charAt(cursorPosition - 1) != '\n');
  }

  private void deleteMarkedChars() {
    int start = mEditText.getSelectionStart();
    int end = mEditText.getSelectionEnd();
    String currentText = mEditText.getText().toString();

    mEditText.setText(currentText.substring(0, start)
        + currentText.substring(end, currentText.length()));
    mEditText.setSelection(start);
  }

  // Auto complete sometimes returns list with "" values!
  private int listRealSize() {
    int num = 0;

    for (String str : mAutoCompleteOptions) {
      if (str.equals(""))
        break;
      num++;
    }

    return num;
  }

  private void chooseAutoCompleteOption() {
    deleteMarkedChars();

    if (listRealSize() <= currentAutoCompleteOption) {
      currentAutoCompleteOption = 3; // Next option will be == 0!
      return;
    }

    final String lastWord = getLastWord();
    final String currentText = mEditText.getText().toString();

    String currentOption = mAutoCompleteOptions.get(currentAutoCompleteOption);
    assert currentOption.length() >= lastWord.length();
    currentOption = currentOption.substring(lastWord.length(),
        currentOption.length());

    int cursorPosition = getCursorPosition();

    mEditText.setText(currentText.substring(0, cursorPosition) + currentOption
        + currentText.substring(cursorPosition, currentText.length()));
    mEditText.setSelection(cursorPosition,
        cursorPosition + currentOption.length());
  }

  // This member field will assist us in the schedule written in
  // nextAutoComplete!
  // It is not private to enhance performance in nextAutoComplete, try to set
  // it to private and you will see the warning :)
  int autoCompleteOptionId = 0;

  @Override
  public void nextAutoComplete() {
    if (null == mAutoCompleteOptions || mAutoCompleteOptions.size() == 0)
      return;

    chooseAutoCompleteOption();
    currentAutoCompleteOption = (currentAutoCompleteOption + 1) % 4;

    if (!containsMarkedChars())
      return;

    final int currentOptionId = ++autoCompleteOptionId;

    // Wait a little bit and choose the current option

    mTimer.schedule(new TimerTask() {
      @Override
      public void run() {
        ((TypingActivity) mTypingDefaultActivityListener)
            .runOnUiThread(new Runnable() {
              @Override
              public void run() {
                if (currentOptionId != autoCompleteOptionId)
                  return;

                moveCurserToTheRight();
              }
            });
      }
    }, TIME_THRESHOLD);
  }
}
