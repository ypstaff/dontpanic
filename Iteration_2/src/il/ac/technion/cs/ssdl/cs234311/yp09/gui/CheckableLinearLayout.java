package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.Checkable;
import android.widget.LinearLayout;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 15/1/2014
 * 
 */
public class CheckableLinearLayout extends LinearLayout implements Checkable {

  /**
   * @param context
   *          current context
   * @param attrs
   *          set of attributed
   */
  public CheckableLinearLayout(final Context context, final AttributeSet attrs) {
    super(context, attrs);
  }

  private boolean isChecked = false;

  @Override
  public boolean isChecked() {
    return isChecked;
  }

  @Override
  public void setChecked(final boolean isChecked) {
    this.isChecked = isChecked;
    changeColor(isChecked);
  }

  @Override
  public void toggle() {
    this.isChecked = !this.isChecked;
    changeColor(this.isChecked);
  }

  private void changeColor(final boolean isItemChecked) {
    if (isItemChecked)
      setBackgroundColor(getResources().getColor(
          android.R.color.holo_blue_light));
    else
      setBackgroundColor(getResources().getColor(android.R.color.transparent));

  }
}
