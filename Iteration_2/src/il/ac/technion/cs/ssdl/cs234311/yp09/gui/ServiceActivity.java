package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;

import java.util.ArrayList;
import java.util.List;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.AbsListView;
import android.widget.ListView;


/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 10/1/2014
 * 
 */
public class ServiceActivity extends GeneralActivity {

  ListView servicesListView;
  List<Service> serviceList;
  private LongFragment longFragment;
  private ScrollingFragment serviceFragment;

  static public String serviceKey = "il.ac.technion.cs.ssdl.cs234311.yp09.gui.serviceKey";

  private int currentService = 0;
  private String value;

  /**
   * possible services
   */
  public static final String[] services = new String[3];

  /**
   * icons for the services
   */
  public static final int[] icons = { R.drawable.sms, R.drawable.facebook,
      R.drawable.google_talk };

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_service);

    final Intent intent = getIntent();
    value = intent.getStringExtra(serviceKey);
    refreshActivity();

    initializeServices(services);

    longFragment = LongFragment.newInstance(LongFragment.Screen.Service);
    getFragmentManager().beginTransaction()
        .add(R.id.long_press_info_frame, longFragment).commit();

    final ProgressBarFragment progressBarFragment = ProgressBarFragment
        .newInstance(ProgressBarFragment.Screen.Service);
    getFragmentManager().beginTransaction()
        .add(R.id.progress_bar_frame, progressBarFragment).commit();

    serviceFragment = ScrollingFragment
        .newInstance(ScrollingFragment.Screen.Service);
    getFragmentManager().beginTransaction()
        .add(R.id.service_edit_fragment, serviceFragment).commit();

    buildServiceList();

    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame_main, mFBFragment).commit();

  }

  @Override
  protected void onResume() {
    super.onResume();
    refreshActivity();
  }

  @Override
  public boolean onCreateOptionsMenu(final Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.service, menu);
    return true;
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return false;
  }

  @Override
  public void onOperation(final int c) {
    super.onOperation(c);
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      break;
    case BLUE_ORANGE:
      break;
    case BLUE_RED:
      break;
    case GREEN:
      makeSound(0);
      currentService = (currentService + 1) % servicesListView.getCount();
      servicesListView.setItemChecked(currentService, true);
      servicesListView.smoothScrollToPosition(currentService);
      break;
    case GREEN_RED:
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      makeSound(1);
      Controller.settingsBackTo = Controller.Screen.Service;
      Intent intent = new Intent(this, SettingsActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      intent.putExtra(LanguageActivity.languageKey, Controller.language);
      startActivity(intent);
      break;
    case LONG_GREEN:
      break;
    case LONG_ORANGE:
      break;
    case LONG_RED:
      makeSound(1);
      finish();
      overridePendingTransition(0, 0);
      break;
    case ORANGE_GREEN:
      break;
    case RED:
      makeSound(0);
      if (value.equals("services")) { // For selecting services
        intent = new Intent(this, ContactActivity.class);
        if (currentService == 0)
          intent.putExtra(ContactActivity.Service, "sms");
        else if (currentService == 1)
          if (!commsModule.isConnectedFacebook()) {
            intent = new Intent(this, LoginActivity.class);
            intent.putExtra(LoginActivity.Login, "facebook");
          } else {
            intent = new Intent(this, ContactActivity.class);
            intent.putExtra(ContactActivity.Service, "facebook");
          }

        else if (currentService == 2)
          if (!commsModule.isConnectedGTalk()) {
            intent = new Intent(this, LoginActivity.class);
            intent.putExtra(LoginActivity.Login, "google");
          } else {
            intent = new Intent(this, ContactActivity.class);
            intent.putExtra(ContactActivity.Service, "google");
          }
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
      } else { // For reading messages and posts
        intent = new Intent(this, ContactHistoryActivity.class);
        if (currentService == 0) {
          intent = new Intent(this, ContactHistoryActivity.class);
          intent.putExtra(ContactHistoryActivity.contactHistoryKey, "sms");
        } else if (currentService == 1)
          if (!commsModule.isConnectedFacebook()) {
            intent = new Intent(this, LoginActivity.class);
            intent.putExtra(LoginActivity.Login, "facebook");
            intent.putExtra(LoginActivity.Next, "facebook_messages");
          } else
            intent = new Intent(this, FacebookActivity.class);

        else if (currentService == 2)
          if (!commsModule.isConnectedGTalk()) {
            intent = new Intent(this, LoginActivity.class);
            intent.putExtra(LoginActivity.Login, "google");
            intent.putExtra(LoginActivity.Next, "google_messages");
          } else {
            intent = new Intent(this, ContactHistoryActivity.class);
            intent.putExtra(ContactHistoryActivity.contactHistoryKey, "google");
          }
        intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
        startActivity(intent);
      }
      break;
    case ORANGE:
      makeSound(0);
      currentService = (currentService - 1 + servicesListView.getCount())
          % servicesListView.getCount();
      servicesListView.setItemChecked(currentService, true);
      servicesListView.smoothScrollToPosition(currentService);
      break;
    default:
      break;
    }
  }

  private void refreshActivity() {

    if (value.equals("services"))
      setTitle(R.string.service);
    else
      setTitle(R.string.recv_messages);
    buildServiceList();
    currentService = 0;

    // serviceFragment.refreshFragment();
    // Owais:
    // owaisFragment.refreshFragment();

  }

  private void buildServiceList() {

    initializeServices(services);
    serviceFragment = ScrollingFragment
        .newInstance(ScrollingFragment.Screen.Service);
    getFragmentManager().beginTransaction()
        .add(R.id.service_edit_fragment, serviceFragment).commit();

    serviceList = new ArrayList<Service>();
    for (int i = 0; i < services.length; i++) {
      final Service service = new Service(icons[i], services[i]);
      serviceList.add(service);
    }

    servicesListView = (ListView) findViewById(R.id.service_fragment);
    servicesListView.setOnTouchListener(new NoTouchListener());
    servicesListView.setChoiceMode(AbsListView.CHOICE_MODE_SINGLE);
    final ServiceListViewAdapter adapter = new ServiceListViewAdapter(this,
        R.layout.listview_layout, serviceList);
    servicesListView.setAdapter(adapter);
    servicesListView.setItemChecked(0, true);
  }

  private void initializeServices(String[] servicesArray) {
    servicesArray[0] = getResources().getString(R.string.sms_service);
    servicesArray[1] = getResources().getString(R.string.facebook_service);
    servicesArray[2] = getResources().getString(R.string.google_service);
  }

}
