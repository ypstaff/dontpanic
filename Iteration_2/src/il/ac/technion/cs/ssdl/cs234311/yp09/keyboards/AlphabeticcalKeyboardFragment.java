package il.ac.technion.cs.ssdl.cs234311.yp09.keyboards;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import android.annotation.SuppressLint;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;


/**
 * @author Owais Musa
 * 
 */
public class AlphabeticcalKeyboardFragment extends Fragment {

  private TableRow mMainTableRow;
  private TableRow mSecondaryTableRow;

  @SuppressLint("NewApi")
  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {
    final View $ = inflater.inflate(R.layout.alphabetical_keyboard_layout,
        container, false);

    mMainTableRow = (TableRow) $.findViewById(R.id.mainTableRow);
    mSecondaryTableRow = (TableRow) $.findViewById(R.id.secondaryTableRow);

    ((KeyboardFragment) getParentFragment())
        .attachAlphaBeticalKeyboardFragment(mMainTableRow, mSecondaryTableRow);

    return $;
  }

}
