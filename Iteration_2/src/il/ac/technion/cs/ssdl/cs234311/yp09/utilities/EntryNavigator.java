package il.ac.technion.cs.ssdl.cs234311.yp09.utilities;

import java.util.Locale;

/**
 * @author Owais Musa
 */
public class EntryNavigator {

  /**
   * @param arr
   *          array for entries
   * @param entryPrefix
   *          prefix to find appropriate entry
   * @return number that represents the appropriate entry's place, -1 if not
   *         located
   */
  public static int getEntryIndex(String arr[], String entryPrefix) {
    for (int i = 0; i < arr.length; i++)
      if (arr[i].toUpperCase(Locale.US).startsWith(
          entryPrefix.toUpperCase(Locale.US)))
        return i;

    return -1;
  }
}
