package il.ac.technion.cs.ssdl.cs234311.yp09.user;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;

/**
 * @author Daniel Eidel Used to store and retrieve user data from a file
 */
public class UserInfoFileIO {
  /**
   * @param currAct
   *          - Type Activity. The main activity, in which the file is saved.
   * @param username
   *          - Type String. The username to be saved.
   * @param password
   *          - Type String. The password to be saved.
   * @param service
   *          - Type String. The servic used (used in filename).
   */
  public static void writeInfoToFile(Activity currAct, String username,
      String password, String service) {

    String filename = "userinfo-" + service + ".txt";
    File file = new File(currAct.getFilesDir(), filename);
    if (file.exists())
      file.delete();
    OutputStreamWriter writer = null;
    try {
      file.createNewFile();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    try {
      writer = new OutputStreamWriter(currAct.openFileOutput(filename,
          Context.MODE_PRIVATE));
      writer.write(username + "\n");
      // System.out.println("SUCCESSwrite1");
      writer.flush();
      writer.write(password + "\n");
      // System.out.println("SUCCESSwrite2");
      writer.flush();
      writer.close();

    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

  }

  /**
   * @param currAct
   *          - Type Activity. The main activity, in which the file is read.
   * @param service
   *          - Type String. The servic used (used in filename)
   * @return
   */
  public static ArrayList<String> getInfoFromFile(Activity currAct,
      String service) {
    String filename = "userinfo-" + service + ".txt";
    File file = new File(currAct.getFilesDir(), filename);
    if (!file.exists())
      // System.out.println("NOTEXISTS");
      return null;
    ArrayList<String> userinfoList = new ArrayList<String>();
    try {
      InputStream inputStream = currAct.openFileInput(filename);
      if (inputStream != null) {
        InputStreamReader streamReader = new InputStreamReader(inputStream);
        BufferedReader bufferedReader = new BufferedReader(streamReader);

        String username = bufferedReader.readLine();
        String password = bufferedReader.readLine();
        userinfoList.add(username);
        userinfoList.add(password);
        bufferedReader.close();

      }
    } catch (FileNotFoundException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return userinfoList;
  }

}
