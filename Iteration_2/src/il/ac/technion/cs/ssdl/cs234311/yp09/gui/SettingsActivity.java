package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.SaveStatistics;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.widget.FrameLayout;
import android.widget.TextView;


/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 16/1/2014
 * 
 */
public class SettingsActivity extends GeneralActivity {

  private TextView languageView;
  private TextView volumeView;
  private TextView exitView;

  // private TextView keyboardView;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_settings);

    final LongFragment longFragment = LongFragment
        .newInstance(LongFragment.Screen.Settings);
    getFragmentManager().beginTransaction()
        .add(R.id.long_press_info_frame, longFragment).commit();

    final ProgressBarFragment progressBarFragment = ProgressBarFragment
        .newInstance(ProgressBarFragment.Screen.Settings);
    getFragmentManager().beginTransaction()
        .add(R.id.progress_bar_frame, progressBarFragment).commit();

    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame, mFBFragment).commit();

    final FrameLayout box1 = (FrameLayout) findViewById(R.id.lang_box_frame);
    final DrawView rec1 = new DrawView(this, DrawView.Color.Blue,
        DrawView.Position.Center);
    box1.addView(rec1);

    final FrameLayout box2 = (FrameLayout) findViewById(R.id.volume_box_frame);
    final DrawView rec2 = new DrawView(this, DrawView.Color.Orange,
        DrawView.Position.Center);
    box2.addView(rec2);

    final FrameLayout box3 = (FrameLayout) findViewById(R.id.exit_box_frame);
    final DrawView rec3 = new DrawView(this, DrawView.Color.Green,
        DrawView.Position.Center);
    box3.addView(rec3);

    languageView = (TextView) findViewById(R.id.lang_option);
    volumeView = (TextView) findViewById(R.id.volume_option);
    exitView = (TextView) findViewById(R.id.exit_option);

    refreshActivity();
  }

  @Override
  public boolean onCreateOptionsMenu(final Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.settings, menu);
    return true;
  }

  @Override
  public void onResume() {
    super.onResume(); // Always call the superclass method first

    refreshActivity();

  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return false;
  }

  @Override
  public void onOperation(final int c) {
    super.onOperation(c);
    final Intent intent;
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      makeSound(0);
      intent = new Intent(this, LanguageActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      startActivity(intent);
      break;
    case BLUE_ORANGE:
      break;
    case BLUE_RED:
      break;
    case GREEN:
      makeSound(0);
      Intent homeIntent = new Intent(Intent.ACTION_MAIN);
      homeIntent.addCategory(Intent.CATEGORY_HOME);
      homeIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
      startActivity(homeIntent);
      if (statisticsMode)
        SaveStatistics.exitTheApp();
      break;
    case GREEN_RED:
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      makeSound(1);
      intent = new Intent(this, TextActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      intent.putExtra(TextActivity.prevKey, "HelpActivityPlease");
      intent.putExtra(TextActivity.messageKey, "ignored_parameter");
      startActivity(intent);
      break;
    case LONG_GREEN:
      break;
    case LONG_ORANGE:
      break;
    case LONG_RED:
      makeSound(1);
      finish();
      overridePendingTransition(0, 0);
      break;
    case ORANGE:
      makeSound(0);
      intent = new Intent(this, VolumeActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      startActivity(intent);
      break;
    case ORANGE_GREEN:
      break;
    case RED:
      break;
    default:
      break;
    }
  }

  private void refreshActivity() {

    setTitle(R.string.action_settings);

    final String value = Controller.language;
    final TextView language = (TextView) findViewById(R.id.lang_textview);
    if (value.equals("English"))
      language.setText(R.string.english);
    else if (value.equals("Hebrew"))
      language.setText(R.string.hebrew);
    else if (value.equals("Arabic"))
      language.setText(R.string.arabic);
    else if (value.equals("Russian"))
      language.setText(R.string.russian);

    final TextView volumeType = (TextView) findViewById(R.id.volume);
    if (VolumeActivity.mute == true)
      volumeType.setText("Mute");
    else
      volumeType.setText(VolumeActivity.strength + "/8");
    languageView.setText(R.string.language);
    volumeView.setText(R.string.volume);
    exitView.setText(R.string.exit);

    // BLTHState state = BluetoothReceiver.getBluetoothState();
    //
    // final TextView bluetoothOn = (TextView) findViewById(R.id.bluetooth);
    //
    // if (state == BLTHState.ACCEPTING)
    // bluetoothOn.setText(R.string.ready);
    // if (state == BLTHState.BLUETOOTH_OFF)
    // bluetoothOn.setText(R.string.off);
    // if (state == BLTHState.BLUETOOTH_ON)
    // bluetoothOn.setText(R.string.on);
    // if (state == BLTHState.CONNECTED)
    // bluetoothOn.setText(R.string.connected);

  }
}
