package il.ac.technion.cs.ssdl.cs234311.yp09.commsModule;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.messages.MessageDBManager;

import java.io.File;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.util.ArrayList;
import java.util.Collection;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import org.jivesoftware.smack.Chat;
import org.jivesoftware.smack.ChatManager;
import org.jivesoftware.smack.ConnectionConfiguration;
import org.jivesoftware.smack.ConnectionConfiguration.SecurityMode;
import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PacketListener;
import org.jivesoftware.smack.Roster;
import org.jivesoftware.smack.RosterEntry;
import org.jivesoftware.smack.SmackAndroid;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.XMPPException;
import org.jivesoftware.smack.filter.MessageTypeFilter;
import org.jivesoftware.smack.filter.PacketFilter;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Packet;
import org.jivesoftware.smack.packet.Presence;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;


/**
 * @author Daniel Eidel
 * @email da.eidel@gmail.com
 * 
 */
public class CommsModule {
  Handler handlerFacebook;
  Handler handlerGTalk;

  static String GTalkUserID;
  static String GTalkUserPassword;
  static String FacebookUserID;
  static String FacebookUserPassword;
  static Activity currAct;
  static XMPPConnection GTalkConnection;
  static XMPPConnection FacebookConnection;
  Collection<RosterEntry> contactsGTalk;
  Collection<RosterEntry> contactsFacebook;
  public static MessageDBManager messagesFacebook;
  public static MessageDBManager messagesGTalk;
  /**
   * List of parsed GTalk contacts
   */
  public static ArrayList<GoogleItem> contactsGTalkParsed;
  /**
   * List of parsed Facebook Contacts
   */
  public static ArrayList<FacebookItem> contactsFacebookParsed;

  /**
   * Facebook server host.
   */
  public static final String FACEBOOK_HOST = "chat.facebook.com";
  /**
   * GTalk Server host
   */
  public static final String GTALK_HOST = "talk.google.com";
  /**
   * XMPP port
   */
  public static final int PORT = 5222;
  /**
   * GTalk service postfix
   */
  public static final String GTALK_SERVICE = "gmail.com";

  void setGTalkConnection(final XMPPConnection connection) {
    CommsModule.GTalkConnection = connection;
  }

  void setFacebookConnection(final XMPPConnection connection) {
    CommsModule.FacebookConnection = connection;
  }

  void connectFacebook() {

    final class connectFacebookThread extends AsyncTask<Void, Integer, Boolean> {

      @Override
      protected Boolean doInBackground(Void... params) {
        // System.out.println("STARTED THREAD");
        contactsFacebookParsed = new ArrayList<FacebookItem>();
        ConnectionConfiguration connConfig = new ConnectionConfiguration(
            FACEBOOK_HOST, PORT);
        connConfig.setSASLAuthenticationEnabled(true);
        connConfig.setSecurityMode(SecurityMode.required);
        connConfig.setRosterLoadedAtLogin(true);
        connConfig.setTruststorePath("/system/etc/security/cacerts.bks");
        connConfig.setTruststorePassword("changeit");
        connConfig.setTruststoreType("bks");
        try {
          SSLContext sc = SSLContext.getInstance("TLS");
          KeyStore keyStore = KeyStore.getInstance(KeyStore.getDefaultType());
          TrustManagerFactory tmf = TrustManagerFactory
              .getInstance(TrustManagerFactory.getDefaultAlgorithm());
          tmf.init(keyStore);
          sc.init(null, null, new java.security.SecureRandom());
          connConfig.setCustomSSLContext(sc);
        } catch (GeneralSecurityException e) { // TODO Auto-generated catch
                                               // block
          e.printStackTrace();
        }
        // System.out.println("STARTED CONNECTION");
        XMPPConnection nConnection = new XMPPConnection(connConfig);
        try {
          nConnection.connect();
        } catch (final XMPPException ex) {
          // System.out.println("not Connected");
          setFacebookConnection(null);
          return Boolean.FALSE;
        }
        // System.out.println("STARTED LOGIN");
        try {
          nConnection.login(FacebookUserID, FacebookUserPassword);
          Presence presence = new Presence(Presence.Type.available);
          nConnection.sendPacket(presence);
          setFacebookConnection(nConnection);
        } catch (XMPPException ex) {
          // System.out.println("Not signed in");
          setFacebookConnection(null);
          return Boolean.FALSE;
        }
        try {
          Thread.sleep(2000);
        } catch (InterruptedException e) { // TODO Auto-generated catch block
          e.printStackTrace();
        }
        // System.out.println("STARTED ROSTER");
        int i = 0;
        if (isConnectedFacebook()) {
          Roster roster = FacebookConnection.getRoster();
          contactsFacebook = roster.getEntries();
          if (contactsFacebookParsed.isEmpty())
            for (final RosterEntry entry : contactsFacebook) {
              String contactID = entry.getUser();
              String contactName = entry.getName();
              Presence contactPresence = roster.getPresence(contactID);
              String presenceString;
              if (contactPresence.getMode() != null)
                presenceString = contactPresence.getMode().toString();
              else
                presenceString = contactPresence.getType().toString();
              FacebookItem contactFacebook = new FacebookItem(contactName,
                  contactID, presenceString, null);
              contactsFacebookParsed.add(contactFacebook);
              // System.out.println(contactID + " " + contactName + " "
              // + presenceString);
              publishProgress(i);
              i++;

            }
        }
        String FaceID[] = FacebookUserID.split("@");
        messagesFacebook = new MessageDBManager(currAct, FaceID[0] + "Facebook");
        PacketFilter filter = new MessageTypeFilter(Message.Type.chat);
        FacebookConnection.addPacketListener(new PacketListener() {
          @Override
          public void processPacket(Packet packet) {
            Message message = (Message) packet;
            String body = message.getBody();
            String from = message.getFrom();
            /*
             * Toast.makeText(currAct.getBaseContext(), from + " says: " + body
             * + " on Facebook", Toast.LENGTH_LONG) .show();
             */
            if (body != null)
              System.out.println(body + " " + from);
            messagesFacebook.insertMessage(from, body,
                System.currentTimeMillis(), 1);

          }
        }, filter);
        return Boolean.TRUE;
      }

      @Override
      protected void onProgressUpdate(Integer... a) {
        // System.out.println("Processing contact " + a[0]);
      }

      @Override
      protected void onPostExecute(Boolean result) {
        // The results of the above method
        // Processing the results here
        handlerFacebook.sendEmptyMessage(0);
      }
    }
    new connectFacebookThread().execute();

  }

  void connectGTalk() {
    final class connectGTalkThread extends AsyncTask<Void, Integer, Boolean> {
      @Override
      protected Boolean doInBackground(Void... params) {
        contactsGTalkParsed = new ArrayList<GoogleItem>();
        ConnectionConfiguration connConfig = new ConnectionConfiguration(
            GTALK_HOST, PORT, GTALK_SERVICE);
        connConfig.setRosterLoadedAtLogin(true);
        connConfig.setCompressionEnabled(true);
        connConfig.setSASLAuthenticationEnabled(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
          connConfig.setTruststoreType("AndroidCAStore");
          connConfig.setTruststorePassword(null);
          connConfig.setTruststorePath(null);
        } else {
          connConfig.setTruststoreType("BKS");
          String path = System.getProperty("javax.net.ssl.trustStore");
          if (path == null)
            path = System.getProperty("java.home") + File.separator + "etc"
                + File.separator + "security" + File.separator + "cacerts.bks";
          connConfig.setTruststorePath(path);
        }
        XMPPConnection nConnection = new XMPPConnection(connConfig);

        try {
          nConnection.connect();
          // Log.i("XMPPChatDemoActivity", "[SettingsDialog] Connected to "
          // + nConnection.getHost());
        } catch (final XMPPException ex) {
          System.out.println(ex.toString());
          // System.out.println("Number 1");
          setGTalkConnection(null);
          return Boolean.FALSE;

        }
        try {
          nConnection.login(GTalkUserID, GTalkUserPassword);
          Presence presence = new Presence(Presence.Type.available);
          nConnection.sendPacket(presence);
          setGTalkConnection(nConnection);

        } catch (final XMPPException ex) {
          // System.out.println("Number 2");
          setGTalkConnection(null);
          return Boolean.FALSE;
        }
        try {
          Thread.sleep(2000);
        } catch (InterruptedException e1) {
          // TODO Auto-generated catch block
          e1.printStackTrace();
        }
        if (isConnectedGTalk()) {
          Roster roster = GTalkConnection.getRoster();
          contactsGTalk = roster.getEntries();
          if (contactsGTalkParsed.isEmpty())
            for (RosterEntry entry : contactsGTalk) {
              String contactID = entry.getUser();
              String contactName = entry.getName();
              Presence contactPresence = roster.getPresence(contactID);
              String presenceString;
              if (contactPresence.getMode() != null)
                presenceString = contactPresence.getMode().toString();
              else
                presenceString = contactPresence.getType().toString();
              GoogleItem contactGTalk = new GoogleItem(contactName, contactID,
                  presenceString, null);
              contactsGTalkParsed.add(contactGTalk);
              /*
               * System.out.println(contactID + " " + contactName + " " +
               * presenceString);
               */
            }
        }
        messagesGTalk = new MessageDBManager(currAct, GTalkUserID + "GTalk");
        PacketFilter filter = new MessageTypeFilter(Message.Type.chat);
        GTalkConnection.addPacketListener(new PacketListener() {
          @Override
          public void processPacket(Packet packet) {
            Message message = (Message) packet;
            String body = message.getBody();
            String[] fromList = message.getFrom().split("/");
            String from = fromList[0];
            /*
             * Toast.makeText(currAct.getBaseContext(), from + " says: " + body
             * + " on Gtalk", Toast.LENGTH_LONG) .show();
             */
            if (body != null) {
              System.out.println(body + " " + from);
              messagesGTalk.insertMessage(from, body,
                  System.currentTimeMillis(), 1);
            }

          }
        }, filter);
        return Boolean.TRUE;

      }

      @Override
      protected void onProgressUpdate(Integer... a) {
        // System.out.println("Processing contact " + a[0]);
      }

      @Override
      protected void onPostExecute(Boolean result) {
        // The results of the above method
        // Processing the results here
        handlerGTalk.sendEmptyMessage(0);
      }
    }

    new connectGTalkThread().execute();

  }

  /**
   * @param act
   *          - Main activity of the app
   */
  public CommsModule(final Activity act) {
    currAct = act;
    contactsGTalkParsed = new ArrayList<GoogleItem>();
    contactsFacebookParsed = new ArrayList<FacebookItem>();
    SmackAndroid.init(act.getBaseContext());

  }

  /**
   * @param username
   *          - GTalk username (minus @gmail.com)
   * @param password
   *          - GTalk password
   * @param handler
   *          - The Handler which is called at the AsyncTask's end
   * @return true if connected successfully
   */
  public boolean loginGTalk(String username, String password, Handler handler) {
    ConnectivityManager cm = (ConnectivityManager) currAct.getBaseContext()
        .getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
      GTalkUserID = username;
      GTalkUserPassword = password;
      handlerGTalk = handler;
      connectGTalk();

    } else {
      Toast.makeText(currAct.getBaseContext(), R.string.no_internet,
          Toast.LENGTH_SHORT).show();
      return false;
    }
    if (GTalkConnection == null)
      return false;

    return true;
  }

  /**
   * @param username
   *          - Facebook username (email used to register)
   * @param password
   *          - Facebook password
   * @param handler
   *          - The Handler which is called at the AsyncTask's end
   * @return true if connected successfully
   */
  public boolean loginFacebook(String username, String password, Handler handler) {
    ConnectivityManager cm = (ConnectivityManager) currAct.getBaseContext()
        .getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()) {
      FacebookUserID = username;
      FacebookUserPassword = password;
      handlerFacebook = handler;
      connectFacebook();

    } else {
      Toast.makeText(currAct.getBaseContext(), R.string.no_internet,
          Toast.LENGTH_SHORT).show();
      return false;
    }

    if (FacebookConnection == null)
      return false;

    return true;

  }

  /**
   * @return - If the user is connected and logged in into Facebook
   */
  public boolean isConnectedFacebook() {
    if (FacebookConnection == null)
      return false;
    return FacebookConnection.isConnected()
        && FacebookConnection.isAuthenticated();
  }

  /**
   * @return If the user is connected and logged in into GTalk
   */
  public boolean isConnectedGTalk() {
    if (GTalkConnection == null)
      return false;
    return GTalkConnection.isConnected() && GTalkConnection.isAuthenticated();
  }

  /**
   * @param msgText
   *          - Text of the message to be sent
   * @param addressee
   *          - The JID of the intended message recipient
   * 
   */
  public void sendGTalkMessage(final String msgText, final String addressee) {
    ConnectivityManager cm = (ConnectivityManager) currAct.getBaseContext()
        .getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    if (activeNetwork == null || !activeNetwork.isConnectedOrConnecting()
        || !isConnectedGTalk())
      return;
    final ChatManager chatMgr = GTalkConnection.getChatManager();
    final Chat newChat = chatMgr.createChat(addressee, new MessageListener() {

      @Override
      public void processMessage(final Chat arg0,
          final org.jivesoftware.smack.packet.Message arg1) {
        // TODO Auto-generated method stub

      }
    });

    try {
      newChat.sendMessage(msgText);
    } catch (final XMPPException ex) {
      Log.e("CommsModule", ex.toString());
    }
    messagesGTalk.insertMessage(addressee, msgText, System.currentTimeMillis(),
        2);
  }

  /**
   * @param msgText
   *          - Text of the message to be sent
   * @param addressee
   *          - The JID of the intended message recipient
   */
  public void sendFacebookMessage(final String msgText, final String addressee) {
    ConnectivityManager cm = (ConnectivityManager) currAct.getBaseContext()
        .getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
    if (activeNetwork == null || !activeNetwork.isConnectedOrConnecting()
        || !isConnectedFacebook())
      return;
    final ChatManager chatMgr = FacebookConnection.getChatManager();
    final Chat newChat = chatMgr.createChat(addressee, new MessageListener() {

      @Override
      public void processMessage(final Chat arg0,
          final org.jivesoftware.smack.packet.Message arg1) {
        // TODO Auto-generated method stub

      }
    });

    try {
      newChat.sendMessage(msgText);
    } catch (final XMPPException ex) {
      Log.e("CommsModule", ex.toString());
    }
    messagesFacebook.insertMessage(addressee, msgText,
        System.currentTimeMillis(), 2);

  }

  public String whoIsConnectedGTalk() {
    String GTalkConnected = null;
    if (isConnectedGTalk())
      GTalkConnected = GTalkUserID;
    return GTalkConnected;
  }

  public String whoIsConnectedFacebook() {
    String FacebookConnected = null;
    if (isConnectedFacebook())
      FacebookConnected = FacebookUserID;
    return FacebookConnected;
  }

  public void disconnectGTalk() {
    if (GTalkConnection != null)
      GTalkConnection.disconnect();
  }

  public void disconnectFacebook() {
    if (FacebookConnection != null)
      FacebookConnection.disconnect();
  }

  /**
   * @param message
   *          - Text of the message to be sent
   * @param phoneNo
   *          - The phone number of the intended message recipient
   */
  public static void sendSMS(final String message, final String phoneNo) {
    if (phoneNo.length() <= 0 || message.length() <= 0) {
      Toast.makeText(currAct.getBaseContext(), R.string.enter_phone_message,
          Toast.LENGTH_SHORT).show();
      return;
    }
    final String SENT = "SMS_SENT";
    final String DELIVERED = "SMS_DELIVERED";
    final PendingIntent sentPI = PendingIntent.getBroadcast(currAct, 0,
        new Intent(SENT), 0);

    final PendingIntent deliveredPI = PendingIntent.getBroadcast(currAct, 0,
        new Intent(DELIVERED), 0);
    currAct.registerReceiver(new BroadcastReceiver() {
      @Override
      public void onReceive(final Context arg0, final Intent arg1) {
        switch (getResultCode()) {
        case Activity.RESULT_OK:
          Toast.makeText(currAct.getBaseContext(), R.string.sms_sent,
              Toast.LENGTH_SHORT).show();
          break;
        case SmsManager.RESULT_ERROR_GENERIC_FAILURE:
          Toast.makeText(currAct.getBaseContext(), R.string.generic_failure,
              Toast.LENGTH_SHORT).show();
          break;
        case SmsManager.RESULT_ERROR_NO_SERVICE:
          Toast.makeText(currAct.getBaseContext(), R.string.no_service,
              Toast.LENGTH_SHORT).show();
          break;
        case SmsManager.RESULT_ERROR_NULL_PDU:
          Toast.makeText(currAct.getBaseContext(), R.string.null_pdu,
              Toast.LENGTH_SHORT).show();
          break;
        case SmsManager.RESULT_ERROR_RADIO_OFF:
          Toast.makeText(currAct.getBaseContext(), R.string.radio_off,
              Toast.LENGTH_SHORT).show();
          break;
        default:
          break;
        }
      }
    }, new IntentFilter(SENT));

    currAct.registerReceiver(new BroadcastReceiver() {
      @Override
      public void onReceive(final Context arg0, final Intent arg1) {
        switch (getResultCode()) {
        case Activity.RESULT_OK:
          Toast.makeText(currAct.getBaseContext(), R.string.sms_delivered,
              Toast.LENGTH_SHORT).show();
          break;
        case Activity.RESULT_CANCELED:
          Toast.makeText(currAct.getBaseContext(), R.string.sms_not_delivered,
              Toast.LENGTH_SHORT).show();
          break;
        default:
          break;
        }
      }
    }, new IntentFilter(DELIVERED));
    final SmsManager SMgr = SmsManager.getDefault();
    final TelephonyManager phoneManager = (TelephonyManager) currAct
        .getApplicationContext().getSystemService(Context.TELEPHONY_SERVICE);
    if (phoneManager.getSimState() == TelephonyManager.SIM_STATE_ABSENT) {
      Toast.makeText(currAct.getBaseContext(), R.string.no_sim,
          Toast.LENGTH_SHORT).show();
      return;
    }
    String myPhoneNo = phoneManager.getLine1Number();
    SMgr.sendTextMessage(phoneNo, myPhoneNo, message, sentPI, deliveredPI);
    ContentValues values = new ContentValues();
    values.put("address", phoneNo);
    values.put("body", message);
    currAct.getContentResolver()
        .insert(Uri.parse("content://sms/sent"), values);

  }
}