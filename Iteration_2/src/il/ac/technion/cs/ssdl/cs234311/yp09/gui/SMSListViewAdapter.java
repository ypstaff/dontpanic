package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.contacts.SMSItem;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 17/1/2014
 * 
 */
public class SMSListViewAdapter extends ArrayAdapter<SMSItem> {

  Context context;

  /**
   * @param context
   *          current context
   * @param resourceId
   *          resource identifier
   * @param items
   *          list of sms items
   */
  public SMSListViewAdapter(final Context context, final int resourceId,
      final List<SMSItem> items) {
    super(context, resourceId, items);
    this.context = context;
  }

  /* private view holder class */
  private class ViewHolder {
    public ViewHolder() {
      // TODO Auto-generated constructor stub
    }

    ImageView type;
    TextView name;
    TextView number;
    CheckBox checkbox;
  }

  @Override
  public View getView(final int position, View convertView,
      final ViewGroup parent) {

    ViewHolder holder = null;
    View $ = convertView;
    final SMSItem smsRow = getItem(position);

    final LayoutInflater mInflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    if ($ == null) {
      $ = mInflater.inflate(R.layout.sms_listview, null);
      holder = new ViewHolder();
      holder.name = (TextView) $.findViewById(R.id.name);
      holder.number = (TextView) $.findViewById(R.id.number);
      holder.type = (ImageView) $.findViewById(R.id.type);
      holder.checkbox = (CheckBox) $.findViewById(R.id.check_box);
      $.setTag(holder);
    } else
      holder = (ViewHolder) $.getTag();

    holder.name.setText(smsRow.contactName);
    holder.number.setText(smsRow.contactPhone);
    holder.type.setImageResource(smsRow.contactType);

    if (smsRow.checked)
      holder.checkbox.setChecked(true);
    else
      holder.checkbox.setChecked(false);
    return $;

  }

}
