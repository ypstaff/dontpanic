package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

/**
 * The interface for a class that listens to operations from a four button
 * device.
 * 
 * @author Itamar
 * 
 */
public interface OperationListener {
  /**
   * Called when the device signals an operation is requested with a given code.
   * 
   * @param c
   *          The code of the operation.
   */
  public void onOperation(int c);
}
