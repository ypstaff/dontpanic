package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;

import java.util.Timer;
import java.util.TimerTask;

import android.app.Fragment;
import android.content.res.Resources;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TableRow;


/**
 * @author Itamar Bitton
 * 
 */

public class FourButtonsFragment extends Fragment {

  boolean[] pressed = new boolean[4];
  boolean multitouch = false, valid = false;
  OperationListener mOpListener;
  PressListener mPressListener;
  Handler mHandler;
  final static String TAG = "FBFrag";

  enum OpCode {
    LONG(1), BLUE(2), ORANGE(4), GREEN(8), RED(16);
    private final int code;

    private OpCode(final int c) {
      code = c;
    }

    public int getCode() {
      return code;
    }
  }

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    final View $ = inflater
        .inflate(R.layout.buttons_fragment, container, false);
    final int[] bIDs = { R.id.blue_button, R.id.orange_button,
        R.id.green_button, R.id.red_button };
    final Resources r = getResources();
    final int[] n = { r.getColor(R.color.blue), r.getColor(R.color.orange),
        r.getColor(R.color.green), r.getColor(R.color.red) };
    final int[] h = { r.getColor(R.color.blue_hi),
        r.getColor(R.color.orange_hi), r.getColor(R.color.green_hi),
        r.getColor(R.color.red_hi) };
    for (int i = 0; i < 4; i++)
      ((Button) $.findViewById(bIDs[i])).setOnTouchListener(new ButtonListener(
          i, n[i], h[i]));

    if (debugMode)
      addDebugViews((LinearLayout) $
          .findViewById(R.id.ButtonsFragmentLinearLayout));

    return $;
  }

  /**
   * Sets the <code>Handler</code> associated with the fragment.
   * 
   * @param h
   *          The <code>Handler</code>.
   */
  public void setHandler(final Handler h) {
    mHandler = h;
  }

  /**
   * The <code>OperationListener</code> associated with the fragment.
   * 
   * @param l
   *          The <code>OperationListener</code>.
   */
  public void setOpListener(final OperationListener l) {
    mOpListener = l;
  }

  /**
   * The <code>PressListener</code> associated with the fragment.
   * 
   * @param l
   *          The <code>PressListener</code>.
   */
  public void setPressListener(final PressListener l) {
    mPressListener = l;
  }

  private class ButtonListener implements OnTouchListener {

    private static final int LONG_PRESS_DELAY = 2000;
    private static final int SHORT_PRESS_DELAY = 0;
    long pressTime, duration;
    private final int id, normal, high;
    private Timer timer;

    public ButtonListener(final int id, final int n, final int h) {
      this.id = id;
      normal = n;
      high = h;
    }

    @Override
    public synchronized boolean onTouch(final View v, final MotionEvent e) {
      switch (e.getAction()) {
      case MotionEvent.ACTION_DOWN:
        pressTime = System.currentTimeMillis();
        ((Button) v).setBackgroundColor(high);
        multitouch = pressed[0] || pressed[1] || pressed[2] || pressed[3];
        pressed[id] = true;
        valid = true;
        if (!multitouch) {
          timer = new Timer();
          timer.schedule(new TimerTask() {
            @Override
            public void run() {
              mHandler.post(new Runnable() {
                @Override
                public void run() {
                  mPressListener.onPress(getCode());
                }
              });
            }
          }, SHORT_PRESS_DELAY);
          timer.schedule(new TimerTask() {
            @Override
            public void run() {
              mPressListener.onHeld(0);
            }
          }, LONG_PRESS_DELAY);
        }
        return true;
      case MotionEvent.ACTION_UP:
        duration = System.currentTimeMillis() - pressTime;
        ((Button) v).setBackgroundColor(normal);
        if (timer != null) {
          timer.cancel();
          timer.purge();
        }
        final int code = getCode();
        pressed[id] = false;
        multitouch = pressed[0] || pressed[1] || pressed[2] || pressed[3];
        if (valid) {
          if (duration >= SHORT_PRESS_DELAY && duration < LONG_PRESS_DELAY)
            mOpListener.onOperation(code);
          if (!multitouch && duration >= LONG_PRESS_DELAY)
            mOpListener.onOperation(code | OpCode.LONG.getCode());
          mPressListener.onRelease(code);
          valid = false;
        }
        return true;
      default:
        return false;
      }
    }

    int getCode() {
      final int code = (pressed[0] ? OpCode.BLUE.getCode() : 0)
          | (pressed[1] ? OpCode.ORANGE.getCode() : 0)
          | (pressed[2] ? OpCode.GREEN.getCode() : 0)
          | (pressed[3] ? OpCode.RED.getCode() : 0);
      return code;
    }
  }

  // *************************************
  // *************************************
  // *************************************
  // *************************************
  // For debug:

  // These values should be the same as in OpCodeInterpreter
  private static final short LONG_MASK = 1, BLUE_MASK = 2, ORANGE_MASK = 4,
      GREEN_MASK = 8, RED_MASK = 16;

  /**
   * Set debugMode to true if you want to show the buttons for debug
   */
  public static boolean debugMode = false;

  private void addButton(final LinearLayout layout, final String text,
      final int onOpClick) {
    final Button button = new Button(getActivity());
    button.setText(text);
    button.setOnClickListener(new OnClickListener() {
      @Override
      public void onClick(final View v) {
        mOpListener.onOperation(onOpClick);
      }
    });

    final TableRow.LayoutParams params = new TableRow.LayoutParams(
        LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, 1);

    layout.addView(button, params);
  }

  private void addDebugViews(final LinearLayout layout) {
    layout.removeAllViews();

    addButton(layout, "1", BLUE_MASK);
    addButton(layout, "2", ORANGE_MASK);
    addButton(layout, "3", GREEN_MASK);
    addButton(layout, "4", RED_MASK);
    addButton(layout, "5", BLUE_MASK | LONG_MASK);
    addButton(layout, "6", ORANGE_MASK | LONG_MASK);
    addButton(layout, "7", GREEN_MASK | LONG_MASK);
    addButton(layout, "8", RED_MASK | LONG_MASK);
    addButton(layout, "9", BLUE_MASK | ORANGE_MASK);
    addButton(layout, "a", ORANGE_MASK | GREEN_MASK);
    addButton(layout, "b", GREEN_MASK | RED_MASK);
    addButton(layout, "c", BLUE_MASK | RED_MASK);
  }

}
