package il.ac.technion.cs.ssdl.cs234311.yp09.keyboards;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.keyboards.KeyboardFragment.KeyboardContent;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.text.Html;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;


/**
 * @author Owais Musa
 * 
 */
public class DefaultKeyboardLayoutFragment extends Fragment {
  private KeyboardFragment mKeyboardFragment;
  private TextView mTextViews[];
  private String originalContent[];

  private String mBlueColor, mOrangeColor, mGreenColor, mRedColor, mBlackColor;

  TableRow mMainTableRow;
  TableRow mSecondaryTableRow;

  private void loadColors() {
    mBlueColor = getActivity().getResources().getString(
        R.string.blue_color_as_string);
    mOrangeColor = getActivity().getResources().getString(
        R.string.orange_color_as_string);
    mGreenColor = getActivity().getResources().getString(
        R.string.green_color_as_string);
    mRedColor = getActivity().getResources().getString(
        R.string.red_color_as_string);
    mBlackColor = getActivity().getResources().getString(
        R.string.black_color_as_string);
  }

  private String getAppropriateColor(int i) {
    switch (i) {
    case 0:
      return mBlueColor;
    case 1:
      return mBlackColor;
    case 2:
      return mOrangeColor;
    case 3:
      return mBlackColor;
    case 4:
      return mGreenColor;
    case 5:
      return mBlackColor;
    case 6:
      return mRedColor;
    default:
      break;
    }

    return null;
  }

  private void resetTextViews() {
    for (int i = 0; i < 12; i++) {
      mTextViews[i].setText(originalContent[i]);
      mTextViews[i]
          .setTextColor(!mKeyboardFragment.getSwitched() ? getResources()
              .getColor(R.color.clickable_color) : getResources().getColor(
              R.color.unclickable_color));
    }
  }

  private void updateSmilies() {
    String[] smiles = mKeyboardFragment.getSmiles();
    boolean switched = mKeyboardFragment.getSwitched();

    assert smiles != null;

    for (int i = 0; i < smiles.length; i++) {
      final TextView smileTextView = new TextView(getActivity());

      smileTextView.setText(smiles[i]);

      smileTextView.setGravity(Gravity.CENTER);

      smileTextView.setTextColor(!switched ? getResources().getColor(
          R.color.clickable_color) : getResources().getColor(
          R.color.unclickable_color));
      smileTextView.setTextColor(!switched ? getResources().getColor(
          R.color.clickable_color) : getResources().getColor(
          R.color.unclickable_color));

      mSecondaryTableRow.addView(mKeyboardFragment
          .getAppropriateFrameLayout(smileTextView));
    }
  }

  private void resetTextViewsInTheRows() {
    for (int i = 0; i < 12; i++)
      if (i < 5)
        mMainTableRow.addView(mTextViews[i]);
      else
        mSecondaryTableRow.addView(mTextViews[i]);
  }

  /**
   * 
   */
  public void updateFragment() {
    mMainTableRow.removeAllViews();
    mSecondaryTableRow.removeAllViews();

    if (mKeyboardFragment.getKeyboardContent() == KeyboardContent.SMILIES) {
      updateSmilies();
      return;
    }

    resetTextViewsInTheRows();

    resetTextViews();

    char charactersArrays[][] = mKeyboardFragment.getCharactersArrays();
    int markedCharacter[] = mKeyboardFragment.getMarkedCharacter();

    int originalNumbersArray[] = { 0, 2, 3, 4, 6 };
    int currentNumbersArray[] = { 0, 1, 2, 3, 4 };

    for (int i = 0; i < 5; i++) {
      String mainHtmlChar;
      if (markedCharacter[originalNumbersArray[i]] < 0)
        mainHtmlChar = "<font color=\""
            + getAppropriateColor(originalNumbersArray[i]) + "\">"
            + charactersArrays[originalNumbersArray[i]][0] + "</font>";
      else if (markedCharacter[originalNumbersArray[i]] == 0)
        mainHtmlChar = "<font color=\""
            + getAppropriateColor(originalNumbersArray[i]) + "\"><b>"
            + charactersArrays[originalNumbersArray[i]][0] + "</b></font>";
      else
        mainHtmlChar = charactersArrays[originalNumbersArray[i]][0] + "";

      mTextViews[currentNumbersArray[i]].setText(Html.fromHtml(mainHtmlChar));
    }

    int currentSecondaryNumbers[] = { 5, 6, 7, 8, 9, 10, 11 };

    for (int i = 0; i < 7; i++) {
      String htmlLetters = "";
      if (i == 1 || i == 5)
        if (markedCharacter[i] == 0)
          htmlLetters += "<font color=\"" + getAppropriateColor(i) + "\"><b>"
              + charactersArrays[i][0] + "</b></font><br>";
        else
          htmlLetters += charactersArrays[i][0] + "<br>";

      for (int j = 1; j < charactersArrays[i].length; j++) {
        if (j > 1 && i != 0 && i != 2 && i != 4 && i != 6)
          htmlLetters += "<br>";
        if (j == markedCharacter[i])
          htmlLetters += "<font color=\"" + getAppropriateColor(i) + "\"><b>"
              + charactersArrays[i][j] + "</b></font>";
        else
          htmlLetters += charactersArrays[i][j];
      }

      mTextViews[currentSecondaryNumbers[i]]
          .setText(Html.fromHtml(htmlLetters));
    }
  }

  private void initializeTextViews(View container) {
    mTextViews = new TextView[12];
    originalContent = new String[12];
    mTextViews[0] = (TextView) container.findViewById(R.id.alphaTextView1);
    mTextViews[1] = (TextView) container.findViewById(R.id.alphaTextView2);
    mTextViews[2] = (TextView) container.findViewById(R.id.alphaTextView3);
    mTextViews[3] = (TextView) container.findViewById(R.id.alphaTextView4);
    mTextViews[4] = (TextView) container.findViewById(R.id.alphaTextView5);
    mTextViews[5] = (TextView) container.findViewById(R.id.alphaTextView6);
    mTextViews[6] = (TextView) container.findViewById(R.id.alphaTextView7);
    mTextViews[7] = (TextView) container.findViewById(R.id.alphaTextView8);
    mTextViews[8] = (TextView) container.findViewById(R.id.alphaTextView9);
    mTextViews[9] = (TextView) container.findViewById(R.id.alphaTextView10);
    mTextViews[10] = (TextView) container.findViewById(R.id.alphaTextView11);
    mTextViews[11] = (TextView) container.findViewById(R.id.alphaTextView12);

    for (int i = 0; i < 12; i++)
      originalContent[i] = mTextViews[i].getText().toString();

    int originalNumbersArray[] = { 0, 2, 3, 4, 6 };
    char charsArray[] = { 'a', 'e', 'i', 'o', 'u' };

    for (int i = 0; i < 5; i++) {
      String mainHtmlChar;
      mainHtmlChar = "<font color=\""
          + getAppropriateColor(originalNumbersArray[i]) + "\">"
          + charsArray[i] + "</font>";
      mTextViews[i].setText(Html.fromHtml(mainHtmlChar));
    }
  }

  @SuppressLint("NewApi")
  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {
    final View $ = inflater.inflate(R.layout.default_keyboard_layout_fragment,
        container, false);

    initializeTextViews($);
    mKeyboardFragment = (KeyboardFragment) getParentFragment();

    mMainTableRow = (TableRow) $.findViewById(R.id.mainTableRow);
    mSecondaryTableRow = (TableRow) $.findViewById(R.id.secondaryTableRow);

    return $;
  }

  @Override
  public void onAttach(final Activity activity) {
    super.onAttach(activity);
    loadColors();
  }

}
