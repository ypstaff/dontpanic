package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.commsModule.FacebookItem;
import il.ac.technion.cs.ssdl.cs234311.yp09.commsModule.GoogleItem;
import il.ac.technion.cs.ssdl.cs234311.yp09.contacts.SMSItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.app.Activity;
import android.content.res.Configuration;
import android.content.res.Resources;

/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date174/1/2014
 * 
 */
public class Controller {

  /**
   * different screen types
   * 
   */
  public enum Screen {
    /**
     * default value
     */
    Undefined, /**
     * Typing screen
     */
    Typing, /**
     * Service screen
     */
    Service, /**
     * contacts screen
     */
    Contact, /**
     * general screen
     */
    General
  }

  /**
   * the current used language
   */
  public static String language = "English";

  /**
   * where should the setting screen back to
   */
  public static Screen settingsBackTo = Screen.Undefined;

  /**
   * sms contacts list
   */
  public static ArrayList<SMSItem> smsList;

  /**
   * google contacts list
   */
  public static List<GoogleItem> googleList;

  /**
   * facebook contact list
   */
  public static List<FacebookItem> facebookList;

  /**
   * the message to send
   */
  public static String message;

  /**
   * @param activity
   *          The running activity
   * @param Wantedlanguage
   *          A string specifying the wanted language
   */
  public static void changeLocale(Activity activity, String Wantedlanguage) {
    final Resources res = activity.getResources();
    final Configuration conf = res.getConfiguration();
    conf.locale = new Locale(Wantedlanguage);
    res.updateConfiguration(conf, null);
  }
}
