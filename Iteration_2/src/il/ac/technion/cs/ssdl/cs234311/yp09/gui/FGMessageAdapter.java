package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.messages.ConversationItem;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 17/1/2014
 * 
 */
public class FGMessageAdapter extends ArrayAdapter<ConversationItem> {

  Context context;

  /**
   * @param context
   *          current context
   * @param resourceId
   *          resource identifier
   * @param items
   *          list of sms items
   */
  public FGMessageAdapter(final Context context, final int resourceId,
      final List<ConversationItem> items) {
    super(context, resourceId, items);
    this.context = context;
  }

  /* private view holder class */
  private class ViewHolder {
    public ViewHolder() {
      // TODO Auto-generated constructor stub
    }

    ImageView type;
    TextView date;
    TextView message;
    TextView from_to;

  }

  @Override
  public View getView(final int position, View convertView,
      final ViewGroup parent) {

    ViewHolder holder = null;
    View $ = convertView;
    final ConversationItem messageRow = getItem(position);

    final LayoutInflater mInflater = (LayoutInflater) context
        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    if ($ == null) {
      $ = mInflater.inflate(R.layout.from_to_item, null);
      holder = new ViewHolder();
      holder.message = (TextView) $.findViewById(R.id.message);
      holder.date = (TextView) $.findViewById(R.id.date);
      holder.type = (ImageView) $.findViewById(R.id.type);
      holder.from_to = (TextView) $.findViewById(R.id.from_to);
      $.setTag(holder);
    } else
      holder = (ViewHolder) $.getTag();

    if (messageRow.messageBody.length() > 15)
      holder.message.setText(messageRow.messageBody.substring(0, 15) + "...");
    else
      holder.message.setText(messageRow.messageBody);

    holder.date.setText(messageRow.messageDate);

    if (messageRow.messageType == 1) {
      holder.type.setImageResource(R.drawable.inc_message);
      holder.from_to.setText("From: " + messageRow.from_to);
    } else if (messageRow.messageType == 2) {
      holder.type.setImageResource(R.drawable.out_message);
      holder.from_to.setText("To: " + messageRow.from_to);
    }

    return $;

  }
}
