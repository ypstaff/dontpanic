package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;


/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 4/1/2014
 * 
 */
public class DoneFragment extends Fragment {

  private TextView doneView;

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    final View $ = inflater.inflate(R.layout.done_fragment, container, false);

    final FrameLayout next_box_frame = (FrameLayout) $
        .findViewById(R.id.done_box_frame);
    final DrawView next_rec = new DrawView(getActivity(), DrawView.Color.Red,
        DrawView.Position.Center);
    next_box_frame.addView(next_rec);

    doneView = (TextView) $.findViewById(R.id.done_option);

    return $;
  }

  @Override
  public void onResume() {
    super.onResume();
    doneView.setText(R.string.done);
  }
}
