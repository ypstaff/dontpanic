package il.ac.technion.cs.ssdl.cs234311.yp09.commsModule;

import android.graphics.Bitmap;

/**
 * @author Daniel Eidel
 * 
 */
public class GoogleItem {
  /**
   * Contact name as shown in roster
   */
  public String contactName;
  /**
   * Contact JID
   */
  public String contactID;
  /**
   * Contact presence, converted to String
   */
  public String contactPresence;
  /**
   * Contact avatar (unused in the iteration)
   */
  public Bitmap contactAvatar;
  /**
   * Flag whether the contact has been selected on screen
   */
  public boolean checked;

  /**
   * @param contactName
   *          - Contact name as shown in roster
   * @param contactID
   *          - Contact JID
   * @param contactPresence
   *          - Contact presence, converted to String
   * @param contactAvatar
   *          - Contact avatar (unused in the iteration)
   */
  public GoogleItem(String contactName, String contactID,
      String contactPresence, Bitmap contactAvatar) {
    this.contactName = contactName;
    this.contactID = contactID;
    this.contactPresence = contactPresence;
    this.contactAvatar = contactAvatar;
    this.checked = false;
  }

}
