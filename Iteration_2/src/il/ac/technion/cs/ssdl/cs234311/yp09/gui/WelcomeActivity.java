package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.facebookFeedGui.FacebookFeedLoginActivity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.CheckBox;


/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 4/1/2014
 * 
 */

public class WelcomeActivity extends GeneralActivity {

  final Context context = this;
  // private static final String TAG = "Main";
  SharedPreferences preferences;

  // FontFitTextView fitTextView;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    preferences = getSharedPreferences(
        "il.ac.technion.cs.ssdl.cs234311.yp09.fbip", MODE_PRIVATE);

    setTitle(R.string.welcome_message);
    setContentView(R.layout.activity_main);

    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame_main, mFBFragment).commit();

    final AckMainFragment ackFragment = new AckMainFragment();
    getFragmentManager().beginTransaction()
        .add(R.id.ack_main_fragment, ackFragment).commit();

    // fitTextView = (FontFitTextView) findViewById(R.id.instructions_main);
    // fitTextView.setMaxTextSize(200);

  }

  @Override
  public boolean onCreateOptionsMenu(final Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.first, menu);
    return true;
  }

  @Override
  public void onPause() {
    super.onPause();
    overridePendingTransition(0, 0);
  }

  /**
   * @param view
   *          to be deleted
   */
  public void nextScr(final View view) {
    final Intent intent = new Intent(this, TypingActivity.class);
    intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
    startActivity(intent);
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return false;
  }

  @Override
  public void onOperation(final int c) {
    super.onOperation(c);
    Intent intent;
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      makeSound(0);
      final CheckBox cb = (CheckBox) findViewById(R.id.check_box);
      cb.setChecked(!cb.isChecked());
      preferences.edit().putBoolean("showWelcomeScreen", !cb.isChecked())
          .commit();
      break;
    case BLUE_ORANGE:
      break;
    case BLUE_RED:
      break;
    case GREEN:
      makeSound(0);
      intent = new Intent(this, FacebookFeedLoginActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      startActivity(intent);
      break;
    case GREEN_RED:
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      break;
    case LONG_GREEN:
      break;
    case LONG_ORANGE:
      break;
    case LONG_RED:
      break;
    case ORANGE:
      break;
    case ORANGE_GREEN:
      break;
    case RED:
      makeSound(0);
      intent = new Intent(this, TypingActivity.class);
      intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
      startActivity(intent);
      break;
    default:
      break;
    }
  }

  @Override
  public synchronized void onPress(int c) {
    // do nothing
  }

  @Override
  public synchronized void onRelease(int c) {
    // do nothing
  }

  @Override
  public void onHeld(int c) {
    // do nothing
  }
}
