package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;


/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 4/1/2014
 * 
 */
public class AckMainFragment extends Fragment {

  @Override
  public View onCreateView(final LayoutInflater inflater,
      final ViewGroup container, final Bundle savedInstanceState) {
    // Inflate the layout for this fragment
    final View $ = inflater.inflate(R.layout.ack_menu_main, container, false);

    final FrameLayout check_box_frame = (FrameLayout) $
        .findViewById(R.id.check_box_frame);
    final DrawView check_rec = new DrawView(getActivity(), DrawView.Color.Blue,
        DrawView.Position.Center);
    check_box_frame.addView(check_rec);

    final FrameLayout next_box_frame = (FrameLayout) $
        .findViewById(R.id.next_box_frame);
    final DrawView next_rec = new DrawView(getActivity(), DrawView.Color.Red,
        DrawView.Position.Center);
    next_box_frame.addView(next_rec);

    return $;
  }
}
