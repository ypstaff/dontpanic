package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import il.ac.technion.cs.ssdl.cs234311.yp09.commsModule.CommsModule;
import il.ac.technion.cs.ssdl.cs234311.yp09.contacts.ContactsModule;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.AsyncTask;
import android.os.Bundle;


/**
 * The first activity launched in the app. Displays a splash screen and starts
 * the appropriate activity.
 * 
 * @author Itamar
 * 
 */
public class SplashActivity extends GeneralActivity {
  Intent intent;
  SharedPreferences preferences;
  private int[] cid = { R.color.blue, R.color.orange, R.color.green,
      R.color.red };

  static SoundPool mSoundPool;
  static int mSoundId;
  static AudioManager mAudioManager;
  static boolean mCanPlayAudio;

  // private static final String TAG = "Splash";
  /**
   * Constructor for the Application
   * 
   * @param context
   * @return
   */
  static public Intent initApp(Context context) {
    return new Intent(context, SplashActivity.class);
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {

    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_splash);

    mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);
    mSoundPool = new SoundPool(1, AudioManager.STREAM_MUSIC, 0);

    new PrefetchContacts().execute();
  }

  private class PrefetchContacts extends AsyncTask<Void, Void, Void> {

    public PrefetchContacts() {
    }

    @Override
    protected Void doInBackground(Void... arg0) {
      preferences = getSharedPreferences(
          "il.ac.technion.cs.ssdl.cs234311.yp09.fbip", MODE_PRIVATE);
      commsModule = new CommsModule(SplashActivity.this);
      Controller.smsList = ContactsModule.contactParser(SplashActivity.this);

      // for (int j = 0; j < 2; j++)
      // for (int i = 0; i < 999999999; i++) {
      // i++;
      // i--;
      // }

      return null;
    }

    @Override
    protected void onPostExecute(Void result) {
      if (preferences.getBoolean("showWelcomeScreen", false)
          || preferences.getBoolean("firstRun", true)) {
        intent = new Intent(SplashActivity.this, WelcomeActivity.class);
        preferences.edit().putBoolean("firstRun", false).commit();
        preferences.edit().putBoolean("showWelcomeScreen", true).commit();
      } else
        intent = new Intent(SplashActivity.this, TypingActivity.class);
      startActivity(intent);
      // close this activity
      finish();
    }

  }
}
