package il.ac.technion.cs.ssdl.cs234311.yp09.utilities;

import il.ac.technion.cs.ssdl.cs234311.yp09.gui.OpCodeInterpreter.Op;
import il.ac.technion.cs.ssdl.cs234311.yp09.utilities.SaveStatistics.MessageType;

import org.junit.Assert;
import org.junit.Test;

public class SaveStatisticsTest {

  @Test
  public void testAddSentencesWordsAndChars() {
    SaveStatistics.startApp();
    sleepFor(300);
    SaveStatistics.startTyping();
    sleepFor(300);
    SaveStatistics.addChar();// char1 = 300
    sleepFor(500);
    SaveStatistics.addChar();// char2 = 500
    sleepFor(1500);
    SaveStatistics.addChar();// char3 = 1500
    sleepFor(5000);
    SaveStatistics.addChar();// char4 = 1500
    SaveStatistics.addWord();// word1 = 3800
    sleepFor(300);
    SaveStatistics.addChar();// cahr5 = 300
    sleepFor(700);
    SaveStatistics.addChar();// char6 = 700
    sleepFor(400);
    SaveStatistics.addChar();// char7 = 400
    sleepFor(500);
    SaveStatistics.addChar();// char8 = 500
    SaveStatistics.addWord();// word2 = 1900
    SaveStatistics.addSentence();// sentence1= 5700
    sleepFor(200);
    SaveStatistics.addChar();// char9 = 200
    sleepFor(7000);
    SaveStatistics.addChar();// char10 = 1500
    sleepFor(500);
    SaveStatistics.addChar();// char11 = 500
    sleepFor(1400);
    SaveStatistics.addChar();// char12 = 1400
    SaveStatistics.addWord();// word3= 3600
    SaveStatistics.addSentence();// sentence2 = 3600
    sleepFor(120);
    SaveStatistics.addChar();// char13 = 120;
    sleepFor(780);
    SaveStatistics.addChar();// char14 = 780;
    sleepFor(900);
    SaveStatistics.addChar();// char15 = 900;
    sleepFor(4000);
    SaveStatistics.addChar();// char16 = 1500;
    SaveStatistics.addWord();// word4 = 3300;
    sleepFor(120);
    SaveStatistics.addChar();// char17 = 120;
    sleepFor(780);
    SaveStatistics.addChar();// char18 = 780;
    sleepFor(900);
    SaveStatistics.addChar();// char19 = 900;
    sleepFor(4000);
    SaveStatistics.addChar();// char20 = 1500;
    SaveStatistics.addWord();// word5 = 3300;
    SaveStatistics.addSentence();// sentance3 = 6600
    SaveStatistics.stopTyping();

    Statistics res = SaveStatistics.exitTheApp();
    // check if the other fields didn't changed
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfAutoComplete);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfAutoCorrect);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfBlueClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfRedClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfOrangeClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfGreenClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfOrangeGreenClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfGreenRedClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfBlueRedClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfBlueOrangeClicks);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfFaceBookMessages);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfSMS);
    Assert.assertEquals(Double.valueOf(0.0), res.numberOfGoogleMessages);
    // check the averages and counts of sentence words and chars.
    Assert.assertEquals(Double.valueOf(3.0), res.numberOfSentence);
    Assert.assertEquals(Double.valueOf(5300.0), res.timePerSentenceAvg);
    Assert.assertEquals(Double.valueOf(5.0), res.numberOfWords);
    Assert.assertEquals(Double.valueOf(3180.0), res.timePerSentenceAvg);
    Assert.assertEquals(Double.valueOf(20.0), res.numberOfWords);
    Assert.assertEquals(Double.valueOf(795.0), res.timePerSentenceAvg);
    Assert.assertEquals(Double.valueOf(16200.0), res.timeUsingTheApp);

  }

  @Test
  public void testOtherFields() {
    SaveStatistics.startApp();
    SaveStatistics.addAutoComplete();
    SaveStatistics.addAutoCorrect();
    SaveStatistics.addAutoCorrect();
    SaveStatistics.clickOn(Op.BLUE);
    SaveStatistics.clickOn(Op.BLUE);
    SaveStatistics.clickOn(Op.BLUE);
    SaveStatistics.clickOn(Op.RED);
    SaveStatistics.clickOn(Op.RED);
    SaveStatistics.clickOn(Op.RED);
    SaveStatistics.clickOn(Op.RED);
    SaveStatistics.clickOn(Op.ORANGE);
    SaveStatistics.clickOn(Op.ORANGE);
    SaveStatistics.clickOn(Op.ORANGE);
    SaveStatistics.clickOn(Op.ORANGE);
    SaveStatistics.clickOn(Op.ORANGE);
    SaveStatistics.clickOn(Op.GREEN);
    SaveStatistics.clickOn(Op.GREEN);
    SaveStatistics.clickOn(Op.GREEN);
    SaveStatistics.clickOn(Op.GREEN);
    SaveStatistics.clickOn(Op.GREEN);
    SaveStatistics.clickOn(Op.GREEN);
    SaveStatistics.clickOn(Op.ORANGE_GREEN);
    SaveStatistics.clickOn(Op.ORANGE_GREEN);
    SaveStatistics.clickOn(Op.ORANGE_GREEN);
    SaveStatistics.clickOn(Op.ORANGE_GREEN);
    SaveStatistics.clickOn(Op.ORANGE_GREEN);
    SaveStatistics.clickOn(Op.ORANGE_GREEN);
    SaveStatistics.clickOn(Op.ORANGE_GREEN);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.GREEN_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_RED);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.clickOn(Op.BLUE_ORANGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.FACEBOOK_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.SMS_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    SaveStatistics.sendMessage(MessageType.GOOGLE_MESSAGE);
    Statistics res = SaveStatistics.exitTheApp();
    Assert.assertEquals(Double.valueOf(1.0), res.numberOfAutoComplete);
    Assert.assertEquals(Double.valueOf(2.0), res.numberOfAutoCorrect);
    Assert.assertEquals(Double.valueOf(3.0), res.numberOfBlueClicks);
    Assert.assertEquals(Double.valueOf(4.0), res.numberOfRedClicks);
    Assert.assertEquals(Double.valueOf(5.0), res.numberOfOrangeClicks);
    Assert.assertEquals(Double.valueOf(6.0), res.numberOfGreenClicks);
    Assert.assertEquals(Double.valueOf(7.0), res.numberOfOrangeGreenClicks);
    Assert.assertEquals(Double.valueOf(8.0), res.numberOfGreenRedClicks);
    Assert.assertEquals(Double.valueOf(9.0), res.numberOfBlueRedClicks);
    Assert.assertEquals(Double.valueOf(10.0), res.numberOfBlueOrangeClicks);
    Assert.assertEquals(Double.valueOf(11.0), res.numberOfFaceBookMessages);
    Assert.assertEquals(Double.valueOf(12.0), res.numberOfSMS);
    Assert.assertEquals(Double.valueOf(13.0), res.numberOfGoogleMessages);

  }

  private void sleepFor(int milisec) {
    try {
      Thread.sleep(milisec);
    } catch (InterruptedException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
  }

}
