package il.ac.technion.cs.ssdl.cs234311.yp09.keyboards;

/**
 * @author owais
 * 
 */
public interface ISearchInContactsActivity {
  /**
   * 
   */
  public void onScrollingClick();

  /**
   * 
   */
  public void onDoneClick();

  /**
   * 
   */
  public void onSelectClick();
}
