package il.ac.technion.cs.ssdl.cs234311.yp09.messages;

/**
 * @author Daniel Eidel
 * 
 */
public class SMSMessageItem {
  /**
   * Type of the message (sent/recieved)
   */
  public int messageType;
  /**
   * Th contents of the message
   */
  public String messageBody;
  /**
   * The date when the message was sent/recieved
   */
  public String messageDate;

  /**
   * @param messageType
   *          - Type int - Type code of the message
   * @param messageBody
   *          - Type String. Message's contents
   * @param messageDate
   *          - The date of the message in format "yyyy MMM dd h:mma"
   */
  public SMSMessageItem(int messageType, String messageBody, String messageDate) {
    this.messageType = messageType;
    this.messageBody = messageBody;
    this.messageDate = messageDate;
  }

}
