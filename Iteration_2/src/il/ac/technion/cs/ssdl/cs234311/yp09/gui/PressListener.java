package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

/**
 * The interface for a class that listens to presses from a four button device.
 * 
 * @author Itamar
 * 
 */
public interface PressListener {
  /**
   * Called when a button is pressed.
   * 
   * @param c
   *          The code of the press.
   */
  public void onPress(int c);

  /**
   * Called when a button is released.
   * 
   * @param c
   *          The code of the release.
   */
  public void onRelease(int c);

  /**
   * Called when a button is considered held for a certain time.
   * 
   * @param c
   *          The code of the hold.
   */
  public void onHeld(int c);
}
