package il.ac.technion.cs.ssdl.cs234311.yp09.gui;

import il.ac.technion.cs.ssdl.cs234311.yp09.R;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;


/**
 * @author Muhammad Watad
 * @email Muhammad.Watad@Gmail.com
 * @date 4/1/2014
 * 
 */

public class TextActivity extends GeneralActivity {

  /**
   * Key for specifying the wanted content
   */
  static public String prevKey = "il.ac.technion.cs.ssdl.cs234311.yp09.gui.prevKey";
  /**
   * Key for specifying the message content (in case prevKey is NOT Help
   * activity).
   */
  static public String messageKey = "il.ac.technion.cs.ssdl.cs234311.yp09.gui.messageKey";

  private String previous;
  private String value;
  private FontFitTextView fontFitTextView;

  @Override
  protected void onCreate(final Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_text);

    final Intent intent = getIntent();
    previous = intent.getStringExtra(prevKey);
    value = intent.getStringExtra(messageKey);

    refreshActivity();

    getFragmentManager().beginTransaction()
        .add(R.id.buttons_frame_main, mFBFragment).commit();

    final DoneFragment ackFragment = new DoneFragment();
    getFragmentManager().beginTransaction()
        .add(R.id.ack_main_fragment, ackFragment).commit();

  }

  @Override
  protected void onResume() {
    super.onResume();
    refreshActivity();
  }

  private void refreshActivity() {
    fontFitTextView = (FontFitTextView) findViewById(R.id.text);

    if (previous.equals("HelpActivityPlease")) {
      setTitle(R.string.help);
      fontFitTextView.setText(R.string.instructions_main);
    } else {
      setTitle(R.string.sms_content);
      fontFitTextView.setText(value);
    }
  }

  @Override
  public boolean onCreateOptionsMenu(final Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    getMenuInflater().inflate(R.menu.first, menu);
    return true;
  }

  @Override
  public void onPause() {
    super.onPause();
    overridePendingTransition(0, 0);
  }

  @Override
  public boolean onPrepareOptionsMenu(Menu menu) {
    return false;
  }

  @Override
  public void onOperation(final int c) {
    super.onOperation(c);
    switch (OpCodeInterpreter.getOp(c)) {
    case BLUE:
      break;
    case BLUE_ORANGE:
      break;
    case BLUE_RED:
      break;
    case GREEN:
      break;
    case GREEN_RED:
      break;
    case INVALID:
      break;
    case LONG_BLUE:
      break;
    case LONG_GREEN:
      break;
    case LONG_ORANGE:
      break;
    case LONG_RED:
      break;
    case ORANGE:
      break;
    case ORANGE_GREEN:
      break;
    case RED:
      makeSound(0);
      finish();
      overridePendingTransition(0, 0);
      break;
    default:
      break;
    }
  }

  @Override
  public synchronized void onPress(int c) {
    // do nothing
  }

  @Override
  public synchronized void onRelease(int c) {
    // do nothing
  }

  @Override
  public void onHeld(int c) {
    // do nothing
  }
}
