package il.ac.technion.cs.ssdl.cs234311.BTpc;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/*
 * Copyright (C) 2011 by Fernando Alexandre

	Permission is hereby granted, free of charge, to any person obtaining a copy
	of this software and associated documentation files (the "Software"), to deal
	in the Software without restriction, including without limitation the rights
	to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
	copies of the Software, and to permit persons to whom the Software is
	furnished to do so, subject to the following conditions:

	The above copyright notice and this permission notice shall be included in
	all copies or substantial portions of the Software.

	THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
	IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
	FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
	AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
	LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
	OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
	THE SOFTWARE.
 */

public class BT_utils {

	private BT_client client;
	
	public BT_utils(){
		client = new BT_client();
	}
	
	public void connect(){
		BT_client bt = new BT_client();
		bt.start();
		
		synchronized (bt.lock) {
			while(!bt.cond) {
				try {
					bt.lock.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			bt.cond = false;
		}
		
		bt.connect();
	}
	
	public void send(String s){
		client.broadcastCommand(s);
	}
	
	public void dissconnect(){
		client.broadcastCommand("quit");
		
		client.dissconnect();
	}
	
	public static void main(String[] args) throws IOException {
		BT_client bt = new BT_client();
		bt.start();
		
		synchronized (bt.lock) {
			while(!bt.cond) {
				try {
					bt.lock.wait();
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			
			bt.cond = false;
		}
		
		bt.connect();
		
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        
		String input;
		do{
			input = br.readLine();
			
			bt.broadcastCommand(input);
		}while(!input.equals("quit"));
		
		bt.dissconnect();
	}

}
