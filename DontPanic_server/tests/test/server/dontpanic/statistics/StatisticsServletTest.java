package test.server.dontpanic.statistics;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.OfyService;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut.ShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Servlets.UserRegServlet;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.ClickData;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statsServlets.UploadStatData;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.googlecode.objectify.Key;

/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date 	14/05/14
 *
 */
public class StatisticsServletTest {

	// maximum eventual consistency
    private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig()
            .setDefaultHighRepJobPolicyUnappliedJobPercentage(100));

    @Before
    public void setUp() throws IOException {
        helper.setUp();
    }

    @After
    public void tearDown() {
        helper.tearDown();
    }
    
    private void createParentUser(String name) throws IOException{
    	 //adding user to parent codes
        UserRegServlet serv = new UserRegServlet();
      	HttpServletRequest req = mock(HttpServletRequest.class);
      	HttpServletResponse resp = mock(HttpServletResponse.class);
    	
		    PrintWriter writer = new PrintWriter("code_parent.txt");
		    when(resp.getWriter()).thenReturn(writer);
      	
  	    Gson gson = new Gson();
      	User us = new User(name + "@gmail.com", name, "supervisor@email.com", "050000000000");
      	String us_json = gson.toJson(us);
  	    when(req.getParameter("user")).thenReturn(us_json);
      	
      	serv.doPost(req, resp);
    	
      	writer.flush(); // it may not have been flushed yet...
  	    File file = new File("code_parent.txt");
      	if(file.exists()){
      		assertTrue(Files.readFirstLine(file, Charset.forName("UTF-8")).equals("user reg successful"));
      		file.delete();
      	}
      	else{
      		fail("file creation error");
      	}
    }
	
  private static List<ShortcutLetter> randomSequence(){
      Random rnd = new Random();
      
      ArrayList<ShortcutLetter> l = new ArrayList<>();
      
      for(int j = 0; j < rnd.nextInt(6); ++j){
          l.add(ShortcutLetter.ONE_TWO);
      }
      
      if(rnd.nextBoolean())
          l.add(ShortcutLetter.FOUR);
      else
          l.add(ShortcutLetter.FOUR);
      
      return l;
  }
    
	@Test
	public void addStatsTest() throws IOException, IllegalShortcutLetter {
	    final String uid = "stat@gmail.com"; 
	    createParentUser("stat");
		
	    Key<User> key = Key.create(User.class, "stat@gmail.com");
    	User user = OfyService.ofy().load().now(key);
    	if(user == null){
    		fail("parent user not created");
    	}
    	
    	UploadStatData serv = new UploadStatData();
    	HttpServletRequest req = mock(HttpServletRequest.class);
    	HttpServletResponse resp = mock(HttpServletResponse.class);
    	
	    PrintWriter writer = new PrintWriter("uploadTest.txt");
	    when(resp.getWriter()).thenReturn(writer);
    	
	    
	    /**********************************************************************************/
	    /*************************	testing add ClickData success *************************/
	    /**********************************************************************************/
	    Set<Long> keys = new HashSet<Long>();
	    
	    when(req.getParameter("type")).thenReturn("click");
	    
	    Gson gson = new Gson();
	    for(int i = 0; i < 200; ++i){
	        
	        ClickData cd = new ClickData(uid, new Date(),randomSequence());
	        
	        String json_code = gson.toJson(cd);
	        when(req.getParameter("info")).thenReturn(json_code);
	        
	        serv.doPost(req, resp);
	        
	        writer.flush(); // it may not have been flushed yet...
	        File file = new File("uploadTest.txt");
	        if(file.exists()){
	          assertFalse(Files.readFirstLine(file, Charset.forName("UTF-8")).contains("stat add fail. error: "));
	          Long id = Long.valueOf(Files.readFirstLine(file, Charset.forName("UTF-8"))).longValue();
	          assertNotNull(id);
	          keys.add(id);
	          file.delete();
	        }
	        else{
	          fail("file creation error");
	        }
	    }

	    for(Long ki: keys){
	        Key<ClickData> k = Key.create(ClickData.class, ki);
	        
	        ClickData code_ps = OfyService.ofy().load().now(k);
	        assertNotNull(code_ps);
	    }
    	
    	/**********************************************************************************/
	    /*************************	testing no user failure		***************************/
	    /**********************************************************************************/
    	writer = new PrintWriter("uploadTest.txt");
	    when(resp.getWriter()).thenReturn(writer);
	    
	    User user2 = new User("nouser@gmail.com", "nouser", "supervisor@email.com", "050000000000");
	  
      ClickData cd = new ClickData(user2.email, new Date(), randomSequence());
      
      String json_code = gson.toJson(cd);
      when(req.getParameter("info")).thenReturn(json_code);
      
      serv.doPost(req, resp);
	
    	writer.flush(); // it may not have been flushed yet...
    	File file = new File("uploadTest.txt");
    	if(file.exists()){
    		assertTrue(Files.readFirstLine(file, Charset.forName("UTF-8"))
    						.contains("stat add fail. error: " + 
    								"java.lang.Exception: no such user: nouser@gmail.com"));
    		file.delete();
    	}
    	else{
    		fail("file creation error");
    	}
	}
}
