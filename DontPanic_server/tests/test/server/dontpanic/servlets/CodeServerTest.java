package test.server.dontpanic.servlets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.OfyService;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.ShortcutTranslator.IllegalShortcutLetter;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Servlets.AddCodeServlet;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Servlets.DeleteCodeServlet;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Servlets.GetCodesServlet;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Servlets.UserRegServlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.googlecode.objectify.Key;

/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date 	12/11/13 - 13/11/13
 *
 */
public class CodeServerTest {

	// maximum eventual consistency
    private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig()
            .setDefaultHighRepJobPolicyUnappliedJobPercentage(100));

    @Before
    public void setUp() throws IOException {
        helper.setUp();
    }

    @After
    public void tearDown() {
        helper.tearDown();
    }
    
    private void createParentUser(String name) throws IOException{
    	 //adding user to parent codes
        UserRegServlet serv = new UserRegServlet();
    	HttpServletRequest req = mock(HttpServletRequest.class);
    	HttpServletResponse resp = mock(HttpServletResponse.class);
    	
		PrintWriter writer = new PrintWriter("code_parent.txt");
	    when(resp.getWriter()).thenReturn(writer);
    	
	    Gson gson = new Gson();
    	User us = new User(name + "@gmail.com", name, "supervisor@email.com", "050000000000");
    	String us_json = gson.toJson(us);
	    when(req.getParameter("user")).thenReturn(us_json);
    	
    	serv.doPost(req, resp);
    	
    	writer.flush(); // it may not have been flushed yet...
	    File file = new File("code_parent.txt");
    	if(file.exists()){
    		assertTrue(Files.readFirstLine(file, Charset.forName("UTF-8")).equals("user reg successful"));
    		file.delete();
    	}
    	else{
    		fail("file creation error");
    	}
    }
	
	@Test
	public void addCodeTest() throws IOException, IllegalShortcutLetter {
		createParentUser("addparent");
		
		Key<User> key = Key.create(User.class, "addparent@gmail.com");
    	User user = OfyService.ofy().load().now(key);
    	if(user == null){
    		fail("parent user not created");
    	}
    	
    	AddCodeServlet serv = new AddCodeServlet();
    	HttpServletRequest req = mock(HttpServletRequest.class);
    	HttpServletResponse resp = mock(HttpServletResponse.class);
    	
		PrintWriter writer = new PrintWriter("addtest.txt");
	    when(resp.getWriter()).thenReturn(writer);
    	
	    
	    /**********************************************************************************/
	    /*************************	testing add success **************************/
	    /**********************************************************************************/
	    boolean[] arr_word = {true, true, false, false};
	    ArrayList<boolean[]> word = new ArrayList<>();
	    word.add(arr_word);
	    word.add(arr_word);
	    word.add(arr_word);
	    ArrayList<String> props = new ArrayList<>();
	    props.add("moshe");
	    props.add("0545950569");
	    props.add("56");
	    
	    Shortcut code = new Shortcut(user, word, Shortcut.CodeOperation.PHONE_CALL, props);
	    
	    Gson gson = new Gson();
	    String json_code = gson.toJson(code);
	    when(req.getParameter("code")).thenReturn(json_code);
    	
	    serv.doPost(req, resp);
    	
    	Long id = null;
	    writer.flush(); // it may not have been flushed yet...
	    File file = new File("addtest.txt");
    	if(file.exists()){
    		assertFalse(Files.readFirstLine(file, Charset.forName("UTF-8")).contains("code add fail. error: "));
    		id = Long.valueOf(Files.readFirstLine(file, Charset.forName("UTF-8"))).longValue();
    		file.delete();
    	}
    	else{
    		fail("file creation error");
    	}
	    
    	assertNotNull(id);
    	Key<Shortcut> k = Key.create(Shortcut.class, id);
    	
    	Shortcut code_ps = OfyService.ofy().load().now(k);
    	assertNotNull(code_ps);
    	
    	assertEquals(code.code, code_ps.code);
    	assertEquals(id, code_ps.id);
    	assertEquals(code.op, code_ps.op);
    	assertEquals(code.properties, code_ps.properties);
    	assertEquals(code.letteredCode.size(), code_ps.letteredCode.size());
    	
    	for(int i = 0; i < code.letteredCode.size(); ++i)
    	    assertEquals(code.letteredCode.get(i), code_ps.letteredCode.get(i));
    	
    	
    	/**********************************************************************************/
	    /*************************	testing no user failure		***************************/
	    /**********************************************************************************/
    	writer = new PrintWriter("addtest.txt");
	    when(resp.getWriter()).thenReturn(writer);
	    
	    User user2 = new User("nouser@gmail.com", "nouser", "supervisor@email.com", "050000000000");
	    Shortcut code2 = new Shortcut(user2, word, Shortcut.CodeOperation.PHONE_CALL, props);
	   
	    json_code = gson.toJson(code2);
	    when(req.getParameter("code")).thenReturn(json_code);
	    
    	serv.doPost(req, resp);
    	
    	writer.flush(); // it may not have been flushed yet...
	    file = new File("addtest.txt");
    	if(file.exists()){
    		assertTrue(Files.readFirstLine(file, Charset.forName("UTF-8"))
    						.contains("code add fail. error: " + 
    								"java.lang.Exception: no such user: nouser@gmail.com"));
    		file.delete();
    	}
    	else{
    		fail("file creation error");
    	}
	}
	
	@Test
	public void getCodesTest() throws IOException, IllegalShortcutLetter {
		createParentUser("getparent");
		
		Key<User> key = Key.create(User.class, "getparent@gmail.com");
    	User user = OfyService.ofy().load().now(key);
    	if(user == null){
    		fail("parent user not created");
    	}
    	
    	GetCodesServlet serv = new GetCodesServlet();
    	HttpServletRequest req = mock(HttpServletRequest.class);
    	HttpServletResponse resp = mock(HttpServletResponse.class);
    	
		  PrintWriter writer = new PrintWriter("addtest.txt");
	    when(resp.getWriter()).thenReturn(writer);
    	
	    ArrayList<Shortcut> code_lst = new ArrayList<Shortcut>();
	    for(int i = 0; i < 200; ++i){
        boolean[] arr_word = {true, true, false, false};
        ArrayList<boolean[]> word = new ArrayList<>();
        word.add(arr_word);
	    	
	    	ArrayList<String> props = new ArrayList<>();
	    	props.add("contact" + i);
	    	props.add("" + i);
	    	props.add("" + i);
	    	
	    	Shortcut c = new Shortcut(user, word, Shortcut.CodeOperation.PHONE_CALL, props);
	    	OfyService.ofy().save().entity(c).now();
	    }
	    
	    when(req.getParameter("uid")).thenReturn("getparent@gmail.com");
	    serv.doGet(req, resp);
	    
	    writer.flush(); // it may not have been flushed yet...
	    File file = new File("addtest.txt");
	    if(file.exists()){
    		assertFalse(Files.readFirstLine(file, Charset.forName("UTF-8")).contains("code add fail. error: "));
    		
    		String json_lst = Files.readFirstLine(file, Charset.forName("UTF-8"));
    		Gson gson = new Gson();
    		Type collectionType = new TypeToken<List<Shortcut>>(){}.getType();
    		List<Shortcut> code_ps_lst = gson.fromJson(json_lst, collectionType);
    		
    		for(Shortcut c: code_ps_lst){
    			code_lst.contains(c);
    		}
    		
    		file.delete();
    	}
    	else{
    		fail("file creation error");
    	}
	}
	
	@Test
	public void deleteCodesTest() throws IOException, IllegalShortcutLetter {
		createParentUser("deleteparent");
		
		Key<User> key = Key.create(User.class, "deleteparent@gmail.com");
    User user = OfyService.ofy().load().now(key);
    if(user == null){
      fail("parent user not created");
    }
		
    AddCodeServlet serv = new AddCodeServlet();
    HttpServletRequest req = mock(HttpServletRequest.class);
    HttpServletResponse resp = mock(HttpServletResponse.class);
    
    PrintWriter writer = new PrintWriter("deletetest.txt");
    when(resp.getWriter()).thenReturn(writer);
    
    
    /**********************************************************************************/
    /*****************************    testing delete success **************************/
    /**********************************************************************************/
    boolean[] arr_word = {true, true, false, false};
    ArrayList<boolean[]> word = new ArrayList<>();
    word.add(arr_word);
    ArrayList<String> props = new ArrayList<>();
    props.add("moshe");
    props.add("0545950569");
    props.add("56");
    
    Shortcut code = new Shortcut(user, word, Shortcut.CodeOperation.PHONE_CALL, props);
    
    Gson gson = new Gson();
    String json_code = gson.toJson(code);
    when(req.getParameter("code")).thenReturn(json_code);
    
    serv.doPost(req, resp);
    
    Long id = null;
    writer.flush(); // it may not have been flushed yet...
    File file = new File("deletetest.txt");
    if(file.exists()){
      assertFalse(Files.readFirstLine(file, Charset.forName("UTF-8")).contains("code add fail. error: "));
      id = Long.valueOf(Files.readFirstLine(file, Charset.forName("UTF-8"))).longValue();
      file.delete();
    }
    else{
      fail("file creation error");
    }
    
    if(id == null){
        fail("shortcut not added");
    }
    
    DeleteCodeServlet serv1 = new DeleteCodeServlet();
    writer = new PrintWriter("deletetest.txt");
    HttpServletRequest req2 = mock(HttpServletRequest.class);
    HttpServletResponse resp2 = mock(HttpServletResponse.class);
    
    when(resp2.getWriter()).thenReturn(writer);
    when(req2.getParameter("shortcut")).thenReturn(json_code);
    when(req2.getParameter("uid")).thenReturn(user.email);
    
    serv1.doPost(req2, resp2);
    writer.flush(); // it may not have been flushed yet...
    file = new File("deletetest.txt");
    if(file.exists()){
      //assertFalse(Files.readFirstLine(file, Charset.forName("UTF-8")).contains("code delete fail. error: "));
      file.delete();
    }
    else{
      fail("file creation error");
    }
    
    //Key<Shortcut> key_s = Key.create(Shortcut.class, id);
    //Shortcut code_ps = OfyService.ofy().load().key(key_s).now();
    
    //assertNull(code_ps);
	}
}
