package test.server.dontpanic.servlets;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.OfyService;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Servlets.GetUserServlet;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Servlets.UserRegServlet;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.Charset;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.google.appengine.tools.development.testing.LocalDatastoreServiceTestConfig;
import com.google.appengine.tools.development.testing.LocalServiceTestHelper;
import com.google.common.io.Files;
import com.google.gson.Gson;
import com.googlecode.objectify.Key;

public class UserServerTest {
	
	// maximum eventual consistency
    private final LocalServiceTestHelper helper =
        new LocalServiceTestHelper(new LocalDatastoreServiceTestConfig()
            .setDefaultHighRepJobPolicyUnappliedJobPercentage(100));

    @Before
    public void setUp() {
        helper.setUp();
    }

    @After
    public void tearDown() {
        helper.tearDown();
        File file1 = new File("regtest.txt");
        File file2 = new File("gettest.txt");
        
        if(file1.exists())
        	file1.delete();
        
        if(file2.exists())
        	file2.delete();
    }
    
    @Test
    public void userRegTest() throws IOException{
    	UserRegServlet serv = new UserRegServlet();
    	HttpServletRequest req = mock(HttpServletRequest.class);
    	HttpServletResponse resp = mock(HttpServletResponse.class);
    	
		PrintWriter writer = new PrintWriter("regtest.txt");
	    when(resp.getWriter()).thenReturn(writer);
    	
	    
	    /**********************************************************************************/
	    /*************************	testing registration success **************************/
	    /**********************************************************************************/
	    Gson gson = new Gson();
    	User us = new User("regtest@gmail.com", "userreg_test", "supervisor@email.com", "050000000000");
    	String us_json = gson.toJson(us);
	    when(req.getParameter("user")).thenReturn(us_json);
    	
    	serv.doPost(req, resp);
    	
	    writer.flush(); // it may not have been flushed yet...
	    File file = new File("regtest.txt");
    	if(file.exists()){
    		assertTrue(Files.readFirstLine(file, Charset.forName("UTF-8")).equals("user reg successful"));
    		file.delete();
    	}
    	else{
    		fail("file creation error");
    	}
	    
	    
    	Key<User> key = Key.create(User.class, us.email);
    	User us_test = OfyService.ofy().load().now(key);
    	
    	assertNotNull(us_test);
    	assertEquals(us_test.email, us.email);
    	assertEquals(us_test.email, us.email);
    	
    	/**********************************************************************************/
	    /*************************	testing double reg failure	***************************/
	    /**********************************************************************************/	
    	writer = new PrintWriter("regtest.txt");
    	when(resp.getWriter()).thenReturn(writer);
    	serv.doPost(req, resp);
		
    	writer.flush(); // it may not have been flushed yet...
    	file = new File("regtest.txt");
    	if(file.exists()){
    		assertTrue(Files.readFirstLine(file, Charset.forName("UTF-8"))
    					.equals("user reg fail. error: java.lang.Exception: " +
    								"double registration for: regtest@gmail.com"));
    		file.delete();
    	}
    	else{
    		fail("file creation error");
    	}
    }

	@Test
	public void userGetTest() throws IOException  {
		GetUserServlet serv = new GetUserServlet();
    	HttpServletRequest req = mock(HttpServletRequest.class);
    	HttpServletResponse resp = mock(HttpServletResponse.class);
    	
		PrintWriter writer = new PrintWriter("gettest.txt");
	    when(resp.getWriter()).thenReturn(writer);
    	
	    Gson gson = new Gson();
    	User us = new User("gettest@gmail.com", "userget_test", "supervisor@email.com", "050000000000");
    	OfyService.ofy().save().entity(us).now();
    	String us_json = gson.toJson(us);
    	
    	/**********************************************************************************/
	    /****************************	testing get success	*******************************/
	    /**********************************************************************************/	
    	when(req.getParameter("uid")).thenReturn(us.email);
    	serv.doGet(req, resp);
    	
	    writer.flush(); // it may not have been flushed yet...
	    File file = new File("gettest.txt");
    	if(file.exists()){
    		//System.out.println(Files.readFirstLine(file, Charset.forName("UTF-8")));
    		assertTrue(Files.readFirstLine(file, Charset.forName("UTF-8")).equals(us_json));
    		file.delete();
    	}
    	else{
    		fail("file creation error");
    	}
    	
    	/**********************************************************************************/
	    /****************************	testing no user failure	***************************/
	    /**********************************************************************************/
    	writer = new PrintWriter("gettest.txt");
    	when(resp.getWriter()).thenReturn(writer);
    	when(req.getParameter("uid")).thenReturn("noemail@gmail.com");
    	serv.doGet(req, resp);
		
    	writer.flush(); // it may not have been flushed yet...
    	file = new File("gettest.txt");
    	if(file.exists()){
    		System.out.println(Files.readFirstLine(file, Charset.forName("UTF-8")));
    		assertTrue(Files.readFirstLine(file, Charset.forName("UTF-8"))
    					.equals("user get fail. error: java.lang.Exception: no user found for: noemail@gmail.com"));
    		file.delete();
    	}
    	else{
    		fail("file creation error");
    	}
	}
}
