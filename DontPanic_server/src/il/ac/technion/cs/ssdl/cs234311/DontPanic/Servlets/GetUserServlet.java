package il.ac.technion.cs.ssdl.cs234311.DontPanic.Servlets;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.OfyService;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.googlecode.objectify.Key;

/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date 	3/11/13
 * 
 * http servlet for getting user data
 * 
 * @name GetUser
 * @method http get
 * @params uid - user id (email)
 */
@SuppressWarnings("serial")
public class GetUserServlet extends HttpServlet {

	private static final String ENTITY_TYPE_UID = "uid";
	private static final Logger log = Logger.getLogger(UserRegServlet.class.getName());

	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		try{
			log.info("starting get servlet");
			String user_id = req.getParameter(ENTITY_TYPE_UID);
			
			Key<User> k = Key.create(User.class, user_id);
			log.info("starting get servlet got = " + user_id);
			
			User user = OfyService.ofy().load().key(k).now();
			if(user == null){
				log.warning("no user found for: " + user_id);
				throw new Exception("no user found for: " + user_id);
			}
			
			Gson gson = new Gson();
			String json_user = gson.toJson(user);
		
			resp.setContentType("application/json; charset=UTF-8");
			resp.getWriter().println(json_user);
			resp.setStatus(HttpServletResponse.SC_OK);
			log.info("user returned: " + user.username);
		}
		catch(Exception ex){
			log.severe("user retrieval fail. error: " + ex.toString());
			resp.getWriter().println("user get fail. error: " + ex.toString());
			resp.setStatus(HttpServletResponse.SC_CONFLICT);
		}
	}
}
