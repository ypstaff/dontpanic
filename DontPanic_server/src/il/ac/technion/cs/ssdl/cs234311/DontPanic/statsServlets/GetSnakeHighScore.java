package il.ac.technion.cs.ssdl.cs234311.DontPanic.statsServlets;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.OfyService;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.RawData;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.SnakeData;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.googlecode.objectify.Key;

/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date 	10/05/14
 *
 *	http servlet for getting statistics
 * 
 * @name GetStatistics
 * @method http get
 * @params uid - user id (email)
 *          type - string of stat type [click/music/snake]
 *          
 *  @return String array of result
 */
@SuppressWarnings("serial")
public class GetSnakeHighScore extends HttpServlet {

    private static final String USER_ID = "uid";
	private static final Logger log = Logger.getLogger(GetSnakeHighScore.class.getName());
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		try{
			log.info("starting add code servlet");
			String uid = req.getParameter(USER_ID);
			
	    Key<User> user_key = Key.create(User.class, uid);
      User user = OfyService.ofy().load().key(user_key).now();
      
      if(user == null){
        log.warning("no such user: " + uid);
        throw new Exception("no such user: " + uid);
      }
        
      List<? extends RawData> lst = OfyService.ofy().load().type(SnakeData.class)
              .filter("userEmail ==", user.email).list();
      
      String[] result_map = new SnakeData().calculateStatistics(lst);
      
			resp.getWriter().println(result_map[0].split(":")[1]);
			resp.setStatus(HttpServletResponse.SC_OK);
			log.info("retrieved high score for: " + user.email);
		}
		catch(Exception ex){
			log.severe("high score get fail. error: \n" + ex.toString());
			resp.getWriter().println("high score get fail. error: " + ex.toString());
			resp.setStatus(HttpServletResponse.SC_CONFLICT);
		}
	}
}
