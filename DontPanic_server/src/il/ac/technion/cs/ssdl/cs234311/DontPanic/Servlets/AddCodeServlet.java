package il.ac.technion.cs.ssdl.cs234311.DontPanic.Servlets;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.OfyService;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.googlecode.objectify.Key;

/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date 	10/11/13
 *
 *	http servlet for adding code to database
 * 
 * @name AddCode
 * @method http post
 * @params code - json serialized Code instance
 */
@SuppressWarnings("serial")
public class AddCodeServlet extends HttpServlet {

	private static final String ENTITY_TYPE_CODE = "code";
	private static final Logger log = Logger.getLogger(AddCodeServlet.class.getName());

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		try{
			log.info("starting add code servlet");
			String json_code = req.getParameter(ENTITY_TYPE_CODE);
			
			Gson gson = new Gson();
			Shortcut code = gson.fromJson(json_code, Shortcut.class);
			
			Key<User> user_key = Key.create(User.class, code.userEmail);
			User user = OfyService.ofy().load().key(user_key).now();
			if(user == null){
				log.warning("no such user: " + code.userEmail);
				throw new Exception("no such user: " + code.userEmail);
			}
			
			int dupl = OfyService.ofy().load().type(Shortcut.class)
							.filter("userEmail ==", user.email)
							.filter("code ==", code.code).count();
			if(dupl != 0){
				log.warning("double code assigning to user " + user.email);
				throw new Exception("double code assigning to user " + user.email);
			}
			
			OfyService.ofy().save().entity(code).now();
			
			resp.getWriter().println(code.id);
			resp.setStatus(HttpServletResponse.SC_OK);
			log.info("user registered: " + user.email);
		}
		catch(Exception ex){
			log.severe("code add fail. error: \n" + ex.toString());
			resp.getWriter().println("code add fail. error: " + ex.toString());
			resp.setStatus(HttpServletResponse.SC_CONFLICT);
		}
	}
}
