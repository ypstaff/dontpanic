package il.ac.technion.cs.ssdl.cs234311.DontPanic.Servlets;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.OfyService;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.Shortcut;

import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.googlecode.objectify.Key;

/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date 	10/11/13
 * 
 * http servlet for deleting code from database
 * 
 * @name DeleteCode
 * @method http post
 * @params shortcut - json shortcut
 */
@SuppressWarnings("serial")
public class DeleteCodeServlet extends HttpServlet {

	private static final String ENTITY_TYPE_CODE = "shortcut";
	private static final String ENTITY_TYPE_USER_ID = "uid";
	private static final Logger log = Logger.getLogger(DeleteCodeServlet.class.getName());

	public void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		try{
		  String uid = req.getParameter(ENTITY_TYPE_USER_ID);
			String shortcut_json = req.getParameter(ENTITY_TYPE_CODE);
			
			log.severe("delete got: " + shortcut_json + " from: " + uid);
			Shortcut code = new Gson().fromJson(shortcut_json, Shortcut.class);
			
			Shortcut sc = null;
			if(code.id != null){
  			Key<Shortcut> key = Key.create(Shortcut.class, code.id);
  			sc = OfyService.ofy().load().key(key).now();
			}
			else{
			  List<Shortcut> lst = OfyService.ofy().load().type(Shortcut.class)
                .filter("userEmail ==", code.userEmail).filter("code ==", code.code).list();
			  if(lst.isEmpty())
			      sc = null;
			  else
			      sc = lst.get(0);
			}
			
			if(sc == null){
			    log.warning("no shortcut under this id");
			    throw new Exception("no such shortcut");
			}
					
			OfyService.ofy().delete().entity(sc).now();
			
			resp.getWriter().println("codes deleted successfuly");
			resp.setStatus(HttpServletResponse.SC_OK);
			log.info("code " + sc.id + "deleted");
		}
		catch(Exception ex){
			log.severe("code delete fail. error: \n" + ex.toString());
			ex.printStackTrace(System.out);
			resp.getWriter().println("code delete fail. error: " + ex.toString());
			resp.setStatus(HttpServletResponse.SC_CONFLICT);
		}
	}
}
