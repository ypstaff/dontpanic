/**
 * 
 */
package il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.googlecode.objectify.annotation.EntitySubclass;

/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date 	14/05/14
 *
 */
@EntitySubclass(index=true)
public class SnakeData extends GameData {

    /**
     * @param _uid
     * @param _time
     * @param info
     */
    public SnakeData(String _uid, Date _time, String[] info) {
        super(_uid, _time, info);
    }

    /**
     * empty c'tor for objectify
     */
    public SnakeData() {
       super();
    }
    
    private static String highScoreForLvl(Set<SnakeData> set){
        Map<String, Double> map = new HashMap<>();
        
        for(SnakeData d: set){
            String lvl = d.gameInfo[1];
            double val = Double.valueOf(d.gameInfo[0]).doubleValue();
            
            if(map.containsKey(lvl)){
                double map_val = map.get(lvl).doubleValue();
                val = (map_val < val) ? val : map_val;
            }
            
            map.put(lvl, val);
        }
        
        String result = "";
        for(Entry<String, Double> e: map.entrySet()){
            result = e.getKey() + "," + e.getValue() + "; ";
        }
        
        return result;
    }
    
    public static String maxElement(Set<SnakeData> set, int idx){
        Double max = 0.0;
        
        for(SnakeData d: set){
            Double value = Double.valueOf(d.gameInfo[idx]);
            if(value > max)
                max = value;
        }
        
        return max.toString();
    }
    
    private static String sumElement(Set<SnakeData> set, int idx){
        Double sum = 0.0;
        
        for(SnakeData d: set){
            Double value = Double.valueOf(d.gameInfo[idx]);
            
            sum += value;
        }
        
        return sum.toString();
    }
    
    @Override
    public String[] calculateStatistics(
            Collection<? extends RawData> datasets) {
        HashSet<SnakeData> s = new HashSet<SnakeData>();
        for(RawData d: datasets){
            if(d instanceof SnakeData){
                s.add((SnakeData)d);
            }
        }
        
        String[] result = new String[4];
        
        result[0] = "high_score:" + highScoreForLvl(s);
        result[1] = "fav_lvl:" + maxElement(s, 1);
        result[2] = "max_time:" + maxElement(s, 2);
        result[3] = "total_time:" + sumElement(s, 2);
        
        return result;
    }

}
