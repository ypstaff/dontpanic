package il.ac.technion.cs.ssdl.cs234311.DontPanic;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.ClickData;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.GameData;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.MusicData;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.RawData;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.SnakeData;

import com.googlecode.objectify.Objectify;
import com.googlecode.objectify.ObjectifyFactory;
import com.googlecode.objectify.ObjectifyService;

/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date	3/11/13
 * 
 * objectify service class - referenced for web.
 *
 */
public class OfyService {
    static {
        factory().register(User.class);
        factory().register(Shortcut.class);
        factory().register(ClickData.class);
        factory().register(GameData.class);
        factory().register(MusicData.class);
        factory().register(RawData.class);
        factory().register(SnakeData.class);
    }

    public static Objectify ofy() {
        return ObjectifyService.ofy();
    }

    public static ObjectifyFactory factory() {
        return ObjectifyService.factory();
    }
}
