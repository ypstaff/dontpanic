package il.ac.technion.cs.ssdl.cs234311.DontPanic.statsServlets;

import il.ac.technion.cs.ssdl.cs234311.DontPanic.OfyService;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.User;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.ClickData;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.MusicData;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.RawData;
import il.ac.technion.cs.ssdl.cs234311.DontPanic.statistics.SnakeData;

import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.Type;
import java.util.List;
import java.util.logging.Logger;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.googlecode.objectify.Key;

/**
 * @author 	Michael Varvaruk
 * @email 	mvarvaruk@gmail.com
 * @date 	10/05/14
 *
 *	http servlet for getting statistics
 * 
 * @name GetStatistics
 * @method http get
 * @params uid - user id (email)
 *          type - string of stat type [click/music/snake]
 *          
 *  @return String array of result
 */
@SuppressWarnings("serial")
public class GetStatistics extends HttpServlet {

    private static final String USER_ID = "uid";
    private static final String STAT_TYPE = "type";
	private static final Logger log = Logger.getLogger(GetStatistics.class.getName());
	
	public void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
		try{
			log.info("starting add code servlet");
			String uid = req.getParameter(USER_ID);
			String type = req.getParameter(STAT_TYPE);
			
			Gson gson = new Gson();
			
			Class<? extends RawData> target = null;
			
			switch (type) {
        case "click":
            target = ClickData.class;
            break;
        case "music":
            target = MusicData.class;
            break;
        case "snake":
            target = SnakeData.class;
            break;
        default:
            throw new Exception("unknown statistics type: " + type);
        }
			
	    Key<User> user_key = Key.create(User.class, uid);
      User user = OfyService.ofy().load().key(user_key).now();
      
      if(user == null){
        log.warning("no such user: " + uid);
        throw new Exception("no such user: " + uid);
      }
        
      List<? extends RawData> lst = OfyService.ofy().load().type(target)
              .filter("uid ==", user.email).list();
      
      Constructor<? extends RawData> cons = target.getConstructor();
      RawData obj = cons.newInstance();
      
      String[] result_map = obj.calculateStatistics(lst);
      Type collectionType = new TypeToken<String[]>(){}.getType();
      String result_json = gson.toJson(result_map, collectionType);
      
      
			resp.getWriter().println(result_json);
			resp.setStatus(HttpServletResponse.SC_OK);
			log.info("retrieved statistics for: " + user.email);
		}
		catch(Exception ex){
			log.severe("stat get fail. error: \n" + ex.toString());
			resp.getWriter().println("stat get fail. error: " + ex.toString());
			resp.setStatus(HttpServletResponse.SC_CONFLICT);
		}
	}
}
